// Generated from C:/Users/Batoul Mougrabi/Desktop/compiler74/src/Java\Sql.g4 by ANTLR 4.7.2
package Java;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link SqlParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface SqlVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link SqlParser#parse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParse(SqlParser.ParseContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#error}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitError(SqlParser.ErrorContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#defVariable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefVariable(SqlParser.DefVariableContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#sql_stmt_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSql_stmt_list(SqlParser.Sql_stmt_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#sql_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSql_stmt(SqlParser.Sql_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#java_anystmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJava_anystmt(SqlParser.Java_anystmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#java_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJava_stmt(SqlParser.Java_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#paramete_array}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParamete_array(SqlParser.Paramete_arrayContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#var_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_stmt(SqlParser.Var_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#var_equal_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_equal_stmt(SqlParser.Var_equal_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#condtion_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondtion_stmt(SqlParser.Condtion_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#body_all_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBody_all_stmt(SqlParser.Body_all_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#if_condition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_condition(SqlParser.If_conditionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#javaIF_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJavaIF_stmt(SqlParser.JavaIF_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#java_else}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJava_else(SqlParser.Java_elseContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#javaIFline_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJavaIFline_stmt(SqlParser.JavaIFline_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#ifline_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfline_body(SqlParser.Ifline_bodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#unrealized_condition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnrealized_condition(SqlParser.Unrealized_conditionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#realized_condition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealized_condition(SqlParser.Realized_conditionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#complete_ifLine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComplete_ifLine(SqlParser.Complete_ifLineContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#differnt_kind_of_for_increament}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDiffernt_kind_of_for_increament(SqlParser.Differnt_kind_of_for_increamentContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#increament_var}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIncreament_var(SqlParser.Increament_varContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#discreament_var}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDiscreament_var(SqlParser.Discreament_varContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#signal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSignal(SqlParser.SignalContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#javaFor_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJavaFor_stmt(SqlParser.JavaFor_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#javaForeach_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJavaForeach_stmt(SqlParser.JavaForeach_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#parameter_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameter_list(SqlParser.Parameter_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#item}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitItem(SqlParser.ItemContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#argument_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgument_list(SqlParser.Argument_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#call_function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCall_function(SqlParser.Call_functionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction(SqlParser.FunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#javaWhile_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJavaWhile_stmt(SqlParser.JavaWhile_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#java_do_while}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJava_do_while(SqlParser.Java_do_whileContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#java_switch_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJava_switch_stmt(SqlParser.Java_switch_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#case_switch}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCase_switch(SqlParser.Case_switchContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#default_switch}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefault_switch(SqlParser.Default_switchContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#print_stm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrint_stm(SqlParser.Print_stmContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#factored_select_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFactored_select_stmt(SqlParser.Factored_select_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#select_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelect_stmt(SqlParser.Select_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#select_or_values}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelect_or_values(SqlParser.Select_or_valuesContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#sel_table_AM}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSel_table_AM(SqlParser.Sel_table_AMContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#sel_exp_where_AM}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSel_exp_where_AM(SqlParser.Sel_exp_where_AMContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#sel_exp_groupBy_AM}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSel_exp_groupBy_AM(SqlParser.Sel_exp_groupBy_AMContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#sel_exp_having_AM}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSel_exp_having_AM(SqlParser.Sel_exp_having_AMContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#column_def}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_def(SqlParser.Column_defContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#type_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_name(SqlParser.Type_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#column_constraint}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_constraint(SqlParser.Column_constraintContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#column_constraint_primary_key}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_constraint_primary_key(SqlParser.Column_constraint_primary_keyContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#column_constraint_foreign_key}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_constraint_foreign_key(SqlParser.Column_constraint_foreign_keyContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#column_constraint_not_null}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_constraint_not_null(SqlParser.Column_constraint_not_nullContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#column_constraint_null}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_constraint_null(SqlParser.Column_constraint_nullContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#column_default}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_default(SqlParser.Column_defaultContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#column_default_value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_default_value(SqlParser.Column_default_valueContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(SqlParser.ExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#database_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDatabase_exp(SqlParser.Database_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#operator_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperator_exp(SqlParser.Operator_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#function_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_exp(SqlParser.Function_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#second_sel_all}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSecond_sel_all(SqlParser.Second_sel_allContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#sel_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSel_expr(SqlParser.Sel_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#foreign_key_clause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForeign_key_clause(SqlParser.Foreign_key_clauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#fk_target_column_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFk_target_column_name(SqlParser.Fk_target_column_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#indexed_column}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexed_column(SqlParser.Indexed_columnContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#table_constraint}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_constraint(SqlParser.Table_constraintContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#table_constraint_primary_key}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_constraint_primary_key(SqlParser.Table_constraint_primary_keyContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#table_constraint_foreign_key}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_constraint_foreign_key(SqlParser.Table_constraint_foreign_keyContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#table_constraint_unique}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_constraint_unique(SqlParser.Table_constraint_uniqueContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#table_constraint_key}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_constraint_key(SqlParser.Table_constraint_keyContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#fk_origin_column_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFk_origin_column_name(SqlParser.Fk_origin_column_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#ordering_term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrdering_term(SqlParser.Ordering_termContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#result_column}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitResult_column(SqlParser.Result_columnContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#table_or_subquery}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_or_subquery(SqlParser.Table_or_subqueryContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#table_or_sub_one}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_or_sub_one(SqlParser.Table_or_sub_oneContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#table_or_sub_two}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_or_sub_two(SqlParser.Table_or_sub_twoContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#table_or_sub_three}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_or_sub_three(SqlParser.Table_or_sub_threeContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#join_clause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoin_clause(SqlParser.Join_clauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#join_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoin_operator(SqlParser.Join_operatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#join_constraint}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoin_constraint(SqlParser.Join_constraintContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#select_core}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelect_core(SqlParser.Select_coreContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#signed_number}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSigned_number(SqlParser.Signed_numberContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#create_table_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreate_table_stmt(SqlParser.Create_table_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(SqlParser.TypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#path}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPath(SqlParser.PathContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#name_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitName_type(SqlParser.Name_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#create_type_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreate_type_stmt(SqlParser.Create_type_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#jarPath}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJarPath(SqlParser.JarPathContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#className}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassName(SqlParser.ClassNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#methodName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodName(SqlParser.MethodNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#returnType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturnType(SqlParser.ReturnTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#parameter_AggFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameter_AggFunction(SqlParser.Parameter_AggFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#aggregationFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAggregationFunction(SqlParser.AggregationFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#literal_value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral_value(SqlParser.Literal_valueContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#unary_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary_operator(SqlParser.Unary_operatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#opration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOpration(SqlParser.OprationContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#module_argument}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModule_argument(SqlParser.Module_argumentContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#column_alias}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_alias(SqlParser.Column_aliasContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#keyword}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKeyword(SqlParser.KeywordContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitName(SqlParser.NameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#function_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_name(SqlParser.Function_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#database_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDatabase_name(SqlParser.Database_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#table_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_name(SqlParser.Table_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#column_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_name(SqlParser.Column_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#collation_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCollation_name(SqlParser.Collation_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#foreign_table}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForeign_table(SqlParser.Foreign_tableContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#index_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndex_name(SqlParser.Index_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#savepoint_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSavepoint_name(SqlParser.Savepoint_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#table_alias}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_alias(SqlParser.Table_aliasContext ctx);
	/**
	 * Visit a parse tree produced by {@link SqlParser#any_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAny_name(SqlParser.Any_nameContext ctx);
}