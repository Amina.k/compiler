// Generated from C:/Users/Batoul Mougrabi/Desktop/compiler74/src/Java\Sql.g4 by ANTLR 4.7.2
package Java;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SqlParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, SCOL=9, 
		DOT=10, OPEN_PAR=11, CLOSE_PAR=12, COMMA=13, ASSIGN=14, STAR=15, PLUS=16, 
		MINUS=17, TILDE=18, PIPE2=19, DIV=20, MOD=21, LT2=22, GT2=23, AMP=24, 
		PIPE=25, LT=26, LT_EQ=27, GT=28, GT_EQ=29, EQ=30, NOT_EQ1=31, NOT_EQ2=32, 
		K_ABORT=33, K_ACTION=34, K_ADD=35, K_AFTER=36, K_ALL=37, K_ALTER=38, K_ANALYZE=39, 
		K_AND=40, K_AS=41, K_ASC=42, K_ATTACH=43, K_AUTOINCREMENT=44, K_BEFORE=45, 
		K_BEGIN=46, K_BETWEEN=47, K_BY=48, K_BREAK=49, K_CASCADE=50, K_CASE=51, 
		K_CAST=52, K_CHECK=53, K_COLLATE=54, K_COLUMN=55, K_COMMIT=56, K_CONFLICT=57, 
		K_CONSTRAINT=58, K_CONTINUE=59, K_CREATE=60, K_CROSS=61, K_CURRENT_DATE=62, 
		K_CURRENT_TIME=63, K_CURRENT_TIMESTAMP=64, K_DATABASE=65, K_DEFAULT=66, 
		K_DEFERRABLE=67, K_DEFERRED=68, K_DELETE=69, K_DESC=70, K_DETACH=71, K_DISTINCT=72, 
		K_DO=73, K_DROP=74, K_EACH=75, K_ELSE=76, K_END=77, K_ENABLE=78, K_ESCAPE=79, 
		K_EXCEPT=80, K_EXCLUSIVE=81, K_EXISTS=82, K_EXPLAIN=83, K_FAIL=84, K_FALSE=85, 
		K_FOR=86, K_FOREIGN=87, K_FROM=88, K_FULL=89, K_GLOB=90, K_GROUP=91, K_HAVING=92, 
		K_IF=93, K_IGNORE=94, K_IMMEDIATE=95, K_IN=96, K_INDEX=97, K_INDEXED=98, 
		K_INITIALLY=99, K_INNER=100, K_INSERT=101, K_INSTEAD=102, K_INTERSECT=103, 
		K_INTO=104, K_IS=105, K_ISNULL=106, K_JOIN=107, K_JSON=108, K_KEY=109, 
		K_LEFT=110, K_LIKE=111, K_LIMIT=112, K_MATCH=113, K_NATURAL=114, K_NEXTVAL=115, 
		K_NO=116, K_NOT=117, K_NOTNULL=118, K_NULL=119, K_OF=120, K_OFFSET=121, 
		K_ON=122, K_ONLY=123, K_OR=124, K_ORDER=125, K_OUTER=126, K_PLAN=127, 
		K_PRAGMA=128, K_PRIMARY=129, K_PRINT=130, K_QUERY=131, K_RAISE=132, K_RECURSIVE=133, 
		K_REFERENCES=134, K_REGEXP=135, K_REINDEX=136, K_RELEASE=137, K_RENAME=138, 
		K_REPLACE=139, K_RESTRICT=140, K_RETURN=141, K_RIGHT=142, K_ROLLBACK=143, 
		K_ROW=144, K_SAVEPOINT=145, K_SELECT=146, K_SET=147, K_SWITCH=148, K_TABLE=149, 
		K_TEMP=150, K_TEMPORARY=151, K_THEN=152, K_TO=153, K_TRUE=154, K_TRANSACTION=155, 
		K_TRIGGER=156, K_UNION=157, K_UNIQUE=158, K_UPDATE=159, K_USING=160, K_VACUUM=161, 
		K_VALUES=162, K_VIEW=163, K_VIRTUAL=164, K_VAR=165, K_WHEN=166, K_WHERE=167, 
		K_WITH=168, K_WITHOUT=169, K_WHILE=170, K_TYPE=171, K_PATH=172, K_AGGREGATION_FUNCTION=173, 
		IDENTIFIER=174, NUMERIC_LITERAL=175, BIND_PARAMETER=176, STRING_LITERAL=177, 
		BLOB_LITERAL=178, SINGLE_LINE_COMMENT=179, MULTILINE_COMMENT=180, SPACES=181, 
		UNEXPECTED_CHAR=182;
	public static final int
		RULE_parse = 0, RULE_error = 1, RULE_defVariable = 2, RULE_sql_stmt_list = 3, 
		RULE_sql_stmt = 4, RULE_java_anystmt = 5, RULE_java_stmt = 6, RULE_paramete_array = 7, 
		RULE_var_stmt = 8, RULE_var_equal_stmt = 9, RULE_condtion_stmt = 10, RULE_body_all_stmt = 11, 
		RULE_if_condition = 12, RULE_javaIF_stmt = 13, RULE_java_else = 14, RULE_javaIFline_stmt = 15, 
		RULE_ifline_body = 16, RULE_unrealized_condition = 17, RULE_realized_condition = 18, 
		RULE_complete_ifLine = 19, RULE_differnt_kind_of_for_increament = 20, 
		RULE_increament_var = 21, RULE_discreament_var = 22, RULE_signal = 23, 
		RULE_javaFor_stmt = 24, RULE_javaForeach_stmt = 25, RULE_parameter_list = 26, 
		RULE_item = 27, RULE_argument_list = 28, RULE_call_function = 29, RULE_function = 30, 
		RULE_javaWhile_stmt = 31, RULE_java_do_while = 32, RULE_java_switch_stmt = 33, 
		RULE_case_switch = 34, RULE_default_switch = 35, RULE_print_stm = 36, 
		RULE_factored_select_stmt = 37, RULE_select_stmt = 38, RULE_select_or_values = 39, 
		RULE_sel_table_AM = 40, RULE_sel_exp_where_AM = 41, RULE_sel_exp_groupBy_AM = 42, 
		RULE_sel_exp_having_AM = 43, RULE_column_def = 44, RULE_type_name = 45, 
		RULE_column_constraint = 46, RULE_column_constraint_primary_key = 47, 
		RULE_column_constraint_foreign_key = 48, RULE_column_constraint_not_null = 49, 
		RULE_column_constraint_null = 50, RULE_column_default = 51, RULE_column_default_value = 52, 
		RULE_expr = 53, RULE_database_exp = 54, RULE_operator_exp = 55, RULE_function_exp = 56, 
		RULE_second_sel_all = 57, RULE_sel_expr = 58, RULE_foreign_key_clause = 59, 
		RULE_fk_target_column_name = 60, RULE_indexed_column = 61, RULE_table_constraint = 62, 
		RULE_table_constraint_primary_key = 63, RULE_table_constraint_foreign_key = 64, 
		RULE_table_constraint_unique = 65, RULE_table_constraint_key = 66, RULE_fk_origin_column_name = 67, 
		RULE_ordering_term = 68, RULE_result_column = 69, RULE_table_or_subquery = 70, 
		RULE_table_or_sub_one = 71, RULE_table_or_sub_two = 72, RULE_table_or_sub_three = 73, 
		RULE_join_clause = 74, RULE_join_operator = 75, RULE_join_constraint = 76, 
		RULE_select_core = 77, RULE_signed_number = 78, RULE_create_table_stmt = 79, 
		RULE_type = 80, RULE_path = 81, RULE_name_type = 82, RULE_create_type_stmt = 83, 
		RULE_jarPath = 84, RULE_className = 85, RULE_methodName = 86, RULE_returnType = 87, 
		RULE_parameter_AggFunction = 88, RULE_aggregationFunction = 89, RULE_literal_value = 90, 
		RULE_unary_operator = 91, RULE_opration = 92, RULE_module_argument = 93, 
		RULE_column_alias = 94, RULE_keyword = 95, RULE_name = 96, RULE_function_name = 97, 
		RULE_database_name = 98, RULE_table_name = 99, RULE_column_name = 100, 
		RULE_collation_name = 101, RULE_foreign_table = 102, RULE_index_name = 103, 
		RULE_savepoint_name = 104, RULE_table_alias = 105, RULE_any_name = 106;
	private static String[] makeRuleNames() {
		return new String[] {
			"parse", "error", "defVariable", "sql_stmt_list", "sql_stmt", "java_anystmt", 
			"java_stmt", "paramete_array", "var_stmt", "var_equal_stmt", "condtion_stmt", 
			"body_all_stmt", "if_condition", "javaIF_stmt", "java_else", "javaIFline_stmt", 
			"ifline_body", "unrealized_condition", "realized_condition", "complete_ifLine", 
			"differnt_kind_of_for_increament", "increament_var", "discreament_var", 
			"signal", "javaFor_stmt", "javaForeach_stmt", "parameter_list", "item", 
			"argument_list", "call_function", "function", "javaWhile_stmt", "java_do_while", 
			"java_switch_stmt", "case_switch", "default_switch", "print_stm", "factored_select_stmt", 
			"select_stmt", "select_or_values", "sel_table_AM", "sel_exp_where_AM", 
			"sel_exp_groupBy_AM", "sel_exp_having_AM", "column_def", "type_name", 
			"column_constraint", "column_constraint_primary_key", "column_constraint_foreign_key", 
			"column_constraint_not_null", "column_constraint_null", "column_default", 
			"column_default_value", "expr", "database_exp", "operator_exp", "function_exp", 
			"second_sel_all", "sel_expr", "foreign_key_clause", "fk_target_column_name", 
			"indexed_column", "table_constraint", "table_constraint_primary_key", 
			"table_constraint_foreign_key", "table_constraint_unique", "table_constraint_key", 
			"fk_origin_column_name", "ordering_term", "result_column", "table_or_subquery", 
			"table_or_sub_one", "table_or_sub_two", "table_or_sub_three", "join_clause", 
			"join_operator", "join_constraint", "select_core", "signed_number", "create_table_stmt", 
			"type", "path", "name_type", "create_type_stmt", "jarPath", "className", 
			"methodName", "returnType", "parameter_AggFunction", "aggregationFunction", 
			"literal_value", "unary_operator", "opration", "module_argument", "column_alias", 
			"keyword", "name", "function_name", "database_name", "table_name", "column_name", 
			"collation_name", "foreign_table", "index_name", "savepoint_name", "table_alias", 
			"any_name"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'['", "']'", "'!'", "'{'", "'}'", "'?'", "':'", "'::'", "';'", 
			"'.'", "'('", "')'", "','", "'='", "'*'", "'+'", "'-'", "'~'", "'||'", 
			"'/'", "'%'", "'<<'", "'>>'", "'&'", "'|'", "'<'", "'<='", "'>'", "'>='", 
			"'=='", "'!='", "'<>'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, "SCOL", "DOT", 
			"OPEN_PAR", "CLOSE_PAR", "COMMA", "ASSIGN", "STAR", "PLUS", "MINUS", 
			"TILDE", "PIPE2", "DIV", "MOD", "LT2", "GT2", "AMP", "PIPE", "LT", "LT_EQ", 
			"GT", "GT_EQ", "EQ", "NOT_EQ1", "NOT_EQ2", "K_ABORT", "K_ACTION", "K_ADD", 
			"K_AFTER", "K_ALL", "K_ALTER", "K_ANALYZE", "K_AND", "K_AS", "K_ASC", 
			"K_ATTACH", "K_AUTOINCREMENT", "K_BEFORE", "K_BEGIN", "K_BETWEEN", "K_BY", 
			"K_BREAK", "K_CASCADE", "K_CASE", "K_CAST", "K_CHECK", "K_COLLATE", "K_COLUMN", 
			"K_COMMIT", "K_CONFLICT", "K_CONSTRAINT", "K_CONTINUE", "K_CREATE", "K_CROSS", 
			"K_CURRENT_DATE", "K_CURRENT_TIME", "K_CURRENT_TIMESTAMP", "K_DATABASE", 
			"K_DEFAULT", "K_DEFERRABLE", "K_DEFERRED", "K_DELETE", "K_DESC", "K_DETACH", 
			"K_DISTINCT", "K_DO", "K_DROP", "K_EACH", "K_ELSE", "K_END", "K_ENABLE", 
			"K_ESCAPE", "K_EXCEPT", "K_EXCLUSIVE", "K_EXISTS", "K_EXPLAIN", "K_FAIL", 
			"K_FALSE", "K_FOR", "K_FOREIGN", "K_FROM", "K_FULL", "K_GLOB", "K_GROUP", 
			"K_HAVING", "K_IF", "K_IGNORE", "K_IMMEDIATE", "K_IN", "K_INDEX", "K_INDEXED", 
			"K_INITIALLY", "K_INNER", "K_INSERT", "K_INSTEAD", "K_INTERSECT", "K_INTO", 
			"K_IS", "K_ISNULL", "K_JOIN", "K_JSON", "K_KEY", "K_LEFT", "K_LIKE", 
			"K_LIMIT", "K_MATCH", "K_NATURAL", "K_NEXTVAL", "K_NO", "K_NOT", "K_NOTNULL", 
			"K_NULL", "K_OF", "K_OFFSET", "K_ON", "K_ONLY", "K_OR", "K_ORDER", "K_OUTER", 
			"K_PLAN", "K_PRAGMA", "K_PRIMARY", "K_PRINT", "K_QUERY", "K_RAISE", "K_RECURSIVE", 
			"K_REFERENCES", "K_REGEXP", "K_REINDEX", "K_RELEASE", "K_RENAME", "K_REPLACE", 
			"K_RESTRICT", "K_RETURN", "K_RIGHT", "K_ROLLBACK", "K_ROW", "K_SAVEPOINT", 
			"K_SELECT", "K_SET", "K_SWITCH", "K_TABLE", "K_TEMP", "K_TEMPORARY", 
			"K_THEN", "K_TO", "K_TRUE", "K_TRANSACTION", "K_TRIGGER", "K_UNION", 
			"K_UNIQUE", "K_UPDATE", "K_USING", "K_VACUUM", "K_VALUES", "K_VIEW", 
			"K_VIRTUAL", "K_VAR", "K_WHEN", "K_WHERE", "K_WITH", "K_WITHOUT", "K_WHILE", 
			"K_TYPE", "K_PATH", "K_AGGREGATION_FUNCTION", "IDENTIFIER", "NUMERIC_LITERAL", 
			"BIND_PARAMETER", "STRING_LITERAL", "BLOB_LITERAL", "SINGLE_LINE_COMMENT", 
			"MULTILINE_COMMENT", "SPACES", "UNEXPECTED_CHAR"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Sql.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public SqlParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ParseContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(SqlParser.EOF, 0); }
		public List<ErrorContext> error() {
			return getRuleContexts(ErrorContext.class);
		}
		public ErrorContext error(int i) {
			return getRuleContext(ErrorContext.class,i);
		}
		public List<Sql_stmt_listContext> sql_stmt_list() {
			return getRuleContexts(Sql_stmt_listContext.class);
		}
		public Sql_stmt_listContext sql_stmt_list(int i) {
			return getRuleContext(Sql_stmt_listContext.class,i);
		}
		public List<DefVariableContext> defVariable() {
			return getRuleContexts(DefVariableContext.class);
		}
		public DefVariableContext defVariable(int i) {
			return getRuleContext(DefVariableContext.class,i);
		}
		public List<FunctionContext> function() {
			return getRuleContexts(FunctionContext.class);
		}
		public FunctionContext function(int i) {
			return getRuleContext(FunctionContext.class,i);
		}
		public List<AggregationFunctionContext> aggregationFunction() {
			return getRuleContexts(AggregationFunctionContext.class);
		}
		public AggregationFunctionContext aggregationFunction(int i) {
			return getRuleContext(AggregationFunctionContext.class,i);
		}
		public ParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitParse(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitParse(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParseContext parse() throws RecognitionException {
		ParseContext _localctx = new ParseContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_parse);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(221);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << SCOL) | (1L << OPEN_PAR) | (1L << K_CREATE))) != 0) || ((((_la - 146)) & ~0x3f) == 0 && ((1L << (_la - 146)) & ((1L << (K_SELECT - 146)) | (1L << (K_VALUES - 146)) | (1L << (K_VAR - 146)) | (1L << (IDENTIFIER - 146)) | (1L << (STRING_LITERAL - 146)) | (1L << (UNEXPECTED_CHAR - 146)))) != 0)) {
				{
				setState(219);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
				case 1:
					{
					setState(214);
					error();
					}
					break;
				case 2:
					{
					setState(215);
					sql_stmt_list();
					}
					break;
				case 3:
					{
					setState(216);
					defVariable();
					}
					break;
				case 4:
					{
					setState(217);
					function();
					}
					break;
				case 5:
					{
					setState(218);
					aggregationFunction();
					}
					break;
				}
				}
				setState(223);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(224);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ErrorContext extends ParserRuleContext {
		public Token UNEXPECTED_CHAR;
		public TerminalNode UNEXPECTED_CHAR() { return getToken(SqlParser.UNEXPECTED_CHAR, 0); }
		public ErrorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_error; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterError(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitError(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitError(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ErrorContext error() throws RecognitionException {
		ErrorContext _localctx = new ErrorContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_error);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(226);
			((ErrorContext)_localctx).UNEXPECTED_CHAR = match(UNEXPECTED_CHAR);

			     throw new RuntimeException("UNEXPECTED_CHAR=" + (((ErrorContext)_localctx).UNEXPECTED_CHAR!=null?((ErrorContext)_localctx).UNEXPECTED_CHAR.getText():null));
			   
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DefVariableContext extends ParserRuleContext {
		public Var_equal_stmtContext var_equal_stmt() {
			return getRuleContext(Var_equal_stmtContext.class,0);
		}
		public DefVariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_defVariable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterDefVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitDefVariable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitDefVariable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DefVariableContext defVariable() throws RecognitionException {
		DefVariableContext _localctx = new DefVariableContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_defVariable);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(229);
			var_equal_stmt();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sql_stmt_listContext extends ParserRuleContext {
		public List<Sql_stmtContext> sql_stmt() {
			return getRuleContexts(Sql_stmtContext.class);
		}
		public Sql_stmtContext sql_stmt(int i) {
			return getRuleContext(Sql_stmtContext.class,i);
		}
		public List<TerminalNode> SCOL() { return getTokens(SqlParser.SCOL); }
		public TerminalNode SCOL(int i) {
			return getToken(SqlParser.SCOL, i);
		}
		public Sql_stmt_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sql_stmt_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterSql_stmt_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitSql_stmt_list(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitSql_stmt_list(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sql_stmt_listContext sql_stmt_list() throws RecognitionException {
		Sql_stmt_listContext _localctx = new Sql_stmt_listContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_sql_stmt_list);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(234);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SCOL) {
				{
				{
				setState(231);
				match(SCOL);
				}
				}
				setState(236);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(237);
			sql_stmt();
			setState(246);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(239); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(238);
						match(SCOL);
						}
						}
						setState(241); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==SCOL );
					setState(243);
					sql_stmt();
					}
					} 
				}
				setState(248);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			}
			setState(252);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(249);
					match(SCOL);
					}
					} 
				}
				setState(254);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sql_stmtContext extends ParserRuleContext {
		public Create_table_stmtContext create_table_stmt() {
			return getRuleContext(Create_table_stmtContext.class,0);
		}
		public Factored_select_stmtContext factored_select_stmt() {
			return getRuleContext(Factored_select_stmtContext.class,0);
		}
		public Create_type_stmtContext create_type_stmt() {
			return getRuleContext(Create_type_stmtContext.class,0);
		}
		public Sql_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sql_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterSql_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitSql_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitSql_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sql_stmtContext sql_stmt() throws RecognitionException {
		Sql_stmtContext _localctx = new Sql_stmtContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_sql_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(258);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				{
				setState(255);
				create_table_stmt();
				}
				break;
			case 2:
				{
				setState(256);
				factored_select_stmt();
				}
				break;
			case 3:
				{
				setState(257);
				create_type_stmt();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Java_anystmtContext extends ParserRuleContext {
		public Sql_stmt_listContext sql_stmt_list() {
			return getRuleContext(Sql_stmt_listContext.class,0);
		}
		public Sql_stmtContext sql_stmt() {
			return getRuleContext(Sql_stmtContext.class,0);
		}
		public Java_stmtContext java_stmt() {
			return getRuleContext(Java_stmtContext.class,0);
		}
		public TerminalNode K_CONTINUE() { return getToken(SqlParser.K_CONTINUE, 0); }
		public TerminalNode SCOL() { return getToken(SqlParser.SCOL, 0); }
		public Java_anystmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_java_anystmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterJava_anystmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitJava_anystmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitJava_anystmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Java_anystmtContext java_anystmt() throws RecognitionException {
		Java_anystmtContext _localctx = new Java_anystmtContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_java_anystmt);
		try {
			setState(265);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(260);
				sql_stmt_list();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(261);
				sql_stmt();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(262);
				java_stmt();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(263);
				match(K_CONTINUE);
				setState(264);
				match(SCOL);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Java_stmtContext extends ParserRuleContext {
		public Call_functionContext call_function() {
			return getRuleContext(Call_functionContext.class,0);
		}
		public TerminalNode SCOL() { return getToken(SqlParser.SCOL, 0); }
		public JavaIF_stmtContext javaIF_stmt() {
			return getRuleContext(JavaIF_stmtContext.class,0);
		}
		public JavaIFline_stmtContext javaIFline_stmt() {
			return getRuleContext(JavaIFline_stmtContext.class,0);
		}
		public JavaFor_stmtContext javaFor_stmt() {
			return getRuleContext(JavaFor_stmtContext.class,0);
		}
		public JavaForeach_stmtContext javaForeach_stmt() {
			return getRuleContext(JavaForeach_stmtContext.class,0);
		}
		public JavaWhile_stmtContext javaWhile_stmt() {
			return getRuleContext(JavaWhile_stmtContext.class,0);
		}
		public Java_do_whileContext java_do_while() {
			return getRuleContext(Java_do_whileContext.class,0);
		}
		public Var_equal_stmtContext var_equal_stmt() {
			return getRuleContext(Var_equal_stmtContext.class,0);
		}
		public Java_switch_stmtContext java_switch_stmt() {
			return getRuleContext(Java_switch_stmtContext.class,0);
		}
		public Differnt_kind_of_for_increamentContext differnt_kind_of_for_increament() {
			return getRuleContext(Differnt_kind_of_for_increamentContext.class,0);
		}
		public Print_stmContext print_stm() {
			return getRuleContext(Print_stmContext.class,0);
		}
		public Java_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_java_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterJava_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitJava_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitJava_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Java_stmtContext java_stmt() throws RecognitionException {
		Java_stmtContext _localctx = new Java_stmtContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_java_stmt);
		try {
			setState(285);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(267);
				call_function();
				setState(269);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
				case 1:
					{
					setState(268);
					match(SCOL);
					}
					break;
				}
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(271);
				javaIF_stmt();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(272);
				javaIFline_stmt();
				setState(273);
				match(SCOL);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(275);
				javaFor_stmt();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(276);
				javaForeach_stmt();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(277);
				javaWhile_stmt();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(278);
				java_do_while();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(279);
				var_equal_stmt();
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(280);
				java_switch_stmt();
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(281);
				differnt_kind_of_for_increament();
				setState(282);
				match(SCOL);
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(284);
				print_stm();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Paramete_arrayContext extends ParserRuleContext {
		public TerminalNode SCOL() { return getToken(SqlParser.SCOL, 0); }
		public List<Any_nameContext> any_name() {
			return getRuleContexts(Any_nameContext.class);
		}
		public Any_nameContext any_name(int i) {
			return getRuleContext(Any_nameContext.class,i);
		}
		public List<Signed_numberContext> signed_number() {
			return getRuleContexts(Signed_numberContext.class);
		}
		public Signed_numberContext signed_number(int i) {
			return getRuleContext(Signed_numberContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SqlParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SqlParser.COMMA, i);
		}
		public Paramete_arrayContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_paramete_array; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterParamete_array(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitParamete_array(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitParamete_array(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Paramete_arrayContext paramete_array() throws RecognitionException {
		Paramete_arrayContext _localctx = new Paramete_arrayContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_paramete_array);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(287);
			match(T__0);
			setState(302);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << OPEN_PAR) | (1L << STAR) | (1L << PLUS) | (1L << MINUS))) != 0) || ((((_la - 174)) & ~0x3f) == 0 && ((1L << (_la - 174)) & ((1L << (IDENTIFIER - 174)) | (1L << (NUMERIC_LITERAL - 174)) | (1L << (STRING_LITERAL - 174)))) != 0)) {
				{
				setState(290);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case OPEN_PAR:
				case IDENTIFIER:
				case STRING_LITERAL:
					{
					setState(288);
					any_name();
					}
					break;
				case STAR:
				case PLUS:
				case MINUS:
				case NUMERIC_LITERAL:
					{
					setState(289);
					signed_number();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(299);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(292);
					match(COMMA);
					setState(295);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case OPEN_PAR:
					case IDENTIFIER:
					case STRING_LITERAL:
						{
						setState(293);
						any_name();
						}
						break;
					case STAR:
					case PLUS:
					case MINUS:
					case NUMERIC_LITERAL:
						{
						setState(294);
						signed_number();
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					}
					}
					setState(301);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(304);
			match(T__1);
			setState(305);
			match(SCOL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Var_stmtContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public TerminalNode K_VAR() { return getToken(SqlParser.K_VAR, 0); }
		public Var_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterVar_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitVar_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitVar_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Var_stmtContext var_stmt() throws RecognitionException {
		Var_stmtContext _localctx = new Var_stmtContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_var_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(308);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_VAR) {
				{
				setState(307);
				match(K_VAR);
				}
			}

			setState(310);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Var_equal_stmtContext extends ParserRuleContext {
		public Var_stmtContext var_stmt() {
			return getRuleContext(Var_stmtContext.class,0);
		}
		public TerminalNode SCOL() { return getToken(SqlParser.SCOL, 0); }
		public TerminalNode ASSIGN() { return getToken(SqlParser.ASSIGN, 0); }
		public Sql_stmtContext sql_stmt() {
			return getRuleContext(Sql_stmtContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public JavaIFline_stmtContext javaIFline_stmt() {
			return getRuleContext(JavaIFline_stmtContext.class,0);
		}
		public Var_equal_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var_equal_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterVar_equal_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitVar_equal_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitVar_equal_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Var_equal_stmtContext var_equal_stmt() throws RecognitionException {
		Var_equal_stmtContext _localctx = new Var_equal_stmtContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_var_equal_stmt);
		try {
			setState(322);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(312);
				var_stmt();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(313);
				var_stmt();
				setState(314);
				match(ASSIGN);
				setState(318);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
				case 1:
					{
					{
					setState(315);
					javaIFline_stmt();
					}
					}
					break;
				case 2:
					{
					setState(316);
					sql_stmt();
					}
					break;
				case 3:
					{
					setState(317);
					expr(0);
					}
					break;
				}
				}
				setState(320);
				match(SCOL);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Condtion_stmtContext extends ParserRuleContext {
		public List<TerminalNode> OPEN_PAR() { return getTokens(SqlParser.OPEN_PAR); }
		public TerminalNode OPEN_PAR(int i) {
			return getToken(SqlParser.OPEN_PAR, i);
		}
		public List<TerminalNode> CLOSE_PAR() { return getTokens(SqlParser.CLOSE_PAR); }
		public TerminalNode CLOSE_PAR(int i) {
			return getToken(SqlParser.CLOSE_PAR, i);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Condtion_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condtion_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterCondtion_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitCondtion_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitCondtion_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Condtion_stmtContext condtion_stmt() throws RecognitionException {
		Condtion_stmtContext _localctx = new Condtion_stmtContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_condtion_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(325);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
			case 1:
				{
				setState(324);
				match(OPEN_PAR);
				}
				break;
			}
			setState(328);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__2) {
				{
				setState(327);
				match(T__2);
				}
			}

			{
			{
			setState(331);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,19,_ctx) ) {
			case 1:
				{
				setState(330);
				match(OPEN_PAR);
				}
				break;
			}
			setState(333);
			expr(0);
			setState(335);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,20,_ctx) ) {
			case 1:
				{
				setState(334);
				match(CLOSE_PAR);
				}
				break;
			}
			}
			}
			setState(338);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==CLOSE_PAR) {
				{
				setState(337);
				match(CLOSE_PAR);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Body_all_stmtContext extends ParserRuleContext {
		public List<Java_anystmtContext> java_anystmt() {
			return getRuleContexts(Java_anystmtContext.class);
		}
		public Java_anystmtContext java_anystmt(int i) {
			return getRuleContext(Java_anystmtContext.class,i);
		}
		public List<TerminalNode> SCOL() { return getTokens(SqlParser.SCOL); }
		public TerminalNode SCOL(int i) {
			return getToken(SqlParser.SCOL, i);
		}
		public Body_all_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_body_all_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterBody_all_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitBody_all_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitBody_all_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Body_all_stmtContext body_all_stmt() throws RecognitionException {
		Body_all_stmtContext _localctx = new Body_all_stmtContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_body_all_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(355);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__3:
				{
				{
				setState(340);
				match(T__3);
				setState(347);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << SCOL) | (1L << OPEN_PAR) | (1L << PLUS) | (1L << MINUS) | (1L << TILDE) | (1L << K_CONTINUE) | (1L << K_CREATE) | (1L << K_CURRENT_DATE) | (1L << K_CURRENT_TIME))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (K_CURRENT_TIMESTAMP - 64)) | (1L << (K_DO - 64)) | (1L << (K_EXISTS - 64)) | (1L << (K_FALSE - 64)) | (1L << (K_FOR - 64)) | (1L << (K_IF - 64)) | (1L << (K_NOT - 64)) | (1L << (K_NULL - 64)))) != 0) || ((((_la - 130)) & ~0x3f) == 0 && ((1L << (_la - 130)) & ((1L << (K_PRINT - 130)) | (1L << (K_SELECT - 130)) | (1L << (K_SWITCH - 130)) | (1L << (K_TRUE - 130)) | (1L << (K_VALUES - 130)) | (1L << (K_VAR - 130)) | (1L << (K_WHILE - 130)) | (1L << (IDENTIFIER - 130)) | (1L << (NUMERIC_LITERAL - 130)) | (1L << (STRING_LITERAL - 130)) | (1L << (BLOB_LITERAL - 130)))) != 0)) {
					{
					{
					setState(341);
					java_anystmt();
					setState(343);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,22,_ctx) ) {
					case 1:
						{
						setState(342);
						match(SCOL);
						}
						break;
					}
					}
					}
					setState(349);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(350);
				match(T__4);
				}
				}
				break;
			case T__2:
			case SCOL:
			case OPEN_PAR:
			case PLUS:
			case MINUS:
			case TILDE:
			case K_CONTINUE:
			case K_CREATE:
			case K_CURRENT_DATE:
			case K_CURRENT_TIME:
			case K_CURRENT_TIMESTAMP:
			case K_DO:
			case K_EXISTS:
			case K_FALSE:
			case K_FOR:
			case K_IF:
			case K_NOT:
			case K_NULL:
			case K_PRINT:
			case K_SELECT:
			case K_SWITCH:
			case K_TRUE:
			case K_VALUES:
			case K_VAR:
			case K_WHILE:
			case IDENTIFIER:
			case NUMERIC_LITERAL:
			case STRING_LITERAL:
			case BLOB_LITERAL:
				{
				{
				{
				setState(351);
				java_anystmt();
				setState(353);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,24,_ctx) ) {
				case 1:
					{
					setState(352);
					match(SCOL);
					}
					break;
				}
				}
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_conditionContext extends ParserRuleContext {
		public TerminalNode K_IF() { return getToken(SqlParser.K_IF, 0); }
		public Condtion_stmtContext condtion_stmt() {
			return getRuleContext(Condtion_stmtContext.class,0);
		}
		public If_conditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_if_condition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterIf_condition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitIf_condition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitIf_condition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final If_conditionContext if_condition() throws RecognitionException {
		If_conditionContext _localctx = new If_conditionContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_if_condition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(357);
			match(K_IF);
			setState(358);
			condtion_stmt();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class JavaIF_stmtContext extends ParserRuleContext {
		public If_conditionContext if_condition() {
			return getRuleContext(If_conditionContext.class,0);
		}
		public Body_all_stmtContext body_all_stmt() {
			return getRuleContext(Body_all_stmtContext.class,0);
		}
		public List<Java_elseContext> java_else() {
			return getRuleContexts(Java_elseContext.class);
		}
		public Java_elseContext java_else(int i) {
			return getRuleContext(Java_elseContext.class,i);
		}
		public JavaIF_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_javaIF_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterJavaIF_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitJavaIF_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitJavaIF_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final JavaIF_stmtContext javaIF_stmt() throws RecognitionException {
		JavaIF_stmtContext _localctx = new JavaIF_stmtContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_javaIF_stmt);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(360);
			if_condition();
			{
			setState(361);
			body_all_stmt();
			}
			setState(365);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,26,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(362);
					java_else();
					}
					} 
				}
				setState(367);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,26,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Java_elseContext extends ParserRuleContext {
		public TerminalNode K_ELSE() { return getToken(SqlParser.K_ELSE, 0); }
		public JavaIF_stmtContext javaIF_stmt() {
			return getRuleContext(JavaIF_stmtContext.class,0);
		}
		public Body_all_stmtContext body_all_stmt() {
			return getRuleContext(Body_all_stmtContext.class,0);
		}
		public Java_elseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_java_else; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterJava_else(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitJava_else(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitJava_else(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Java_elseContext java_else() throws RecognitionException {
		Java_elseContext _localctx = new Java_elseContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_java_else);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(368);
			match(K_ELSE);
			setState(371);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,27,_ctx) ) {
			case 1:
				{
				{
				setState(369);
				javaIF_stmt();
				}
				}
				break;
			case 2:
				{
				{
				setState(370);
				body_all_stmt();
				}
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class JavaIFline_stmtContext extends ParserRuleContext {
		public Condtion_stmtContext condtion_stmt() {
			return getRuleContext(Condtion_stmtContext.class,0);
		}
		public Ifline_bodyContext ifline_body() {
			return getRuleContext(Ifline_bodyContext.class,0);
		}
		public JavaIFline_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_javaIFline_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterJavaIFline_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitJavaIFline_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitJavaIFline_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final JavaIFline_stmtContext javaIFline_stmt() throws RecognitionException {
		JavaIFline_stmtContext _localctx = new JavaIFline_stmtContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_javaIFline_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(373);
			condtion_stmt();
			setState(374);
			match(T__5);
			setState(375);
			ifline_body();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Ifline_bodyContext extends ParserRuleContext {
		public Realized_conditionContext realized_condition() {
			return getRuleContext(Realized_conditionContext.class,0);
		}
		public Unrealized_conditionContext unrealized_condition() {
			return getRuleContext(Unrealized_conditionContext.class,0);
		}
		public List<TerminalNode> OPEN_PAR() { return getTokens(SqlParser.OPEN_PAR); }
		public TerminalNode OPEN_PAR(int i) {
			return getToken(SqlParser.OPEN_PAR, i);
		}
		public List<TerminalNode> CLOSE_PAR() { return getTokens(SqlParser.CLOSE_PAR); }
		public TerminalNode CLOSE_PAR(int i) {
			return getToken(SqlParser.CLOSE_PAR, i);
		}
		public Ifline_bodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifline_body; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterIfline_body(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitIfline_body(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitIfline_body(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Ifline_bodyContext ifline_body() throws RecognitionException {
		Ifline_bodyContext _localctx = new Ifline_bodyContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_ifline_body);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(378);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,28,_ctx) ) {
			case 1:
				{
				setState(377);
				match(OPEN_PAR);
				}
				break;
			}
			setState(380);
			realized_condition();
			setState(382);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==CLOSE_PAR) {
				{
				setState(381);
				match(CLOSE_PAR);
				}
			}

			setState(384);
			match(T__6);
			setState(386);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,30,_ctx) ) {
			case 1:
				{
				setState(385);
				match(OPEN_PAR);
				}
				break;
			}
			setState(388);
			unrealized_condition();
			setState(390);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,31,_ctx) ) {
			case 1:
				{
				setState(389);
				match(CLOSE_PAR);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Unrealized_conditionContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Complete_ifLineContext complete_ifLine() {
			return getRuleContext(Complete_ifLineContext.class,0);
		}
		public Unrealized_conditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unrealized_condition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterUnrealized_condition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitUnrealized_condition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitUnrealized_condition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Unrealized_conditionContext unrealized_condition() throws RecognitionException {
		Unrealized_conditionContext _localctx = new Unrealized_conditionContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_unrealized_condition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(394);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,32,_ctx) ) {
			case 1:
				{
				setState(392);
				expr(0);
				}
				break;
			case 2:
				{
				setState(393);
				complete_ifLine();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Realized_conditionContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Complete_ifLineContext complete_ifLine() {
			return getRuleContext(Complete_ifLineContext.class,0);
		}
		public Realized_conditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_realized_condition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterRealized_condition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitRealized_condition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitRealized_condition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Realized_conditionContext realized_condition() throws RecognitionException {
		Realized_conditionContext _localctx = new Realized_conditionContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_realized_condition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(398);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,33,_ctx) ) {
			case 1:
				{
				setState(396);
				expr(0);
				}
				break;
			case 2:
				{
				setState(397);
				complete_ifLine();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Complete_ifLineContext extends ParserRuleContext {
		public JavaIFline_stmtContext javaIFline_stmt() {
			return getRuleContext(JavaIFline_stmtContext.class,0);
		}
		public Complete_ifLineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_complete_ifLine; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterComplete_ifLine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitComplete_ifLine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitComplete_ifLine(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Complete_ifLineContext complete_ifLine() throws RecognitionException {
		Complete_ifLineContext _localctx = new Complete_ifLineContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_complete_ifLine);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(400);
			javaIFline_stmt();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Differnt_kind_of_for_increamentContext extends ParserRuleContext {
		public Increament_varContext increament_var() {
			return getRuleContext(Increament_varContext.class,0);
		}
		public Discreament_varContext discreament_var() {
			return getRuleContext(Discreament_varContext.class,0);
		}
		public Literal_valueContext literal_value() {
			return getRuleContext(Literal_valueContext.class,0);
		}
		public Call_functionContext call_function() {
			return getRuleContext(Call_functionContext.class,0);
		}
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public SignalContext signal() {
			return getRuleContext(SignalContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(SqlParser.ASSIGN, 0); }
		public TerminalNode PLUS() { return getToken(SqlParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(SqlParser.MINUS, 0); }
		public Differnt_kind_of_for_increamentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_differnt_kind_of_for_increament; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterDiffernt_kind_of_for_increament(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitDiffernt_kind_of_for_increament(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitDiffernt_kind_of_for_increament(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Differnt_kind_of_for_increamentContext differnt_kind_of_for_increament() throws RecognitionException {
		Differnt_kind_of_for_increamentContext _localctx = new Differnt_kind_of_for_increamentContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_differnt_kind_of_for_increament);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(408);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,35,_ctx) ) {
			case 1:
				{
				setState(402);
				any_name();
				setState(403);
				signal();
				setState(404);
				match(ASSIGN);
				setState(406);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,34,_ctx) ) {
				case 1:
					{
					setState(405);
					_la = _input.LA(1);
					if ( !(_la==PLUS || _la==MINUS) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
					break;
				}
				}
				break;
			}
			setState(414);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,36,_ctx) ) {
			case 1:
				{
				setState(410);
				increament_var();
				}
				break;
			case 2:
				{
				setState(411);
				discreament_var();
				}
				break;
			case 3:
				{
				setState(412);
				literal_value();
				}
				break;
			case 4:
				{
				setState(413);
				call_function();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Increament_varContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public List<TerminalNode> PLUS() { return getTokens(SqlParser.PLUS); }
		public TerminalNode PLUS(int i) {
			return getToken(SqlParser.PLUS, i);
		}
		public Increament_varContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_increament_var; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterIncreament_var(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitIncreament_var(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitIncreament_var(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Increament_varContext increament_var() throws RecognitionException {
		Increament_varContext _localctx = new Increament_varContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_increament_var);
		try {
			setState(423);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OPEN_PAR:
			case IDENTIFIER:
			case STRING_LITERAL:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(416);
				any_name();
				setState(417);
				match(PLUS);
				setState(418);
				match(PLUS);
				}
				}
				break;
			case PLUS:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(420);
				match(PLUS);
				setState(421);
				match(PLUS);
				setState(422);
				any_name();
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Discreament_varContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public TerminalNode MINUS() { return getToken(SqlParser.MINUS, 0); }
		public Discreament_varContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_discreament_var; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterDiscreament_var(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitDiscreament_var(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitDiscreament_var(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Discreament_varContext discreament_var() throws RecognitionException {
		Discreament_varContext _localctx = new Discreament_varContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_discreament_var);
		try {
			setState(430);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OPEN_PAR:
			case IDENTIFIER:
			case STRING_LITERAL:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(425);
				any_name();
				setState(426);
				match(MINUS);
				}
				}
				break;
			case MINUS:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(428);
				match(MINUS);
				setState(429);
				any_name();
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SignalContext extends ParserRuleContext {
		public TerminalNode STAR() { return getToken(SqlParser.STAR, 0); }
		public TerminalNode DIV() { return getToken(SqlParser.DIV, 0); }
		public TerminalNode MOD() { return getToken(SqlParser.MOD, 0); }
		public TerminalNode PLUS() { return getToken(SqlParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(SqlParser.MINUS, 0); }
		public SignalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_signal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterSignal(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitSignal(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitSignal(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SignalContext signal() throws RecognitionException {
		SignalContext _localctx = new SignalContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_signal);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(432);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << STAR) | (1L << PLUS) | (1L << MINUS) | (1L << DIV) | (1L << MOD))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class JavaFor_stmtContext extends ParserRuleContext {
		public TerminalNode K_FOR() { return getToken(SqlParser.K_FOR, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SqlParser.OPEN_PAR, 0); }
		public Var_stmtContext var_stmt() {
			return getRuleContext(Var_stmtContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(SqlParser.ASSIGN, 0); }
		public Signed_numberContext signed_number() {
			return getRuleContext(Signed_numberContext.class,0);
		}
		public List<TerminalNode> SCOL() { return getTokens(SqlParser.SCOL); }
		public TerminalNode SCOL(int i) {
			return getToken(SqlParser.SCOL, i);
		}
		public Differnt_kind_of_for_increamentContext differnt_kind_of_for_increament() {
			return getRuleContext(Differnt_kind_of_for_increamentContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SqlParser.CLOSE_PAR, 0); }
		public Body_all_stmtContext body_all_stmt() {
			return getRuleContext(Body_all_stmtContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public JavaFor_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_javaFor_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterJavaFor_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitJavaFor_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitJavaFor_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final JavaFor_stmtContext javaFor_stmt() throws RecognitionException {
		JavaFor_stmtContext _localctx = new JavaFor_stmtContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_javaFor_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(434);
			match(K_FOR);
			setState(435);
			match(OPEN_PAR);
			setState(436);
			var_stmt();
			setState(437);
			match(ASSIGN);
			setState(438);
			signed_number();
			setState(439);
			match(SCOL);
			{
			setState(440);
			expr(0);
			}
			setState(441);
			match(SCOL);
			setState(442);
			differnt_kind_of_for_increament();
			setState(443);
			match(CLOSE_PAR);
			setState(444);
			body_all_stmt();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class JavaForeach_stmtContext extends ParserRuleContext {
		public TerminalNode K_FOR() { return getToken(SqlParser.K_FOR, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SqlParser.OPEN_PAR, 0); }
		public Var_stmtContext var_stmt() {
			return getRuleContext(Var_stmtContext.class,0);
		}
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SqlParser.CLOSE_PAR, 0); }
		public Body_all_stmtContext body_all_stmt() {
			return getRuleContext(Body_all_stmtContext.class,0);
		}
		public JavaForeach_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_javaForeach_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterJavaForeach_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitJavaForeach_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitJavaForeach_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final JavaForeach_stmtContext javaForeach_stmt() throws RecognitionException {
		JavaForeach_stmtContext _localctx = new JavaForeach_stmtContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_javaForeach_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(446);
			match(K_FOR);
			setState(447);
			match(OPEN_PAR);
			setState(448);
			var_stmt();
			setState(449);
			match(T__6);
			setState(450);
			any_name();
			setState(451);
			match(CLOSE_PAR);
			setState(452);
			body_all_stmt();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Parameter_listContext extends ParserRuleContext {
		public List<ItemContext> item() {
			return getRuleContexts(ItemContext.class);
		}
		public ItemContext item(int i) {
			return getRuleContext(ItemContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SqlParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SqlParser.COMMA, i);
		}
		public Parameter_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameter_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterParameter_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitParameter_list(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitParameter_list(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Parameter_listContext parameter_list() throws RecognitionException {
		Parameter_listContext _localctx = new Parameter_listContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_parameter_list);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(454);
			item();
			setState(459);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(455);
				match(COMMA);
				setState(456);
				item();
				}
				}
				setState(461);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ItemContext extends ParserRuleContext {
		public Var_stmtContext var_stmt() {
			return getRuleContext(Var_stmtContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(SqlParser.ASSIGN, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public ItemContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_item; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterItem(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitItem(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitItem(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ItemContext item() throws RecognitionException {
		ItemContext _localctx = new ItemContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_item);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(462);
			var_stmt();
			setState(465);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ASSIGN) {
				{
				setState(463);
				match(ASSIGN);
				setState(464);
				expr(0);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Argument_listContext extends ParserRuleContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<Differnt_kind_of_for_increamentContext> differnt_kind_of_for_increament() {
			return getRuleContexts(Differnt_kind_of_for_increamentContext.class);
		}
		public Differnt_kind_of_for_increamentContext differnt_kind_of_for_increament(int i) {
			return getRuleContext(Differnt_kind_of_for_increamentContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SqlParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SqlParser.COMMA, i);
		}
		public Argument_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_argument_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterArgument_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitArgument_list(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitArgument_list(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Argument_listContext argument_list() throws RecognitionException {
		Argument_listContext _localctx = new Argument_listContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_argument_list);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(469);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,41,_ctx) ) {
			case 1:
				{
				setState(467);
				expr(0);
				}
				break;
			case 2:
				{
				setState(468);
				differnt_kind_of_for_increament();
				}
				break;
			}
			setState(478);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(471);
				match(COMMA);
				setState(474);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,42,_ctx) ) {
				case 1:
					{
					setState(472);
					expr(0);
					}
					break;
				case 2:
					{
					setState(473);
					differnt_kind_of_for_increament();
					}
					break;
				}
				}
				}
				setState(480);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Call_functionContext extends ParserRuleContext {
		public Function_nameContext function_name() {
			return getRuleContext(Function_nameContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SqlParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SqlParser.CLOSE_PAR, 0); }
		public Argument_listContext argument_list() {
			return getRuleContext(Argument_listContext.class,0);
		}
		public Call_functionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_call_function; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterCall_function(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitCall_function(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitCall_function(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Call_functionContext call_function() throws RecognitionException {
		Call_functionContext _localctx = new Call_functionContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_call_function);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(481);
			function_name();
			setState(482);
			match(OPEN_PAR);
			setState(484);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << OPEN_PAR) | (1L << PLUS) | (1L << MINUS) | (1L << TILDE) | (1L << K_CURRENT_DATE) | (1L << K_CURRENT_TIME))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (K_CURRENT_TIMESTAMP - 64)) | (1L << (K_EXISTS - 64)) | (1L << (K_FALSE - 64)) | (1L << (K_NOT - 64)) | (1L << (K_NULL - 64)))) != 0) || ((((_la - 154)) & ~0x3f) == 0 && ((1L << (_la - 154)) & ((1L << (K_TRUE - 154)) | (1L << (IDENTIFIER - 154)) | (1L << (NUMERIC_LITERAL - 154)) | (1L << (STRING_LITERAL - 154)) | (1L << (BLOB_LITERAL - 154)))) != 0)) {
				{
				setState(483);
				argument_list();
				}
			}

			setState(486);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionContext extends ParserRuleContext {
		public Function_nameContext function_name() {
			return getRuleContext(Function_nameContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SqlParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SqlParser.CLOSE_PAR, 0); }
		public List<Parameter_listContext> parameter_list() {
			return getRuleContexts(Parameter_listContext.class);
		}
		public Parameter_listContext parameter_list(int i) {
			return getRuleContext(Parameter_listContext.class,i);
		}
		public List<Java_stmtContext> java_stmt() {
			return getRuleContexts(Java_stmtContext.class);
		}
		public Java_stmtContext java_stmt(int i) {
			return getRuleContext(Java_stmtContext.class,i);
		}
		public TerminalNode K_RETURN() { return getToken(SqlParser.K_RETURN, 0); }
		public List<TerminalNode> SCOL() { return getTokens(SqlParser.SCOL); }
		public TerminalNode SCOL(int i) {
			return getToken(SqlParser.SCOL, i);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public FunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionContext function() throws RecognitionException {
		FunctionContext _localctx = new FunctionContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_function);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(488);
			function_name();
			setState(489);
			match(OPEN_PAR);
			setState(493);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==OPEN_PAR || ((((_la - 165)) & ~0x3f) == 0 && ((1L << (_la - 165)) & ((1L << (K_VAR - 165)) | (1L << (IDENTIFIER - 165)) | (1L << (STRING_LITERAL - 165)))) != 0)) {
				{
				{
				setState(490);
				parameter_list();
				}
				}
				setState(495);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(496);
			match(CLOSE_PAR);
			setState(497);
			match(T__3);
			setState(504);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << OPEN_PAR) | (1L << PLUS) | (1L << MINUS) | (1L << TILDE) | (1L << K_CURRENT_DATE) | (1L << K_CURRENT_TIME))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (K_CURRENT_TIMESTAMP - 64)) | (1L << (K_DO - 64)) | (1L << (K_EXISTS - 64)) | (1L << (K_FALSE - 64)) | (1L << (K_FOR - 64)) | (1L << (K_IF - 64)) | (1L << (K_NOT - 64)) | (1L << (K_NULL - 64)))) != 0) || ((((_la - 130)) & ~0x3f) == 0 && ((1L << (_la - 130)) & ((1L << (K_PRINT - 130)) | (1L << (K_SWITCH - 130)) | (1L << (K_TRUE - 130)) | (1L << (K_VAR - 130)) | (1L << (K_WHILE - 130)) | (1L << (IDENTIFIER - 130)) | (1L << (NUMERIC_LITERAL - 130)) | (1L << (STRING_LITERAL - 130)) | (1L << (BLOB_LITERAL - 130)))) != 0)) {
				{
				{
				setState(498);
				java_stmt();
				setState(500);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SCOL) {
					{
					setState(499);
					match(SCOL);
					}
				}

				}
				}
				setState(506);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(511);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_RETURN) {
				{
				setState(507);
				match(K_RETURN);
				{
				setState(508);
				expr(0);
				}
				setState(509);
				match(SCOL);
				}
			}

			setState(513);
			match(T__4);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class JavaWhile_stmtContext extends ParserRuleContext {
		public TerminalNode K_WHILE() { return getToken(SqlParser.K_WHILE, 0); }
		public Condtion_stmtContext condtion_stmt() {
			return getRuleContext(Condtion_stmtContext.class,0);
		}
		public Body_all_stmtContext body_all_stmt() {
			return getRuleContext(Body_all_stmtContext.class,0);
		}
		public JavaWhile_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_javaWhile_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterJavaWhile_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitJavaWhile_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitJavaWhile_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final JavaWhile_stmtContext javaWhile_stmt() throws RecognitionException {
		JavaWhile_stmtContext _localctx = new JavaWhile_stmtContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_javaWhile_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(515);
			match(K_WHILE);
			setState(516);
			condtion_stmt();
			setState(517);
			body_all_stmt();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Java_do_whileContext extends ParserRuleContext {
		public TerminalNode K_DO() { return getToken(SqlParser.K_DO, 0); }
		public Body_all_stmtContext body_all_stmt() {
			return getRuleContext(Body_all_stmtContext.class,0);
		}
		public TerminalNode K_WHILE() { return getToken(SqlParser.K_WHILE, 0); }
		public Condtion_stmtContext condtion_stmt() {
			return getRuleContext(Condtion_stmtContext.class,0);
		}
		public TerminalNode SCOL() { return getToken(SqlParser.SCOL, 0); }
		public Java_do_whileContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_java_do_while; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterJava_do_while(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitJava_do_while(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitJava_do_while(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Java_do_whileContext java_do_while() throws RecognitionException {
		Java_do_whileContext _localctx = new Java_do_whileContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_java_do_while);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(519);
			match(K_DO);
			setState(520);
			body_all_stmt();
			setState(521);
			match(K_WHILE);
			setState(522);
			condtion_stmt();
			setState(523);
			match(SCOL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Java_switch_stmtContext extends ParserRuleContext {
		public TerminalNode K_SWITCH() { return getToken(SqlParser.K_SWITCH, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SqlParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SqlParser.CLOSE_PAR, 0); }
		public TerminalNode IDENTIFIER() { return getToken(SqlParser.IDENTIFIER, 0); }
		public TerminalNode NUMERIC_LITERAL() { return getToken(SqlParser.NUMERIC_LITERAL, 0); }
		public Call_functionContext call_function() {
			return getRuleContext(Call_functionContext.class,0);
		}
		public List<Case_switchContext> case_switch() {
			return getRuleContexts(Case_switchContext.class);
		}
		public Case_switchContext case_switch(int i) {
			return getRuleContext(Case_switchContext.class,i);
		}
		public Default_switchContext default_switch() {
			return getRuleContext(Default_switchContext.class,0);
		}
		public Java_switch_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_java_switch_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterJava_switch_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitJava_switch_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitJava_switch_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Java_switch_stmtContext java_switch_stmt() throws RecognitionException {
		Java_switch_stmtContext _localctx = new Java_switch_stmtContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_java_switch_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(525);
			match(K_SWITCH);
			setState(526);
			match(OPEN_PAR);
			setState(530);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,49,_ctx) ) {
			case 1:
				{
				setState(527);
				match(IDENTIFIER);
				}
				break;
			case 2:
				{
				setState(528);
				match(NUMERIC_LITERAL);
				}
				break;
			case 3:
				{
				setState(529);
				call_function();
				}
				break;
			}
			setState(532);
			match(CLOSE_PAR);
			setState(533);
			match(T__3);
			setState(537);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==K_CASE) {
				{
				{
				setState(534);
				case_switch();
				}
				}
				setState(539);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(541);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_DEFAULT) {
				{
				setState(540);
				default_switch();
				}
			}

			setState(543);
			match(T__4);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Case_switchContext extends ParserRuleContext {
		public TerminalNode K_CASE() { return getToken(SqlParser.K_CASE, 0); }
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Signed_numberContext signed_number() {
			return getRuleContext(Signed_numberContext.class,0);
		}
		public TerminalNode K_BREAK() { return getToken(SqlParser.K_BREAK, 0); }
		public TerminalNode SCOL() { return getToken(SqlParser.SCOL, 0); }
		public List<Java_anystmtContext> java_anystmt() {
			return getRuleContexts(Java_anystmtContext.class);
		}
		public Java_anystmtContext java_anystmt(int i) {
			return getRuleContext(Java_anystmtContext.class,i);
		}
		public Case_switchContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_case_switch; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterCase_switch(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitCase_switch(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitCase_switch(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Case_switchContext case_switch() throws RecognitionException {
		Case_switchContext _localctx = new Case_switchContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_case_switch);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(545);
			match(K_CASE);
			setState(548);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OPEN_PAR:
			case IDENTIFIER:
			case STRING_LITERAL:
				{
				setState(546);
				any_name();
				}
				break;
			case STAR:
			case PLUS:
			case MINUS:
			case NUMERIC_LITERAL:
				{
				setState(547);
				signed_number();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(550);
			match(T__6);
			setState(569);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__2:
			case SCOL:
			case OPEN_PAR:
			case PLUS:
			case MINUS:
			case TILDE:
			case K_BREAK:
			case K_CONTINUE:
			case K_CREATE:
			case K_CURRENT_DATE:
			case K_CURRENT_TIME:
			case K_CURRENT_TIMESTAMP:
			case K_DO:
			case K_EXISTS:
			case K_FALSE:
			case K_FOR:
			case K_IF:
			case K_NOT:
			case K_NULL:
			case K_PRINT:
			case K_SELECT:
			case K_SWITCH:
			case K_TRUE:
			case K_VALUES:
			case K_VAR:
			case K_WHILE:
			case IDENTIFIER:
			case NUMERIC_LITERAL:
			case STRING_LITERAL:
			case BLOB_LITERAL:
				{
				{
				setState(554);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << SCOL) | (1L << OPEN_PAR) | (1L << PLUS) | (1L << MINUS) | (1L << TILDE) | (1L << K_CONTINUE) | (1L << K_CREATE) | (1L << K_CURRENT_DATE) | (1L << K_CURRENT_TIME))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (K_CURRENT_TIMESTAMP - 64)) | (1L << (K_DO - 64)) | (1L << (K_EXISTS - 64)) | (1L << (K_FALSE - 64)) | (1L << (K_FOR - 64)) | (1L << (K_IF - 64)) | (1L << (K_NOT - 64)) | (1L << (K_NULL - 64)))) != 0) || ((((_la - 130)) & ~0x3f) == 0 && ((1L << (_la - 130)) & ((1L << (K_PRINT - 130)) | (1L << (K_SELECT - 130)) | (1L << (K_SWITCH - 130)) | (1L << (K_TRUE - 130)) | (1L << (K_VALUES - 130)) | (1L << (K_VAR - 130)) | (1L << (K_WHILE - 130)) | (1L << (IDENTIFIER - 130)) | (1L << (NUMERIC_LITERAL - 130)) | (1L << (STRING_LITERAL - 130)) | (1L << (BLOB_LITERAL - 130)))) != 0)) {
					{
					{
					setState(551);
					java_anystmt();
					}
					}
					setState(556);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(557);
				match(K_BREAK);
				setState(558);
				match(SCOL);
				}
				}
				break;
			case T__3:
				{
				{
				setState(559);
				match(T__3);
				setState(563);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << SCOL) | (1L << OPEN_PAR) | (1L << PLUS) | (1L << MINUS) | (1L << TILDE) | (1L << K_CONTINUE) | (1L << K_CREATE) | (1L << K_CURRENT_DATE) | (1L << K_CURRENT_TIME))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (K_CURRENT_TIMESTAMP - 64)) | (1L << (K_DO - 64)) | (1L << (K_EXISTS - 64)) | (1L << (K_FALSE - 64)) | (1L << (K_FOR - 64)) | (1L << (K_IF - 64)) | (1L << (K_NOT - 64)) | (1L << (K_NULL - 64)))) != 0) || ((((_la - 130)) & ~0x3f) == 0 && ((1L << (_la - 130)) & ((1L << (K_PRINT - 130)) | (1L << (K_SELECT - 130)) | (1L << (K_SWITCH - 130)) | (1L << (K_TRUE - 130)) | (1L << (K_VALUES - 130)) | (1L << (K_VAR - 130)) | (1L << (K_WHILE - 130)) | (1L << (IDENTIFIER - 130)) | (1L << (NUMERIC_LITERAL - 130)) | (1L << (STRING_LITERAL - 130)) | (1L << (BLOB_LITERAL - 130)))) != 0)) {
					{
					{
					setState(560);
					java_anystmt();
					}
					}
					setState(565);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(566);
				match(K_BREAK);
				setState(567);
				match(SCOL);
				setState(568);
				match(T__4);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Default_switchContext extends ParserRuleContext {
		public TerminalNode K_DEFAULT() { return getToken(SqlParser.K_DEFAULT, 0); }
		public List<Java_anystmtContext> java_anystmt() {
			return getRuleContexts(Java_anystmtContext.class);
		}
		public Java_anystmtContext java_anystmt(int i) {
			return getRuleContext(Java_anystmtContext.class,i);
		}
		public Default_switchContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_default_switch; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterDefault_switch(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitDefault_switch(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitDefault_switch(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Default_switchContext default_switch() throws RecognitionException {
		Default_switchContext _localctx = new Default_switchContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_default_switch);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(571);
			match(K_DEFAULT);
			setState(572);
			match(T__6);
			setState(587);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__2:
			case T__4:
			case SCOL:
			case OPEN_PAR:
			case PLUS:
			case MINUS:
			case TILDE:
			case K_CONTINUE:
			case K_CREATE:
			case K_CURRENT_DATE:
			case K_CURRENT_TIME:
			case K_CURRENT_TIMESTAMP:
			case K_DO:
			case K_EXISTS:
			case K_FALSE:
			case K_FOR:
			case K_IF:
			case K_NOT:
			case K_NULL:
			case K_PRINT:
			case K_SELECT:
			case K_SWITCH:
			case K_TRUE:
			case K_VALUES:
			case K_VAR:
			case K_WHILE:
			case IDENTIFIER:
			case NUMERIC_LITERAL:
			case STRING_LITERAL:
			case BLOB_LITERAL:
				{
				setState(576);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << SCOL) | (1L << OPEN_PAR) | (1L << PLUS) | (1L << MINUS) | (1L << TILDE) | (1L << K_CONTINUE) | (1L << K_CREATE) | (1L << K_CURRENT_DATE) | (1L << K_CURRENT_TIME))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (K_CURRENT_TIMESTAMP - 64)) | (1L << (K_DO - 64)) | (1L << (K_EXISTS - 64)) | (1L << (K_FALSE - 64)) | (1L << (K_FOR - 64)) | (1L << (K_IF - 64)) | (1L << (K_NOT - 64)) | (1L << (K_NULL - 64)))) != 0) || ((((_la - 130)) & ~0x3f) == 0 && ((1L << (_la - 130)) & ((1L << (K_PRINT - 130)) | (1L << (K_SELECT - 130)) | (1L << (K_SWITCH - 130)) | (1L << (K_TRUE - 130)) | (1L << (K_VALUES - 130)) | (1L << (K_VAR - 130)) | (1L << (K_WHILE - 130)) | (1L << (IDENTIFIER - 130)) | (1L << (NUMERIC_LITERAL - 130)) | (1L << (STRING_LITERAL - 130)) | (1L << (BLOB_LITERAL - 130)))) != 0)) {
					{
					{
					setState(573);
					java_anystmt();
					}
					}
					setState(578);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case T__3:
				{
				{
				setState(579);
				match(T__3);
				setState(583);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << SCOL) | (1L << OPEN_PAR) | (1L << PLUS) | (1L << MINUS) | (1L << TILDE) | (1L << K_CONTINUE) | (1L << K_CREATE) | (1L << K_CURRENT_DATE) | (1L << K_CURRENT_TIME))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (K_CURRENT_TIMESTAMP - 64)) | (1L << (K_DO - 64)) | (1L << (K_EXISTS - 64)) | (1L << (K_FALSE - 64)) | (1L << (K_FOR - 64)) | (1L << (K_IF - 64)) | (1L << (K_NOT - 64)) | (1L << (K_NULL - 64)))) != 0) || ((((_la - 130)) & ~0x3f) == 0 && ((1L << (_la - 130)) & ((1L << (K_PRINT - 130)) | (1L << (K_SELECT - 130)) | (1L << (K_SWITCH - 130)) | (1L << (K_TRUE - 130)) | (1L << (K_VALUES - 130)) | (1L << (K_VAR - 130)) | (1L << (K_WHILE - 130)) | (1L << (IDENTIFIER - 130)) | (1L << (NUMERIC_LITERAL - 130)) | (1L << (STRING_LITERAL - 130)) | (1L << (BLOB_LITERAL - 130)))) != 0)) {
					{
					{
					setState(580);
					java_anystmt();
					}
					}
					setState(585);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(586);
				match(T__4);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Print_stmContext extends ParserRuleContext {
		public TerminalNode K_PRINT() { return getToken(SqlParser.K_PRINT, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SqlParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SqlParser.CLOSE_PAR, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> SCOL() { return getTokens(SqlParser.SCOL); }
		public TerminalNode SCOL(int i) {
			return getToken(SqlParser.SCOL, i);
		}
		public Print_stmContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_print_stm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterPrint_stm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitPrint_stm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitPrint_stm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Print_stmContext print_stm() throws RecognitionException {
		Print_stmContext _localctx = new Print_stmContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_print_stm);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(589);
			match(K_PRINT);
			setState(590);
			match(OPEN_PAR);
			setState(594);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << OPEN_PAR) | (1L << PLUS) | (1L << MINUS) | (1L << TILDE) | (1L << K_CURRENT_DATE) | (1L << K_CURRENT_TIME))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (K_CURRENT_TIMESTAMP - 64)) | (1L << (K_EXISTS - 64)) | (1L << (K_FALSE - 64)) | (1L << (K_NOT - 64)) | (1L << (K_NULL - 64)))) != 0) || ((((_la - 154)) & ~0x3f) == 0 && ((1L << (_la - 154)) & ((1L << (K_TRUE - 154)) | (1L << (IDENTIFIER - 154)) | (1L << (NUMERIC_LITERAL - 154)) | (1L << (STRING_LITERAL - 154)) | (1L << (BLOB_LITERAL - 154)))) != 0)) {
				{
				{
				setState(591);
				expr(0);
				}
				}
				setState(596);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(597);
			match(CLOSE_PAR);
			setState(599); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(598);
					match(SCOL);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(601); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,60,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Factored_select_stmtContext extends ParserRuleContext {
		public Select_coreContext select_core() {
			return getRuleContext(Select_coreContext.class,0);
		}
		public TerminalNode K_ORDER() { return getToken(SqlParser.K_ORDER, 0); }
		public TerminalNode K_BY() { return getToken(SqlParser.K_BY, 0); }
		public List<Ordering_termContext> ordering_term() {
			return getRuleContexts(Ordering_termContext.class);
		}
		public Ordering_termContext ordering_term(int i) {
			return getRuleContext(Ordering_termContext.class,i);
		}
		public TerminalNode K_LIMIT() { return getToken(SqlParser.K_LIMIT, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SqlParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SqlParser.COMMA, i);
		}
		public TerminalNode K_OFFSET() { return getToken(SqlParser.K_OFFSET, 0); }
		public Factored_select_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_factored_select_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterFactored_select_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitFactored_select_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitFactored_select_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Factored_select_stmtContext factored_select_stmt() throws RecognitionException {
		Factored_select_stmtContext _localctx = new Factored_select_stmtContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_factored_select_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(603);
			select_core();
			setState(614);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_ORDER) {
				{
				setState(604);
				match(K_ORDER);
				setState(605);
				match(K_BY);
				setState(606);
				ordering_term();
				setState(611);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(607);
					match(COMMA);
					setState(608);
					ordering_term();
					}
					}
					setState(613);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(622);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_LIMIT) {
				{
				setState(616);
				match(K_LIMIT);
				setState(617);
				expr(0);
				setState(620);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==COMMA || _la==K_OFFSET) {
					{
					setState(618);
					_la = _input.LA(1);
					if ( !(_la==COMMA || _la==K_OFFSET) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(619);
					expr(0);
					}
				}

				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Select_stmtContext extends ParserRuleContext {
		public Select_or_valuesContext select_or_values() {
			return getRuleContext(Select_or_valuesContext.class,0);
		}
		public TerminalNode K_ORDER() { return getToken(SqlParser.K_ORDER, 0); }
		public TerminalNode K_BY() { return getToken(SqlParser.K_BY, 0); }
		public List<Ordering_termContext> ordering_term() {
			return getRuleContexts(Ordering_termContext.class);
		}
		public Ordering_termContext ordering_term(int i) {
			return getRuleContext(Ordering_termContext.class,i);
		}
		public TerminalNode K_LIMIT() { return getToken(SqlParser.K_LIMIT, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SqlParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SqlParser.COMMA, i);
		}
		public TerminalNode K_OFFSET() { return getToken(SqlParser.K_OFFSET, 0); }
		public Select_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_select_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterSelect_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitSelect_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitSelect_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Select_stmtContext select_stmt() throws RecognitionException {
		Select_stmtContext _localctx = new Select_stmtContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_select_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(624);
			select_or_values();
			setState(635);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_ORDER) {
				{
				setState(625);
				match(K_ORDER);
				setState(626);
				match(K_BY);
				setState(627);
				ordering_term();
				setState(632);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(628);
					match(COMMA);
					setState(629);
					ordering_term();
					}
					}
					setState(634);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(643);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_LIMIT) {
				{
				setState(637);
				match(K_LIMIT);
				setState(638);
				expr(0);
				setState(641);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==COMMA || _la==K_OFFSET) {
					{
					setState(639);
					_la = _input.LA(1);
					if ( !(_la==COMMA || _la==K_OFFSET) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(640);
					expr(0);
					}
				}

				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Select_or_valuesContext extends ParserRuleContext {
		public TerminalNode K_SELECT() { return getToken(SqlParser.K_SELECT, 0); }
		public List<Result_columnContext> result_column() {
			return getRuleContexts(Result_columnContext.class);
		}
		public Result_columnContext result_column(int i) {
			return getRuleContext(Result_columnContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SqlParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SqlParser.COMMA, i);
		}
		public Sel_table_AMContext sel_table_AM() {
			return getRuleContext(Sel_table_AMContext.class,0);
		}
		public Sel_exp_where_AMContext sel_exp_where_AM() {
			return getRuleContext(Sel_exp_where_AMContext.class,0);
		}
		public Sel_exp_groupBy_AMContext sel_exp_groupBy_AM() {
			return getRuleContext(Sel_exp_groupBy_AMContext.class,0);
		}
		public TerminalNode K_DISTINCT() { return getToken(SqlParser.K_DISTINCT, 0); }
		public TerminalNode K_ALL() { return getToken(SqlParser.K_ALL, 0); }
		public Sel_exp_having_AMContext sel_exp_having_AM() {
			return getRuleContext(Sel_exp_having_AMContext.class,0);
		}
		public TerminalNode K_VALUES() { return getToken(SqlParser.K_VALUES, 0); }
		public List<TerminalNode> OPEN_PAR() { return getTokens(SqlParser.OPEN_PAR); }
		public TerminalNode OPEN_PAR(int i) {
			return getToken(SqlParser.OPEN_PAR, i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> CLOSE_PAR() { return getTokens(SqlParser.CLOSE_PAR); }
		public TerminalNode CLOSE_PAR(int i) {
			return getToken(SqlParser.CLOSE_PAR, i);
		}
		public Select_or_valuesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_select_or_values; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterSelect_or_values(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitSelect_or_values(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitSelect_or_values(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Select_or_valuesContext select_or_values() throws RecognitionException {
		Select_or_valuesContext _localctx = new Select_or_valuesContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_select_or_values);
		int _la;
		try {
			setState(697);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case K_SELECT:
				enterOuterAlt(_localctx, 1);
				{
				setState(645);
				match(K_SELECT);
				setState(647);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_ALL || _la==K_DISTINCT) {
					{
					setState(646);
					_la = _input.LA(1);
					if ( !(_la==K_ALL || _la==K_DISTINCT) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
				}

				setState(649);
				result_column();
				setState(654);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(650);
					match(COMMA);
					setState(651);
					result_column();
					}
					}
					setState(656);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(658);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_FROM) {
					{
					setState(657);
					sel_table_AM();
					}
				}

				setState(661);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_WHERE) {
					{
					setState(660);
					sel_exp_where_AM();
					}
				}

				setState(667);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_GROUP) {
					{
					setState(663);
					sel_exp_groupBy_AM();
					setState(665);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==K_HAVING) {
						{
						setState(664);
						sel_exp_having_AM();
						}
					}

					}
				}

				}
				break;
			case K_VALUES:
				enterOuterAlt(_localctx, 2);
				{
				setState(669);
				match(K_VALUES);
				setState(670);
				match(OPEN_PAR);
				setState(671);
				expr(0);
				setState(676);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(672);
					match(COMMA);
					setState(673);
					expr(0);
					}
					}
					setState(678);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(679);
				match(CLOSE_PAR);
				setState(694);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(680);
					match(COMMA);
					setState(681);
					match(OPEN_PAR);
					setState(682);
					expr(0);
					setState(687);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(683);
						match(COMMA);
						setState(684);
						expr(0);
						}
						}
						setState(689);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(690);
					match(CLOSE_PAR);
					}
					}
					setState(696);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sel_table_AMContext extends ParserRuleContext {
		public TerminalNode K_FROM() { return getToken(SqlParser.K_FROM, 0); }
		public List<Table_or_subqueryContext> table_or_subquery() {
			return getRuleContexts(Table_or_subqueryContext.class);
		}
		public Table_or_subqueryContext table_or_subquery(int i) {
			return getRuleContext(Table_or_subqueryContext.class,i);
		}
		public Join_clauseContext join_clause() {
			return getRuleContext(Join_clauseContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(SqlParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SqlParser.COMMA, i);
		}
		public Sel_table_AMContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sel_table_AM; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterSel_table_AM(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitSel_table_AM(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitSel_table_AM(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sel_table_AMContext sel_table_AM() throws RecognitionException {
		Sel_table_AMContext _localctx = new Sel_table_AMContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_sel_table_AM);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(699);
			match(K_FROM);
			setState(709);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,80,_ctx) ) {
			case 1:
				{
				setState(700);
				table_or_subquery();
				setState(705);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(701);
					match(COMMA);
					setState(702);
					table_or_subquery();
					}
					}
					setState(707);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case 2:
				{
				setState(708);
				join_clause();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sel_exp_where_AMContext extends ParserRuleContext {
		public TerminalNode K_WHERE() { return getToken(SqlParser.K_WHERE, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Sel_exp_where_AMContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sel_exp_where_AM; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterSel_exp_where_AM(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitSel_exp_where_AM(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitSel_exp_where_AM(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sel_exp_where_AMContext sel_exp_where_AM() throws RecognitionException {
		Sel_exp_where_AMContext _localctx = new Sel_exp_where_AMContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_sel_exp_where_AM);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(711);
			match(K_WHERE);
			setState(712);
			expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sel_exp_groupBy_AMContext extends ParserRuleContext {
		public TerminalNode K_GROUP() { return getToken(SqlParser.K_GROUP, 0); }
		public TerminalNode K_BY() { return getToken(SqlParser.K_BY, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SqlParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SqlParser.COMMA, i);
		}
		public Sel_exp_groupBy_AMContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sel_exp_groupBy_AM; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterSel_exp_groupBy_AM(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitSel_exp_groupBy_AM(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitSel_exp_groupBy_AM(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sel_exp_groupBy_AMContext sel_exp_groupBy_AM() throws RecognitionException {
		Sel_exp_groupBy_AMContext _localctx = new Sel_exp_groupBy_AMContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_sel_exp_groupBy_AM);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(714);
			match(K_GROUP);
			setState(715);
			match(K_BY);
			setState(716);
			expr(0);
			setState(721);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(717);
				match(COMMA);
				setState(718);
				expr(0);
				}
				}
				setState(723);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sel_exp_having_AMContext extends ParserRuleContext {
		public TerminalNode K_HAVING() { return getToken(SqlParser.K_HAVING, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Sel_exp_having_AMContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sel_exp_having_AM; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterSel_exp_having_AM(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitSel_exp_having_AM(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitSel_exp_having_AM(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sel_exp_having_AMContext sel_exp_having_AM() throws RecognitionException {
		Sel_exp_having_AMContext _localctx = new Sel_exp_having_AMContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_sel_exp_having_AM);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(724);
			match(K_HAVING);
			setState(725);
			expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_defContext extends ParserRuleContext {
		public Column_nameContext column_name() {
			return getRuleContext(Column_nameContext.class,0);
		}
		public List<Column_constraintContext> column_constraint() {
			return getRuleContexts(Column_constraintContext.class);
		}
		public Column_constraintContext column_constraint(int i) {
			return getRuleContext(Column_constraintContext.class,i);
		}
		public List<Type_nameContext> type_name() {
			return getRuleContexts(Type_nameContext.class);
		}
		public Type_nameContext type_name(int i) {
			return getRuleContext(Type_nameContext.class,i);
		}
		public Column_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterColumn_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitColumn_def(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitColumn_def(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_defContext column_def() throws RecognitionException {
		Column_defContext _localctx = new Column_defContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_column_def);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(727);
			column_name();
			setState(732);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 11)) & ~0x3f) == 0 && ((1L << (_la - 11)) & ((1L << (OPEN_PAR - 11)) | (1L << (K_CHECK - 11)) | (1L << (K_COLLATE - 11)) | (1L << (K_CONSTRAINT - 11)) | (1L << (K_DEFAULT - 11)))) != 0) || ((((_la - 117)) & ~0x3f) == 0 && ((1L << (_la - 117)) & ((1L << (K_NOT - 117)) | (1L << (K_NULL - 117)) | (1L << (K_PRIMARY - 117)) | (1L << (K_REFERENCES - 117)) | (1L << (K_UNIQUE - 117)) | (1L << (IDENTIFIER - 117)) | (1L << (STRING_LITERAL - 117)))) != 0)) {
				{
				setState(730);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case K_CHECK:
				case K_COLLATE:
				case K_CONSTRAINT:
				case K_DEFAULT:
				case K_NOT:
				case K_NULL:
				case K_PRIMARY:
				case K_REFERENCES:
				case K_UNIQUE:
					{
					setState(728);
					column_constraint();
					}
					break;
				case OPEN_PAR:
				case IDENTIFIER:
				case STRING_LITERAL:
					{
					setState(729);
					type_name();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(734);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_nameContext extends ParserRuleContext {
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SqlParser.OPEN_PAR, 0); }
		public List<Signed_numberContext> signed_number() {
			return getRuleContexts(Signed_numberContext.class);
		}
		public Signed_numberContext signed_number(int i) {
			return getRuleContext(Signed_numberContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SqlParser.CLOSE_PAR, 0); }
		public TerminalNode COMMA() { return getToken(SqlParser.COMMA, 0); }
		public List<Any_nameContext> any_name() {
			return getRuleContexts(Any_nameContext.class);
		}
		public Any_nameContext any_name(int i) {
			return getRuleContext(Any_nameContext.class,i);
		}
		public Type_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterType_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitType_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitType_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Type_nameContext type_name() throws RecognitionException {
		Type_nameContext _localctx = new Type_nameContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_type_name);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(735);
			name();
			setState(755);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,87,_ctx) ) {
			case 1:
				{
				setState(736);
				match(OPEN_PAR);
				setState(737);
				signed_number();
				setState(739);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==OPEN_PAR || _la==IDENTIFIER || _la==STRING_LITERAL) {
					{
					setState(738);
					any_name();
					}
				}

				setState(741);
				match(CLOSE_PAR);
				}
				break;
			case 2:
				{
				setState(743);
				match(OPEN_PAR);
				setState(744);
				signed_number();
				setState(746);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==OPEN_PAR || _la==IDENTIFIER || _la==STRING_LITERAL) {
					{
					setState(745);
					any_name();
					}
				}

				setState(748);
				match(COMMA);
				setState(749);
				signed_number();
				setState(751);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==OPEN_PAR || _la==IDENTIFIER || _la==STRING_LITERAL) {
					{
					setState(750);
					any_name();
					}
				}

				setState(753);
				match(CLOSE_PAR);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_constraintContext extends ParserRuleContext {
		public Column_constraint_primary_keyContext column_constraint_primary_key() {
			return getRuleContext(Column_constraint_primary_keyContext.class,0);
		}
		public Column_constraint_foreign_keyContext column_constraint_foreign_key() {
			return getRuleContext(Column_constraint_foreign_keyContext.class,0);
		}
		public Column_constraint_not_nullContext column_constraint_not_null() {
			return getRuleContext(Column_constraint_not_nullContext.class,0);
		}
		public Column_constraint_nullContext column_constraint_null() {
			return getRuleContext(Column_constraint_nullContext.class,0);
		}
		public TerminalNode K_UNIQUE() { return getToken(SqlParser.K_UNIQUE, 0); }
		public TerminalNode K_CHECK() { return getToken(SqlParser.K_CHECK, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SqlParser.OPEN_PAR, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SqlParser.CLOSE_PAR, 0); }
		public Column_defaultContext column_default() {
			return getRuleContext(Column_defaultContext.class,0);
		}
		public TerminalNode K_COLLATE() { return getToken(SqlParser.K_COLLATE, 0); }
		public Collation_nameContext collation_name() {
			return getRuleContext(Collation_nameContext.class,0);
		}
		public TerminalNode K_CONSTRAINT() { return getToken(SqlParser.K_CONSTRAINT, 0); }
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public Column_constraintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_constraint; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterColumn_constraint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitColumn_constraint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitColumn_constraint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_constraintContext column_constraint() throws RecognitionException {
		Column_constraintContext _localctx = new Column_constraintContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_column_constraint);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(759);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_CONSTRAINT) {
				{
				setState(757);
				match(K_CONSTRAINT);
				setState(758);
				name();
				}
			}

			setState(774);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case K_PRIMARY:
				{
				setState(761);
				column_constraint_primary_key();
				}
				break;
			case K_REFERENCES:
				{
				setState(762);
				column_constraint_foreign_key();
				}
				break;
			case K_NOT:
				{
				setState(763);
				column_constraint_not_null();
				}
				break;
			case K_NULL:
				{
				setState(764);
				column_constraint_null();
				}
				break;
			case K_UNIQUE:
				{
				setState(765);
				match(K_UNIQUE);
				}
				break;
			case K_CHECK:
				{
				setState(766);
				match(K_CHECK);
				setState(767);
				match(OPEN_PAR);
				setState(768);
				expr(0);
				setState(769);
				match(CLOSE_PAR);
				}
				break;
			case K_DEFAULT:
				{
				setState(771);
				column_default();
				}
				break;
			case K_COLLATE:
				{
				setState(772);
				match(K_COLLATE);
				setState(773);
				collation_name();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_constraint_primary_keyContext extends ParserRuleContext {
		public TerminalNode K_PRIMARY() { return getToken(SqlParser.K_PRIMARY, 0); }
		public TerminalNode K_KEY() { return getToken(SqlParser.K_KEY, 0); }
		public TerminalNode K_AUTOINCREMENT() { return getToken(SqlParser.K_AUTOINCREMENT, 0); }
		public TerminalNode K_ASC() { return getToken(SqlParser.K_ASC, 0); }
		public TerminalNode K_DESC() { return getToken(SqlParser.K_DESC, 0); }
		public Column_constraint_primary_keyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_constraint_primary_key; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterColumn_constraint_primary_key(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitColumn_constraint_primary_key(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitColumn_constraint_primary_key(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_constraint_primary_keyContext column_constraint_primary_key() throws RecognitionException {
		Column_constraint_primary_keyContext _localctx = new Column_constraint_primary_keyContext(_ctx, getState());
		enterRule(_localctx, 94, RULE_column_constraint_primary_key);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(776);
			match(K_PRIMARY);
			setState(777);
			match(K_KEY);
			setState(779);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_ASC || _la==K_DESC) {
				{
				setState(778);
				_la = _input.LA(1);
				if ( !(_la==K_ASC || _la==K_DESC) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			setState(782);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_AUTOINCREMENT) {
				{
				setState(781);
				match(K_AUTOINCREMENT);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_constraint_foreign_keyContext extends ParserRuleContext {
		public Foreign_key_clauseContext foreign_key_clause() {
			return getRuleContext(Foreign_key_clauseContext.class,0);
		}
		public Column_constraint_foreign_keyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_constraint_foreign_key; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterColumn_constraint_foreign_key(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitColumn_constraint_foreign_key(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitColumn_constraint_foreign_key(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_constraint_foreign_keyContext column_constraint_foreign_key() throws RecognitionException {
		Column_constraint_foreign_keyContext _localctx = new Column_constraint_foreign_keyContext(_ctx, getState());
		enterRule(_localctx, 96, RULE_column_constraint_foreign_key);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(784);
			foreign_key_clause();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_constraint_not_nullContext extends ParserRuleContext {
		public TerminalNode K_NOT() { return getToken(SqlParser.K_NOT, 0); }
		public TerminalNode K_NULL() { return getToken(SqlParser.K_NULL, 0); }
		public Column_constraint_not_nullContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_constraint_not_null; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterColumn_constraint_not_null(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitColumn_constraint_not_null(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitColumn_constraint_not_null(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_constraint_not_nullContext column_constraint_not_null() throws RecognitionException {
		Column_constraint_not_nullContext _localctx = new Column_constraint_not_nullContext(_ctx, getState());
		enterRule(_localctx, 98, RULE_column_constraint_not_null);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(786);
			match(K_NOT);
			setState(787);
			match(K_NULL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_constraint_nullContext extends ParserRuleContext {
		public TerminalNode K_NULL() { return getToken(SqlParser.K_NULL, 0); }
		public Column_constraint_nullContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_constraint_null; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterColumn_constraint_null(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitColumn_constraint_null(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitColumn_constraint_null(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_constraint_nullContext column_constraint_null() throws RecognitionException {
		Column_constraint_nullContext _localctx = new Column_constraint_nullContext(_ctx, getState());
		enterRule(_localctx, 100, RULE_column_constraint_null);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(789);
			match(K_NULL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_defaultContext extends ParserRuleContext {
		public TerminalNode K_DEFAULT() { return getToken(SqlParser.K_DEFAULT, 0); }
		public Column_default_valueContext column_default_value() {
			return getRuleContext(Column_default_valueContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SqlParser.OPEN_PAR, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SqlParser.CLOSE_PAR, 0); }
		public TerminalNode K_NEXTVAL() { return getToken(SqlParser.K_NEXTVAL, 0); }
		public List<Any_nameContext> any_name() {
			return getRuleContexts(Any_nameContext.class);
		}
		public Any_nameContext any_name(int i) {
			return getRuleContext(Any_nameContext.class,i);
		}
		public Column_defaultContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_default; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterColumn_default(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitColumn_default(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitColumn_default(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_defaultContext column_default() throws RecognitionException {
		Column_defaultContext _localctx = new Column_defaultContext(_ctx, getState());
		enterRule(_localctx, 102, RULE_column_default);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(791);
			match(K_DEFAULT);
			setState(803);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,92,_ctx) ) {
			case 1:
				{
				setState(792);
				column_default_value();
				}
				break;
			case 2:
				{
				setState(793);
				match(OPEN_PAR);
				setState(794);
				expr(0);
				setState(795);
				match(CLOSE_PAR);
				}
				break;
			case 3:
				{
				setState(797);
				match(K_NEXTVAL);
				setState(798);
				match(OPEN_PAR);
				setState(799);
				expr(0);
				setState(800);
				match(CLOSE_PAR);
				}
				break;
			case 4:
				{
				setState(802);
				any_name();
				}
				break;
			}
			setState(811);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__7) {
				{
				setState(805);
				match(T__7);
				setState(807); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(806);
						any_name();
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(809); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,93,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_default_valueContext extends ParserRuleContext {
		public Signed_numberContext signed_number() {
			return getRuleContext(Signed_numberContext.class,0);
		}
		public Literal_valueContext literal_value() {
			return getRuleContext(Literal_valueContext.class,0);
		}
		public Column_default_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_default_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterColumn_default_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitColumn_default_value(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitColumn_default_value(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_default_valueContext column_default_value() throws RecognitionException {
		Column_default_valueContext _localctx = new Column_default_valueContext(_ctx, getState());
		enterRule(_localctx, 104, RULE_column_default_value);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(815);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,95,_ctx) ) {
			case 1:
				{
				setState(813);
				signed_number();
				}
				break;
			case 2:
				{
				setState(814);
				literal_value();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public Literal_valueContext literal_value() {
			return getRuleContext(Literal_valueContext.class,0);
		}
		public Database_expContext database_exp() {
			return getRuleContext(Database_expContext.class,0);
		}
		public Operator_expContext operator_exp() {
			return getRuleContext(Operator_expContext.class,0);
		}
		public Function_expContext function_exp() {
			return getRuleContext(Function_expContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SqlParser.OPEN_PAR, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SqlParser.CLOSE_PAR, 0); }
		public Sel_exprContext sel_expr() {
			return getRuleContext(Sel_exprContext.class,0);
		}
		public TerminalNode K_TRUE() { return getToken(SqlParser.K_TRUE, 0); }
		public TerminalNode K_FALSE() { return getToken(SqlParser.K_FALSE, 0); }
		public TerminalNode PIPE2() { return getToken(SqlParser.PIPE2, 0); }
		public TerminalNode STAR() { return getToken(SqlParser.STAR, 0); }
		public TerminalNode DIV() { return getToken(SqlParser.DIV, 0); }
		public TerminalNode MOD() { return getToken(SqlParser.MOD, 0); }
		public TerminalNode PLUS() { return getToken(SqlParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(SqlParser.MINUS, 0); }
		public TerminalNode LT2() { return getToken(SqlParser.LT2, 0); }
		public TerminalNode GT2() { return getToken(SqlParser.GT2, 0); }
		public TerminalNode AMP() { return getToken(SqlParser.AMP, 0); }
		public TerminalNode PIPE() { return getToken(SqlParser.PIPE, 0); }
		public TerminalNode LT() { return getToken(SqlParser.LT, 0); }
		public TerminalNode LT_EQ() { return getToken(SqlParser.LT_EQ, 0); }
		public TerminalNode GT() { return getToken(SqlParser.GT, 0); }
		public TerminalNode GT_EQ() { return getToken(SqlParser.GT_EQ, 0); }
		public TerminalNode ASSIGN() { return getToken(SqlParser.ASSIGN, 0); }
		public TerminalNode EQ() { return getToken(SqlParser.EQ, 0); }
		public TerminalNode NOT_EQ1() { return getToken(SqlParser.NOT_EQ1, 0); }
		public TerminalNode NOT_EQ2() { return getToken(SqlParser.NOT_EQ2, 0); }
		public TerminalNode K_IS() { return getToken(SqlParser.K_IS, 0); }
		public TerminalNode K_NOT() { return getToken(SqlParser.K_NOT, 0); }
		public TerminalNode K_IN() { return getToken(SqlParser.K_IN, 0); }
		public TerminalNode K_LIKE() { return getToken(SqlParser.K_LIKE, 0); }
		public TerminalNode K_GLOB() { return getToken(SqlParser.K_GLOB, 0); }
		public TerminalNode K_MATCH() { return getToken(SqlParser.K_MATCH, 0); }
		public TerminalNode K_REGEXP() { return getToken(SqlParser.K_REGEXP, 0); }
		public TerminalNode K_AND() { return getToken(SqlParser.K_AND, 0); }
		public TerminalNode K_OR() { return getToken(SqlParser.K_OR, 0); }
		public Second_sel_allContext second_sel_all() {
			return getRuleContext(Second_sel_allContext.class,0);
		}
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		return expr(0);
	}

	private ExprContext expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExprContext _localctx = new ExprContext(_ctx, _parentState);
		ExprContext _prevctx = _localctx;
		int _startState = 106;
		enterRecursionRule(_localctx, 106, RULE_expr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(829);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,96,_ctx) ) {
			case 1:
				{
				setState(818);
				literal_value();
				}
				break;
			case 2:
				{
				setState(819);
				database_exp();
				}
				break;
			case 3:
				{
				setState(820);
				operator_exp();
				}
				break;
			case 4:
				{
				setState(821);
				function_exp();
				}
				break;
			case 5:
				{
				setState(822);
				match(OPEN_PAR);
				setState(823);
				expr(0);
				setState(824);
				match(CLOSE_PAR);
				}
				break;
			case 6:
				{
				setState(826);
				sel_expr();
				}
				break;
			case 7:
				{
				setState(827);
				match(K_TRUE);
				}
				break;
			case 8:
				{
				setState(828);
				match(K_FALSE);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(876);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,100,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(874);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,99,_ctx) ) {
					case 1:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(831);
						if (!(precpred(_ctx, 14))) throw new FailedPredicateException(this, "precpred(_ctx, 14)");
						setState(832);
						match(PIPE2);
						setState(833);
						expr(15);
						}
						break;
					case 2:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(834);
						if (!(precpred(_ctx, 13))) throw new FailedPredicateException(this, "precpred(_ctx, 13)");
						setState(835);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << STAR) | (1L << DIV) | (1L << MOD))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(836);
						expr(14);
						}
						break;
					case 3:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(837);
						if (!(precpred(_ctx, 12))) throw new FailedPredicateException(this, "precpred(_ctx, 12)");
						setState(838);
						_la = _input.LA(1);
						if ( !(_la==PLUS || _la==MINUS) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(839);
						expr(13);
						}
						break;
					case 4:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(840);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(841);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LT2) | (1L << GT2) | (1L << AMP) | (1L << PIPE))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(842);
						expr(12);
						}
						break;
					case 5:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(843);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(844);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LT) | (1L << LT_EQ) | (1L << GT) | (1L << GT_EQ))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(845);
						expr(11);
						}
						break;
					case 6:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(846);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(859);
						_errHandler.sync(this);
						switch ( getInterpreter().adaptivePredict(_input,97,_ctx) ) {
						case 1:
							{
							setState(847);
							match(ASSIGN);
							}
							break;
						case 2:
							{
							setState(848);
							match(EQ);
							}
							break;
						case 3:
							{
							setState(849);
							match(NOT_EQ1);
							}
							break;
						case 4:
							{
							setState(850);
							match(NOT_EQ2);
							}
							break;
						case 5:
							{
							setState(851);
							match(K_IS);
							}
							break;
						case 6:
							{
							setState(852);
							match(K_IS);
							setState(853);
							match(K_NOT);
							}
							break;
						case 7:
							{
							setState(854);
							match(K_IN);
							}
							break;
						case 8:
							{
							setState(855);
							match(K_LIKE);
							}
							break;
						case 9:
							{
							setState(856);
							match(K_GLOB);
							}
							break;
						case 10:
							{
							setState(857);
							match(K_MATCH);
							}
							break;
						case 11:
							{
							setState(858);
							match(K_REGEXP);
							}
							break;
						}
						setState(861);
						expr(10);
						}
						break;
					case 7:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(862);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(863);
						match(K_AND);
						setState(864);
						expr(9);
						}
						break;
					case 8:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(865);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(866);
						match(K_OR);
						setState(867);
						expr(8);
						}
						break;
					case 9:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(868);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(870);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==K_NOT) {
							{
							setState(869);
							match(K_NOT);
							}
						}

						setState(872);
						match(K_IN);
						setState(873);
						second_sel_all();
						}
						break;
					}
					} 
				}
				setState(878);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,100,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Database_expContext extends ParserRuleContext {
		public Column_nameContext column_name() {
			return getRuleContext(Column_nameContext.class,0);
		}
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public List<TerminalNode> DOT() { return getTokens(SqlParser.DOT); }
		public TerminalNode DOT(int i) {
			return getToken(SqlParser.DOT, i);
		}
		public Database_nameContext database_name() {
			return getRuleContext(Database_nameContext.class,0);
		}
		public Database_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_database_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterDatabase_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitDatabase_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitDatabase_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Database_expContext database_exp() throws RecognitionException {
		Database_expContext _localctx = new Database_expContext(_ctx, getState());
		enterRule(_localctx, 108, RULE_database_exp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(887);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,102,_ctx) ) {
			case 1:
				{
				setState(882);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,101,_ctx) ) {
				case 1:
					{
					setState(879);
					database_name();
					setState(880);
					match(DOT);
					}
					break;
				}
				setState(884);
				table_name();
				setState(885);
				match(DOT);
				}
				break;
			}
			setState(889);
			column_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Operator_expContext extends ParserRuleContext {
		public Unary_operatorContext unary_operator() {
			return getRuleContext(Unary_operatorContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Operator_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operator_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterOperator_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitOperator_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitOperator_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Operator_expContext operator_exp() throws RecognitionException {
		Operator_expContext _localctx = new Operator_expContext(_ctx, getState());
		enterRule(_localctx, 110, RULE_operator_exp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(891);
			unary_operator();
			setState(892);
			expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_expContext extends ParserRuleContext {
		public Function_nameContext function_name() {
			return getRuleContext(Function_nameContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SqlParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SqlParser.CLOSE_PAR, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode STAR() { return getToken(SqlParser.STAR, 0); }
		public TerminalNode K_DISTINCT() { return getToken(SqlParser.K_DISTINCT, 0); }
		public List<TerminalNode> COMMA() { return getTokens(SqlParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SqlParser.COMMA, i);
		}
		public Function_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterFunction_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitFunction_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitFunction_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Function_expContext function_exp() throws RecognitionException {
		Function_expContext _localctx = new Function_expContext(_ctx, getState());
		enterRule(_localctx, 112, RULE_function_exp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(894);
			function_name();
			setState(895);
			match(OPEN_PAR);
			setState(908);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OPEN_PAR:
			case PLUS:
			case MINUS:
			case TILDE:
			case K_CURRENT_DATE:
			case K_CURRENT_TIME:
			case K_CURRENT_TIMESTAMP:
			case K_DISTINCT:
			case K_EXISTS:
			case K_FALSE:
			case K_NOT:
			case K_NULL:
			case K_TRUE:
			case IDENTIFIER:
			case NUMERIC_LITERAL:
			case STRING_LITERAL:
			case BLOB_LITERAL:
				{
				setState(897);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_DISTINCT) {
					{
					setState(896);
					match(K_DISTINCT);
					}
				}

				setState(899);
				expr(0);
				setState(904);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(900);
					match(COMMA);
					setState(901);
					expr(0);
					}
					}
					setState(906);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case STAR:
				{
				setState(907);
				match(STAR);
				}
				break;
			case CLOSE_PAR:
				break;
			default:
				break;
			}
			setState(910);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Second_sel_allContext extends ParserRuleContext {
		public TerminalNode OPEN_PAR() { return getToken(SqlParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SqlParser.CLOSE_PAR, 0); }
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public Select_stmtContext select_stmt() {
			return getRuleContext(Select_stmtContext.class,0);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public Database_nameContext database_name() {
			return getRuleContext(Database_nameContext.class,0);
		}
		public TerminalNode DOT() { return getToken(SqlParser.DOT, 0); }
		public List<TerminalNode> COMMA() { return getTokens(SqlParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SqlParser.COMMA, i);
		}
		public Second_sel_allContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_second_sel_all; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterSecond_sel_all(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitSecond_sel_all(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitSecond_sel_all(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Second_sel_allContext second_sel_all() throws RecognitionException {
		Second_sel_allContext _localctx = new Second_sel_allContext(_ctx, getState());
		enterRule(_localctx, 114, RULE_second_sel_all);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(931);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,109,_ctx) ) {
			case 1:
				{
				setState(912);
				match(OPEN_PAR);
				setState(922);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case K_SELECT:
				case K_VALUES:
					{
					setState(913);
					select_stmt();
					}
					break;
				case OPEN_PAR:
				case PLUS:
				case MINUS:
				case TILDE:
				case K_CURRENT_DATE:
				case K_CURRENT_TIME:
				case K_CURRENT_TIMESTAMP:
				case K_EXISTS:
				case K_FALSE:
				case K_NOT:
				case K_NULL:
				case K_TRUE:
				case IDENTIFIER:
				case NUMERIC_LITERAL:
				case STRING_LITERAL:
				case BLOB_LITERAL:
					{
					setState(914);
					expr(0);
					setState(919);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(915);
						match(COMMA);
						setState(916);
						expr(0);
						}
						}
						setState(921);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
					break;
				case CLOSE_PAR:
					break;
				default:
					break;
				}
				setState(924);
				match(CLOSE_PAR);
				}
				break;
			case 2:
				{
				setState(928);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,108,_ctx) ) {
				case 1:
					{
					setState(925);
					database_name();
					setState(926);
					match(DOT);
					}
					break;
				}
				setState(930);
				table_name();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sel_exprContext extends ParserRuleContext {
		public TerminalNode OPEN_PAR() { return getToken(SqlParser.OPEN_PAR, 0); }
		public Select_stmtContext select_stmt() {
			return getRuleContext(Select_stmtContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SqlParser.CLOSE_PAR, 0); }
		public TerminalNode K_EXISTS() { return getToken(SqlParser.K_EXISTS, 0); }
		public TerminalNode K_NOT() { return getToken(SqlParser.K_NOT, 0); }
		public Sel_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sel_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterSel_expr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitSel_expr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitSel_expr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sel_exprContext sel_expr() throws RecognitionException {
		Sel_exprContext _localctx = new Sel_exprContext(_ctx, getState());
		enterRule(_localctx, 116, RULE_sel_expr);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(937);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_EXISTS || _la==K_NOT) {
				{
				setState(934);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_NOT) {
					{
					setState(933);
					match(K_NOT);
					}
				}

				setState(936);
				match(K_EXISTS);
				}
			}

			setState(939);
			match(OPEN_PAR);
			setState(940);
			select_stmt();
			setState(941);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Foreign_key_clauseContext extends ParserRuleContext {
		public TerminalNode K_REFERENCES() { return getToken(SqlParser.K_REFERENCES, 0); }
		public Foreign_tableContext foreign_table() {
			return getRuleContext(Foreign_tableContext.class,0);
		}
		public Database_nameContext database_name() {
			return getRuleContext(Database_nameContext.class,0);
		}
		public TerminalNode DOT() { return getToken(SqlParser.DOT, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SqlParser.OPEN_PAR, 0); }
		public List<Fk_target_column_nameContext> fk_target_column_name() {
			return getRuleContexts(Fk_target_column_nameContext.class);
		}
		public Fk_target_column_nameContext fk_target_column_name(int i) {
			return getRuleContext(Fk_target_column_nameContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SqlParser.CLOSE_PAR, 0); }
		public TerminalNode K_DEFERRABLE() { return getToken(SqlParser.K_DEFERRABLE, 0); }
		public List<TerminalNode> K_ON() { return getTokens(SqlParser.K_ON); }
		public TerminalNode K_ON(int i) {
			return getToken(SqlParser.K_ON, i);
		}
		public List<TerminalNode> K_MATCH() { return getTokens(SqlParser.K_MATCH); }
		public TerminalNode K_MATCH(int i) {
			return getToken(SqlParser.K_MATCH, i);
		}
		public List<NameContext> name() {
			return getRuleContexts(NameContext.class);
		}
		public NameContext name(int i) {
			return getRuleContext(NameContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SqlParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SqlParser.COMMA, i);
		}
		public List<TerminalNode> K_DELETE() { return getTokens(SqlParser.K_DELETE); }
		public TerminalNode K_DELETE(int i) {
			return getToken(SqlParser.K_DELETE, i);
		}
		public List<TerminalNode> K_UPDATE() { return getTokens(SqlParser.K_UPDATE); }
		public TerminalNode K_UPDATE(int i) {
			return getToken(SqlParser.K_UPDATE, i);
		}
		public TerminalNode K_NOT() { return getToken(SqlParser.K_NOT, 0); }
		public TerminalNode K_INITIALLY() { return getToken(SqlParser.K_INITIALLY, 0); }
		public TerminalNode K_DEFERRED() { return getToken(SqlParser.K_DEFERRED, 0); }
		public TerminalNode K_IMMEDIATE() { return getToken(SqlParser.K_IMMEDIATE, 0); }
		public TerminalNode K_ENABLE() { return getToken(SqlParser.K_ENABLE, 0); }
		public List<TerminalNode> K_SET() { return getTokens(SqlParser.K_SET); }
		public TerminalNode K_SET(int i) {
			return getToken(SqlParser.K_SET, i);
		}
		public List<TerminalNode> K_NULL() { return getTokens(SqlParser.K_NULL); }
		public TerminalNode K_NULL(int i) {
			return getToken(SqlParser.K_NULL, i);
		}
		public List<TerminalNode> K_DEFAULT() { return getTokens(SqlParser.K_DEFAULT); }
		public TerminalNode K_DEFAULT(int i) {
			return getToken(SqlParser.K_DEFAULT, i);
		}
		public List<TerminalNode> K_CASCADE() { return getTokens(SqlParser.K_CASCADE); }
		public TerminalNode K_CASCADE(int i) {
			return getToken(SqlParser.K_CASCADE, i);
		}
		public List<TerminalNode> K_RESTRICT() { return getTokens(SqlParser.K_RESTRICT); }
		public TerminalNode K_RESTRICT(int i) {
			return getToken(SqlParser.K_RESTRICT, i);
		}
		public List<TerminalNode> K_NO() { return getTokens(SqlParser.K_NO); }
		public TerminalNode K_NO(int i) {
			return getToken(SqlParser.K_NO, i);
		}
		public List<TerminalNode> K_ACTION() { return getTokens(SqlParser.K_ACTION); }
		public TerminalNode K_ACTION(int i) {
			return getToken(SqlParser.K_ACTION, i);
		}
		public Foreign_key_clauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_foreign_key_clause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterForeign_key_clause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitForeign_key_clause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitForeign_key_clause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Foreign_key_clauseContext foreign_key_clause() throws RecognitionException {
		Foreign_key_clauseContext _localctx = new Foreign_key_clauseContext(_ctx, getState());
		enterRule(_localctx, 118, RULE_foreign_key_clause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(943);
			match(K_REFERENCES);
			setState(947);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,112,_ctx) ) {
			case 1:
				{
				setState(944);
				database_name();
				setState(945);
				match(DOT);
				}
				break;
			}
			setState(949);
			foreign_table();
			setState(961);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,114,_ctx) ) {
			case 1:
				{
				setState(950);
				match(OPEN_PAR);
				setState(951);
				fk_target_column_name();
				setState(956);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(952);
					match(COMMA);
					setState(953);
					fk_target_column_name();
					}
					}
					setState(958);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(959);
				match(CLOSE_PAR);
				}
				break;
			}
			setState(981);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==K_MATCH || _la==K_ON) {
				{
				{
				setState(977);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case K_ON:
					{
					setState(963);
					match(K_ON);
					setState(964);
					_la = _input.LA(1);
					if ( !(_la==K_DELETE || _la==K_UPDATE) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(973);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,115,_ctx) ) {
					case 1:
						{
						setState(965);
						match(K_SET);
						setState(966);
						match(K_NULL);
						}
						break;
					case 2:
						{
						setState(967);
						match(K_SET);
						setState(968);
						match(K_DEFAULT);
						}
						break;
					case 3:
						{
						setState(969);
						match(K_CASCADE);
						}
						break;
					case 4:
						{
						setState(970);
						match(K_RESTRICT);
						}
						break;
					case 5:
						{
						setState(971);
						match(K_NO);
						setState(972);
						match(K_ACTION);
						}
						break;
					}
					}
					break;
				case K_MATCH:
					{
					setState(975);
					match(K_MATCH);
					setState(976);
					name();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				}
				setState(983);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(997);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,121,_ctx) ) {
			case 1:
				{
				setState(985);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_NOT) {
					{
					setState(984);
					match(K_NOT);
					}
				}

				setState(987);
				match(K_DEFERRABLE);
				setState(992);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,119,_ctx) ) {
				case 1:
					{
					setState(988);
					match(K_INITIALLY);
					setState(989);
					match(K_DEFERRED);
					}
					break;
				case 2:
					{
					setState(990);
					match(K_INITIALLY);
					setState(991);
					match(K_IMMEDIATE);
					}
					break;
				}
				setState(995);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_ENABLE) {
					{
					setState(994);
					match(K_ENABLE);
					}
				}

				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Fk_target_column_nameContext extends ParserRuleContext {
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public Fk_target_column_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fk_target_column_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterFk_target_column_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitFk_target_column_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitFk_target_column_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Fk_target_column_nameContext fk_target_column_name() throws RecognitionException {
		Fk_target_column_nameContext _localctx = new Fk_target_column_nameContext(_ctx, getState());
		enterRule(_localctx, 120, RULE_fk_target_column_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(999);
			name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Indexed_columnContext extends ParserRuleContext {
		public Column_nameContext column_name() {
			return getRuleContext(Column_nameContext.class,0);
		}
		public TerminalNode K_COLLATE() { return getToken(SqlParser.K_COLLATE, 0); }
		public Collation_nameContext collation_name() {
			return getRuleContext(Collation_nameContext.class,0);
		}
		public TerminalNode K_ASC() { return getToken(SqlParser.K_ASC, 0); }
		public TerminalNode K_DESC() { return getToken(SqlParser.K_DESC, 0); }
		public Indexed_columnContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_indexed_column; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterIndexed_column(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitIndexed_column(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitIndexed_column(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Indexed_columnContext indexed_column() throws RecognitionException {
		Indexed_columnContext _localctx = new Indexed_columnContext(_ctx, getState());
		enterRule(_localctx, 122, RULE_indexed_column);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1001);
			column_name();
			setState(1004);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_COLLATE) {
				{
				setState(1002);
				match(K_COLLATE);
				setState(1003);
				collation_name();
				}
			}

			setState(1007);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_ASC || _la==K_DESC) {
				{
				setState(1006);
				_la = _input.LA(1);
				if ( !(_la==K_ASC || _la==K_DESC) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_constraintContext extends ParserRuleContext {
		public Table_constraint_primary_keyContext table_constraint_primary_key() {
			return getRuleContext(Table_constraint_primary_keyContext.class,0);
		}
		public Table_constraint_keyContext table_constraint_key() {
			return getRuleContext(Table_constraint_keyContext.class,0);
		}
		public Table_constraint_uniqueContext table_constraint_unique() {
			return getRuleContext(Table_constraint_uniqueContext.class,0);
		}
		public TerminalNode K_CHECK() { return getToken(SqlParser.K_CHECK, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SqlParser.OPEN_PAR, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SqlParser.CLOSE_PAR, 0); }
		public Table_constraint_foreign_keyContext table_constraint_foreign_key() {
			return getRuleContext(Table_constraint_foreign_keyContext.class,0);
		}
		public TerminalNode K_CONSTRAINT() { return getToken(SqlParser.K_CONSTRAINT, 0); }
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public Table_constraintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_constraint; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterTable_constraint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitTable_constraint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitTable_constraint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_constraintContext table_constraint() throws RecognitionException {
		Table_constraintContext _localctx = new Table_constraintContext(_ctx, getState());
		enterRule(_localctx, 124, RULE_table_constraint);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1011);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_CONSTRAINT) {
				{
				setState(1009);
				match(K_CONSTRAINT);
				setState(1010);
				name();
				}
			}

			setState(1022);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case K_PRIMARY:
				{
				setState(1013);
				table_constraint_primary_key();
				}
				break;
			case K_KEY:
				{
				setState(1014);
				table_constraint_key();
				}
				break;
			case K_UNIQUE:
				{
				setState(1015);
				table_constraint_unique();
				}
				break;
			case K_CHECK:
				{
				setState(1016);
				match(K_CHECK);
				setState(1017);
				match(OPEN_PAR);
				setState(1018);
				expr(0);
				setState(1019);
				match(CLOSE_PAR);
				}
				break;
			case K_FOREIGN:
				{
				setState(1021);
				table_constraint_foreign_key();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_constraint_primary_keyContext extends ParserRuleContext {
		public TerminalNode K_PRIMARY() { return getToken(SqlParser.K_PRIMARY, 0); }
		public TerminalNode K_KEY() { return getToken(SqlParser.K_KEY, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SqlParser.OPEN_PAR, 0); }
		public List<Indexed_columnContext> indexed_column() {
			return getRuleContexts(Indexed_columnContext.class);
		}
		public Indexed_columnContext indexed_column(int i) {
			return getRuleContext(Indexed_columnContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SqlParser.CLOSE_PAR, 0); }
		public List<TerminalNode> COMMA() { return getTokens(SqlParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SqlParser.COMMA, i);
		}
		public Table_constraint_primary_keyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_constraint_primary_key; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterTable_constraint_primary_key(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitTable_constraint_primary_key(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitTable_constraint_primary_key(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_constraint_primary_keyContext table_constraint_primary_key() throws RecognitionException {
		Table_constraint_primary_keyContext _localctx = new Table_constraint_primary_keyContext(_ctx, getState());
		enterRule(_localctx, 126, RULE_table_constraint_primary_key);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1024);
			match(K_PRIMARY);
			setState(1025);
			match(K_KEY);
			setState(1026);
			match(OPEN_PAR);
			setState(1027);
			indexed_column();
			setState(1032);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(1028);
				match(COMMA);
				setState(1029);
				indexed_column();
				}
				}
				setState(1034);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1035);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_constraint_foreign_keyContext extends ParserRuleContext {
		public TerminalNode K_FOREIGN() { return getToken(SqlParser.K_FOREIGN, 0); }
		public TerminalNode K_KEY() { return getToken(SqlParser.K_KEY, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SqlParser.OPEN_PAR, 0); }
		public List<Fk_origin_column_nameContext> fk_origin_column_name() {
			return getRuleContexts(Fk_origin_column_nameContext.class);
		}
		public Fk_origin_column_nameContext fk_origin_column_name(int i) {
			return getRuleContext(Fk_origin_column_nameContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SqlParser.CLOSE_PAR, 0); }
		public Foreign_key_clauseContext foreign_key_clause() {
			return getRuleContext(Foreign_key_clauseContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(SqlParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SqlParser.COMMA, i);
		}
		public Table_constraint_foreign_keyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_constraint_foreign_key; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterTable_constraint_foreign_key(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitTable_constraint_foreign_key(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitTable_constraint_foreign_key(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_constraint_foreign_keyContext table_constraint_foreign_key() throws RecognitionException {
		Table_constraint_foreign_keyContext _localctx = new Table_constraint_foreign_keyContext(_ctx, getState());
		enterRule(_localctx, 128, RULE_table_constraint_foreign_key);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1037);
			match(K_FOREIGN);
			setState(1038);
			match(K_KEY);
			setState(1039);
			match(OPEN_PAR);
			setState(1040);
			fk_origin_column_name();
			setState(1045);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(1041);
				match(COMMA);
				setState(1042);
				fk_origin_column_name();
				}
				}
				setState(1047);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1048);
			match(CLOSE_PAR);
			setState(1049);
			foreign_key_clause();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_constraint_uniqueContext extends ParserRuleContext {
		public TerminalNode K_UNIQUE() { return getToken(SqlParser.K_UNIQUE, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SqlParser.OPEN_PAR, 0); }
		public List<Indexed_columnContext> indexed_column() {
			return getRuleContexts(Indexed_columnContext.class);
		}
		public Indexed_columnContext indexed_column(int i) {
			return getRuleContext(Indexed_columnContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SqlParser.CLOSE_PAR, 0); }
		public TerminalNode K_KEY() { return getToken(SqlParser.K_KEY, 0); }
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(SqlParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SqlParser.COMMA, i);
		}
		public Table_constraint_uniqueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_constraint_unique; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterTable_constraint_unique(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitTable_constraint_unique(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitTable_constraint_unique(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_constraint_uniqueContext table_constraint_unique() throws RecognitionException {
		Table_constraint_uniqueContext _localctx = new Table_constraint_uniqueContext(_ctx, getState());
		enterRule(_localctx, 130, RULE_table_constraint_unique);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1051);
			match(K_UNIQUE);
			setState(1053);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_KEY) {
				{
				setState(1052);
				match(K_KEY);
				}
			}

			setState(1056);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,129,_ctx) ) {
			case 1:
				{
				setState(1055);
				name();
				}
				break;
			}
			setState(1058);
			match(OPEN_PAR);
			setState(1059);
			indexed_column();
			setState(1064);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(1060);
				match(COMMA);
				setState(1061);
				indexed_column();
				}
				}
				setState(1066);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1067);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_constraint_keyContext extends ParserRuleContext {
		public TerminalNode K_KEY() { return getToken(SqlParser.K_KEY, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SqlParser.OPEN_PAR, 0); }
		public List<Indexed_columnContext> indexed_column() {
			return getRuleContexts(Indexed_columnContext.class);
		}
		public Indexed_columnContext indexed_column(int i) {
			return getRuleContext(Indexed_columnContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SqlParser.CLOSE_PAR, 0); }
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(SqlParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SqlParser.COMMA, i);
		}
		public Table_constraint_keyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_constraint_key; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterTable_constraint_key(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitTable_constraint_key(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitTable_constraint_key(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_constraint_keyContext table_constraint_key() throws RecognitionException {
		Table_constraint_keyContext _localctx = new Table_constraint_keyContext(_ctx, getState());
		enterRule(_localctx, 132, RULE_table_constraint_key);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1069);
			match(K_KEY);
			setState(1071);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,131,_ctx) ) {
			case 1:
				{
				setState(1070);
				name();
				}
				break;
			}
			setState(1073);
			match(OPEN_PAR);
			setState(1074);
			indexed_column();
			setState(1079);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(1075);
				match(COMMA);
				setState(1076);
				indexed_column();
				}
				}
				setState(1081);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1082);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Fk_origin_column_nameContext extends ParserRuleContext {
		public Column_nameContext column_name() {
			return getRuleContext(Column_nameContext.class,0);
		}
		public Fk_origin_column_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fk_origin_column_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterFk_origin_column_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitFk_origin_column_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitFk_origin_column_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Fk_origin_column_nameContext fk_origin_column_name() throws RecognitionException {
		Fk_origin_column_nameContext _localctx = new Fk_origin_column_nameContext(_ctx, getState());
		enterRule(_localctx, 134, RULE_fk_origin_column_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1084);
			column_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Ordering_termContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode K_ASC() { return getToken(SqlParser.K_ASC, 0); }
		public TerminalNode K_DESC() { return getToken(SqlParser.K_DESC, 0); }
		public Ordering_termContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ordering_term; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterOrdering_term(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitOrdering_term(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitOrdering_term(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Ordering_termContext ordering_term() throws RecognitionException {
		Ordering_termContext _localctx = new Ordering_termContext(_ctx, getState());
		enterRule(_localctx, 136, RULE_ordering_term);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1086);
			expr(0);
			setState(1088);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_ASC || _la==K_DESC) {
				{
				setState(1087);
				_la = _input.LA(1);
				if ( !(_la==K_ASC || _la==K_DESC) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Result_columnContext extends ParserRuleContext {
		public TerminalNode STAR() { return getToken(SqlParser.STAR, 0); }
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public TerminalNode DOT() { return getToken(SqlParser.DOT, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Column_aliasContext column_alias() {
			return getRuleContext(Column_aliasContext.class,0);
		}
		public TerminalNode K_AS() { return getToken(SqlParser.K_AS, 0); }
		public Result_columnContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_result_column; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterResult_column(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitResult_column(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitResult_column(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Result_columnContext result_column() throws RecognitionException {
		Result_columnContext _localctx = new Result_columnContext(_ctx, getState());
		enterRule(_localctx, 138, RULE_result_column);
		int _la;
		try {
			setState(1102);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,136,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1090);
				match(STAR);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1091);
				table_name();
				setState(1092);
				match(DOT);
				setState(1093);
				match(STAR);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1095);
				expr(0);
				setState(1100);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,135,_ctx) ) {
				case 1:
					{
					setState(1097);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==K_AS) {
						{
						setState(1096);
						match(K_AS);
						}
					}

					setState(1099);
					column_alias();
					}
					break;
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_or_subqueryContext extends ParserRuleContext {
		public Table_or_sub_oneContext table_or_sub_one() {
			return getRuleContext(Table_or_sub_oneContext.class,0);
		}
		public Table_or_sub_twoContext table_or_sub_two() {
			return getRuleContext(Table_or_sub_twoContext.class,0);
		}
		public Table_or_sub_threeContext table_or_sub_three() {
			return getRuleContext(Table_or_sub_threeContext.class,0);
		}
		public Table_or_subqueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_or_subquery; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterTable_or_subquery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitTable_or_subquery(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitTable_or_subquery(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_or_subqueryContext table_or_subquery() throws RecognitionException {
		Table_or_subqueryContext _localctx = new Table_or_subqueryContext(_ctx, getState());
		enterRule(_localctx, 140, RULE_table_or_subquery);
		try {
			setState(1107);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,137,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1104);
				table_or_sub_one();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1105);
				table_or_sub_two();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1106);
				table_or_sub_three();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_or_sub_oneContext extends ParserRuleContext {
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public Database_nameContext database_name() {
			return getRuleContext(Database_nameContext.class,0);
		}
		public TerminalNode DOT() { return getToken(SqlParser.DOT, 0); }
		public Table_aliasContext table_alias() {
			return getRuleContext(Table_aliasContext.class,0);
		}
		public TerminalNode K_INDEXED() { return getToken(SqlParser.K_INDEXED, 0); }
		public TerminalNode K_BY() { return getToken(SqlParser.K_BY, 0); }
		public Index_nameContext index_name() {
			return getRuleContext(Index_nameContext.class,0);
		}
		public TerminalNode K_NOT() { return getToken(SqlParser.K_NOT, 0); }
		public TerminalNode K_AS() { return getToken(SqlParser.K_AS, 0); }
		public Table_or_sub_oneContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_or_sub_one; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterTable_or_sub_one(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitTable_or_sub_one(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitTable_or_sub_one(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_or_sub_oneContext table_or_sub_one() throws RecognitionException {
		Table_or_sub_oneContext _localctx = new Table_or_sub_oneContext(_ctx, getState());
		enterRule(_localctx, 142, RULE_table_or_sub_one);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1112);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,138,_ctx) ) {
			case 1:
				{
				setState(1109);
				database_name();
				setState(1110);
				match(DOT);
				}
				break;
			}
			setState(1114);
			table_name();
			setState(1119);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,140,_ctx) ) {
			case 1:
				{
				setState(1116);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_AS) {
					{
					setState(1115);
					match(K_AS);
					}
				}

				setState(1118);
				table_alias();
				}
				break;
			}
			setState(1126);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,141,_ctx) ) {
			case 1:
				{
				setState(1121);
				match(K_INDEXED);
				setState(1122);
				match(K_BY);
				setState(1123);
				index_name();
				}
				break;
			case 2:
				{
				setState(1124);
				match(K_NOT);
				setState(1125);
				match(K_INDEXED);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_or_sub_twoContext extends ParserRuleContext {
		public TerminalNode OPEN_PAR() { return getToken(SqlParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SqlParser.CLOSE_PAR, 0); }
		public List<Table_or_subqueryContext> table_or_subquery() {
			return getRuleContexts(Table_or_subqueryContext.class);
		}
		public Table_or_subqueryContext table_or_subquery(int i) {
			return getRuleContext(Table_or_subqueryContext.class,i);
		}
		public Join_clauseContext join_clause() {
			return getRuleContext(Join_clauseContext.class,0);
		}
		public Table_aliasContext table_alias() {
			return getRuleContext(Table_aliasContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(SqlParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SqlParser.COMMA, i);
		}
		public TerminalNode K_AS() { return getToken(SqlParser.K_AS, 0); }
		public Table_or_sub_twoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_or_sub_two; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterTable_or_sub_two(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitTable_or_sub_two(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitTable_or_sub_two(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_or_sub_twoContext table_or_sub_two() throws RecognitionException {
		Table_or_sub_twoContext _localctx = new Table_or_sub_twoContext(_ctx, getState());
		enterRule(_localctx, 144, RULE_table_or_sub_two);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1128);
			match(OPEN_PAR);
			setState(1138);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,143,_ctx) ) {
			case 1:
				{
				setState(1129);
				table_or_subquery();
				setState(1134);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(1130);
					match(COMMA);
					setState(1131);
					table_or_subquery();
					}
					}
					setState(1136);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case 2:
				{
				setState(1137);
				join_clause();
				}
				break;
			}
			setState(1140);
			match(CLOSE_PAR);
			setState(1145);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,145,_ctx) ) {
			case 1:
				{
				setState(1142);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_AS) {
					{
					setState(1141);
					match(K_AS);
					}
				}

				setState(1144);
				table_alias();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_or_sub_threeContext extends ParserRuleContext {
		public TerminalNode OPEN_PAR() { return getToken(SqlParser.OPEN_PAR, 0); }
		public Select_stmtContext select_stmt() {
			return getRuleContext(Select_stmtContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SqlParser.CLOSE_PAR, 0); }
		public Table_aliasContext table_alias() {
			return getRuleContext(Table_aliasContext.class,0);
		}
		public TerminalNode K_AS() { return getToken(SqlParser.K_AS, 0); }
		public Table_or_sub_threeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_or_sub_three; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterTable_or_sub_three(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitTable_or_sub_three(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitTable_or_sub_three(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_or_sub_threeContext table_or_sub_three() throws RecognitionException {
		Table_or_sub_threeContext _localctx = new Table_or_sub_threeContext(_ctx, getState());
		enterRule(_localctx, 146, RULE_table_or_sub_three);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1147);
			match(OPEN_PAR);
			setState(1148);
			select_stmt();
			setState(1149);
			match(CLOSE_PAR);
			setState(1154);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,147,_ctx) ) {
			case 1:
				{
				setState(1151);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_AS) {
					{
					setState(1150);
					match(K_AS);
					}
				}

				setState(1153);
				table_alias();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Join_clauseContext extends ParserRuleContext {
		public List<Table_or_subqueryContext> table_or_subquery() {
			return getRuleContexts(Table_or_subqueryContext.class);
		}
		public Table_or_subqueryContext table_or_subquery(int i) {
			return getRuleContext(Table_or_subqueryContext.class,i);
		}
		public List<Join_operatorContext> join_operator() {
			return getRuleContexts(Join_operatorContext.class);
		}
		public Join_operatorContext join_operator(int i) {
			return getRuleContext(Join_operatorContext.class,i);
		}
		public List<Join_constraintContext> join_constraint() {
			return getRuleContexts(Join_constraintContext.class);
		}
		public Join_constraintContext join_constraint(int i) {
			return getRuleContext(Join_constraintContext.class,i);
		}
		public Join_clauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_join_clause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterJoin_clause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitJoin_clause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitJoin_clause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Join_clauseContext join_clause() throws RecognitionException {
		Join_clauseContext _localctx = new Join_clauseContext(_ctx, getState());
		enterRule(_localctx, 148, RULE_join_clause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1156);
			table_or_subquery();
			setState(1163);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA || ((((_la - 100)) & ~0x3f) == 0 && ((1L << (_la - 100)) & ((1L << (K_INNER - 100)) | (1L << (K_JOIN - 100)) | (1L << (K_LEFT - 100)))) != 0)) {
				{
				{
				setState(1157);
				join_operator();
				setState(1158);
				table_or_subquery();
				setState(1159);
				join_constraint();
				}
				}
				setState(1165);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Join_operatorContext extends ParserRuleContext {
		public TerminalNode COMMA() { return getToken(SqlParser.COMMA, 0); }
		public TerminalNode K_JOIN() { return getToken(SqlParser.K_JOIN, 0); }
		public TerminalNode K_LEFT() { return getToken(SqlParser.K_LEFT, 0); }
		public TerminalNode K_INNER() { return getToken(SqlParser.K_INNER, 0); }
		public TerminalNode K_OUTER() { return getToken(SqlParser.K_OUTER, 0); }
		public Join_operatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_join_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterJoin_operator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitJoin_operator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitJoin_operator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Join_operatorContext join_operator() throws RecognitionException {
		Join_operatorContext _localctx = new Join_operatorContext(_ctx, getState());
		enterRule(_localctx, 150, RULE_join_operator);
		int _la;
		try {
			setState(1175);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case COMMA:
				enterOuterAlt(_localctx, 1);
				{
				setState(1166);
				match(COMMA);
				}
				break;
			case K_INNER:
			case K_JOIN:
			case K_LEFT:
				enterOuterAlt(_localctx, 2);
				{
				setState(1172);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case K_LEFT:
					{
					setState(1167);
					match(K_LEFT);
					setState(1169);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==K_OUTER) {
						{
						setState(1168);
						match(K_OUTER);
						}
					}

					}
					break;
				case K_INNER:
					{
					setState(1171);
					match(K_INNER);
					}
					break;
				case K_JOIN:
					break;
				default:
					break;
				}
				setState(1174);
				match(K_JOIN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Join_constraintContext extends ParserRuleContext {
		public TerminalNode K_ON() { return getToken(SqlParser.K_ON, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Join_constraintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_join_constraint; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterJoin_constraint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitJoin_constraint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitJoin_constraint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Join_constraintContext join_constraint() throws RecognitionException {
		Join_constraintContext _localctx = new Join_constraintContext(_ctx, getState());
		enterRule(_localctx, 152, RULE_join_constraint);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1179);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_ON) {
				{
				setState(1177);
				match(K_ON);
				setState(1178);
				expr(0);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Select_coreContext extends ParserRuleContext {
		public TerminalNode K_SELECT() { return getToken(SqlParser.K_SELECT, 0); }
		public List<Result_columnContext> result_column() {
			return getRuleContexts(Result_columnContext.class);
		}
		public Result_columnContext result_column(int i) {
			return getRuleContext(Result_columnContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SqlParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SqlParser.COMMA, i);
		}
		public Sel_table_AMContext sel_table_AM() {
			return getRuleContext(Sel_table_AMContext.class,0);
		}
		public Sel_exp_where_AMContext sel_exp_where_AM() {
			return getRuleContext(Sel_exp_where_AMContext.class,0);
		}
		public Sel_exp_groupBy_AMContext sel_exp_groupBy_AM() {
			return getRuleContext(Sel_exp_groupBy_AMContext.class,0);
		}
		public TerminalNode K_DISTINCT() { return getToken(SqlParser.K_DISTINCT, 0); }
		public TerminalNode K_ALL() { return getToken(SqlParser.K_ALL, 0); }
		public Sel_exp_having_AMContext sel_exp_having_AM() {
			return getRuleContext(Sel_exp_having_AMContext.class,0);
		}
		public TerminalNode K_VALUES() { return getToken(SqlParser.K_VALUES, 0); }
		public List<TerminalNode> OPEN_PAR() { return getTokens(SqlParser.OPEN_PAR); }
		public TerminalNode OPEN_PAR(int i) {
			return getToken(SqlParser.OPEN_PAR, i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> CLOSE_PAR() { return getTokens(SqlParser.CLOSE_PAR); }
		public TerminalNode CLOSE_PAR(int i) {
			return getToken(SqlParser.CLOSE_PAR, i);
		}
		public Select_coreContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_select_core; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterSelect_core(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitSelect_core(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitSelect_core(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Select_coreContext select_core() throws RecognitionException {
		Select_coreContext _localctx = new Select_coreContext(_ctx, getState());
		enterRule(_localctx, 154, RULE_select_core);
		int _la;
		try {
			setState(1233);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case K_SELECT:
				enterOuterAlt(_localctx, 1);
				{
				setState(1181);
				match(K_SELECT);
				setState(1183);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_ALL || _la==K_DISTINCT) {
					{
					setState(1182);
					_la = _input.LA(1);
					if ( !(_la==K_ALL || _la==K_DISTINCT) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
				}

				setState(1185);
				result_column();
				setState(1190);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(1186);
					match(COMMA);
					setState(1187);
					result_column();
					}
					}
					setState(1192);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(1194);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_FROM) {
					{
					setState(1193);
					sel_table_AM();
					}
				}

				setState(1197);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_WHERE) {
					{
					setState(1196);
					sel_exp_where_AM();
					}
				}

				setState(1203);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_GROUP) {
					{
					setState(1199);
					sel_exp_groupBy_AM();
					setState(1201);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==K_HAVING) {
						{
						setState(1200);
						sel_exp_having_AM();
						}
					}

					}
				}

				}
				break;
			case K_VALUES:
				enterOuterAlt(_localctx, 2);
				{
				setState(1205);
				match(K_VALUES);
				setState(1206);
				match(OPEN_PAR);
				setState(1207);
				expr(0);
				setState(1212);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(1208);
					match(COMMA);
					setState(1209);
					expr(0);
					}
					}
					setState(1214);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(1215);
				match(CLOSE_PAR);
				setState(1230);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(1216);
					match(COMMA);
					setState(1217);
					match(OPEN_PAR);
					setState(1218);
					expr(0);
					setState(1223);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(1219);
						match(COMMA);
						setState(1220);
						expr(0);
						}
						}
						setState(1225);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(1226);
					match(CLOSE_PAR);
					}
					}
					setState(1232);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Signed_numberContext extends ParserRuleContext {
		public TerminalNode NUMERIC_LITERAL() { return getToken(SqlParser.NUMERIC_LITERAL, 0); }
		public TerminalNode STAR() { return getToken(SqlParser.STAR, 0); }
		public TerminalNode PLUS() { return getToken(SqlParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(SqlParser.MINUS, 0); }
		public Signed_numberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_signed_number; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterSigned_number(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitSigned_number(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitSigned_number(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Signed_numberContext signed_number() throws RecognitionException {
		Signed_numberContext _localctx = new Signed_numberContext(_ctx, getState());
		enterRule(_localctx, 156, RULE_signed_number);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1240);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case PLUS:
			case MINUS:
			case NUMERIC_LITERAL:
				{
				setState(1236);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==PLUS || _la==MINUS) {
					{
					setState(1235);
					_la = _input.LA(1);
					if ( !(_la==PLUS || _la==MINUS) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
				}

				setState(1238);
				match(NUMERIC_LITERAL);
				}
				break;
			case STAR:
				{
				setState(1239);
				match(STAR);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Create_table_stmtContext extends ParserRuleContext {
		public TerminalNode K_CREATE() { return getToken(SqlParser.K_CREATE, 0); }
		public TerminalNode K_TABLE() { return getToken(SqlParser.K_TABLE, 0); }
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SqlParser.OPEN_PAR, 0); }
		public List<Column_defContext> column_def() {
			return getRuleContexts(Column_defContext.class);
		}
		public Column_defContext column_def(int i) {
			return getRuleContext(Column_defContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SqlParser.CLOSE_PAR, 0); }
		public TerminalNode SCOL() { return getToken(SqlParser.SCOL, 0); }
		public List<TerminalNode> COMMA() { return getTokens(SqlParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SqlParser.COMMA, i);
		}
		public TerminalNode K_TYPE() { return getToken(SqlParser.K_TYPE, 0); }
		public List<TerminalNode> ASSIGN() { return getTokens(SqlParser.ASSIGN); }
		public TerminalNode ASSIGN(int i) {
			return getToken(SqlParser.ASSIGN, i);
		}
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode K_PATH() { return getToken(SqlParser.K_PATH, 0); }
		public PathContext path() {
			return getRuleContext(PathContext.class,0);
		}
		public Create_table_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_create_table_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterCreate_table_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitCreate_table_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitCreate_table_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Create_table_stmtContext create_table_stmt() throws RecognitionException {
		Create_table_stmtContext _localctx = new Create_table_stmtContext(_ctx, getState());
		enterRule(_localctx, 158, RULE_create_table_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1242);
			match(K_CREATE);
			setState(1243);
			match(K_TABLE);
			setState(1244);
			table_name();
			setState(1245);
			match(OPEN_PAR);
			setState(1246);
			column_def();
			setState(1251);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(1247);
				match(COMMA);
				setState(1248);
				column_def();
				}
				}
				setState(1253);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1261);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_TYPE) {
				{
				setState(1254);
				match(K_TYPE);
				setState(1255);
				match(ASSIGN);
				setState(1256);
				type();
				setState(1257);
				match(K_PATH);
				setState(1258);
				match(ASSIGN);
				setState(1259);
				path();
				}
			}

			setState(1263);
			match(CLOSE_PAR);
			setState(1264);
			match(SCOL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(SqlParser.IDENTIFIER, 0); }
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 160, RULE_type);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1266);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PathContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(SqlParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(SqlParser.IDENTIFIER, i);
		}
		public List<TerminalNode> DIV() { return getTokens(SqlParser.DIV); }
		public TerminalNode DIV(int i) {
			return getToken(SqlParser.DIV, i);
		}
		public PathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_path; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterPath(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitPath(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitPath(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PathContext path() throws RecognitionException {
		PathContext _localctx = new PathContext(_ctx, getState());
		enterRule(_localctx, 162, RULE_path);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1268);
			match(IDENTIFIER);
			setState(1273);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==DIV) {
				{
				{
				setState(1269);
				match(DIV);
				setState(1270);
				match(IDENTIFIER);
				}
				}
				setState(1275);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Name_typeContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Name_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_name_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterName_type(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitName_type(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitName_type(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Name_typeContext name_type() throws RecognitionException {
		Name_typeContext _localctx = new Name_typeContext(_ctx, getState());
		enterRule(_localctx, 164, RULE_name_type);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1276);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Create_type_stmtContext extends ParserRuleContext {
		public TerminalNode K_CREATE() { return getToken(SqlParser.K_CREATE, 0); }
		public TerminalNode K_TYPE() { return getToken(SqlParser.K_TYPE, 0); }
		public Name_typeContext name_type() {
			return getRuleContext(Name_typeContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SqlParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SqlParser.CLOSE_PAR, 0); }
		public TerminalNode SCOL() { return getToken(SqlParser.SCOL, 0); }
		public List<Column_nameContext> column_name() {
			return getRuleContexts(Column_nameContext.class);
		}
		public Column_nameContext column_name(int i) {
			return getRuleContext(Column_nameContext.class,i);
		}
		public List<Any_nameContext> any_name() {
			return getRuleContexts(Any_nameContext.class);
		}
		public Any_nameContext any_name(int i) {
			return getRuleContext(Any_nameContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SqlParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SqlParser.COMMA, i);
		}
		public Create_type_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_create_type_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterCreate_type_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitCreate_type_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitCreate_type_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Create_type_stmtContext create_type_stmt() throws RecognitionException {
		Create_type_stmtContext _localctx = new Create_type_stmtContext(_ctx, getState());
		enterRule(_localctx, 166, RULE_create_type_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1278);
			match(K_CREATE);
			setState(1279);
			match(K_TYPE);
			setState(1280);
			name_type();
			setState(1281);
			match(OPEN_PAR);
			{
			setState(1293);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==OPEN_PAR || _la==IDENTIFIER || _la==STRING_LITERAL) {
				{
				setState(1282);
				column_name();
				setState(1283);
				any_name();
				setState(1290);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(1284);
					match(COMMA);
					setState(1285);
					column_name();
					setState(1286);
					any_name();
					}
					}
					setState(1292);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			}
			setState(1295);
			match(CLOSE_PAR);
			setState(1296);
			match(SCOL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class JarPathContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public JarPathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_jarPath; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterJarPath(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitJarPath(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitJarPath(this);
			else return visitor.visitChildren(this);
		}
	}

	public final JarPathContext jarPath() throws RecognitionException {
		JarPathContext _localctx = new JarPathContext(_ctx, getState());
		enterRule(_localctx, 168, RULE_jarPath);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1298);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassNameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public ClassNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_className; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterClassName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitClassName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitClassName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ClassNameContext className() throws RecognitionException {
		ClassNameContext _localctx = new ClassNameContext(_ctx, getState());
		enterRule(_localctx, 170, RULE_className);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1300);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodNameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public MethodNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterMethodName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitMethodName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitMethodName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MethodNameContext methodName() throws RecognitionException {
		MethodNameContext _localctx = new MethodNameContext(_ctx, getState());
		enterRule(_localctx, 172, RULE_methodName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1302);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReturnTypeContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public ReturnTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_returnType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterReturnType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitReturnType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitReturnType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ReturnTypeContext returnType() throws RecognitionException {
		ReturnTypeContext _localctx = new ReturnTypeContext(_ctx, getState());
		enterRule(_localctx, 174, RULE_returnType);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1304);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Parameter_AggFunctionContext extends ParserRuleContext {
		public Paramete_arrayContext paramete_array() {
			return getRuleContext(Paramete_arrayContext.class,0);
		}
		public Parameter_AggFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameter_AggFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterParameter_AggFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitParameter_AggFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitParameter_AggFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Parameter_AggFunctionContext parameter_AggFunction() throws RecognitionException {
		Parameter_AggFunctionContext _localctx = new Parameter_AggFunctionContext(_ctx, getState());
		enterRule(_localctx, 176, RULE_parameter_AggFunction);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1306);
			paramete_array();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AggregationFunctionContext extends ParserRuleContext {
		public TerminalNode K_CREATE() { return getToken(SqlParser.K_CREATE, 0); }
		public TerminalNode K_AGGREGATION_FUNCTION() { return getToken(SqlParser.K_AGGREGATION_FUNCTION, 0); }
		public Function_nameContext function_name() {
			return getRuleContext(Function_nameContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SqlParser.OPEN_PAR, 0); }
		public JarPathContext jarPath() {
			return getRuleContext(JarPathContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(SqlParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SqlParser.COMMA, i);
		}
		public ClassNameContext className() {
			return getRuleContext(ClassNameContext.class,0);
		}
		public MethodNameContext methodName() {
			return getRuleContext(MethodNameContext.class,0);
		}
		public ReturnTypeContext returnType() {
			return getRuleContext(ReturnTypeContext.class,0);
		}
		public Parameter_AggFunctionContext parameter_AggFunction() {
			return getRuleContext(Parameter_AggFunctionContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SqlParser.CLOSE_PAR, 0); }
		public AggregationFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aggregationFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterAggregationFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitAggregationFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitAggregationFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AggregationFunctionContext aggregationFunction() throws RecognitionException {
		AggregationFunctionContext _localctx = new AggregationFunctionContext(_ctx, getState());
		enterRule(_localctx, 178, RULE_aggregationFunction);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1308);
			match(K_CREATE);
			setState(1309);
			match(K_AGGREGATION_FUNCTION);
			setState(1310);
			function_name();
			setState(1311);
			match(OPEN_PAR);
			setState(1312);
			jarPath();
			setState(1313);
			match(COMMA);
			setState(1314);
			className();
			setState(1315);
			match(COMMA);
			setState(1316);
			methodName();
			setState(1317);
			match(COMMA);
			setState(1318);
			returnType();
			setState(1319);
			match(COMMA);
			setState(1320);
			parameter_AggFunction();
			setState(1321);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Literal_valueContext extends ParserRuleContext {
		public TerminalNode NUMERIC_LITERAL() { return getToken(SqlParser.NUMERIC_LITERAL, 0); }
		public TerminalNode STRING_LITERAL() { return getToken(SqlParser.STRING_LITERAL, 0); }
		public TerminalNode BLOB_LITERAL() { return getToken(SqlParser.BLOB_LITERAL, 0); }
		public TerminalNode K_NULL() { return getToken(SqlParser.K_NULL, 0); }
		public TerminalNode K_CURRENT_TIME() { return getToken(SqlParser.K_CURRENT_TIME, 0); }
		public TerminalNode K_CURRENT_DATE() { return getToken(SqlParser.K_CURRENT_DATE, 0); }
		public TerminalNode K_CURRENT_TIMESTAMP() { return getToken(SqlParser.K_CURRENT_TIMESTAMP, 0); }
		public Literal_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literal_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterLiteral_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitLiteral_value(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitLiteral_value(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Literal_valueContext literal_value() throws RecognitionException {
		Literal_valueContext _localctx = new Literal_valueContext(_ctx, getState());
		enterRule(_localctx, 180, RULE_literal_value);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1323);
			_la = _input.LA(1);
			if ( !(((((_la - 62)) & ~0x3f) == 0 && ((1L << (_la - 62)) & ((1L << (K_CURRENT_DATE - 62)) | (1L << (K_CURRENT_TIME - 62)) | (1L << (K_CURRENT_TIMESTAMP - 62)) | (1L << (K_NULL - 62)))) != 0) || ((((_la - 175)) & ~0x3f) == 0 && ((1L << (_la - 175)) & ((1L << (NUMERIC_LITERAL - 175)) | (1L << (STRING_LITERAL - 175)) | (1L << (BLOB_LITERAL - 175)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Unary_operatorContext extends ParserRuleContext {
		public TerminalNode MINUS() { return getToken(SqlParser.MINUS, 0); }
		public TerminalNode PLUS() { return getToken(SqlParser.PLUS, 0); }
		public TerminalNode TILDE() { return getToken(SqlParser.TILDE, 0); }
		public TerminalNode K_NOT() { return getToken(SqlParser.K_NOT, 0); }
		public Unary_operatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unary_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterUnary_operator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitUnary_operator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitUnary_operator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Unary_operatorContext unary_operator() throws RecognitionException {
		Unary_operatorContext _localctx = new Unary_operatorContext(_ctx, getState());
		enterRule(_localctx, 182, RULE_unary_operator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1325);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PLUS) | (1L << MINUS) | (1L << TILDE))) != 0) || _la==K_NOT) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OprationContext extends ParserRuleContext {
		public TerminalNode GT() { return getToken(SqlParser.GT, 0); }
		public TerminalNode LT() { return getToken(SqlParser.LT, 0); }
		public TerminalNode ASSIGN() { return getToken(SqlParser.ASSIGN, 0); }
		public OprationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_opration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterOpration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitOpration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitOpration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OprationContext opration() throws RecognitionException {
		OprationContext _localctx = new OprationContext(_ctx, getState());
		enterRule(_localctx, 184, RULE_opration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1327);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ASSIGN) | (1L << LT) | (1L << GT))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Module_argumentContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Column_defContext column_def() {
			return getRuleContext(Column_defContext.class,0);
		}
		public Module_argumentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_module_argument; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterModule_argument(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitModule_argument(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitModule_argument(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Module_argumentContext module_argument() throws RecognitionException {
		Module_argumentContext _localctx = new Module_argumentContext(_ctx, getState());
		enterRule(_localctx, 186, RULE_module_argument);
		try {
			setState(1331);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,170,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1329);
				expr(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1330);
				column_def();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_aliasContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(SqlParser.IDENTIFIER, 0); }
		public TerminalNode STRING_LITERAL() { return getToken(SqlParser.STRING_LITERAL, 0); }
		public Column_aliasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_alias; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterColumn_alias(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitColumn_alias(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitColumn_alias(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_aliasContext column_alias() throws RecognitionException {
		Column_aliasContext _localctx = new Column_aliasContext(_ctx, getState());
		enterRule(_localctx, 188, RULE_column_alias);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1333);
			_la = _input.LA(1);
			if ( !(_la==IDENTIFIER || _la==STRING_LITERAL) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KeywordContext extends ParserRuleContext {
		public TerminalNode K_ABORT() { return getToken(SqlParser.K_ABORT, 0); }
		public TerminalNode K_ACTION() { return getToken(SqlParser.K_ACTION, 0); }
		public TerminalNode K_ADD() { return getToken(SqlParser.K_ADD, 0); }
		public TerminalNode K_AFTER() { return getToken(SqlParser.K_AFTER, 0); }
		public TerminalNode K_ALL() { return getToken(SqlParser.K_ALL, 0); }
		public TerminalNode K_ALTER() { return getToken(SqlParser.K_ALTER, 0); }
		public TerminalNode K_ANALYZE() { return getToken(SqlParser.K_ANALYZE, 0); }
		public TerminalNode K_AND() { return getToken(SqlParser.K_AND, 0); }
		public TerminalNode K_AS() { return getToken(SqlParser.K_AS, 0); }
		public TerminalNode K_ASC() { return getToken(SqlParser.K_ASC, 0); }
		public TerminalNode K_ATTACH() { return getToken(SqlParser.K_ATTACH, 0); }
		public TerminalNode K_AUTOINCREMENT() { return getToken(SqlParser.K_AUTOINCREMENT, 0); }
		public TerminalNode K_BEFORE() { return getToken(SqlParser.K_BEFORE, 0); }
		public TerminalNode K_BEGIN() { return getToken(SqlParser.K_BEGIN, 0); }
		public TerminalNode K_BETWEEN() { return getToken(SqlParser.K_BETWEEN, 0); }
		public TerminalNode K_BY() { return getToken(SqlParser.K_BY, 0); }
		public TerminalNode K_BREAK() { return getToken(SqlParser.K_BREAK, 0); }
		public TerminalNode K_CASCADE() { return getToken(SqlParser.K_CASCADE, 0); }
		public TerminalNode K_CASE() { return getToken(SqlParser.K_CASE, 0); }
		public TerminalNode K_CAST() { return getToken(SqlParser.K_CAST, 0); }
		public TerminalNode K_CHECK() { return getToken(SqlParser.K_CHECK, 0); }
		public TerminalNode K_COLLATE() { return getToken(SqlParser.K_COLLATE, 0); }
		public TerminalNode K_COLUMN() { return getToken(SqlParser.K_COLUMN, 0); }
		public TerminalNode K_COMMIT() { return getToken(SqlParser.K_COMMIT, 0); }
		public TerminalNode K_CONFLICT() { return getToken(SqlParser.K_CONFLICT, 0); }
		public TerminalNode K_CONSTRAINT() { return getToken(SqlParser.K_CONSTRAINT, 0); }
		public TerminalNode K_CONTINUE() { return getToken(SqlParser.K_CONTINUE, 0); }
		public TerminalNode K_CREATE() { return getToken(SqlParser.K_CREATE, 0); }
		public TerminalNode K_CROSS() { return getToken(SqlParser.K_CROSS, 0); }
		public TerminalNode K_CURRENT_DATE() { return getToken(SqlParser.K_CURRENT_DATE, 0); }
		public TerminalNode K_CURRENT_TIME() { return getToken(SqlParser.K_CURRENT_TIME, 0); }
		public TerminalNode K_CURRENT_TIMESTAMP() { return getToken(SqlParser.K_CURRENT_TIMESTAMP, 0); }
		public TerminalNode K_DATABASE() { return getToken(SqlParser.K_DATABASE, 0); }
		public TerminalNode K_DEFAULT() { return getToken(SqlParser.K_DEFAULT, 0); }
		public TerminalNode K_DEFERRABLE() { return getToken(SqlParser.K_DEFERRABLE, 0); }
		public TerminalNode K_DEFERRED() { return getToken(SqlParser.K_DEFERRED, 0); }
		public TerminalNode K_DELETE() { return getToken(SqlParser.K_DELETE, 0); }
		public TerminalNode K_DESC() { return getToken(SqlParser.K_DESC, 0); }
		public TerminalNode K_DETACH() { return getToken(SqlParser.K_DETACH, 0); }
		public TerminalNode K_DISTINCT() { return getToken(SqlParser.K_DISTINCT, 0); }
		public TerminalNode K_DO() { return getToken(SqlParser.K_DO, 0); }
		public TerminalNode K_DROP() { return getToken(SqlParser.K_DROP, 0); }
		public TerminalNode K_EACH() { return getToken(SqlParser.K_EACH, 0); }
		public TerminalNode K_ELSE() { return getToken(SqlParser.K_ELSE, 0); }
		public TerminalNode K_END() { return getToken(SqlParser.K_END, 0); }
		public TerminalNode K_ENABLE() { return getToken(SqlParser.K_ENABLE, 0); }
		public TerminalNode K_ESCAPE() { return getToken(SqlParser.K_ESCAPE, 0); }
		public TerminalNode K_EXCEPT() { return getToken(SqlParser.K_EXCEPT, 0); }
		public TerminalNode K_EXCLUSIVE() { return getToken(SqlParser.K_EXCLUSIVE, 0); }
		public TerminalNode K_EXISTS() { return getToken(SqlParser.K_EXISTS, 0); }
		public TerminalNode K_EXPLAIN() { return getToken(SqlParser.K_EXPLAIN, 0); }
		public TerminalNode K_FAIL() { return getToken(SqlParser.K_FAIL, 0); }
		public TerminalNode K_FOR() { return getToken(SqlParser.K_FOR, 0); }
		public TerminalNode K_FOREIGN() { return getToken(SqlParser.K_FOREIGN, 0); }
		public TerminalNode K_FROM() { return getToken(SqlParser.K_FROM, 0); }
		public TerminalNode K_FULL() { return getToken(SqlParser.K_FULL, 0); }
		public TerminalNode K_GLOB() { return getToken(SqlParser.K_GLOB, 0); }
		public TerminalNode K_GROUP() { return getToken(SqlParser.K_GROUP, 0); }
		public TerminalNode K_HAVING() { return getToken(SqlParser.K_HAVING, 0); }
		public TerminalNode K_IF() { return getToken(SqlParser.K_IF, 0); }
		public TerminalNode K_IGNORE() { return getToken(SqlParser.K_IGNORE, 0); }
		public TerminalNode K_IMMEDIATE() { return getToken(SqlParser.K_IMMEDIATE, 0); }
		public TerminalNode K_IN() { return getToken(SqlParser.K_IN, 0); }
		public TerminalNode K_INDEX() { return getToken(SqlParser.K_INDEX, 0); }
		public TerminalNode K_INDEXED() { return getToken(SqlParser.K_INDEXED, 0); }
		public TerminalNode K_INITIALLY() { return getToken(SqlParser.K_INITIALLY, 0); }
		public TerminalNode K_INNER() { return getToken(SqlParser.K_INNER, 0); }
		public TerminalNode K_INSERT() { return getToken(SqlParser.K_INSERT, 0); }
		public TerminalNode K_INSTEAD() { return getToken(SqlParser.K_INSTEAD, 0); }
		public TerminalNode K_INTERSECT() { return getToken(SqlParser.K_INTERSECT, 0); }
		public TerminalNode K_INTO() { return getToken(SqlParser.K_INTO, 0); }
		public TerminalNode K_IS() { return getToken(SqlParser.K_IS, 0); }
		public TerminalNode K_ISNULL() { return getToken(SqlParser.K_ISNULL, 0); }
		public TerminalNode K_JOIN() { return getToken(SqlParser.K_JOIN, 0); }
		public TerminalNode K_KEY() { return getToken(SqlParser.K_KEY, 0); }
		public TerminalNode K_LEFT() { return getToken(SqlParser.K_LEFT, 0); }
		public TerminalNode K_LIKE() { return getToken(SqlParser.K_LIKE, 0); }
		public TerminalNode K_LIMIT() { return getToken(SqlParser.K_LIMIT, 0); }
		public TerminalNode K_MATCH() { return getToken(SqlParser.K_MATCH, 0); }
		public TerminalNode K_NATURAL() { return getToken(SqlParser.K_NATURAL, 0); }
		public TerminalNode K_NO() { return getToken(SqlParser.K_NO, 0); }
		public TerminalNode K_NOT() { return getToken(SqlParser.K_NOT, 0); }
		public TerminalNode K_NOTNULL() { return getToken(SqlParser.K_NOTNULL, 0); }
		public TerminalNode K_NULL() { return getToken(SqlParser.K_NULL, 0); }
		public TerminalNode K_OF() { return getToken(SqlParser.K_OF, 0); }
		public TerminalNode K_OFFSET() { return getToken(SqlParser.K_OFFSET, 0); }
		public TerminalNode K_ON() { return getToken(SqlParser.K_ON, 0); }
		public TerminalNode K_OR() { return getToken(SqlParser.K_OR, 0); }
		public TerminalNode K_ORDER() { return getToken(SqlParser.K_ORDER, 0); }
		public TerminalNode K_OUTER() { return getToken(SqlParser.K_OUTER, 0); }
		public TerminalNode K_PLAN() { return getToken(SqlParser.K_PLAN, 0); }
		public TerminalNode K_PRAGMA() { return getToken(SqlParser.K_PRAGMA, 0); }
		public TerminalNode K_PRIMARY() { return getToken(SqlParser.K_PRIMARY, 0); }
		public TerminalNode K_PRINT() { return getToken(SqlParser.K_PRINT, 0); }
		public TerminalNode K_QUERY() { return getToken(SqlParser.K_QUERY, 0); }
		public TerminalNode K_RAISE() { return getToken(SqlParser.K_RAISE, 0); }
		public TerminalNode K_RECURSIVE() { return getToken(SqlParser.K_RECURSIVE, 0); }
		public TerminalNode K_REFERENCES() { return getToken(SqlParser.K_REFERENCES, 0); }
		public TerminalNode K_REGEXP() { return getToken(SqlParser.K_REGEXP, 0); }
		public TerminalNode K_REINDEX() { return getToken(SqlParser.K_REINDEX, 0); }
		public TerminalNode K_RELEASE() { return getToken(SqlParser.K_RELEASE, 0); }
		public TerminalNode K_RENAME() { return getToken(SqlParser.K_RENAME, 0); }
		public TerminalNode K_REPLACE() { return getToken(SqlParser.K_REPLACE, 0); }
		public TerminalNode K_RESTRICT() { return getToken(SqlParser.K_RESTRICT, 0); }
		public TerminalNode K_RIGHT() { return getToken(SqlParser.K_RIGHT, 0); }
		public TerminalNode K_RETURN() { return getToken(SqlParser.K_RETURN, 0); }
		public TerminalNode K_ROLLBACK() { return getToken(SqlParser.K_ROLLBACK, 0); }
		public TerminalNode K_ROW() { return getToken(SqlParser.K_ROW, 0); }
		public TerminalNode K_SAVEPOINT() { return getToken(SqlParser.K_SAVEPOINT, 0); }
		public TerminalNode K_SELECT() { return getToken(SqlParser.K_SELECT, 0); }
		public TerminalNode K_SET() { return getToken(SqlParser.K_SET, 0); }
		public TerminalNode K_SWITCH() { return getToken(SqlParser.K_SWITCH, 0); }
		public TerminalNode K_TABLE() { return getToken(SqlParser.K_TABLE, 0); }
		public TerminalNode K_TRUE() { return getToken(SqlParser.K_TRUE, 0); }
		public TerminalNode K_TEMP() { return getToken(SqlParser.K_TEMP, 0); }
		public TerminalNode K_TEMPORARY() { return getToken(SqlParser.K_TEMPORARY, 0); }
		public TerminalNode K_THEN() { return getToken(SqlParser.K_THEN, 0); }
		public TerminalNode K_TO() { return getToken(SqlParser.K_TO, 0); }
		public TerminalNode K_TRANSACTION() { return getToken(SqlParser.K_TRANSACTION, 0); }
		public TerminalNode K_TRIGGER() { return getToken(SqlParser.K_TRIGGER, 0); }
		public TerminalNode K_UNION() { return getToken(SqlParser.K_UNION, 0); }
		public TerminalNode K_UNIQUE() { return getToken(SqlParser.K_UNIQUE, 0); }
		public TerminalNode K_UPDATE() { return getToken(SqlParser.K_UPDATE, 0); }
		public TerminalNode K_USING() { return getToken(SqlParser.K_USING, 0); }
		public TerminalNode K_VACUUM() { return getToken(SqlParser.K_VACUUM, 0); }
		public TerminalNode K_VALUES() { return getToken(SqlParser.K_VALUES, 0); }
		public TerminalNode K_VIEW() { return getToken(SqlParser.K_VIEW, 0); }
		public TerminalNode K_VIRTUAL() { return getToken(SqlParser.K_VIRTUAL, 0); }
		public TerminalNode K_WHEN() { return getToken(SqlParser.K_WHEN, 0); }
		public TerminalNode K_WHERE() { return getToken(SqlParser.K_WHERE, 0); }
		public TerminalNode K_WITH() { return getToken(SqlParser.K_WITH, 0); }
		public TerminalNode K_WITHOUT() { return getToken(SqlParser.K_WITHOUT, 0); }
		public TerminalNode K_NEXTVAL() { return getToken(SqlParser.K_NEXTVAL, 0); }
		public KeywordContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_keyword; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterKeyword(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitKeyword(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitKeyword(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KeywordContext keyword() throws RecognitionException {
		KeywordContext _localctx = new KeywordContext(_ctx, getState());
		enterRule(_localctx, 190, RULE_keyword);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1335);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << K_ABORT) | (1L << K_ACTION) | (1L << K_ADD) | (1L << K_AFTER) | (1L << K_ALL) | (1L << K_ALTER) | (1L << K_ANALYZE) | (1L << K_AND) | (1L << K_AS) | (1L << K_ASC) | (1L << K_ATTACH) | (1L << K_AUTOINCREMENT) | (1L << K_BEFORE) | (1L << K_BEGIN) | (1L << K_BETWEEN) | (1L << K_BY) | (1L << K_BREAK) | (1L << K_CASCADE) | (1L << K_CASE) | (1L << K_CAST) | (1L << K_CHECK) | (1L << K_COLLATE) | (1L << K_COLUMN) | (1L << K_COMMIT) | (1L << K_CONFLICT) | (1L << K_CONSTRAINT) | (1L << K_CONTINUE) | (1L << K_CREATE) | (1L << K_CROSS) | (1L << K_CURRENT_DATE) | (1L << K_CURRENT_TIME))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (K_CURRENT_TIMESTAMP - 64)) | (1L << (K_DATABASE - 64)) | (1L << (K_DEFAULT - 64)) | (1L << (K_DEFERRABLE - 64)) | (1L << (K_DEFERRED - 64)) | (1L << (K_DELETE - 64)) | (1L << (K_DESC - 64)) | (1L << (K_DETACH - 64)) | (1L << (K_DISTINCT - 64)) | (1L << (K_DO - 64)) | (1L << (K_DROP - 64)) | (1L << (K_EACH - 64)) | (1L << (K_ELSE - 64)) | (1L << (K_END - 64)) | (1L << (K_ENABLE - 64)) | (1L << (K_ESCAPE - 64)) | (1L << (K_EXCEPT - 64)) | (1L << (K_EXCLUSIVE - 64)) | (1L << (K_EXISTS - 64)) | (1L << (K_EXPLAIN - 64)) | (1L << (K_FAIL - 64)) | (1L << (K_FOR - 64)) | (1L << (K_FOREIGN - 64)) | (1L << (K_FROM - 64)) | (1L << (K_FULL - 64)) | (1L << (K_GLOB - 64)) | (1L << (K_GROUP - 64)) | (1L << (K_HAVING - 64)) | (1L << (K_IF - 64)) | (1L << (K_IGNORE - 64)) | (1L << (K_IMMEDIATE - 64)) | (1L << (K_IN - 64)) | (1L << (K_INDEX - 64)) | (1L << (K_INDEXED - 64)) | (1L << (K_INITIALLY - 64)) | (1L << (K_INNER - 64)) | (1L << (K_INSERT - 64)) | (1L << (K_INSTEAD - 64)) | (1L << (K_INTERSECT - 64)) | (1L << (K_INTO - 64)) | (1L << (K_IS - 64)) | (1L << (K_ISNULL - 64)) | (1L << (K_JOIN - 64)) | (1L << (K_KEY - 64)) | (1L << (K_LEFT - 64)) | (1L << (K_LIKE - 64)) | (1L << (K_LIMIT - 64)) | (1L << (K_MATCH - 64)) | (1L << (K_NATURAL - 64)) | (1L << (K_NEXTVAL - 64)) | (1L << (K_NO - 64)) | (1L << (K_NOT - 64)) | (1L << (K_NOTNULL - 64)) | (1L << (K_NULL - 64)) | (1L << (K_OF - 64)) | (1L << (K_OFFSET - 64)) | (1L << (K_ON - 64)) | (1L << (K_OR - 64)) | (1L << (K_ORDER - 64)) | (1L << (K_OUTER - 64)) | (1L << (K_PLAN - 64)))) != 0) || ((((_la - 128)) & ~0x3f) == 0 && ((1L << (_la - 128)) & ((1L << (K_PRAGMA - 128)) | (1L << (K_PRIMARY - 128)) | (1L << (K_PRINT - 128)) | (1L << (K_QUERY - 128)) | (1L << (K_RAISE - 128)) | (1L << (K_RECURSIVE - 128)) | (1L << (K_REFERENCES - 128)) | (1L << (K_REGEXP - 128)) | (1L << (K_REINDEX - 128)) | (1L << (K_RELEASE - 128)) | (1L << (K_RENAME - 128)) | (1L << (K_REPLACE - 128)) | (1L << (K_RESTRICT - 128)) | (1L << (K_RETURN - 128)) | (1L << (K_RIGHT - 128)) | (1L << (K_ROLLBACK - 128)) | (1L << (K_ROW - 128)) | (1L << (K_SAVEPOINT - 128)) | (1L << (K_SELECT - 128)) | (1L << (K_SET - 128)) | (1L << (K_SWITCH - 128)) | (1L << (K_TABLE - 128)) | (1L << (K_TEMP - 128)) | (1L << (K_TEMPORARY - 128)) | (1L << (K_THEN - 128)) | (1L << (K_TO - 128)) | (1L << (K_TRUE - 128)) | (1L << (K_TRANSACTION - 128)) | (1L << (K_TRIGGER - 128)) | (1L << (K_UNION - 128)) | (1L << (K_UNIQUE - 128)) | (1L << (K_UPDATE - 128)) | (1L << (K_USING - 128)) | (1L << (K_VACUUM - 128)) | (1L << (K_VALUES - 128)) | (1L << (K_VIEW - 128)) | (1L << (K_VIRTUAL - 128)) | (1L << (K_WHEN - 128)) | (1L << (K_WHERE - 128)) | (1L << (K_WITH - 128)) | (1L << (K_WITHOUT - 128)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public NameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NameContext name() throws RecognitionException {
		NameContext _localctx = new NameContext(_ctx, getState());
		enterRule(_localctx, 192, RULE_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1337);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Function_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterFunction_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitFunction_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitFunction_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Function_nameContext function_name() throws RecognitionException {
		Function_nameContext _localctx = new Function_nameContext(_ctx, getState());
		enterRule(_localctx, 194, RULE_function_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1339);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Database_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Database_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_database_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterDatabase_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitDatabase_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitDatabase_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Database_nameContext database_name() throws RecognitionException {
		Database_nameContext _localctx = new Database_nameContext(_ctx, getState());
		enterRule(_localctx, 196, RULE_database_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1341);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Table_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterTable_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitTable_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitTable_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_nameContext table_name() throws RecognitionException {
		Table_nameContext _localctx = new Table_nameContext(_ctx, getState());
		enterRule(_localctx, 198, RULE_table_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1343);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Column_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterColumn_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitColumn_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitColumn_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_nameContext column_name() throws RecognitionException {
		Column_nameContext _localctx = new Column_nameContext(_ctx, getState());
		enterRule(_localctx, 200, RULE_column_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1345);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Collation_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Collation_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_collation_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterCollation_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitCollation_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitCollation_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Collation_nameContext collation_name() throws RecognitionException {
		Collation_nameContext _localctx = new Collation_nameContext(_ctx, getState());
		enterRule(_localctx, 202, RULE_collation_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1347);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Foreign_tableContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Foreign_tableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_foreign_table; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterForeign_table(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitForeign_table(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitForeign_table(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Foreign_tableContext foreign_table() throws RecognitionException {
		Foreign_tableContext _localctx = new Foreign_tableContext(_ctx, getState());
		enterRule(_localctx, 204, RULE_foreign_table);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1349);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Index_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Index_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_index_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterIndex_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitIndex_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitIndex_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Index_nameContext index_name() throws RecognitionException {
		Index_nameContext _localctx = new Index_nameContext(_ctx, getState());
		enterRule(_localctx, 206, RULE_index_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1351);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Savepoint_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Savepoint_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_savepoint_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterSavepoint_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitSavepoint_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitSavepoint_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Savepoint_nameContext savepoint_name() throws RecognitionException {
		Savepoint_nameContext _localctx = new Savepoint_nameContext(_ctx, getState());
		enterRule(_localctx, 208, RULE_savepoint_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1353);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_aliasContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Table_aliasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_alias; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterTable_alias(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitTable_alias(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitTable_alias(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_aliasContext table_alias() throws RecognitionException {
		Table_aliasContext _localctx = new Table_aliasContext(_ctx, getState());
		enterRule(_localctx, 210, RULE_table_alias);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1355);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Any_nameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(SqlParser.IDENTIFIER, 0); }
		public TerminalNode STRING_LITERAL() { return getToken(SqlParser.STRING_LITERAL, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SqlParser.OPEN_PAR, 0); }
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SqlParser.CLOSE_PAR, 0); }
		public Any_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_any_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).enterAny_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SqlListener ) ((SqlListener)listener).exitAny_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SqlVisitor ) return ((SqlVisitor<? extends T>)visitor).visitAny_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Any_nameContext any_name() throws RecognitionException {
		Any_nameContext _localctx = new Any_nameContext(_ctx, getState());
		enterRule(_localctx, 212, RULE_any_name);
		try {
			setState(1363);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IDENTIFIER:
				enterOuterAlt(_localctx, 1);
				{
				setState(1357);
				match(IDENTIFIER);
				}
				break;
			case STRING_LITERAL:
				enterOuterAlt(_localctx, 2);
				{
				setState(1358);
				match(STRING_LITERAL);
				}
				break;
			case OPEN_PAR:
				enterOuterAlt(_localctx, 3);
				{
				setState(1359);
				match(OPEN_PAR);
				setState(1360);
				any_name();
				setState(1361);
				match(CLOSE_PAR);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 53:
			return expr_sempred((ExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expr_sempred(ExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 14);
		case 1:
			return precpred(_ctx, 13);
		case 2:
			return precpred(_ctx, 12);
		case 3:
			return precpred(_ctx, 11);
		case 4:
			return precpred(_ctx, 10);
		case 5:
			return precpred(_ctx, 9);
		case 6:
			return precpred(_ctx, 8);
		case 7:
			return precpred(_ctx, 7);
		case 8:
			return precpred(_ctx, 4);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\u00b8\u0558\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I"+
		"\tI\4J\tJ\4K\tK\4L\tL\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\4T\tT"+
		"\4U\tU\4V\tV\4W\tW\4X\tX\4Y\tY\4Z\tZ\4[\t[\4\\\t\\\4]\t]\4^\t^\4_\t_\4"+
		"`\t`\4a\ta\4b\tb\4c\tc\4d\td\4e\te\4f\tf\4g\tg\4h\th\4i\ti\4j\tj\4k\t"+
		"k\4l\tl\3\2\3\2\3\2\3\2\3\2\7\2\u00de\n\2\f\2\16\2\u00e1\13\2\3\2\3\2"+
		"\3\3\3\3\3\3\3\4\3\4\3\5\7\5\u00eb\n\5\f\5\16\5\u00ee\13\5\3\5\3\5\6\5"+
		"\u00f2\n\5\r\5\16\5\u00f3\3\5\7\5\u00f7\n\5\f\5\16\5\u00fa\13\5\3\5\7"+
		"\5\u00fd\n\5\f\5\16\5\u0100\13\5\3\6\3\6\3\6\5\6\u0105\n\6\3\7\3\7\3\7"+
		"\3\7\3\7\5\7\u010c\n\7\3\b\3\b\5\b\u0110\n\b\3\b\3\b\3\b\3\b\3\b\3\b\3"+
		"\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\b\u0120\n\b\3\t\3\t\3\t\5\t\u0125\n\t"+
		"\3\t\3\t\3\t\5\t\u012a\n\t\7\t\u012c\n\t\f\t\16\t\u012f\13\t\5\t\u0131"+
		"\n\t\3\t\3\t\3\t\3\n\5\n\u0137\n\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3"+
		"\13\5\13\u0141\n\13\3\13\3\13\5\13\u0145\n\13\3\f\5\f\u0148\n\f\3\f\5"+
		"\f\u014b\n\f\3\f\5\f\u014e\n\f\3\f\3\f\5\f\u0152\n\f\3\f\5\f\u0155\n\f"+
		"\3\r\3\r\3\r\5\r\u015a\n\r\7\r\u015c\n\r\f\r\16\r\u015f\13\r\3\r\3\r\3"+
		"\r\5\r\u0164\n\r\5\r\u0166\n\r\3\16\3\16\3\16\3\17\3\17\3\17\7\17\u016e"+
		"\n\17\f\17\16\17\u0171\13\17\3\20\3\20\3\20\5\20\u0176\n\20\3\21\3\21"+
		"\3\21\3\21\3\22\5\22\u017d\n\22\3\22\3\22\5\22\u0181\n\22\3\22\3\22\5"+
		"\22\u0185\n\22\3\22\3\22\5\22\u0189\n\22\3\23\3\23\5\23\u018d\n\23\3\24"+
		"\3\24\5\24\u0191\n\24\3\25\3\25\3\26\3\26\3\26\3\26\5\26\u0199\n\26\5"+
		"\26\u019b\n\26\3\26\3\26\3\26\3\26\5\26\u01a1\n\26\3\27\3\27\3\27\3\27"+
		"\3\27\3\27\3\27\5\27\u01aa\n\27\3\30\3\30\3\30\3\30\3\30\5\30\u01b1\n"+
		"\30\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3"+
		"\32\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\34\3\34\3\34\7\34\u01cc"+
		"\n\34\f\34\16\34\u01cf\13\34\3\35\3\35\3\35\5\35\u01d4\n\35\3\36\3\36"+
		"\5\36\u01d8\n\36\3\36\3\36\3\36\5\36\u01dd\n\36\7\36\u01df\n\36\f\36\16"+
		"\36\u01e2\13\36\3\37\3\37\3\37\5\37\u01e7\n\37\3\37\3\37\3 \3 \3 \7 \u01ee"+
		"\n \f \16 \u01f1\13 \3 \3 \3 \3 \5 \u01f7\n \7 \u01f9\n \f \16 \u01fc"+
		"\13 \3 \3 \3 \3 \5 \u0202\n \3 \3 \3!\3!\3!\3!\3\"\3\"\3\"\3\"\3\"\3\""+
		"\3#\3#\3#\3#\3#\5#\u0215\n#\3#\3#\3#\7#\u021a\n#\f#\16#\u021d\13#\3#\5"+
		"#\u0220\n#\3#\3#\3$\3$\3$\5$\u0227\n$\3$\3$\7$\u022b\n$\f$\16$\u022e\13"+
		"$\3$\3$\3$\3$\7$\u0234\n$\f$\16$\u0237\13$\3$\3$\3$\5$\u023c\n$\3%\3%"+
		"\3%\7%\u0241\n%\f%\16%\u0244\13%\3%\3%\7%\u0248\n%\f%\16%\u024b\13%\3"+
		"%\5%\u024e\n%\3&\3&\3&\7&\u0253\n&\f&\16&\u0256\13&\3&\3&\6&\u025a\n&"+
		"\r&\16&\u025b\3\'\3\'\3\'\3\'\3\'\3\'\7\'\u0264\n\'\f\'\16\'\u0267\13"+
		"\'\5\'\u0269\n\'\3\'\3\'\3\'\3\'\5\'\u026f\n\'\5\'\u0271\n\'\3(\3(\3("+
		"\3(\3(\3(\7(\u0279\n(\f(\16(\u027c\13(\5(\u027e\n(\3(\3(\3(\3(\5(\u0284"+
		"\n(\5(\u0286\n(\3)\3)\5)\u028a\n)\3)\3)\3)\7)\u028f\n)\f)\16)\u0292\13"+
		")\3)\5)\u0295\n)\3)\5)\u0298\n)\3)\3)\5)\u029c\n)\5)\u029e\n)\3)\3)\3"+
		")\3)\3)\7)\u02a5\n)\f)\16)\u02a8\13)\3)\3)\3)\3)\3)\3)\7)\u02b0\n)\f)"+
		"\16)\u02b3\13)\3)\3)\7)\u02b7\n)\f)\16)\u02ba\13)\5)\u02bc\n)\3*\3*\3"+
		"*\3*\7*\u02c2\n*\f*\16*\u02c5\13*\3*\5*\u02c8\n*\3+\3+\3+\3,\3,\3,\3,"+
		"\3,\7,\u02d2\n,\f,\16,\u02d5\13,\3-\3-\3-\3.\3.\3.\7.\u02dd\n.\f.\16."+
		"\u02e0\13.\3/\3/\3/\3/\5/\u02e6\n/\3/\3/\3/\3/\3/\5/\u02ed\n/\3/\3/\3"+
		"/\5/\u02f2\n/\3/\3/\5/\u02f6\n/\3\60\3\60\5\60\u02fa\n\60\3\60\3\60\3"+
		"\60\3\60\3\60\3\60\3\60\3\60\3\60\3\60\3\60\3\60\3\60\5\60\u0309\n\60"+
		"\3\61\3\61\3\61\5\61\u030e\n\61\3\61\5\61\u0311\n\61\3\62\3\62\3\63\3"+
		"\63\3\63\3\64\3\64\3\65\3\65\3\65\3\65\3\65\3\65\3\65\3\65\3\65\3\65\3"+
		"\65\3\65\5\65\u0326\n\65\3\65\3\65\6\65\u032a\n\65\r\65\16\65\u032b\5"+
		"\65\u032e\n\65\3\66\3\66\5\66\u0332\n\66\3\67\3\67\3\67\3\67\3\67\3\67"+
		"\3\67\3\67\3\67\3\67\3\67\3\67\5\67\u0340\n\67\3\67\3\67\3\67\3\67\3\67"+
		"\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67"+
		"\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\5\67\u035e\n\67\3\67\3\67"+
		"\3\67\3\67\3\67\3\67\3\67\3\67\3\67\5\67\u0369\n\67\3\67\3\67\7\67\u036d"+
		"\n\67\f\67\16\67\u0370\13\67\38\38\38\58\u0375\n8\38\38\38\58\u037a\n"+
		"8\38\38\39\39\39\3:\3:\3:\5:\u0384\n:\3:\3:\3:\7:\u0389\n:\f:\16:\u038c"+
		"\13:\3:\5:\u038f\n:\3:\3:\3;\3;\3;\3;\3;\7;\u0398\n;\f;\16;\u039b\13;"+
		"\5;\u039d\n;\3;\3;\3;\3;\5;\u03a3\n;\3;\5;\u03a6\n;\3<\5<\u03a9\n<\3<"+
		"\5<\u03ac\n<\3<\3<\3<\3<\3=\3=\3=\3=\5=\u03b6\n=\3=\3=\3=\3=\3=\7=\u03bd"+
		"\n=\f=\16=\u03c0\13=\3=\3=\5=\u03c4\n=\3=\3=\3=\3=\3=\3=\3=\3=\3=\3=\5"+
		"=\u03d0\n=\3=\3=\5=\u03d4\n=\7=\u03d6\n=\f=\16=\u03d9\13=\3=\5=\u03dc"+
		"\n=\3=\3=\3=\3=\3=\5=\u03e3\n=\3=\5=\u03e6\n=\5=\u03e8\n=\3>\3>\3?\3?"+
		"\3?\5?\u03ef\n?\3?\5?\u03f2\n?\3@\3@\5@\u03f6\n@\3@\3@\3@\3@\3@\3@\3@"+
		"\3@\3@\5@\u0401\n@\3A\3A\3A\3A\3A\3A\7A\u0409\nA\fA\16A\u040c\13A\3A\3"+
		"A\3B\3B\3B\3B\3B\3B\7B\u0416\nB\fB\16B\u0419\13B\3B\3B\3B\3C\3C\5C\u0420"+
		"\nC\3C\5C\u0423\nC\3C\3C\3C\3C\7C\u0429\nC\fC\16C\u042c\13C\3C\3C\3D\3"+
		"D\5D\u0432\nD\3D\3D\3D\3D\7D\u0438\nD\fD\16D\u043b\13D\3D\3D\3E\3E\3F"+
		"\3F\5F\u0443\nF\3G\3G\3G\3G\3G\3G\3G\5G\u044c\nG\3G\5G\u044f\nG\5G\u0451"+
		"\nG\3H\3H\3H\5H\u0456\nH\3I\3I\3I\5I\u045b\nI\3I\3I\5I\u045f\nI\3I\5I"+
		"\u0462\nI\3I\3I\3I\3I\3I\5I\u0469\nI\3J\3J\3J\3J\7J\u046f\nJ\fJ\16J\u0472"+
		"\13J\3J\5J\u0475\nJ\3J\3J\5J\u0479\nJ\3J\5J\u047c\nJ\3K\3K\3K\3K\5K\u0482"+
		"\nK\3K\5K\u0485\nK\3L\3L\3L\3L\3L\7L\u048c\nL\fL\16L\u048f\13L\3M\3M\3"+
		"M\5M\u0494\nM\3M\5M\u0497\nM\3M\5M\u049a\nM\3N\3N\5N\u049e\nN\3O\3O\5"+
		"O\u04a2\nO\3O\3O\3O\7O\u04a7\nO\fO\16O\u04aa\13O\3O\5O\u04ad\nO\3O\5O"+
		"\u04b0\nO\3O\3O\5O\u04b4\nO\5O\u04b6\nO\3O\3O\3O\3O\3O\7O\u04bd\nO\fO"+
		"\16O\u04c0\13O\3O\3O\3O\3O\3O\3O\7O\u04c8\nO\fO\16O\u04cb\13O\3O\3O\7"+
		"O\u04cf\nO\fO\16O\u04d2\13O\5O\u04d4\nO\3P\5P\u04d7\nP\3P\3P\5P\u04db"+
		"\nP\3Q\3Q\3Q\3Q\3Q\3Q\3Q\7Q\u04e4\nQ\fQ\16Q\u04e7\13Q\3Q\3Q\3Q\3Q\3Q\3"+
		"Q\3Q\5Q\u04f0\nQ\3Q\3Q\3Q\3R\3R\3S\3S\3S\7S\u04fa\nS\fS\16S\u04fd\13S"+
		"\3T\3T\3U\3U\3U\3U\3U\3U\3U\3U\3U\3U\7U\u050b\nU\fU\16U\u050e\13U\5U\u0510"+
		"\nU\3U\3U\3U\3V\3V\3W\3W\3X\3X\3Y\3Y\3Z\3Z\3[\3[\3[\3[\3[\3[\3[\3[\3["+
		"\3[\3[\3[\3[\3[\3[\3\\\3\\\3]\3]\3^\3^\3_\3_\5_\u0536\n_\3`\3`\3a\3a\3"+
		"b\3b\3c\3c\3d\3d\3e\3e\3f\3f\3g\3g\3h\3h\3i\3i\3j\3j\3k\3k\3l\3l\3l\3"+
		"l\3l\3l\5l\u0556\nl\3l\2\3lm\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \""+
		"$&(*,.\60\62\64\668:<>@BDFHJLNPRTVXZ\\^`bdfhjlnprtvxz|~\u0080\u0082\u0084"+
		"\u0086\u0088\u008a\u008c\u008e\u0090\u0092\u0094\u0096\u0098\u009a\u009c"+
		"\u009e\u00a0\u00a2\u00a4\u00a6\u00a8\u00aa\u00ac\u00ae\u00b0\u00b2\u00b4"+
		"\u00b6\u00b8\u00ba\u00bc\u00be\u00c0\u00c2\u00c4\u00c6\u00c8\u00ca\u00cc"+
		"\u00ce\u00d0\u00d2\u00d4\u00d6\2\20\3\2\22\23\4\2\21\23\26\27\4\2\17\17"+
		"{{\4\2\'\'JJ\4\2,,HH\4\2\21\21\26\27\3\2\30\33\3\2\34\37\4\2GG\u00a1\u00a1"+
		"\6\2@Byy\u00b1\u00b1\u00b3\u00b4\4\2\22\24ww\5\2\20\20\34\34\36\36\4\2"+
		"\u00b0\u00b0\u00b3\u00b3\7\2#VXmo|~\u00a6\u00a8\u00ab\2\u05d8\2\u00df"+
		"\3\2\2\2\4\u00e4\3\2\2\2\6\u00e7\3\2\2\2\b\u00ec\3\2\2\2\n\u0104\3\2\2"+
		"\2\f\u010b\3\2\2\2\16\u011f\3\2\2\2\20\u0121\3\2\2\2\22\u0136\3\2\2\2"+
		"\24\u0144\3\2\2\2\26\u0147\3\2\2\2\30\u0165\3\2\2\2\32\u0167\3\2\2\2\34"+
		"\u016a\3\2\2\2\36\u0172\3\2\2\2 \u0177\3\2\2\2\"\u017c\3\2\2\2$\u018c"+
		"\3\2\2\2&\u0190\3\2\2\2(\u0192\3\2\2\2*\u019a\3\2\2\2,\u01a9\3\2\2\2."+
		"\u01b0\3\2\2\2\60\u01b2\3\2\2\2\62\u01b4\3\2\2\2\64\u01c0\3\2\2\2\66\u01c8"+
		"\3\2\2\28\u01d0\3\2\2\2:\u01d7\3\2\2\2<\u01e3\3\2\2\2>\u01ea\3\2\2\2@"+
		"\u0205\3\2\2\2B\u0209\3\2\2\2D\u020f\3\2\2\2F\u0223\3\2\2\2H\u023d\3\2"+
		"\2\2J\u024f\3\2\2\2L\u025d\3\2\2\2N\u0272\3\2\2\2P\u02bb\3\2\2\2R\u02bd"+
		"\3\2\2\2T\u02c9\3\2\2\2V\u02cc\3\2\2\2X\u02d6\3\2\2\2Z\u02d9\3\2\2\2\\"+
		"\u02e1\3\2\2\2^\u02f9\3\2\2\2`\u030a\3\2\2\2b\u0312\3\2\2\2d\u0314\3\2"+
		"\2\2f\u0317\3\2\2\2h\u0319\3\2\2\2j\u0331\3\2\2\2l\u033f\3\2\2\2n\u0379"+
		"\3\2\2\2p\u037d\3\2\2\2r\u0380\3\2\2\2t\u03a5\3\2\2\2v\u03ab\3\2\2\2x"+
		"\u03b1\3\2\2\2z\u03e9\3\2\2\2|\u03eb\3\2\2\2~\u03f5\3\2\2\2\u0080\u0402"+
		"\3\2\2\2\u0082\u040f\3\2\2\2\u0084\u041d\3\2\2\2\u0086\u042f\3\2\2\2\u0088"+
		"\u043e\3\2\2\2\u008a\u0440\3\2\2\2\u008c\u0450\3\2\2\2\u008e\u0455\3\2"+
		"\2\2\u0090\u045a\3\2\2\2\u0092\u046a\3\2\2\2\u0094\u047d\3\2\2\2\u0096"+
		"\u0486\3\2\2\2\u0098\u0499\3\2\2\2\u009a\u049d\3\2\2\2\u009c\u04d3\3\2"+
		"\2\2\u009e\u04da\3\2\2\2\u00a0\u04dc\3\2\2\2\u00a2\u04f4\3\2\2\2\u00a4"+
		"\u04f6\3\2\2\2\u00a6\u04fe\3\2\2\2\u00a8\u0500\3\2\2\2\u00aa\u0514\3\2"+
		"\2\2\u00ac\u0516\3\2\2\2\u00ae\u0518\3\2\2\2\u00b0\u051a\3\2\2\2\u00b2"+
		"\u051c\3\2\2\2\u00b4\u051e\3\2\2\2\u00b6\u052d\3\2\2\2\u00b8\u052f\3\2"+
		"\2\2\u00ba\u0531\3\2\2\2\u00bc\u0535\3\2\2\2\u00be\u0537\3\2\2\2\u00c0"+
		"\u0539\3\2\2\2\u00c2\u053b\3\2\2\2\u00c4\u053d\3\2\2\2\u00c6\u053f\3\2"+
		"\2\2\u00c8\u0541\3\2\2\2\u00ca\u0543\3\2\2\2\u00cc\u0545\3\2\2\2\u00ce"+
		"\u0547\3\2\2\2\u00d0\u0549\3\2\2\2\u00d2\u054b\3\2\2\2\u00d4\u054d\3\2"+
		"\2\2\u00d6\u0555\3\2\2\2\u00d8\u00de\5\4\3\2\u00d9\u00de\5\b\5\2\u00da"+
		"\u00de\5\6\4\2\u00db\u00de\5> \2\u00dc\u00de\5\u00b4[\2\u00dd\u00d8\3"+
		"\2\2\2\u00dd\u00d9\3\2\2\2\u00dd\u00da\3\2\2\2\u00dd\u00db\3\2\2\2\u00dd"+
		"\u00dc\3\2\2\2\u00de\u00e1\3\2\2\2\u00df\u00dd\3\2\2\2\u00df\u00e0\3\2"+
		"\2\2\u00e0\u00e2\3\2\2\2\u00e1\u00df\3\2\2\2\u00e2\u00e3\7\2\2\3\u00e3"+
		"\3\3\2\2\2\u00e4\u00e5\7\u00b8\2\2\u00e5\u00e6\b\3\1\2\u00e6\5\3\2\2\2"+
		"\u00e7\u00e8\5\24\13\2\u00e8\7\3\2\2\2\u00e9\u00eb\7\13\2\2\u00ea\u00e9"+
		"\3\2\2\2\u00eb\u00ee\3\2\2\2\u00ec\u00ea\3\2\2\2\u00ec\u00ed\3\2\2\2\u00ed"+
		"\u00ef\3\2\2\2\u00ee\u00ec\3\2\2\2\u00ef\u00f8\5\n\6\2\u00f0\u00f2\7\13"+
		"\2\2\u00f1\u00f0\3\2\2\2\u00f2\u00f3\3\2\2\2\u00f3\u00f1\3\2\2\2\u00f3"+
		"\u00f4\3\2\2\2\u00f4\u00f5\3\2\2\2\u00f5\u00f7\5\n\6\2\u00f6\u00f1\3\2"+
		"\2\2\u00f7\u00fa\3\2\2\2\u00f8\u00f6\3\2\2\2\u00f8\u00f9\3\2\2\2\u00f9"+
		"\u00fe\3\2\2\2\u00fa\u00f8\3\2\2\2\u00fb\u00fd\7\13\2\2\u00fc\u00fb\3"+
		"\2\2\2\u00fd\u0100\3\2\2\2\u00fe\u00fc\3\2\2\2\u00fe\u00ff\3\2\2\2\u00ff"+
		"\t\3\2\2\2\u0100\u00fe\3\2\2\2\u0101\u0105\5\u00a0Q\2\u0102\u0105\5L\'"+
		"\2\u0103\u0105\5\u00a8U\2\u0104\u0101\3\2\2\2\u0104\u0102\3\2\2\2\u0104"+
		"\u0103\3\2\2\2\u0105\13\3\2\2\2\u0106\u010c\5\b\5\2\u0107\u010c\5\n\6"+
		"\2\u0108\u010c\5\16\b\2\u0109\u010a\7=\2\2\u010a\u010c\7\13\2\2\u010b"+
		"\u0106\3\2\2\2\u010b\u0107\3\2\2\2\u010b\u0108\3\2\2\2\u010b\u0109\3\2"+
		"\2\2\u010c\r\3\2\2\2\u010d\u010f\5<\37\2\u010e\u0110\7\13\2\2\u010f\u010e"+
		"\3\2\2\2\u010f\u0110\3\2\2\2\u0110\u0120\3\2\2\2\u0111\u0120\5\34\17\2"+
		"\u0112\u0113\5 \21\2\u0113\u0114\7\13\2\2\u0114\u0120\3\2\2\2\u0115\u0120"+
		"\5\62\32\2\u0116\u0120\5\64\33\2\u0117\u0120\5@!\2\u0118\u0120\5B\"\2"+
		"\u0119\u0120\5\24\13\2\u011a\u0120\5D#\2\u011b\u011c\5*\26\2\u011c\u011d"+
		"\7\13\2\2\u011d\u0120\3\2\2\2\u011e\u0120\5J&\2\u011f\u010d\3\2\2\2\u011f"+
		"\u0111\3\2\2\2\u011f\u0112\3\2\2\2\u011f\u0115\3\2\2\2\u011f\u0116\3\2"+
		"\2\2\u011f\u0117\3\2\2\2\u011f\u0118\3\2\2\2\u011f\u0119\3\2\2\2\u011f"+
		"\u011a\3\2\2\2\u011f\u011b\3\2\2\2\u011f\u011e\3\2\2\2\u0120\17\3\2\2"+
		"\2\u0121\u0130\7\3\2\2\u0122\u0125\5\u00d6l\2\u0123\u0125\5\u009eP\2\u0124"+
		"\u0122\3\2\2\2\u0124\u0123\3\2\2\2\u0125\u012d\3\2\2\2\u0126\u0129\7\17"+
		"\2\2\u0127\u012a\5\u00d6l\2\u0128\u012a\5\u009eP\2\u0129\u0127\3\2\2\2"+
		"\u0129\u0128\3\2\2\2\u012a\u012c\3\2\2\2\u012b\u0126\3\2\2\2\u012c\u012f"+
		"\3\2\2\2\u012d\u012b\3\2\2\2\u012d\u012e\3\2\2\2\u012e\u0131\3\2\2\2\u012f"+
		"\u012d\3\2\2\2\u0130\u0124\3\2\2\2\u0130\u0131\3\2\2\2\u0131\u0132\3\2"+
		"\2\2\u0132\u0133\7\4\2\2\u0133\u0134\7\13\2\2\u0134\21\3\2\2\2\u0135\u0137"+
		"\7\u00a7\2\2\u0136\u0135\3\2\2\2\u0136\u0137\3\2\2\2\u0137\u0138\3\2\2"+
		"\2\u0138\u0139\5\u00d6l\2\u0139\23\3\2\2\2\u013a\u0145\5\22\n\2\u013b"+
		"\u013c\5\22\n\2\u013c\u0140\7\20\2\2\u013d\u0141\5 \21\2\u013e\u0141\5"+
		"\n\6\2\u013f\u0141\5l\67\2\u0140\u013d\3\2\2\2\u0140\u013e\3\2\2\2\u0140"+
		"\u013f\3\2\2\2\u0141\u0142\3\2\2\2\u0142\u0143\7\13\2\2\u0143\u0145\3"+
		"\2\2\2\u0144\u013a\3\2\2\2\u0144\u013b\3\2\2\2\u0145\25\3\2\2\2\u0146"+
		"\u0148\7\r\2\2\u0147\u0146\3\2\2\2\u0147\u0148\3\2\2\2\u0148\u014a\3\2"+
		"\2\2\u0149\u014b\7\5\2\2\u014a\u0149\3\2\2\2\u014a\u014b\3\2\2\2\u014b"+
		"\u014d\3\2\2\2\u014c\u014e\7\r\2\2\u014d\u014c\3\2\2\2\u014d\u014e\3\2"+
		"\2\2\u014e\u014f\3\2\2\2\u014f\u0151\5l\67\2\u0150\u0152\7\16\2\2\u0151"+
		"\u0150\3\2\2\2\u0151\u0152\3\2\2\2\u0152\u0154\3\2\2\2\u0153\u0155\7\16"+
		"\2\2\u0154\u0153\3\2\2\2\u0154\u0155\3\2\2\2\u0155\27\3\2\2\2\u0156\u015d"+
		"\7\6\2\2\u0157\u0159\5\f\7\2\u0158\u015a\7\13\2\2\u0159\u0158\3\2\2\2"+
		"\u0159\u015a\3\2\2\2\u015a\u015c\3\2\2\2\u015b\u0157\3\2\2\2\u015c\u015f"+
		"\3\2\2\2\u015d\u015b\3\2\2\2\u015d\u015e\3\2\2\2\u015e\u0160\3\2\2\2\u015f"+
		"\u015d\3\2\2\2\u0160\u0166\7\7\2\2\u0161\u0163\5\f\7\2\u0162\u0164\7\13"+
		"\2\2\u0163\u0162\3\2\2\2\u0163\u0164\3\2\2\2\u0164\u0166\3\2\2\2\u0165"+
		"\u0156\3\2\2\2\u0165\u0161\3\2\2\2\u0166\31\3\2\2\2\u0167\u0168\7_\2\2"+
		"\u0168\u0169\5\26\f\2\u0169\33\3\2\2\2\u016a\u016b\5\32\16\2\u016b\u016f"+
		"\5\30\r\2\u016c\u016e\5\36\20\2\u016d\u016c\3\2\2\2\u016e\u0171\3\2\2"+
		"\2\u016f\u016d\3\2\2\2\u016f\u0170\3\2\2\2\u0170\35\3\2\2\2\u0171\u016f"+
		"\3\2\2\2\u0172\u0175\7N\2\2\u0173\u0176\5\34\17\2\u0174\u0176\5\30\r\2"+
		"\u0175\u0173\3\2\2\2\u0175\u0174\3\2\2\2\u0176\37\3\2\2\2\u0177\u0178"+
		"\5\26\f\2\u0178\u0179\7\b\2\2\u0179\u017a\5\"\22\2\u017a!\3\2\2\2\u017b"+
		"\u017d\7\r\2\2\u017c\u017b\3\2\2\2\u017c\u017d\3\2\2\2\u017d\u017e\3\2"+
		"\2\2\u017e\u0180\5&\24\2\u017f\u0181\7\16\2\2\u0180\u017f\3\2\2\2\u0180"+
		"\u0181\3\2\2\2\u0181\u0182\3\2\2\2\u0182\u0184\7\t\2\2\u0183\u0185\7\r"+
		"\2\2\u0184\u0183\3\2\2\2\u0184\u0185\3\2\2\2\u0185\u0186\3\2\2\2\u0186"+
		"\u0188\5$\23\2\u0187\u0189\7\16\2\2\u0188\u0187\3\2\2\2\u0188\u0189\3"+
		"\2\2\2\u0189#\3\2\2\2\u018a\u018d\5l\67\2\u018b\u018d\5(\25\2\u018c\u018a"+
		"\3\2\2\2\u018c\u018b\3\2\2\2\u018d%\3\2\2\2\u018e\u0191\5l\67\2\u018f"+
		"\u0191\5(\25\2\u0190\u018e\3\2\2\2\u0190\u018f\3\2\2\2\u0191\'\3\2\2\2"+
		"\u0192\u0193\5 \21\2\u0193)\3\2\2\2\u0194\u0195\5\u00d6l\2\u0195\u0196"+
		"\5\60\31\2\u0196\u0198\7\20\2\2\u0197\u0199\t\2\2\2\u0198\u0197\3\2\2"+
		"\2\u0198\u0199\3\2\2\2\u0199\u019b\3\2\2\2\u019a\u0194\3\2\2\2\u019a\u019b"+
		"\3\2\2\2\u019b\u01a0\3\2\2\2\u019c\u01a1\5,\27\2\u019d\u01a1\5.\30\2\u019e"+
		"\u01a1\5\u00b6\\\2\u019f\u01a1\5<\37\2\u01a0\u019c\3\2\2\2\u01a0\u019d"+
		"\3\2\2\2\u01a0\u019e\3\2\2\2\u01a0\u019f\3\2\2\2\u01a1+\3\2\2\2\u01a2"+
		"\u01a3\5\u00d6l\2\u01a3\u01a4\7\22\2\2\u01a4\u01a5\7\22\2\2\u01a5\u01aa"+
		"\3\2\2\2\u01a6\u01a7\7\22\2\2\u01a7\u01a8\7\22\2\2\u01a8\u01aa\5\u00d6"+
		"l\2\u01a9\u01a2\3\2\2\2\u01a9\u01a6\3\2\2\2\u01aa-\3\2\2\2\u01ab\u01ac"+
		"\5\u00d6l\2\u01ac\u01ad\7\23\2\2\u01ad\u01b1\3\2\2\2\u01ae\u01af\7\23"+
		"\2\2\u01af\u01b1\5\u00d6l\2\u01b0\u01ab\3\2\2\2\u01b0\u01ae\3\2\2\2\u01b1"+
		"/\3\2\2\2\u01b2\u01b3\t\3\2\2\u01b3\61\3\2\2\2\u01b4\u01b5\7X\2\2\u01b5"+
		"\u01b6\7\r\2\2\u01b6\u01b7\5\22\n\2\u01b7\u01b8\7\20\2\2\u01b8\u01b9\5"+
		"\u009eP\2\u01b9\u01ba\7\13\2\2\u01ba\u01bb\5l\67\2\u01bb\u01bc\7\13\2"+
		"\2\u01bc\u01bd\5*\26\2\u01bd\u01be\7\16\2\2\u01be\u01bf\5\30\r\2\u01bf"+
		"\63\3\2\2\2\u01c0\u01c1\7X\2\2\u01c1\u01c2\7\r\2\2\u01c2\u01c3\5\22\n"+
		"\2\u01c3\u01c4\7\t\2\2\u01c4\u01c5\5\u00d6l\2\u01c5\u01c6\7\16\2\2\u01c6"+
		"\u01c7\5\30\r\2\u01c7\65\3\2\2\2\u01c8\u01cd\58\35\2\u01c9\u01ca\7\17"+
		"\2\2\u01ca\u01cc\58\35\2\u01cb\u01c9\3\2\2\2\u01cc\u01cf\3\2\2\2\u01cd"+
		"\u01cb\3\2\2\2\u01cd\u01ce\3\2\2\2\u01ce\67\3\2\2\2\u01cf\u01cd\3\2\2"+
		"\2\u01d0\u01d3\5\22\n\2\u01d1\u01d2\7\20\2\2\u01d2\u01d4\5l\67\2\u01d3"+
		"\u01d1\3\2\2\2\u01d3\u01d4\3\2\2\2\u01d49\3\2\2\2\u01d5\u01d8\5l\67\2"+
		"\u01d6\u01d8\5*\26\2\u01d7\u01d5\3\2\2\2\u01d7\u01d6\3\2\2\2\u01d8\u01e0"+
		"\3\2\2\2\u01d9\u01dc\7\17\2\2\u01da\u01dd\5l\67\2\u01db\u01dd\5*\26\2"+
		"\u01dc\u01da\3\2\2\2\u01dc\u01db\3\2\2\2\u01dd\u01df\3\2\2\2\u01de\u01d9"+
		"\3\2\2\2\u01df\u01e2\3\2\2\2\u01e0\u01de\3\2\2\2\u01e0\u01e1\3\2\2\2\u01e1"+
		";\3\2\2\2\u01e2\u01e0\3\2\2\2\u01e3\u01e4\5\u00c4c\2\u01e4\u01e6\7\r\2"+
		"\2\u01e5\u01e7\5:\36\2\u01e6\u01e5\3\2\2\2\u01e6\u01e7\3\2\2\2\u01e7\u01e8"+
		"\3\2\2\2\u01e8\u01e9\7\16\2\2\u01e9=\3\2\2\2\u01ea\u01eb\5\u00c4c\2\u01eb"+
		"\u01ef\7\r\2\2\u01ec\u01ee\5\66\34\2\u01ed\u01ec\3\2\2\2\u01ee\u01f1\3"+
		"\2\2\2\u01ef\u01ed\3\2\2\2\u01ef\u01f0\3\2\2\2\u01f0\u01f2\3\2\2\2\u01f1"+
		"\u01ef\3\2\2\2\u01f2\u01f3\7\16\2\2\u01f3\u01fa\7\6\2\2\u01f4\u01f6\5"+
		"\16\b\2\u01f5\u01f7\7\13\2\2\u01f6\u01f5\3\2\2\2\u01f6\u01f7\3\2\2\2\u01f7"+
		"\u01f9\3\2\2\2\u01f8\u01f4\3\2\2\2\u01f9\u01fc\3\2\2\2\u01fa\u01f8\3\2"+
		"\2\2\u01fa\u01fb\3\2\2\2\u01fb\u0201\3\2\2\2\u01fc\u01fa\3\2\2\2\u01fd"+
		"\u01fe\7\u008f\2\2\u01fe\u01ff\5l\67\2\u01ff\u0200\7\13\2\2\u0200\u0202"+
		"\3\2\2\2\u0201\u01fd\3\2\2\2\u0201\u0202\3\2\2\2\u0202\u0203\3\2\2\2\u0203"+
		"\u0204\7\7\2\2\u0204?\3\2\2\2\u0205\u0206\7\u00ac\2\2\u0206\u0207\5\26"+
		"\f\2\u0207\u0208\5\30\r\2\u0208A\3\2\2\2\u0209\u020a\7K\2\2\u020a\u020b"+
		"\5\30\r\2\u020b\u020c\7\u00ac\2\2\u020c\u020d\5\26\f\2\u020d\u020e\7\13"+
		"\2\2\u020eC\3\2\2\2\u020f\u0210\7\u0096\2\2\u0210\u0214\7\r\2\2\u0211"+
		"\u0215\7\u00b0\2\2\u0212\u0215\7\u00b1\2\2\u0213\u0215\5<\37\2\u0214\u0211"+
		"\3\2\2\2\u0214\u0212\3\2\2\2\u0214\u0213\3\2\2\2\u0215\u0216\3\2\2\2\u0216"+
		"\u0217\7\16\2\2\u0217\u021b\7\6\2\2\u0218\u021a\5F$\2\u0219\u0218\3\2"+
		"\2\2\u021a\u021d\3\2\2\2\u021b\u0219\3\2\2\2\u021b\u021c\3\2\2\2\u021c"+
		"\u021f\3\2\2\2\u021d\u021b\3\2\2\2\u021e\u0220\5H%\2\u021f\u021e\3\2\2"+
		"\2\u021f\u0220\3\2\2\2\u0220\u0221\3\2\2\2\u0221\u0222\7\7\2\2\u0222E"+
		"\3\2\2\2\u0223\u0226\7\65\2\2\u0224\u0227\5\u00d6l\2\u0225\u0227\5\u009e"+
		"P\2\u0226\u0224\3\2\2\2\u0226\u0225\3\2\2\2\u0227\u0228\3\2\2\2\u0228"+
		"\u023b\7\t\2\2\u0229\u022b\5\f\7\2\u022a\u0229\3\2\2\2\u022b\u022e\3\2"+
		"\2\2\u022c\u022a\3\2\2\2\u022c\u022d\3\2\2\2\u022d\u022f\3\2\2\2\u022e"+
		"\u022c\3\2\2\2\u022f\u0230\7\63\2\2\u0230\u023c\7\13\2\2\u0231\u0235\7"+
		"\6\2\2\u0232\u0234\5\f\7\2\u0233\u0232\3\2\2\2\u0234\u0237\3\2\2\2\u0235"+
		"\u0233\3\2\2\2\u0235\u0236\3\2\2\2\u0236\u0238\3\2\2\2\u0237\u0235\3\2"+
		"\2\2\u0238\u0239\7\63\2\2\u0239\u023a\7\13\2\2\u023a\u023c\7\7\2\2\u023b"+
		"\u022c\3\2\2\2\u023b\u0231\3\2\2\2\u023cG\3\2\2\2\u023d\u023e\7D\2\2\u023e"+
		"\u024d\7\t\2\2\u023f\u0241\5\f\7\2\u0240\u023f\3\2\2\2\u0241\u0244\3\2"+
		"\2\2\u0242\u0240\3\2\2\2\u0242\u0243\3\2\2\2\u0243\u024e\3\2\2\2\u0244"+
		"\u0242\3\2\2\2\u0245\u0249\7\6\2\2\u0246\u0248\5\f\7\2\u0247\u0246\3\2"+
		"\2\2\u0248\u024b\3\2\2\2\u0249\u0247\3\2\2\2\u0249\u024a\3\2\2\2\u024a"+
		"\u024c\3\2\2\2\u024b\u0249\3\2\2\2\u024c\u024e\7\7\2\2\u024d\u0242\3\2"+
		"\2\2\u024d\u0245\3\2\2\2\u024eI\3\2\2\2\u024f\u0250\7\u0084\2\2\u0250"+
		"\u0254\7\r\2\2\u0251\u0253\5l\67\2\u0252\u0251\3\2\2\2\u0253\u0256\3\2"+
		"\2\2\u0254\u0252\3\2\2\2\u0254\u0255\3\2\2\2\u0255\u0257\3\2\2\2\u0256"+
		"\u0254\3\2\2\2\u0257\u0259\7\16\2\2\u0258\u025a\7\13\2\2\u0259\u0258\3"+
		"\2\2\2\u025a\u025b\3\2\2\2\u025b\u0259\3\2\2\2\u025b\u025c\3\2\2\2\u025c"+
		"K\3\2\2\2\u025d\u0268\5\u009cO\2\u025e\u025f\7\177\2\2\u025f\u0260\7\62"+
		"\2\2\u0260\u0265\5\u008aF\2\u0261\u0262\7\17\2\2\u0262\u0264\5\u008aF"+
		"\2\u0263\u0261\3\2\2\2\u0264\u0267\3\2\2\2\u0265\u0263\3\2\2\2\u0265\u0266"+
		"\3\2\2\2\u0266\u0269\3\2\2\2\u0267\u0265\3\2\2\2\u0268\u025e\3\2\2\2\u0268"+
		"\u0269\3\2\2\2\u0269\u0270\3\2\2\2\u026a\u026b\7r\2\2\u026b\u026e\5l\67"+
		"\2\u026c\u026d\t\4\2\2\u026d\u026f\5l\67\2\u026e\u026c\3\2\2\2\u026e\u026f"+
		"\3\2\2\2\u026f\u0271\3\2\2\2\u0270\u026a\3\2\2\2\u0270\u0271\3\2\2\2\u0271"+
		"M\3\2\2\2\u0272\u027d\5P)\2\u0273\u0274\7\177\2\2\u0274\u0275\7\62\2\2"+
		"\u0275\u027a\5\u008aF\2\u0276\u0277\7\17\2\2\u0277\u0279\5\u008aF\2\u0278"+
		"\u0276\3\2\2\2\u0279\u027c\3\2\2\2\u027a\u0278\3\2\2\2\u027a\u027b\3\2"+
		"\2\2\u027b\u027e\3\2\2\2\u027c\u027a\3\2\2\2\u027d\u0273\3\2\2\2\u027d"+
		"\u027e\3\2\2\2\u027e\u0285\3\2\2\2\u027f\u0280\7r\2\2\u0280\u0283\5l\67"+
		"\2\u0281\u0282\t\4\2\2\u0282\u0284\5l\67\2\u0283\u0281\3\2\2\2\u0283\u0284"+
		"\3\2\2\2\u0284\u0286\3\2\2\2\u0285\u027f\3\2\2\2\u0285\u0286\3\2\2\2\u0286"+
		"O\3\2\2\2\u0287\u0289\7\u0094\2\2\u0288\u028a\t\5\2\2\u0289\u0288\3\2"+
		"\2\2\u0289\u028a\3\2\2\2\u028a\u028b\3\2\2\2\u028b\u0290\5\u008cG\2\u028c"+
		"\u028d\7\17\2\2\u028d\u028f\5\u008cG\2\u028e\u028c\3\2\2\2\u028f\u0292"+
		"\3\2\2\2\u0290\u028e\3\2\2\2\u0290\u0291\3\2\2\2\u0291\u0294\3\2\2\2\u0292"+
		"\u0290\3\2\2\2\u0293\u0295\5R*\2\u0294\u0293\3\2\2\2\u0294\u0295\3\2\2"+
		"\2\u0295\u0297\3\2\2\2\u0296\u0298\5T+\2\u0297\u0296\3\2\2\2\u0297\u0298"+
		"\3\2\2\2\u0298\u029d\3\2\2\2\u0299\u029b\5V,\2\u029a\u029c\5X-\2\u029b"+
		"\u029a\3\2\2\2\u029b\u029c\3\2\2\2\u029c\u029e\3\2\2\2\u029d\u0299\3\2"+
		"\2\2\u029d\u029e\3\2\2\2\u029e\u02bc\3\2\2\2\u029f\u02a0\7\u00a4\2\2\u02a0"+
		"\u02a1\7\r\2\2\u02a1\u02a6\5l\67\2\u02a2\u02a3\7\17\2\2\u02a3\u02a5\5"+
		"l\67\2\u02a4\u02a2\3\2\2\2\u02a5\u02a8\3\2\2\2\u02a6\u02a4\3\2\2\2\u02a6"+
		"\u02a7\3\2\2\2\u02a7\u02a9\3\2\2\2\u02a8\u02a6\3\2\2\2\u02a9\u02b8\7\16"+
		"\2\2\u02aa\u02ab\7\17\2\2\u02ab\u02ac\7\r\2\2\u02ac\u02b1\5l\67\2\u02ad"+
		"\u02ae\7\17\2\2\u02ae\u02b0\5l\67\2\u02af\u02ad\3\2\2\2\u02b0\u02b3\3"+
		"\2\2\2\u02b1\u02af\3\2\2\2\u02b1\u02b2\3\2\2\2\u02b2\u02b4\3\2\2\2\u02b3"+
		"\u02b1\3\2\2\2\u02b4\u02b5\7\16\2\2\u02b5\u02b7\3\2\2\2\u02b6\u02aa\3"+
		"\2\2\2\u02b7\u02ba\3\2\2\2\u02b8\u02b6\3\2\2\2\u02b8\u02b9\3\2\2\2\u02b9"+
		"\u02bc\3\2\2\2\u02ba\u02b8\3\2\2\2\u02bb\u0287\3\2\2\2\u02bb\u029f\3\2"+
		"\2\2\u02bcQ\3\2\2\2\u02bd\u02c7\7Z\2\2\u02be\u02c3\5\u008eH\2\u02bf\u02c0"+
		"\7\17\2\2\u02c0\u02c2\5\u008eH\2\u02c1\u02bf\3\2\2\2\u02c2\u02c5\3\2\2"+
		"\2\u02c3\u02c1\3\2\2\2\u02c3\u02c4\3\2\2\2\u02c4\u02c8\3\2\2\2\u02c5\u02c3"+
		"\3\2\2\2\u02c6\u02c8\5\u0096L\2\u02c7\u02be\3\2\2\2\u02c7\u02c6\3\2\2"+
		"\2\u02c8S\3\2\2\2\u02c9\u02ca\7\u00a9\2\2\u02ca\u02cb\5l\67\2\u02cbU\3"+
		"\2\2\2\u02cc\u02cd\7]\2\2\u02cd\u02ce\7\62\2\2\u02ce\u02d3\5l\67\2\u02cf"+
		"\u02d0\7\17\2\2\u02d0\u02d2\5l\67\2\u02d1\u02cf\3\2\2\2\u02d2\u02d5\3"+
		"\2\2\2\u02d3\u02d1\3\2\2\2\u02d3\u02d4\3\2\2\2\u02d4W\3\2\2\2\u02d5\u02d3"+
		"\3\2\2\2\u02d6\u02d7\7^\2\2\u02d7\u02d8\5l\67\2\u02d8Y\3\2\2\2\u02d9\u02de"+
		"\5\u00caf\2\u02da\u02dd\5^\60\2\u02db\u02dd\5\\/\2\u02dc\u02da\3\2\2\2"+
		"\u02dc\u02db\3\2\2\2\u02dd\u02e0\3\2\2\2\u02de\u02dc\3\2\2\2\u02de\u02df"+
		"\3\2\2\2\u02df[\3\2\2\2\u02e0\u02de\3\2\2\2\u02e1\u02f5\5\u00c2b\2\u02e2"+
		"\u02e3\7\r\2\2\u02e3\u02e5\5\u009eP\2\u02e4\u02e6\5\u00d6l\2\u02e5\u02e4"+
		"\3\2\2\2\u02e5\u02e6\3\2\2\2\u02e6\u02e7\3\2\2\2\u02e7\u02e8\7\16\2\2"+
		"\u02e8\u02f6\3\2\2\2\u02e9\u02ea\7\r\2\2\u02ea\u02ec\5\u009eP\2\u02eb"+
		"\u02ed\5\u00d6l\2\u02ec\u02eb\3\2\2\2\u02ec\u02ed\3\2\2\2\u02ed\u02ee"+
		"\3\2\2\2\u02ee\u02ef\7\17\2\2\u02ef\u02f1\5\u009eP\2\u02f0\u02f2\5\u00d6"+
		"l\2\u02f1\u02f0\3\2\2\2\u02f1\u02f2\3\2\2\2\u02f2\u02f3\3\2\2\2\u02f3"+
		"\u02f4\7\16\2\2\u02f4\u02f6\3\2\2\2\u02f5\u02e2\3\2\2\2\u02f5\u02e9\3"+
		"\2\2\2\u02f5\u02f6\3\2\2\2\u02f6]\3\2\2\2\u02f7\u02f8\7<\2\2\u02f8\u02fa"+
		"\5\u00c2b\2\u02f9\u02f7\3\2\2\2\u02f9\u02fa\3\2\2\2\u02fa\u0308\3\2\2"+
		"\2\u02fb\u0309\5`\61\2\u02fc\u0309\5b\62\2\u02fd\u0309\5d\63\2\u02fe\u0309"+
		"\5f\64\2\u02ff\u0309\7\u00a0\2\2\u0300\u0301\7\67\2\2\u0301\u0302\7\r"+
		"\2\2\u0302\u0303\5l\67\2\u0303\u0304\7\16\2\2\u0304\u0309\3\2\2\2\u0305"+
		"\u0309\5h\65\2\u0306\u0307\78\2\2\u0307\u0309\5\u00ccg\2\u0308\u02fb\3"+
		"\2\2\2\u0308\u02fc\3\2\2\2\u0308\u02fd\3\2\2\2\u0308\u02fe\3\2\2\2\u0308"+
		"\u02ff\3\2\2\2\u0308\u0300\3\2\2\2\u0308\u0305\3\2\2\2\u0308\u0306\3\2"+
		"\2\2\u0309_\3\2\2\2\u030a\u030b\7\u0083\2\2\u030b\u030d\7o\2\2\u030c\u030e"+
		"\t\6\2\2\u030d\u030c\3\2\2\2\u030d\u030e\3\2\2\2\u030e\u0310\3\2\2\2\u030f"+
		"\u0311\7.\2\2\u0310\u030f\3\2\2\2\u0310\u0311\3\2\2\2\u0311a\3\2\2\2\u0312"+
		"\u0313\5x=\2\u0313c\3\2\2\2\u0314\u0315\7w\2\2\u0315\u0316\7y\2\2\u0316"+
		"e\3\2\2\2\u0317\u0318\7y\2\2\u0318g\3\2\2\2\u0319\u0325\7D\2\2\u031a\u0326"+
		"\5j\66\2\u031b\u031c\7\r\2\2\u031c\u031d\5l\67\2\u031d\u031e\7\16\2\2"+
		"\u031e\u0326\3\2\2\2\u031f\u0320\7u\2\2\u0320\u0321\7\r\2\2\u0321\u0322"+
		"\5l\67\2\u0322\u0323\7\16\2\2\u0323\u0326\3\2\2\2\u0324\u0326\5\u00d6"+
		"l\2\u0325\u031a\3\2\2\2\u0325\u031b\3\2\2\2\u0325\u031f\3\2\2\2\u0325"+
		"\u0324\3\2\2\2\u0326\u032d\3\2\2\2\u0327\u0329\7\n\2\2\u0328\u032a\5\u00d6"+
		"l\2\u0329\u0328\3\2\2\2\u032a\u032b\3\2\2\2\u032b\u0329\3\2\2\2\u032b"+
		"\u032c\3\2\2\2\u032c\u032e\3\2\2\2\u032d\u0327\3\2\2\2\u032d\u032e\3\2"+
		"\2\2\u032ei\3\2\2\2\u032f\u0332\5\u009eP\2\u0330\u0332\5\u00b6\\\2\u0331"+
		"\u032f\3\2\2\2\u0331\u0330\3\2\2\2\u0332k\3\2\2\2\u0333\u0334\b\67\1\2"+
		"\u0334\u0340\5\u00b6\\\2\u0335\u0340\5n8\2\u0336\u0340\5p9\2\u0337\u0340"+
		"\5r:\2\u0338\u0339\7\r\2\2\u0339\u033a\5l\67\2\u033a\u033b\7\16\2\2\u033b"+
		"\u0340\3\2\2\2\u033c\u0340\5v<\2\u033d\u0340\7\u009c\2\2\u033e\u0340\7"+
		"W\2\2\u033f\u0333\3\2\2\2\u033f\u0335\3\2\2\2\u033f\u0336\3\2\2\2\u033f"+
		"\u0337\3\2\2\2\u033f\u0338\3\2\2\2\u033f\u033c\3\2\2\2\u033f\u033d\3\2"+
		"\2\2\u033f\u033e\3\2\2\2\u0340\u036e\3\2\2\2\u0341\u0342\f\20\2\2\u0342"+
		"\u0343\7\25\2\2\u0343\u036d\5l\67\21\u0344\u0345\f\17\2\2\u0345\u0346"+
		"\t\7\2\2\u0346\u036d\5l\67\20\u0347\u0348\f\16\2\2\u0348\u0349\t\2\2\2"+
		"\u0349\u036d\5l\67\17\u034a\u034b\f\r\2\2\u034b\u034c\t\b\2\2\u034c\u036d"+
		"\5l\67\16\u034d\u034e\f\f\2\2\u034e\u034f\t\t\2\2\u034f\u036d\5l\67\r"+
		"\u0350\u035d\f\13\2\2\u0351\u035e\7\20\2\2\u0352\u035e\7 \2\2\u0353\u035e"+
		"\7!\2\2\u0354\u035e\7\"\2\2\u0355\u035e\7k\2\2\u0356\u0357\7k\2\2\u0357"+
		"\u035e\7w\2\2\u0358\u035e\7b\2\2\u0359\u035e\7q\2\2\u035a\u035e\7\\\2"+
		"\2\u035b\u035e\7s\2\2\u035c\u035e\7\u0089\2\2\u035d\u0351\3\2\2\2\u035d"+
		"\u0352\3\2\2\2\u035d\u0353\3\2\2\2\u035d\u0354\3\2\2\2\u035d\u0355\3\2"+
		"\2\2\u035d\u0356\3\2\2\2\u035d\u0358\3\2\2\2\u035d\u0359\3\2\2\2\u035d"+
		"\u035a\3\2\2\2\u035d\u035b\3\2\2\2\u035d\u035c\3\2\2\2\u035e\u035f\3\2"+
		"\2\2\u035f\u036d\5l\67\f\u0360\u0361\f\n\2\2\u0361\u0362\7*\2\2\u0362"+
		"\u036d\5l\67\13\u0363\u0364\f\t\2\2\u0364\u0365\7~\2\2\u0365\u036d\5l"+
		"\67\n\u0366\u0368\f\6\2\2\u0367\u0369\7w\2\2\u0368\u0367\3\2\2\2\u0368"+
		"\u0369\3\2\2\2\u0369\u036a\3\2\2\2\u036a\u036b\7b\2\2\u036b\u036d\5t;"+
		"\2\u036c\u0341\3\2\2\2\u036c\u0344\3\2\2\2\u036c\u0347\3\2\2\2\u036c\u034a"+
		"\3\2\2\2\u036c\u034d\3\2\2\2\u036c\u0350\3\2\2\2\u036c\u0360\3\2\2\2\u036c"+
		"\u0363\3\2\2\2\u036c\u0366\3\2\2\2\u036d\u0370\3\2\2\2\u036e\u036c\3\2"+
		"\2\2\u036e\u036f\3\2\2\2\u036fm\3\2\2\2\u0370\u036e\3\2\2\2\u0371\u0372"+
		"\5\u00c6d\2\u0372\u0373\7\f\2\2\u0373\u0375\3\2\2\2\u0374\u0371\3\2\2"+
		"\2\u0374\u0375\3\2\2\2\u0375\u0376\3\2\2\2\u0376\u0377\5\u00c8e\2\u0377"+
		"\u0378\7\f\2\2\u0378\u037a\3\2\2\2\u0379\u0374\3\2\2\2\u0379\u037a\3\2"+
		"\2\2\u037a\u037b\3\2\2\2\u037b\u037c\5\u00caf\2\u037co\3\2\2\2\u037d\u037e"+
		"\5\u00b8]\2\u037e\u037f\5l\67\2\u037fq\3\2\2\2\u0380\u0381\5\u00c4c\2"+
		"\u0381\u038e\7\r\2\2\u0382\u0384\7J\2\2\u0383\u0382\3\2\2\2\u0383\u0384"+
		"\3\2\2\2\u0384\u0385\3\2\2\2\u0385\u038a\5l\67\2\u0386\u0387\7\17\2\2"+
		"\u0387\u0389\5l\67\2\u0388\u0386\3\2\2\2\u0389\u038c\3\2\2\2\u038a\u0388"+
		"\3\2\2\2\u038a\u038b\3\2\2\2\u038b\u038f\3\2\2\2\u038c\u038a\3\2\2\2\u038d"+
		"\u038f\7\21\2\2\u038e\u0383\3\2\2\2\u038e\u038d\3\2\2\2\u038e\u038f\3"+
		"\2\2\2\u038f\u0390\3\2\2\2\u0390\u0391\7\16\2\2\u0391s\3\2\2\2\u0392\u039c"+
		"\7\r\2\2\u0393\u039d\5N(\2\u0394\u0399\5l\67\2\u0395\u0396\7\17\2\2\u0396"+
		"\u0398\5l\67\2\u0397\u0395\3\2\2\2\u0398\u039b\3\2\2\2\u0399\u0397\3\2"+
		"\2\2\u0399\u039a\3\2\2\2\u039a\u039d\3\2\2\2\u039b\u0399\3\2\2\2\u039c"+
		"\u0393\3\2\2\2\u039c\u0394\3\2\2\2\u039c\u039d\3\2\2\2\u039d\u039e\3\2"+
		"\2\2\u039e\u03a6\7\16\2\2\u039f\u03a0\5\u00c6d\2\u03a0\u03a1\7\f\2\2\u03a1"+
		"\u03a3\3\2\2\2\u03a2\u039f\3\2\2\2\u03a2\u03a3\3\2\2\2\u03a3\u03a4\3\2"+
		"\2\2\u03a4\u03a6\5\u00c8e\2\u03a5\u0392\3\2\2\2\u03a5\u03a2\3\2\2\2\u03a6"+
		"u\3\2\2\2\u03a7\u03a9\7w\2\2\u03a8\u03a7\3\2\2\2\u03a8\u03a9\3\2\2\2\u03a9"+
		"\u03aa\3\2\2\2\u03aa\u03ac\7T\2\2\u03ab\u03a8\3\2\2\2\u03ab\u03ac\3\2"+
		"\2\2\u03ac\u03ad\3\2\2\2\u03ad\u03ae\7\r\2\2\u03ae\u03af\5N(\2\u03af\u03b0"+
		"\7\16\2\2\u03b0w\3\2\2\2\u03b1\u03b5\7\u0088\2\2\u03b2\u03b3\5\u00c6d"+
		"\2\u03b3\u03b4\7\f\2\2\u03b4\u03b6\3\2\2\2\u03b5\u03b2\3\2\2\2\u03b5\u03b6"+
		"\3\2\2\2\u03b6\u03b7\3\2\2\2\u03b7\u03c3\5\u00ceh\2\u03b8\u03b9\7\r\2"+
		"\2\u03b9\u03be\5z>\2\u03ba\u03bb\7\17\2\2\u03bb\u03bd\5z>\2\u03bc\u03ba"+
		"\3\2\2\2\u03bd\u03c0\3\2\2\2\u03be\u03bc\3\2\2\2\u03be\u03bf\3\2\2\2\u03bf"+
		"\u03c1\3\2\2\2\u03c0\u03be\3\2\2\2\u03c1\u03c2\7\16\2\2\u03c2\u03c4\3"+
		"\2\2\2\u03c3\u03b8\3\2\2\2\u03c3\u03c4\3\2\2\2\u03c4\u03d7\3\2\2\2\u03c5"+
		"\u03c6\7|\2\2\u03c6\u03cf\t\n\2\2\u03c7\u03c8\7\u0095\2\2\u03c8\u03d0"+
		"\7y\2\2\u03c9\u03ca\7\u0095\2\2\u03ca\u03d0\7D\2\2\u03cb\u03d0\7\64\2"+
		"\2\u03cc\u03d0\7\u008e\2\2\u03cd\u03ce\7v\2\2\u03ce\u03d0\7$\2\2\u03cf"+
		"\u03c7\3\2\2\2\u03cf\u03c9\3\2\2\2\u03cf\u03cb\3\2\2\2\u03cf\u03cc\3\2"+
		"\2\2\u03cf\u03cd\3\2\2\2\u03d0\u03d4\3\2\2\2\u03d1\u03d2\7s\2\2\u03d2"+
		"\u03d4\5\u00c2b\2\u03d3\u03c5\3\2\2\2\u03d3\u03d1\3\2\2\2\u03d4\u03d6"+
		"\3\2\2\2\u03d5\u03d3\3\2\2\2\u03d6\u03d9\3\2\2\2\u03d7\u03d5\3\2\2\2\u03d7"+
		"\u03d8\3\2\2\2\u03d8\u03e7\3\2\2\2\u03d9\u03d7\3\2\2\2\u03da\u03dc\7w"+
		"\2\2\u03db\u03da\3\2\2\2\u03db\u03dc\3\2\2\2\u03dc\u03dd\3\2\2\2\u03dd"+
		"\u03e2\7E\2\2\u03de\u03df\7e\2\2\u03df\u03e3\7F\2\2\u03e0\u03e1\7e\2\2"+
		"\u03e1\u03e3\7a\2\2\u03e2\u03de\3\2\2\2\u03e2\u03e0\3\2\2\2\u03e2\u03e3"+
		"\3\2\2\2\u03e3\u03e5\3\2\2\2\u03e4\u03e6\7P\2\2\u03e5\u03e4\3\2\2\2\u03e5"+
		"\u03e6\3\2\2\2\u03e6\u03e8\3\2\2\2\u03e7\u03db\3\2\2\2\u03e7\u03e8\3\2"+
		"\2\2\u03e8y\3\2\2\2\u03e9\u03ea\5\u00c2b\2\u03ea{\3\2\2\2\u03eb\u03ee"+
		"\5\u00caf\2\u03ec\u03ed\78\2\2\u03ed\u03ef\5\u00ccg\2\u03ee\u03ec\3\2"+
		"\2\2\u03ee\u03ef\3\2\2\2\u03ef\u03f1\3\2\2\2\u03f0\u03f2\t\6\2\2\u03f1"+
		"\u03f0\3\2\2\2\u03f1\u03f2\3\2\2\2\u03f2}\3\2\2\2\u03f3\u03f4\7<\2\2\u03f4"+
		"\u03f6\5\u00c2b\2\u03f5\u03f3\3\2\2\2\u03f5\u03f6\3\2\2\2\u03f6\u0400"+
		"\3\2\2\2\u03f7\u0401\5\u0080A\2\u03f8\u0401\5\u0086D\2\u03f9\u0401\5\u0084"+
		"C\2\u03fa\u03fb\7\67\2\2\u03fb\u03fc\7\r\2\2\u03fc\u03fd\5l\67\2\u03fd"+
		"\u03fe\7\16\2\2\u03fe\u0401\3\2\2\2\u03ff\u0401\5\u0082B\2\u0400\u03f7"+
		"\3\2\2\2\u0400\u03f8\3\2\2\2\u0400\u03f9\3\2\2\2\u0400\u03fa\3\2\2\2\u0400"+
		"\u03ff\3\2\2\2\u0401\177\3\2\2\2\u0402\u0403\7\u0083\2\2\u0403\u0404\7"+
		"o\2\2\u0404\u0405\7\r\2\2\u0405\u040a\5|?\2\u0406\u0407\7\17\2\2\u0407"+
		"\u0409\5|?\2\u0408\u0406\3\2\2\2\u0409\u040c\3\2\2\2\u040a\u0408\3\2\2"+
		"\2\u040a\u040b\3\2\2\2\u040b\u040d\3\2\2\2\u040c\u040a\3\2\2\2\u040d\u040e"+
		"\7\16\2\2\u040e\u0081\3\2\2\2\u040f\u0410\7Y\2\2\u0410\u0411\7o\2\2\u0411"+
		"\u0412\7\r\2\2\u0412\u0417\5\u0088E\2\u0413\u0414\7\17\2\2\u0414\u0416"+
		"\5\u0088E\2\u0415\u0413\3\2\2\2\u0416\u0419\3\2\2\2\u0417\u0415\3\2\2"+
		"\2\u0417\u0418\3\2\2\2\u0418\u041a\3\2\2\2\u0419\u0417\3\2\2\2\u041a\u041b"+
		"\7\16\2\2\u041b\u041c\5x=\2\u041c\u0083\3\2\2\2\u041d\u041f\7\u00a0\2"+
		"\2\u041e\u0420\7o\2\2\u041f\u041e\3\2\2\2\u041f\u0420\3\2\2\2\u0420\u0422"+
		"\3\2\2\2\u0421\u0423\5\u00c2b\2\u0422\u0421\3\2\2\2\u0422\u0423\3\2\2"+
		"\2\u0423\u0424\3\2\2\2\u0424\u0425\7\r\2\2\u0425\u042a\5|?\2\u0426\u0427"+
		"\7\17\2\2\u0427\u0429\5|?\2\u0428\u0426\3\2\2\2\u0429\u042c\3\2\2\2\u042a"+
		"\u0428\3\2\2\2\u042a\u042b\3\2\2\2\u042b\u042d\3\2\2\2\u042c\u042a\3\2"+
		"\2\2\u042d\u042e\7\16\2\2\u042e\u0085\3\2\2\2\u042f\u0431\7o\2\2\u0430"+
		"\u0432\5\u00c2b\2\u0431\u0430\3\2\2\2\u0431\u0432\3\2\2\2\u0432\u0433"+
		"\3\2\2\2\u0433\u0434\7\r\2\2\u0434\u0439\5|?\2\u0435\u0436\7\17\2\2\u0436"+
		"\u0438\5|?\2\u0437\u0435\3\2\2\2\u0438\u043b\3\2\2\2\u0439\u0437\3\2\2"+
		"\2\u0439\u043a\3\2\2\2\u043a\u043c\3\2\2\2\u043b\u0439\3\2\2\2\u043c\u043d"+
		"\7\16\2\2\u043d\u0087\3\2\2\2\u043e\u043f\5\u00caf\2\u043f\u0089\3\2\2"+
		"\2\u0440\u0442\5l\67\2\u0441\u0443\t\6\2\2\u0442\u0441\3\2\2\2\u0442\u0443"+
		"\3\2\2\2\u0443\u008b\3\2\2\2\u0444\u0451\7\21\2\2\u0445\u0446\5\u00c8"+
		"e\2\u0446\u0447\7\f\2\2\u0447\u0448\7\21\2\2\u0448\u0451\3\2\2\2\u0449"+
		"\u044e\5l\67\2\u044a\u044c\7+\2\2\u044b\u044a\3\2\2\2\u044b\u044c\3\2"+
		"\2\2\u044c\u044d\3\2\2\2\u044d\u044f\5\u00be`\2\u044e\u044b\3\2\2\2\u044e"+
		"\u044f\3\2\2\2\u044f\u0451\3\2\2\2\u0450\u0444\3\2\2\2\u0450\u0445\3\2"+
		"\2\2\u0450\u0449\3\2\2\2\u0451\u008d\3\2\2\2\u0452\u0456\5\u0090I\2\u0453"+
		"\u0456\5\u0092J\2\u0454\u0456\5\u0094K\2\u0455\u0452\3\2\2\2\u0455\u0453"+
		"\3\2\2\2\u0455\u0454\3\2\2\2\u0456\u008f\3\2\2\2\u0457\u0458\5\u00c6d"+
		"\2\u0458\u0459\7\f\2\2\u0459\u045b\3\2\2\2\u045a\u0457\3\2\2\2\u045a\u045b"+
		"\3\2\2\2\u045b\u045c\3\2\2\2\u045c\u0461\5\u00c8e\2\u045d\u045f\7+\2\2"+
		"\u045e\u045d\3\2\2\2\u045e\u045f\3\2\2\2\u045f\u0460\3\2\2\2\u0460\u0462"+
		"\5\u00d4k\2\u0461\u045e\3\2\2\2\u0461\u0462\3\2\2\2\u0462\u0468\3\2\2"+
		"\2\u0463\u0464\7d\2\2\u0464\u0465\7\62\2\2\u0465\u0469\5\u00d0i\2\u0466"+
		"\u0467\7w\2\2\u0467\u0469\7d\2\2\u0468\u0463\3\2\2\2\u0468\u0466\3\2\2"+
		"\2\u0468\u0469\3\2\2\2\u0469\u0091\3\2\2\2\u046a\u0474\7\r\2\2\u046b\u0470"+
		"\5\u008eH\2\u046c\u046d\7\17\2\2\u046d\u046f\5\u008eH\2\u046e\u046c\3"+
		"\2\2\2\u046f\u0472\3\2\2\2\u0470\u046e\3\2\2\2\u0470\u0471\3\2\2\2\u0471"+
		"\u0475\3\2\2\2\u0472\u0470\3\2\2\2\u0473\u0475\5\u0096L\2\u0474\u046b"+
		"\3\2\2\2\u0474\u0473\3\2\2\2\u0475\u0476\3\2\2\2\u0476\u047b\7\16\2\2"+
		"\u0477\u0479\7+\2\2\u0478\u0477\3\2\2\2\u0478\u0479\3\2\2\2\u0479\u047a"+
		"\3\2\2\2\u047a\u047c\5\u00d4k\2\u047b\u0478\3\2\2\2\u047b\u047c\3\2\2"+
		"\2\u047c\u0093\3\2\2\2\u047d\u047e\7\r\2\2\u047e\u047f\5N(\2\u047f\u0484"+
		"\7\16\2\2\u0480\u0482\7+\2\2\u0481\u0480\3\2\2\2\u0481\u0482\3\2\2\2\u0482"+
		"\u0483\3\2\2\2\u0483\u0485\5\u00d4k\2\u0484\u0481\3\2\2\2\u0484\u0485"+
		"\3\2\2\2\u0485\u0095\3\2\2\2\u0486\u048d\5\u008eH\2\u0487\u0488\5\u0098"+
		"M\2\u0488\u0489\5\u008eH\2\u0489\u048a\5\u009aN\2\u048a\u048c\3\2\2\2"+
		"\u048b\u0487\3\2\2\2\u048c\u048f\3\2\2\2\u048d\u048b\3\2\2\2\u048d\u048e"+
		"\3\2\2\2\u048e\u0097\3\2\2\2\u048f\u048d\3\2\2\2\u0490\u049a\7\17\2\2"+
		"\u0491\u0493\7p\2\2\u0492\u0494\7\u0080\2\2\u0493\u0492\3\2\2\2\u0493"+
		"\u0494\3\2\2\2\u0494\u0497\3\2\2\2\u0495\u0497\7f\2\2\u0496\u0491\3\2"+
		"\2\2\u0496\u0495\3\2\2\2\u0496\u0497\3\2\2\2\u0497\u0498\3\2\2\2\u0498"+
		"\u049a\7m\2\2\u0499\u0490\3\2\2\2\u0499\u0496\3\2\2\2\u049a\u0099\3\2"+
		"\2\2\u049b\u049c\7|\2\2\u049c\u049e\5l\67\2\u049d\u049b\3\2\2\2\u049d"+
		"\u049e\3\2\2\2\u049e\u009b\3\2\2\2\u049f\u04a1\7\u0094\2\2\u04a0\u04a2"+
		"\t\5\2\2\u04a1\u04a0\3\2\2\2\u04a1\u04a2\3\2\2\2\u04a2\u04a3\3\2\2\2\u04a3"+
		"\u04a8\5\u008cG\2\u04a4\u04a5\7\17\2\2\u04a5\u04a7\5\u008cG\2\u04a6\u04a4"+
		"\3\2\2\2\u04a7\u04aa\3\2\2\2\u04a8\u04a6\3\2\2\2\u04a8\u04a9\3\2\2\2\u04a9"+
		"\u04ac\3\2\2\2\u04aa\u04a8\3\2\2\2\u04ab\u04ad\5R*\2\u04ac\u04ab\3\2\2"+
		"\2\u04ac\u04ad\3\2\2\2\u04ad\u04af\3\2\2\2\u04ae\u04b0\5T+\2\u04af\u04ae"+
		"\3\2\2\2\u04af\u04b0\3\2\2\2\u04b0\u04b5\3\2\2\2\u04b1\u04b3\5V,\2\u04b2"+
		"\u04b4\5X-\2\u04b3\u04b2\3\2\2\2\u04b3\u04b4\3\2\2\2\u04b4\u04b6\3\2\2"+
		"\2\u04b5\u04b1\3\2\2\2\u04b5\u04b6\3\2\2\2\u04b6\u04d4\3\2\2\2\u04b7\u04b8"+
		"\7\u00a4\2\2\u04b8\u04b9\7\r\2\2\u04b9\u04be\5l\67\2\u04ba\u04bb\7\17"+
		"\2\2\u04bb\u04bd\5l\67\2\u04bc\u04ba\3\2\2\2\u04bd\u04c0\3\2\2\2\u04be"+
		"\u04bc\3\2\2\2\u04be\u04bf\3\2\2\2\u04bf\u04c1\3\2\2\2\u04c0\u04be\3\2"+
		"\2\2\u04c1\u04d0\7\16\2\2\u04c2\u04c3\7\17\2\2\u04c3\u04c4\7\r\2\2\u04c4"+
		"\u04c9\5l\67\2\u04c5\u04c6\7\17\2\2\u04c6\u04c8\5l\67\2\u04c7\u04c5\3"+
		"\2\2\2\u04c8\u04cb\3\2\2\2\u04c9\u04c7\3\2\2\2\u04c9\u04ca\3\2\2\2\u04ca"+
		"\u04cc\3\2\2\2\u04cb\u04c9\3\2\2\2\u04cc\u04cd\7\16\2\2\u04cd\u04cf\3"+
		"\2\2\2\u04ce\u04c2\3\2\2\2\u04cf\u04d2\3\2\2\2\u04d0\u04ce\3\2\2\2\u04d0"+
		"\u04d1\3\2\2\2\u04d1\u04d4\3\2\2\2\u04d2\u04d0\3\2\2\2\u04d3\u049f\3\2"+
		"\2\2\u04d3\u04b7\3\2\2\2\u04d4\u009d\3\2\2\2\u04d5\u04d7\t\2\2\2\u04d6"+
		"\u04d5\3\2\2\2\u04d6\u04d7\3\2\2\2\u04d7\u04d8\3\2\2\2\u04d8\u04db\7\u00b1"+
		"\2\2\u04d9\u04db\7\21\2\2\u04da\u04d6\3\2\2\2\u04da\u04d9\3\2\2\2\u04db"+
		"\u009f\3\2\2\2\u04dc\u04dd\7>\2\2\u04dd\u04de\7\u0097\2\2\u04de\u04df"+
		"\5\u00c8e\2\u04df\u04e0\7\r\2\2\u04e0\u04e5\5Z.\2\u04e1\u04e2\7\17\2\2"+
		"\u04e2\u04e4\5Z.\2\u04e3\u04e1\3\2\2\2\u04e4\u04e7\3\2\2\2\u04e5\u04e3"+
		"\3\2\2\2\u04e5\u04e6\3\2\2\2\u04e6\u04ef\3\2\2\2\u04e7\u04e5\3\2\2\2\u04e8"+
		"\u04e9\7\u00ad\2\2\u04e9\u04ea\7\20\2\2\u04ea\u04eb\5\u00a2R\2\u04eb\u04ec"+
		"\7\u00ae\2\2\u04ec\u04ed\7\20\2\2\u04ed\u04ee\5\u00a4S\2\u04ee\u04f0\3"+
		"\2\2\2\u04ef\u04e8\3\2\2\2\u04ef\u04f0\3\2\2\2\u04f0\u04f1\3\2\2\2\u04f1"+
		"\u04f2\7\16\2\2\u04f2\u04f3\7\13\2\2\u04f3\u00a1\3\2\2\2\u04f4\u04f5\7"+
		"\u00b0\2\2\u04f5\u00a3\3\2\2\2\u04f6\u04fb\7\u00b0\2\2\u04f7\u04f8\7\26"+
		"\2\2\u04f8\u04fa\7\u00b0\2\2\u04f9\u04f7\3\2\2\2\u04fa\u04fd\3\2\2\2\u04fb"+
		"\u04f9\3\2\2\2\u04fb\u04fc\3\2\2\2\u04fc\u00a5\3\2\2\2\u04fd\u04fb\3\2"+
		"\2\2\u04fe\u04ff\5\u00d6l\2\u04ff\u00a7\3\2\2\2\u0500\u0501\7>\2\2\u0501"+
		"\u0502\7\u00ad\2\2\u0502\u0503\5\u00a6T\2\u0503\u050f\7\r\2\2\u0504\u0505"+
		"\5\u00caf\2\u0505\u050c\5\u00d6l\2\u0506\u0507\7\17\2\2\u0507\u0508\5"+
		"\u00caf\2\u0508\u0509\5\u00d6l\2\u0509\u050b\3\2\2\2\u050a\u0506\3\2\2"+
		"\2\u050b\u050e\3\2\2\2\u050c\u050a\3\2\2\2\u050c\u050d\3\2\2\2\u050d\u0510"+
		"\3\2\2\2\u050e\u050c\3\2\2\2\u050f\u0504\3\2\2\2\u050f\u0510\3\2\2\2\u0510"+
		"\u0511\3\2\2\2\u0511\u0512\7\16\2\2\u0512\u0513\7\13\2\2\u0513\u00a9\3"+
		"\2\2\2\u0514\u0515\5\u00d6l\2\u0515\u00ab\3\2\2\2\u0516\u0517\5\u00d6"+
		"l\2\u0517\u00ad\3\2\2\2\u0518\u0519\5\u00d6l\2\u0519\u00af\3\2\2\2\u051a"+
		"\u051b\5\u00d6l\2\u051b\u00b1\3\2\2\2\u051c\u051d\5\20\t\2\u051d\u00b3"+
		"\3\2\2\2\u051e\u051f\7>\2\2\u051f\u0520\7\u00af\2\2\u0520\u0521\5\u00c4"+
		"c\2\u0521\u0522\7\r\2\2\u0522\u0523\5\u00aaV\2\u0523\u0524\7\17\2\2\u0524"+
		"\u0525\5\u00acW\2\u0525\u0526\7\17\2\2\u0526\u0527\5\u00aeX\2\u0527\u0528"+
		"\7\17\2\2\u0528\u0529\5\u00b0Y\2\u0529\u052a\7\17\2\2\u052a\u052b\5\u00b2"+
		"Z\2\u052b\u052c\7\16\2\2\u052c\u00b5\3\2\2\2\u052d\u052e\t\13\2\2\u052e"+
		"\u00b7\3\2\2\2\u052f\u0530\t\f\2\2\u0530\u00b9\3\2\2\2\u0531\u0532\t\r"+
		"\2\2\u0532\u00bb\3\2\2\2\u0533\u0536\5l\67\2\u0534\u0536\5Z.\2\u0535\u0533"+
		"\3\2\2\2\u0535\u0534\3\2\2\2\u0536\u00bd\3\2\2\2\u0537\u0538\t\16\2\2"+
		"\u0538\u00bf\3\2\2\2\u0539\u053a\t\17\2\2\u053a\u00c1\3\2\2\2\u053b\u053c"+
		"\5\u00d6l\2\u053c\u00c3\3\2\2\2\u053d\u053e\5\u00d6l\2\u053e\u00c5\3\2"+
		"\2\2\u053f\u0540\5\u00d6l\2\u0540\u00c7\3\2\2\2\u0541\u0542\5\u00d6l\2"+
		"\u0542\u00c9\3\2\2\2\u0543\u0544\5\u00d6l\2\u0544\u00cb\3\2\2\2\u0545"+
		"\u0546\5\u00d6l\2\u0546\u00cd\3\2\2\2\u0547\u0548\5\u00d6l\2\u0548\u00cf"+
		"\3\2\2\2\u0549\u054a\5\u00d6l\2\u054a\u00d1\3\2\2\2\u054b\u054c\5\u00d6"+
		"l\2\u054c\u00d3\3\2\2\2\u054d\u054e\5\u00d6l\2\u054e\u00d5\3\2\2\2\u054f"+
		"\u0556\7\u00b0\2\2\u0550\u0556\7\u00b3\2\2\u0551\u0552\7\r\2\2\u0552\u0553"+
		"\5\u00d6l\2\u0553\u0554\7\16\2\2\u0554\u0556\3\2\2\2\u0555\u054f\3\2\2"+
		"\2\u0555\u0550\3\2\2\2\u0555\u0551\3\2\2\2\u0556\u00d7\3\2\2\2\u00ae\u00dd"+
		"\u00df\u00ec\u00f3\u00f8\u00fe\u0104\u010b\u010f\u011f\u0124\u0129\u012d"+
		"\u0130\u0136\u0140\u0144\u0147\u014a\u014d\u0151\u0154\u0159\u015d\u0163"+
		"\u0165\u016f\u0175\u017c\u0180\u0184\u0188\u018c\u0190\u0198\u019a\u01a0"+
		"\u01a9\u01b0\u01cd\u01d3\u01d7\u01dc\u01e0\u01e6\u01ef\u01f6\u01fa\u0201"+
		"\u0214\u021b\u021f\u0226\u022c\u0235\u023b\u0242\u0249\u024d\u0254\u025b"+
		"\u0265\u0268\u026e\u0270\u027a\u027d\u0283\u0285\u0289\u0290\u0294\u0297"+
		"\u029b\u029d\u02a6\u02b1\u02b8\u02bb\u02c3\u02c7\u02d3\u02dc\u02de\u02e5"+
		"\u02ec\u02f1\u02f5\u02f9\u0308\u030d\u0310\u0325\u032b\u032d\u0331\u033f"+
		"\u035d\u0368\u036c\u036e\u0374\u0379\u0383\u038a\u038e\u0399\u039c\u03a2"+
		"\u03a5\u03a8\u03ab\u03b5\u03be\u03c3\u03cf\u03d3\u03d7\u03db\u03e2\u03e5"+
		"\u03e7\u03ee\u03f1\u03f5\u0400\u040a\u0417\u041f\u0422\u042a\u0431\u0439"+
		"\u0442\u044b\u044e\u0450\u0455\u045a\u045e\u0461\u0468\u0470\u0474\u0478"+
		"\u047b\u0481\u0484\u048d\u0493\u0496\u0499\u049d\u04a1\u04a8\u04ac\u04af"+
		"\u04b3\u04b5\u04be\u04c9\u04d0\u04d3\u04d6\u04da\u04e5\u04ef\u04fb\u050c"+
		"\u050f\u0535\u0555";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}