// Generated from C:/Users/Amina Alkurde/IdeaProjects/Compiler/src/Java\Sql.g4 by ANTLR 4.8
package generated.Java;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SqlParser}.
 */
public interface SqlListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SqlParser#parse}.
	 * @param ctx the parse tree
	 */
	void enterParse(SqlParser.ParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#parse}.
	 * @param ctx the parse tree
	 */
	void exitParse(SqlParser.ParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#error}.
	 * @param ctx the parse tree
	 */
	void enterError(SqlParser.ErrorContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#error}.
	 * @param ctx the parse tree
	 */
	void exitError(SqlParser.ErrorContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#defVariable}.
	 * @param ctx the parse tree
	 */
	void enterDefVariable(SqlParser.DefVariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#defVariable}.
	 * @param ctx the parse tree
	 */
	void exitDefVariable(SqlParser.DefVariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#sql_stmt_list}.
	 * @param ctx the parse tree
	 */
	void enterSql_stmt_list(SqlParser.Sql_stmt_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#sql_stmt_list}.
	 * @param ctx the parse tree
	 */
	void exitSql_stmt_list(SqlParser.Sql_stmt_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#sql_stmt}.
	 * @param ctx the parse tree
	 */
	void enterSql_stmt(SqlParser.Sql_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#sql_stmt}.
	 * @param ctx the parse tree
	 */
	void exitSql_stmt(SqlParser.Sql_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#java_anystmt}.
	 * @param ctx the parse tree
	 */
	void enterJava_anystmt(SqlParser.Java_anystmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#java_anystmt}.
	 * @param ctx the parse tree
	 */
	void exitJava_anystmt(SqlParser.Java_anystmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#java_stmt}.
	 * @param ctx the parse tree
	 */
	void enterJava_stmt(SqlParser.Java_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#java_stmt}.
	 * @param ctx the parse tree
	 */
	void exitJava_stmt(SqlParser.Java_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#var_stmt}.
	 * @param ctx the parse tree
	 */
	void enterVar_stmt(SqlParser.Var_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#var_stmt}.
	 * @param ctx the parse tree
	 */
	void exitVar_stmt(SqlParser.Var_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#var_equal_stmt}.
	 * @param ctx the parse tree
	 */
	void enterVar_equal_stmt(SqlParser.Var_equal_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#var_equal_stmt}.
	 * @param ctx the parse tree
	 */
	void exitVar_equal_stmt(SqlParser.Var_equal_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#condtion_stmt}.
	 * @param ctx the parse tree
	 */
	void enterCondtion_stmt(SqlParser.Condtion_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#condtion_stmt}.
	 * @param ctx the parse tree
	 */
	void exitCondtion_stmt(SqlParser.Condtion_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#body_all_stmt}.
	 * @param ctx the parse tree
	 */
	void enterBody_all_stmt(SqlParser.Body_all_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#body_all_stmt}.
	 * @param ctx the parse tree
	 */
	void exitBody_all_stmt(SqlParser.Body_all_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#if_condition}.
	 * @param ctx the parse tree
	 */
	void enterIf_condition(SqlParser.If_conditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#if_condition}.
	 * @param ctx the parse tree
	 */
	void exitIf_condition(SqlParser.If_conditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#javaIF_stmt}.
	 * @param ctx the parse tree
	 */
	void enterJavaIF_stmt(SqlParser.JavaIF_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#javaIF_stmt}.
	 * @param ctx the parse tree
	 */
	void exitJavaIF_stmt(SqlParser.JavaIF_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#java_else}.
	 * @param ctx the parse tree
	 */
	void enterJava_else(SqlParser.Java_elseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#java_else}.
	 * @param ctx the parse tree
	 */
	void exitJava_else(SqlParser.Java_elseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#javaIFline_stmt}.
	 * @param ctx the parse tree
	 */
	void enterJavaIFline_stmt(SqlParser.JavaIFline_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#javaIFline_stmt}.
	 * @param ctx the parse tree
	 */
	void exitJavaIFline_stmt(SqlParser.JavaIFline_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#ifline_body}.
	 * @param ctx the parse tree
	 */
	void enterIfline_body(SqlParser.Ifline_bodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#ifline_body}.
	 * @param ctx the parse tree
	 */
	void exitIfline_body(SqlParser.Ifline_bodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#unrealized_condition}.
	 * @param ctx the parse tree
	 */
	void enterUnrealized_condition(SqlParser.Unrealized_conditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#unrealized_condition}.
	 * @param ctx the parse tree
	 */
	void exitUnrealized_condition(SqlParser.Unrealized_conditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#realized_condition}.
	 * @param ctx the parse tree
	 */
	void enterRealized_condition(SqlParser.Realized_conditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#realized_condition}.
	 * @param ctx the parse tree
	 */
	void exitRealized_condition(SqlParser.Realized_conditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#complete_ifLine}.
	 * @param ctx the parse tree
	 */
	void enterComplete_ifLine(SqlParser.Complete_ifLineContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#complete_ifLine}.
	 * @param ctx the parse tree
	 */
	void exitComplete_ifLine(SqlParser.Complete_ifLineContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#differnt_kind_of_for_increament}.
	 * @param ctx the parse tree
	 */
	void enterDiffernt_kind_of_for_increament(SqlParser.Differnt_kind_of_for_increamentContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#differnt_kind_of_for_increament}.
	 * @param ctx the parse tree
	 */
	void exitDiffernt_kind_of_for_increament(SqlParser.Differnt_kind_of_for_increamentContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#increament_var}.
	 * @param ctx the parse tree
	 */
	void enterIncreament_var(SqlParser.Increament_varContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#increament_var}.
	 * @param ctx the parse tree
	 */
	void exitIncreament_var(SqlParser.Increament_varContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#discreament_var}.
	 * @param ctx the parse tree
	 */
	void enterDiscreament_var(SqlParser.Discreament_varContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#discreament_var}.
	 * @param ctx the parse tree
	 */
	void exitDiscreament_var(SqlParser.Discreament_varContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#signal}.
	 * @param ctx the parse tree
	 */
	void enterSignal(SqlParser.SignalContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#signal}.
	 * @param ctx the parse tree
	 */
	void exitSignal(SqlParser.SignalContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#javaFor_stmt}.
	 * @param ctx the parse tree
	 */
	void enterJavaFor_stmt(SqlParser.JavaFor_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#javaFor_stmt}.
	 * @param ctx the parse tree
	 */
	void exitJavaFor_stmt(SqlParser.JavaFor_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#javaForeach_stmt}.
	 * @param ctx the parse tree
	 */
	void enterJavaForeach_stmt(SqlParser.JavaForeach_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#javaForeach_stmt}.
	 * @param ctx the parse tree
	 */
	void exitJavaForeach_stmt(SqlParser.JavaForeach_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#parameter_list}.
	 * @param ctx the parse tree
	 */
	void enterParameter_list(SqlParser.Parameter_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#parameter_list}.
	 * @param ctx the parse tree
	 */
	void exitParameter_list(SqlParser.Parameter_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#item}.
	 * @param ctx the parse tree
	 */
	void enterItem(SqlParser.ItemContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#item}.
	 * @param ctx the parse tree
	 */
	void exitItem(SqlParser.ItemContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#argument_list}.
	 * @param ctx the parse tree
	 */
	void enterArgument_list(SqlParser.Argument_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#argument_list}.
	 * @param ctx the parse tree
	 */
	void exitArgument_list(SqlParser.Argument_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#call_function}.
	 * @param ctx the parse tree
	 */
	void enterCall_function(SqlParser.Call_functionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#call_function}.
	 * @param ctx the parse tree
	 */
	void exitCall_function(SqlParser.Call_functionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#function}.
	 * @param ctx the parse tree
	 */
	void enterFunction(SqlParser.FunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#function}.
	 * @param ctx the parse tree
	 */
	void exitFunction(SqlParser.FunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#javaWhile_stmt}.
	 * @param ctx the parse tree
	 */
	void enterJavaWhile_stmt(SqlParser.JavaWhile_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#javaWhile_stmt}.
	 * @param ctx the parse tree
	 */
	void exitJavaWhile_stmt(SqlParser.JavaWhile_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#java_do_while}.
	 * @param ctx the parse tree
	 */
	void enterJava_do_while(SqlParser.Java_do_whileContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#java_do_while}.
	 * @param ctx the parse tree
	 */
	void exitJava_do_while(SqlParser.Java_do_whileContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#java_switch_stmt}.
	 * @param ctx the parse tree
	 */
	void enterJava_switch_stmt(SqlParser.Java_switch_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#java_switch_stmt}.
	 * @param ctx the parse tree
	 */
	void exitJava_switch_stmt(SqlParser.Java_switch_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#case_switch}.
	 * @param ctx the parse tree
	 */
	void enterCase_switch(SqlParser.Case_switchContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#case_switch}.
	 * @param ctx the parse tree
	 */
	void exitCase_switch(SqlParser.Case_switchContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#default_switch}.
	 * @param ctx the parse tree
	 */
	void enterDefault_switch(SqlParser.Default_switchContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#default_switch}.
	 * @param ctx the parse tree
	 */
	void exitDefault_switch(SqlParser.Default_switchContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#print_stm}.
	 * @param ctx the parse tree
	 */
	void enterPrint_stm(SqlParser.Print_stmContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#print_stm}.
	 * @param ctx the parse tree
	 */
	void exitPrint_stm(SqlParser.Print_stmContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#factored_select_stmt}.
	 * @param ctx the parse tree
	 */
	void enterFactored_select_stmt(SqlParser.Factored_select_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#factored_select_stmt}.
	 * @param ctx the parse tree
	 */
	void exitFactored_select_stmt(SqlParser.Factored_select_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#select_core}.
	 * @param ctx the parse tree
	 */
	void enterSelect_core(SqlParser.Select_coreContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#select_core}.
	 * @param ctx the parse tree
	 */
	void exitSelect_core(SqlParser.Select_coreContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#ordering_term}.
	 * @param ctx the parse tree
	 */
	void enterOrdering_term(SqlParser.Ordering_termContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#ordering_term}.
	 * @param ctx the parse tree
	 */
	void exitOrdering_term(SqlParser.Ordering_termContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#result_column}.
	 * @param ctx the parse tree
	 */
	void enterResult_column(SqlParser.Result_columnContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#result_column}.
	 * @param ctx the parse tree
	 */
	void exitResult_column(SqlParser.Result_columnContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#star}.
	 * @param ctx the parse tree
	 */
	void enterStar(SqlParser.StarContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#star}.
	 * @param ctx the parse tree
	 */
	void exitStar(SqlParser.StarContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#sel_table_AM}.
	 * @param ctx the parse tree
	 */
	void enterSel_table_AM(SqlParser.Sel_table_AMContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#sel_table_AM}.
	 * @param ctx the parse tree
	 */
	void exitSel_table_AM(SqlParser.Sel_table_AMContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#sel_exp_where_AM}.
	 * @param ctx the parse tree
	 */
	void enterSel_exp_where_AM(SqlParser.Sel_exp_where_AMContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#sel_exp_where_AM}.
	 * @param ctx the parse tree
	 */
	void exitSel_exp_where_AM(SqlParser.Sel_exp_where_AMContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#sel_exp_groupBy_AM}.
	 * @param ctx the parse tree
	 */
	void enterSel_exp_groupBy_AM(SqlParser.Sel_exp_groupBy_AMContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#sel_exp_groupBy_AM}.
	 * @param ctx the parse tree
	 */
	void exitSel_exp_groupBy_AM(SqlParser.Sel_exp_groupBy_AMContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#sel_exp_having_AM}.
	 * @param ctx the parse tree
	 */
	void enterSel_exp_having_AM(SqlParser.Sel_exp_having_AMContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#sel_exp_having_AM}.
	 * @param ctx the parse tree
	 */
	void exitSel_exp_having_AM(SqlParser.Sel_exp_having_AMContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#table_or_subquery}.
	 * @param ctx the parse tree
	 */
	void enterTable_or_subquery(SqlParser.Table_or_subqueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#table_or_subquery}.
	 * @param ctx the parse tree
	 */
	void exitTable_or_subquery(SqlParser.Table_or_subqueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#table_or_sub_one}.
	 * @param ctx the parse tree
	 */
	void enterTable_or_sub_one(SqlParser.Table_or_sub_oneContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#table_or_sub_one}.
	 * @param ctx the parse tree
	 */
	void exitTable_or_sub_one(SqlParser.Table_or_sub_oneContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#table_or_sub_two}.
	 * @param ctx the parse tree
	 */
	void enterTable_or_sub_two(SqlParser.Table_or_sub_twoContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#table_or_sub_two}.
	 * @param ctx the parse tree
	 */
	void exitTable_or_sub_two(SqlParser.Table_or_sub_twoContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#table_or_sub_three}.
	 * @param ctx the parse tree
	 */
	void enterTable_or_sub_three(SqlParser.Table_or_sub_threeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#table_or_sub_three}.
	 * @param ctx the parse tree
	 */
	void exitTable_or_sub_three(SqlParser.Table_or_sub_threeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#join_clause}.
	 * @param ctx the parse tree
	 */
	void enterJoin_clause(SqlParser.Join_clauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#join_clause}.
	 * @param ctx the parse tree
	 */
	void exitJoin_clause(SqlParser.Join_clauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#join}.
	 * @param ctx the parse tree
	 */
	void enterJoin(SqlParser.JoinContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#join}.
	 * @param ctx the parse tree
	 */
	void exitJoin(SqlParser.JoinContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#select_stmt}.
	 * @param ctx the parse tree
	 */
	void enterSelect_stmt(SqlParser.Select_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#select_stmt}.
	 * @param ctx the parse tree
	 */
	void exitSelect_stmt(SqlParser.Select_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#select_or_values}.
	 * @param ctx the parse tree
	 */
	void enterSelect_or_values(SqlParser.Select_or_valuesContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#select_or_values}.
	 * @param ctx the parse tree
	 */
	void exitSelect_or_values(SqlParser.Select_or_valuesContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#column_def}.
	 * @param ctx the parse tree
	 */
	void enterColumn_def(SqlParser.Column_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#column_def}.
	 * @param ctx the parse tree
	 */
	void exitColumn_def(SqlParser.Column_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#type_name}.
	 * @param ctx the parse tree
	 */
	void enterType_name(SqlParser.Type_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#type_name}.
	 * @param ctx the parse tree
	 */
	void exitType_name(SqlParser.Type_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#column_constraint}.
	 * @param ctx the parse tree
	 */
	void enterColumn_constraint(SqlParser.Column_constraintContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#column_constraint}.
	 * @param ctx the parse tree
	 */
	void exitColumn_constraint(SqlParser.Column_constraintContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#column_constraint_primary_key}.
	 * @param ctx the parse tree
	 */
	void enterColumn_constraint_primary_key(SqlParser.Column_constraint_primary_keyContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#column_constraint_primary_key}.
	 * @param ctx the parse tree
	 */
	void exitColumn_constraint_primary_key(SqlParser.Column_constraint_primary_keyContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#column_constraint_foreign_key}.
	 * @param ctx the parse tree
	 */
	void enterColumn_constraint_foreign_key(SqlParser.Column_constraint_foreign_keyContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#column_constraint_foreign_key}.
	 * @param ctx the parse tree
	 */
	void exitColumn_constraint_foreign_key(SqlParser.Column_constraint_foreign_keyContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#column_constraint_not_null}.
	 * @param ctx the parse tree
	 */
	void enterColumn_constraint_not_null(SqlParser.Column_constraint_not_nullContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#column_constraint_not_null}.
	 * @param ctx the parse tree
	 */
	void exitColumn_constraint_not_null(SqlParser.Column_constraint_not_nullContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#column_constraint_null}.
	 * @param ctx the parse tree
	 */
	void enterColumn_constraint_null(SqlParser.Column_constraint_nullContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#column_constraint_null}.
	 * @param ctx the parse tree
	 */
	void exitColumn_constraint_null(SqlParser.Column_constraint_nullContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#column_default}.
	 * @param ctx the parse tree
	 */
	void enterColumn_default(SqlParser.Column_defaultContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#column_default}.
	 * @param ctx the parse tree
	 */
	void exitColumn_default(SqlParser.Column_defaultContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#column_default_value}.
	 * @param ctx the parse tree
	 */
	void enterColumn_default_value(SqlParser.Column_default_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#column_default_value}.
	 * @param ctx the parse tree
	 */
	void exitColumn_default_value(SqlParser.Column_default_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(SqlParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(SqlParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#database_exp}.
	 * @param ctx the parse tree
	 */
	void enterDatabase_exp(SqlParser.Database_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#database_exp}.
	 * @param ctx the parse tree
	 */
	void exitDatabase_exp(SqlParser.Database_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#operator_exp}.
	 * @param ctx the parse tree
	 */
	void enterOperator_exp(SqlParser.Operator_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#operator_exp}.
	 * @param ctx the parse tree
	 */
	void exitOperator_exp(SqlParser.Operator_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#function_exp}.
	 * @param ctx the parse tree
	 */
	void enterFunction_exp(SqlParser.Function_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#function_exp}.
	 * @param ctx the parse tree
	 */
	void exitFunction_exp(SqlParser.Function_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#second_sel_all}.
	 * @param ctx the parse tree
	 */
	void enterSecond_sel_all(SqlParser.Second_sel_allContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#second_sel_all}.
	 * @param ctx the parse tree
	 */
	void exitSecond_sel_all(SqlParser.Second_sel_allContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#sel_expr}.
	 * @param ctx the parse tree
	 */
	void enterSel_expr(SqlParser.Sel_exprContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#sel_expr}.
	 * @param ctx the parse tree
	 */
	void exitSel_expr(SqlParser.Sel_exprContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#foreign_key_clause}.
	 * @param ctx the parse tree
	 */
	void enterForeign_key_clause(SqlParser.Foreign_key_clauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#foreign_key_clause}.
	 * @param ctx the parse tree
	 */
	void exitForeign_key_clause(SqlParser.Foreign_key_clauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#fk_target_column_name}.
	 * @param ctx the parse tree
	 */
	void enterFk_target_column_name(SqlParser.Fk_target_column_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#fk_target_column_name}.
	 * @param ctx the parse tree
	 */
	void exitFk_target_column_name(SqlParser.Fk_target_column_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#indexed_column}.
	 * @param ctx the parse tree
	 */
	void enterIndexed_column(SqlParser.Indexed_columnContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#indexed_column}.
	 * @param ctx the parse tree
	 */
	void exitIndexed_column(SqlParser.Indexed_columnContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#table_constraint}.
	 * @param ctx the parse tree
	 */
	void enterTable_constraint(SqlParser.Table_constraintContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#table_constraint}.
	 * @param ctx the parse tree
	 */
	void exitTable_constraint(SqlParser.Table_constraintContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#table_constraint_primary_key}.
	 * @param ctx the parse tree
	 */
	void enterTable_constraint_primary_key(SqlParser.Table_constraint_primary_keyContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#table_constraint_primary_key}.
	 * @param ctx the parse tree
	 */
	void exitTable_constraint_primary_key(SqlParser.Table_constraint_primary_keyContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#table_constraint_foreign_key}.
	 * @param ctx the parse tree
	 */
	void enterTable_constraint_foreign_key(SqlParser.Table_constraint_foreign_keyContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#table_constraint_foreign_key}.
	 * @param ctx the parse tree
	 */
	void exitTable_constraint_foreign_key(SqlParser.Table_constraint_foreign_keyContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#table_constraint_unique}.
	 * @param ctx the parse tree
	 */
	void enterTable_constraint_unique(SqlParser.Table_constraint_uniqueContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#table_constraint_unique}.
	 * @param ctx the parse tree
	 */
	void exitTable_constraint_unique(SqlParser.Table_constraint_uniqueContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#table_constraint_key}.
	 * @param ctx the parse tree
	 */
	void enterTable_constraint_key(SqlParser.Table_constraint_keyContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#table_constraint_key}.
	 * @param ctx the parse tree
	 */
	void exitTable_constraint_key(SqlParser.Table_constraint_keyContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#fk_origin_column_name}.
	 * @param ctx the parse tree
	 */
	void enterFk_origin_column_name(SqlParser.Fk_origin_column_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#fk_origin_column_name}.
	 * @param ctx the parse tree
	 */
	void exitFk_origin_column_name(SqlParser.Fk_origin_column_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#join_operator}.
	 * @param ctx the parse tree
	 */
	void enterJoin_operator(SqlParser.Join_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#join_operator}.
	 * @param ctx the parse tree
	 */
	void exitJoin_operator(SqlParser.Join_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#join_constraint}.
	 * @param ctx the parse tree
	 */
	void enterJoin_constraint(SqlParser.Join_constraintContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#join_constraint}.
	 * @param ctx the parse tree
	 */
	void exitJoin_constraint(SqlParser.Join_constraintContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#signed_number}.
	 * @param ctx the parse tree
	 */
	void enterSigned_number(SqlParser.Signed_numberContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#signed_number}.
	 * @param ctx the parse tree
	 */
	void exitSigned_number(SqlParser.Signed_numberContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#create_table_stmt}.
	 * @param ctx the parse tree
	 */
	void enterCreate_table_stmt(SqlParser.Create_table_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#create_table_stmt}.
	 * @param ctx the parse tree
	 */
	void exitCreate_table_stmt(SqlParser.Create_table_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(SqlParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(SqlParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#path}.
	 * @param ctx the parse tree
	 */
	void enterPath(SqlParser.PathContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#path}.
	 * @param ctx the parse tree
	 */
	void exitPath(SqlParser.PathContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#create_type_stmt}.
	 * @param ctx the parse tree
	 */
	void enterCreate_type_stmt(SqlParser.Create_type_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#create_type_stmt}.
	 * @param ctx the parse tree
	 */
	void exitCreate_type_stmt(SqlParser.Create_type_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#column_def_type}.
	 * @param ctx the parse tree
	 */
	void enterColumn_def_type(SqlParser.Column_def_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#column_def_type}.
	 * @param ctx the parse tree
	 */
	void exitColumn_def_type(SqlParser.Column_def_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#name_type}.
	 * @param ctx the parse tree
	 */
	void enterName_type(SqlParser.Name_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#name_type}.
	 * @param ctx the parse tree
	 */
	void exitName_type(SqlParser.Name_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#jarPath}.
	 * @param ctx the parse tree
	 */
	void enterJarPath(SqlParser.JarPathContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#jarPath}.
	 * @param ctx the parse tree
	 */
	void exitJarPath(SqlParser.JarPathContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#className}.
	 * @param ctx the parse tree
	 */
	void enterClassName(SqlParser.ClassNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#className}.
	 * @param ctx the parse tree
	 */
	void exitClassName(SqlParser.ClassNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#methodName}.
	 * @param ctx the parse tree
	 */
	void enterMethodName(SqlParser.MethodNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#methodName}.
	 * @param ctx the parse tree
	 */
	void exitMethodName(SqlParser.MethodNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#returnType}.
	 * @param ctx the parse tree
	 */
	void enterReturnType(SqlParser.ReturnTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#returnType}.
	 * @param ctx the parse tree
	 */
	void exitReturnType(SqlParser.ReturnTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#parameter_AggFunction}.
	 * @param ctx the parse tree
	 */
	void enterParameter_AggFunction(SqlParser.Parameter_AggFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#parameter_AggFunction}.
	 * @param ctx the parse tree
	 */
	void exitParameter_AggFunction(SqlParser.Parameter_AggFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#paramete_array}.
	 * @param ctx the parse tree
	 */
	void enterParamete_array(SqlParser.Paramete_arrayContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#paramete_array}.
	 * @param ctx the parse tree
	 */
	void exitParamete_array(SqlParser.Paramete_arrayContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#any_name_item}.
	 * @param ctx the parse tree
	 */
	void enterAny_name_item(SqlParser.Any_name_itemContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#any_name_item}.
	 * @param ctx the parse tree
	 */
	void exitAny_name_item(SqlParser.Any_name_itemContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#number_item}.
	 * @param ctx the parse tree
	 */
	void enterNumber_item(SqlParser.Number_itemContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#number_item}.
	 * @param ctx the parse tree
	 */
	void exitNumber_item(SqlParser.Number_itemContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#aggregationFunction}.
	 * @param ctx the parse tree
	 */
	void enterAggregationFunction(SqlParser.AggregationFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#aggregationFunction}.
	 * @param ctx the parse tree
	 */
	void exitAggregationFunction(SqlParser.AggregationFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#literal_value}.
	 * @param ctx the parse tree
	 */
	void enterLiteral_value(SqlParser.Literal_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#literal_value}.
	 * @param ctx the parse tree
	 */
	void exitLiteral_value(SqlParser.Literal_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#unary_operator}.
	 * @param ctx the parse tree
	 */
	void enterUnary_operator(SqlParser.Unary_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#unary_operator}.
	 * @param ctx the parse tree
	 */
	void exitUnary_operator(SqlParser.Unary_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#opration}.
	 * @param ctx the parse tree
	 */
	void enterOpration(SqlParser.OprationContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#opration}.
	 * @param ctx the parse tree
	 */
	void exitOpration(SqlParser.OprationContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#module_argument}.
	 * @param ctx the parse tree
	 */
	void enterModule_argument(SqlParser.Module_argumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#module_argument}.
	 * @param ctx the parse tree
	 */
	void exitModule_argument(SqlParser.Module_argumentContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#column_alias}.
	 * @param ctx the parse tree
	 */
	void enterColumn_alias(SqlParser.Column_aliasContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#column_alias}.
	 * @param ctx the parse tree
	 */
	void exitColumn_alias(SqlParser.Column_aliasContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#keyword}.
	 * @param ctx the parse tree
	 */
	void enterKeyword(SqlParser.KeywordContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#keyword}.
	 * @param ctx the parse tree
	 */
	void exitKeyword(SqlParser.KeywordContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#table_alias}.
	 * @param ctx the parse tree
	 */
	void enterTable_alias(SqlParser.Table_aliasContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#table_alias}.
	 * @param ctx the parse tree
	 */
	void exitTable_alias(SqlParser.Table_aliasContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#name}.
	 * @param ctx the parse tree
	 */
	void enterName(SqlParser.NameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#name}.
	 * @param ctx the parse tree
	 */
	void exitName(SqlParser.NameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#function_name}.
	 * @param ctx the parse tree
	 */
	void enterFunction_name(SqlParser.Function_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#function_name}.
	 * @param ctx the parse tree
	 */
	void exitFunction_name(SqlParser.Function_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#database_name}.
	 * @param ctx the parse tree
	 */
	void enterDatabase_name(SqlParser.Database_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#database_name}.
	 * @param ctx the parse tree
	 */
	void exitDatabase_name(SqlParser.Database_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#table_name}.
	 * @param ctx the parse tree
	 */
	void enterTable_name(SqlParser.Table_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#table_name}.
	 * @param ctx the parse tree
	 */
	void exitTable_name(SqlParser.Table_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#column_name}.
	 * @param ctx the parse tree
	 */
	void enterColumn_name(SqlParser.Column_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#column_name}.
	 * @param ctx the parse tree
	 */
	void exitColumn_name(SqlParser.Column_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#collation_name}.
	 * @param ctx the parse tree
	 */
	void enterCollation_name(SqlParser.Collation_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#collation_name}.
	 * @param ctx the parse tree
	 */
	void exitCollation_name(SqlParser.Collation_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#foreign_table}.
	 * @param ctx the parse tree
	 */
	void enterForeign_table(SqlParser.Foreign_tableContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#foreign_table}.
	 * @param ctx the parse tree
	 */
	void exitForeign_table(SqlParser.Foreign_tableContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#index_name}.
	 * @param ctx the parse tree
	 */
	void enterIndex_name(SqlParser.Index_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#index_name}.
	 * @param ctx the parse tree
	 */
	void exitIndex_name(SqlParser.Index_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#savepoint_name}.
	 * @param ctx the parse tree
	 */
	void enterSavepoint_name(SqlParser.Savepoint_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#savepoint_name}.
	 * @param ctx the parse tree
	 */
	void exitSavepoint_name(SqlParser.Savepoint_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SqlParser#any_name}.
	 * @param ctx the parse tree
	 */
	void enterAny_name(SqlParser.Any_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SqlParser#any_name}.
	 * @param ctx the parse tree
	 */
	void exitAny_name(SqlParser.Any_nameContext ctx);
}