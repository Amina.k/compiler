package Java.Base;

import Java.AST.Parse;

import Java.AST.temp.SQL.CreateTable.*;

import Java.AST.temp.SQL.CreateTable.CreateTableStatmt;
import Java.AST.temp.SQL.otherSql.*;
import Java.AST.temp.SQL.selectOrValue.ExpSqlAM;
import Java.AST.temp.SQL.selectOrValue.SelTableAm;
import Java.AST.temp.SQL.selectOrValue.SelectOrValue;
import Java.AST.temp.SQL.selectOrValue.tableOrSub.*;
import Java.AST.temp.SQL.selectStmt.OrderingTerm;
import Java.AST.temp.SQL.selectStmt.ResultColumn;
import Java.AST.temp.SQL.sqlStmt.*;
import Java.AST.temp.condition.*;
import Java.AST.temp.decleration.*;
import Java.AST.temp.exp.*;
import Java.AST.temp.function.*;
import Java.AST.temp.loop.*;
import Java.AST.temp.other.*;
import Java.AST.temp.print.PrintStmt;
import Java.Main;
import Java.SymbolTable.*;
import generated.Java.SqlBaseVisitor;
import generated.Java.SqlParser;
import org.antlr.v4.codegen.model.chunk.ActionTemplate;
import sun.security.pkcs.PKCS8Key;


import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BaseVisitor extends SqlBaseVisitor {

    public static String variableName;

    SelectOrValue selectOrValueAll;


    public static ArrayList<String> tablesNamePrev = new ArrayList<>();

    @Override
    public Parse visitParse(SqlParser.ParseContext ctx) {
        System.out.println("visit Parse");
        Parse p = new Parse();
        if (ctx.sql_stmt_list() != null) {
            for (int i = 0; i < ctx.sql_stmt_list().size(); i++)
                p.setSqlStmtLists(visitSql_stmt_list(ctx.sql_stmt_list(i)));
        }

        if (ctx.defVariable() != null) {
            for (int i = 0; i < ctx.defVariable().size(); i++) {
                if (ctx.defVariable(i).var_equal_stmt() != null)
                    p.setVarEqualStmts(visitVar_equal_stmt(ctx.defVariable(i).var_equal_stmt()));
            }
        }
        if (ctx.function() != null) {
            for (int i = 0; i < ctx.function().size(); i++) {
                p.setFunctions(visitFunction(ctx.function(i)));
            }
        }

        p.setLine(ctx.getStart().getLine()); //get line number
        p.setCol(ctx.getStart().getCharPositionInLine());
        return p;
    }

    @Override
    public JavaAnyStmt visitJava_anystmt(SqlParser.Java_anystmtContext ctx) {
        System.out.println("visit java any stmt");
        JavaAnyStmt javaAnyStmt = new JavaAnyStmt();
        javaAnyStmt.setOk(true);
        Scope javaAnyStmtScope = Main.parentScope;

        if (ctx.java_stmt() != null) {
            Main.parentScope = javaAnyStmtScope;
            javaAnyStmt.setJavaStmt(visitJava_stmt(ctx.java_stmt()));
        }
        if (ctx.sql_stmt_list() != null) {
            Main.parentScope = javaAnyStmtScope;
            javaAnyStmt.setSqlStmtList(visitSql_stmt_list(ctx.sql_stmt_list()));
        }
        return javaAnyStmt;
    }

    @Override
    public SqlStmtList visitSql_stmt_list(SqlParser.Sql_stmt_listContext ctx) {
        System.out.println("visit sql stmt list");
        SqlStmtList sqlStmtList = new SqlStmtList();
        sqlStmtList.setOk(true);
        for (int i = 0; i < ctx.sql_stmt().size(); i++) {
            sqlStmtList.setSqlStmts(visitSql_stmt(ctx.sql_stmt(i)));
        }
        return sqlStmtList;
    }

    @Override
    public SqlStmt visitSql_stmt(SqlParser.Sql_stmtContext ctx) {
        System.out.println("visit sql stmt");
        SqlStmt sqlStmt = new SqlStmt();
        sqlStmt.setOk(true);
        // Scope currentScope = Main.parentScope;
        Scope sqlScope = new Scope();
        sqlScope.setId("sql");
        sqlScope.setParent(null);
        System.out.println("sql scope " + sqlScope.getId());
        Main.symbolTable.addScope(sqlScope);


        if (ctx.factored_select_stmt() != null) {
            Main.parentScope = sqlScope;
            sqlStmt.setFactoredSelectStmt(visitFactored_select_stmt(ctx.factored_select_stmt()));
        }
        if (ctx.create_table_stmt() != null) {
            Main.parentScope = sqlScope;
            sqlStmt.setCreateTableStatmt(visitCreate_table_stmt(ctx.create_table_stmt()));
            sqlStmt.setNameTable(sqlStmt.getCreateTableStatmt().getTableName());
            System.out.println("name rrttyy " + sqlStmt.getNameTable());
        }
        if (ctx.create_type_stmt() != null) {
            Main.parentScope = sqlScope;
            sqlStmt.setCreateTypeStmt(visitCreate_type_stmt(ctx.create_type_stmt()));
        }
        if (ctx.aggregationFunction() != null) {
            Main.parentScope = sqlScope;
            sqlStmt.setAggregationFunction(visitAggregationFunction(ctx.aggregationFunction()));
        }

        return sqlStmt;
    }

    @Override
    public JavaStmt visitJava_stmt(SqlParser.Java_stmtContext ctx) {
        System.out.println("visit java stmt");
        JavaStmt javaStmt = new JavaStmt();
        javaStmt.setOk(true);
        Scope parentScope = Main.parentScope;

        if (ctx.java_switch_stmt() != null) {
            Main.parentScope = parentScope;
            javaStmt.setaSwitch((visitJava_switch_stmt(ctx.java_switch_stmt())));
        } else if (ctx.print_stm() != null) {
            Main.parentScope = parentScope;
            javaStmt.setPrintStmt(visitPrint_stm(ctx.print_stm()));
        } else if (ctx.javaWhile_stmt() != null) {
            Main.parentScope = parentScope;
            javaStmt.setaWhile(visitJavaWhile_stmt(ctx.javaWhile_stmt()));
        } else if (ctx.java_do_while() != null) {
            Main.parentScope = parentScope;
            javaStmt.setDoWhile(visitJava_do_while(ctx.java_do_while()));
        } else if (ctx.javaFor_stmt() != null) {
            Main.parentScope = parentScope;
            javaStmt.setForNoraml(visitJavaFor_stmt(ctx.javaFor_stmt()));
        } else if (ctx.javaForeach_stmt() != null) {
            Main.parentScope = parentScope;
            javaStmt.setForEach(visitJavaForeach_stmt(ctx.javaForeach_stmt()));
        } else if (ctx.javaIFline_stmt() != null) {
            Main.parentScope = parentScope;
            javaStmt.setIfLine(visitJavaIFline_stmt(ctx.javaIFline_stmt()));
        } else if (ctx.javaIF_stmt() != null) {
            Main.parentScope = parentScope;
            javaStmt.setIfNOrmal(visitJavaIF_stmt(ctx.javaIF_stmt()));
        }
        if (ctx.call_function() != null) {
            Main.parentScope = parentScope;
            javaStmt.setCallFunction(visitCall_function(ctx.call_function()));
        } else if (ctx.var_equal_stmt() != null) {
            Main.parentScope = parentScope;
            javaStmt.setVarEqualStmt(visitVar_equal_stmt(ctx.var_equal_stmt()));
        } else if (ctx.differnt_kind_of_for_increament() != null) {
            Main.parentScope = parentScope;
            javaStmt.setDifferentKindOfForIncreament(visitDiffernt_kind_of_for_increament(ctx.differnt_kind_of_for_increament()));
        }
        return javaStmt;

    }

    @Override
    public ParameterArray visitParamete_array(SqlParser.Paramete_arrayContext ctx) {
        System.out.println("visit parameter array");
        ParameterArray parameterArray = new ParameterArray();
        parameterArray.setOk(true);
        if (ctx.number_item() != null)
            parameterArray.setNumberItem(visitNumber_item(ctx.number_item()));
        else if (ctx.any_name_item() != null)
            parameterArray.setAnyNameItem(visitAny_name_item(ctx.any_name_item()));


        return parameterArray;
    }

    @Override
    public AnyNameItem visitAny_name_item(SqlParser.Any_name_itemContext ctx) {
        System.out.println("visit any name item ");
        AnyNameItem anyNameItem = new AnyNameItem();
        anyNameItem.setOk(true);

        Scope currentScope = Main.symbolTable.getScopes().get(Main.symbolTable.getScopes().size() - 1);
        Symbol anyNameSymbol = new Symbol();
        anyNameSymbol.setName("parameter array anyNames");
        anyNameSymbol.setScope(currentScope);
        Type StringType = new Type();
        StringType.setName(Type.STRING_CONST);
        anyNameSymbol.setType(StringType);
        currentScope.addSymbol(anyNameSymbol.getName(), anyNameSymbol);

        ArrayList<AnyName> anyNames = new ArrayList<>();
        for (int i = 0; i < ctx.any_name().size(); i++)
            if (ctx.any_name(i) != null) {
                anyNames.add(visitAny_name(ctx.any_name(i)));
                anyNameSymbol.addSymbolValue(ctx.any_name(i).getText()); // save the any name in the array list  in Symbol class ..
            }
        anyNameItem.setAnyNames(anyNames);
        StringType.setValue(anyNameSymbol.getArraySymbolValue().toString());

        System.out.println("the array of any name : " + anyNameSymbol.getArraySymbolValue().toString());
        return anyNameItem;
    }

    @Override
    public NumberItem visitNumber_item(SqlParser.Number_itemContext ctx) {
        System.out.println("visit number item ");
        NumberItem numberItem = new NumberItem();
        numberItem.setOk(true);

        Scope currentScope = Main.symbolTable.getScopes().get(Main.symbolTable.getScopes().size() - 1);
        Symbol NumberSymbol = new Symbol();
        NumberSymbol.setName("parameter array numbers");
        NumberSymbol.setScope(currentScope);
        Type numberType = new Type();
        numberType.setName(Type.NUMBER_CONST);
        NumberSymbol.setType(numberType);
        currentScope.addSymbol(NumberSymbol.getName(), NumberSymbol);

        ArrayList<Integer> signedNumbers = new ArrayList<>();

        for (int i = 0; i < ctx.signed_number().size(); i++)
            if (ctx.signed_number(i) != null) {
                numberItem.setSignedNumber_(visitSigned_number(ctx.signed_number(i)));
                NumberSymbol.addSymbolValue(ctx.signed_number(i).getText());// save the numbers in the array list  in Symbol class ..
            }
        numberItem.setSignedNumber(signedNumbers);
        numberType.setValue(NumberSymbol.getArraySymbolValue().toString());

        System.out.println("the array of numbers : " + NumberSymbol.getArraySymbolValue().toString());

        return numberItem;
    }

    @Override
    public AggregationFunction visitAggregationFunction(SqlParser.AggregationFunctionContext ctx) {
        System.out.println("visit aggregation Function");
        AggregationFunction aggregationFunction = new AggregationFunction();

        Scope aggScope = new Scope();
        aggScope.setId("AggFun_" + ctx.function_name().getText());
        aggScope.setParent(Main.parentScope);
        Main.symbolTable.addScope(aggScope);

        Type StringType = new Type();
        StringType.setName(Type.STRING_CONST);
        if (ctx.function_name() != null)
            aggregationFunction.setAggregationFunctionName(ctx.function_name().any_name().getText());
        if (ctx.jarPath() != null) {
            aggregationFunction.setJarPath(ctx.jarPath().any_name().getText());
            System.out.println("jar path");
            Symbol pathSymbol = new Symbol();
            pathSymbol.setScope(aggScope);
            pathSymbol.setName(ctx.jarPath().any_name().getText());
            pathSymbol.setIsParam(true);

            pathSymbol.setType(StringType);
            aggScope.addSymbol(pathSymbol.getName(), pathSymbol);
        }
        if (ctx.className() != null) {
            aggregationFunction.setClassName(ctx.className().any_name().getText());
            System.out.println("class name");
            Symbol classNameSymbol = new Symbol();
            classNameSymbol.setScope(aggScope);
            classNameSymbol.setName(ctx.className().any_name().getText());
            classNameSymbol.setIsParam(true);

            classNameSymbol.setType(StringType);
            aggScope.addSymbol(classNameSymbol.getName(), classNameSymbol);
        }
        if (ctx.methodName() != null) {
            aggregationFunction.setMethodName(ctx.methodName().any_name().getText());
            System.out.println("method name");
            Symbol methodNameSymbol = new Symbol();
            methodNameSymbol.setScope(aggScope);
            methodNameSymbol.setName(ctx.methodName().any_name().getText());
            methodNameSymbol.setIsParam(true);

            methodNameSymbol.setType(StringType);
            aggScope.addSymbol(methodNameSymbol.getName(), methodNameSymbol);
        }
        if (ctx.returnType() != null) {
            //  aggregationFunction.setReturnType(ctx.returnType().any_name().getText());
            System.out.println("visit return type");
            Symbol returnTypeSymbol = new Symbol();
            returnTypeSymbol.setScope(aggScope);

            if (ctx.returnType().any_name() != null) {
                if (ctx.returnType().any_name().STRING_LITERAL() != null) {
                    aggregationFunction.setReturnType(ctx.returnType().any_name().getText());
                    returnTypeSymbol.setName(ctx.returnType().any_name().STRING_LITERAL().getText());
                    returnTypeSymbol.setIsParam(true);
                    returnTypeSymbol.setType(StringType);
                    aggScope.addSymbol(returnTypeSymbol.getName(), returnTypeSymbol);
                }
            } else if (ctx.returnType().NUMERIC_LITERAL() != null) {
                aggregationFunction.setReturnType(ctx.returnType().NUMERIC_LITERAL().getText());
                returnTypeSymbol.setName(ctx.returnType().NUMERIC_LITERAL().getText());
                returnTypeSymbol.setIsParam(true);
                Type numType = new Type();
                numType.setName(Type.NUMBER_CONST);
                returnTypeSymbol.setType(numType);
                aggScope.addSymbol(returnTypeSymbol.getName(), returnTypeSymbol);
            } else if (ctx.returnType().K_TRUE() != null || ctx.returnType().K_TRUE() != null) {
                aggregationFunction.setReturnType(ctx.returnType().getText());
                returnTypeSymbol.setName(ctx.returnType().getText()); // temp
                returnTypeSymbol.setIsParam(true);
                Type boolType = new Type();
                boolType.setName(Type.BOOLEAN_CONST);
                returnTypeSymbol.setType(boolType);
                aggScope.addSymbol(returnTypeSymbol.getName(), returnTypeSymbol);
            }
        }
        if (ctx.parameter_AggFunction() != null) {
            aggregationFunction.setParams(visitParamete_array(ctx.parameter_AggFunction().paramete_array()));
        }
        Main.symbolTable.addAgg(aggregationFunction);

        return aggregationFunction;
    }

    @Override
    public CreateTypeStmt visitCreate_type_stmt(SqlParser.Create_type_stmtContext ctx) {
        System.out.println("visit create type stmt");
        CreateTypeStmt createTypeStmt = new CreateTypeStmt();
        createTypeStmt.setOk(true);

        Scope createtypeScope = new Scope();
        createtypeScope.setId(ctx.name_type().getText());
        createtypeScope.setParent(Main.parentScope);
        Main.symbolTable.addScope(createtypeScope);


        Type newType = new Type();
        newType.setName(ctx.name_type().getText());
        newType.setValue("type");

        Main.symbolTable.addType(newType);
        System.out.println("the name of new type: " + newType.getName());

        if (ctx.name_type() != null)
            createTypeStmt.setNameType(ctx.name_type().any_name().getText());

        ArrayList<ColDefType> colDefTypes = new ArrayList<>();
        for (int i = 0; i < ctx.column_def_type().size(); i++)
            if (ctx.column_def_type(i) != null) {
                Main.parentType = newType;
                Main.parentScope = createtypeScope;
                colDefTypes.add(visitColumn_def_type(ctx.column_def_type(i)));
            }
        createTypeStmt.setColDefTypes(colDefTypes);
        createTypeStmt.setType(newType); // add to class..

        createTypeStmt.setLine(ctx.getStart().getLine()); //get line number
        createTypeStmt.setCol(ctx.getStart().getCharPositionInLine());


        return createTypeStmt;
    }


    @Override
    public ColDefType visitColumn_def_type(SqlParser.Column_def_typeContext ctx) {
        System.out.println("visit column  def type ");
        ColDefType colDefType = new ColDefType();
        colDefType.setOk(true);

        Type currentType = Main.parentType;
        Scope currentScope = Main.parentScope;
        System.out.println("the name of new type: " + currentType.getName());
        Type type = new Type();

        Symbol symbol;
        symbol = editSymbol(currentScope, ctx.column_name().any_name().getText());
        if (symbol.getName() != null) {

            colDefType.setSymbolColumn(null);
        } else {
            symbol = new Symbol();
            symbol.setScope(currentScope);

            if (ctx.column_name() != null) {
                colDefType.setColumnName(visitAny_name(ctx.column_name().any_name()));
                symbol.setName(ctx.column_name().any_name().getText());
            }

            if (ctx.any_name() != null) {
                colDefType.setTypeName(visitAny_name(ctx.any_name()));

                type.setValue(ctx.any_name().getText());
                type.setName(ctx.column_name().any_name().getText()); // key ..
                type.setFrom(currentScope.getId()); // the name of type
                System.out.println("from : " + type.getFrom());
                symbol.setType(type);
                currentType.addType(type.getName(), type);
                Main.symbolTable.addTypeToArrayTypes(type);
                System.out.println("the type saved: " + type.getValue());

            }
            currentScope.addSymbol(symbol.getName(), symbol);
            System.out.println("after add column name  : " + symbol.getName());
            colDefType.setSymbolColumn(symbol);  /// add to class
        }
        colDefType.setLine(ctx.getStart().getLine()); //get line number
        colDefType.setCol(ctx.getStart().getCharPositionInLine());

        return colDefType;
    }

    @Override
    public DeclerationVariable visitVar_stmt(SqlParser.Var_stmtContext ctx) {
        System.out.println("Visit Var stmt");
        Scope varScope = Main.parentScope;
        System.out.println("var scope " + varScope.getId());

        Symbol varSymbol = new Symbol();    // this check for the saved in item ..

        DeclerationVariable decVar = new DeclerationVariable();
        decVar.setOk(true);
        decVar.setNameVar(ctx.any_name().IDENTIFIER().getText());
        if (ctx.K_VAR() != null) {
            decVar.setVar("var");
        }

        System.out.println(decVar.getVar());

        Symbol symbol = new Symbol();
        if (ctx.K_VAR() != null)
            symbol = editSymbol(varScope, "var " + ctx.any_name().IDENTIFIER().getText());

        if (symbol.getName() != null) {
            System.out.println("saved");
            decVar.setSymbolVariable(null);

        } else {
            System.out.println("not saved");
            varSymbol = new Symbol();
            if (ctx.K_VAR() != null)
                varSymbol.setName("var " + ctx.any_name().IDENTIFIER().getText());
            else {
                varSymbol.setName(ctx.any_name().IDENTIFIER().getText());
            }
            varSymbol.setScope(varScope);
            // no type yet ..
            System.out.println("this is scope " + varScope.getId());
            if (ctx.K_VAR() != null)
                varScope.addSymbol("var " + ctx.any_name().IDENTIFIER().getText(), varSymbol);
            else varScope.addSymbol(ctx.any_name().IDENTIFIER().getText(), varSymbol);
            decVar.setSymbolVariable(varSymbol);

        }

        decVar.setLine(ctx.getStart().getLine()); //get line number
        decVar.setCol(ctx.getStart().getCharPositionInLine());

        return decVar;
    }

    @Override
    public VarEqualStmt visitVar_equal_stmt(SqlParser.Var_equal_stmtContext ctx) {
        System.out.println("visit var equal stmt");
        // this this make later ..
        VarEqualStmt varEqualStmt = new VarEqualStmt();
        varEqualStmt.setOk(true);
        Scope currentScope = Main.parentScope;
        if (ctx.call_function() != null) {
            Main.parentScope = currentScope;
            varEqualStmt.setCallFunction(visitCall_function(ctx.call_function()));
        }

        if (ctx.var_stmt() != null) {
            Main.parentScope = currentScope;
            varEqualStmt.setDeclerationVariable(visitVar_stmt(ctx.var_stmt()));
            variableName = varEqualStmt.getDeclerationVariable().getNameVar();
        }

        if (ctx.javaIFline_stmt() != null) {
            Main.parentScope = currentScope;
            varEqualStmt.setIfLine(visitJavaIFline_stmt(ctx.javaIFline_stmt()));
        }
        if (ctx.sql_stmt() != null) {
            Main.parentScope = currentScope;
            System.out.println("lllljjjiioopp");
            varEqualStmt.setSqlStmt(visitSql_stmt(ctx.sql_stmt()));


//            System.out.println("kkdsfk;ds;f;kdsl;f " + varEqualStmt.getSqlStmt().getFactoredSelectStmt().getSelectOrValue().getSelTableAm().getTableOrSubOnes().get(0).getTableName());



            /*/-/-/-/-/-/-/-/--//--///--/-/-/-//-/-/-/-*/

//            Symbol symbol = new Symbol();
//            symbol.setName("sql1");
//            Type type = new Type();
//            type.setName(varEqualStmt.getSqlStmt().getFactoredSelectStmt().getSelectOrValue().getSelTableAm().getTableOrSubOnes().get(0).getTableName());
//            type.setValue(ctx.var_stmt().any_name().getText());
//            symbol.setType(type);
//            symbol.setScope(currentScope);
//            currentScope.addSymbol(symbol.getName(), symbol);
//
//            symbol.setLine(ctx.getStart().getLine());
//            symbol.setCol(ctx.getStart().getCharPositionInLine());


//            varEqualStmt.setSymbol(symbol);


            /*/-/-/-/-/-/-/-/--//--///--/-/-/-//-/-/-/-*/


        }

        // save the type of var
        if (ctx.expr() != null) {
            Symbol symbol = editSymbol(currentScope, ctx.var_stmt().any_name().getText());
            if (symbol.getName() == null) {
                System.out.println("haha11");
                symbol = editSymbol(currentScope, "var " + ctx.var_stmt().any_name().IDENTIFIER().getText());
            }
            System.out.println("symmmm " + symbol.getName());
            Type type = new Type();
            String typeName = "notType";
            if (ctx.expr().literal_value() != null)
                if (ctx.expr().literal_value().NUMERIC_LITERAL() != null)
                    typeName = Type.NUMBER_CONST;
                else if (ctx.expr().literal_value().STRING_LITERAL() != null)
                    typeName = Type.CHAR_CONST;
            if (ctx.expr().K_TRUE() != null || ctx.expr().K_FALSE() != null)
                typeName = Type.BOOLEAN_CONST;
            if (ctx.expr().database_exp() != null)
                if (ctx.expr().database_exp().column_name().any_name() != null) {
                    typeName = Type.STRING_CONST;
//                    System.out.println("ctx "+ctx.expr().database_exp().column_name().any_name().getText());
                }
            type.setName(typeName);
            type.setValue(ctx.expr().getText());
            symbol.setType(type);


            varEqualStmt.setExp(visitExpr(ctx.expr()));

            System.out.println("the type of symbol " + symbol.getType().getName());
            System.out.println("the value of symbol " + type.getValue());
        }
        return varEqualStmt;

    }

    @Override
    public ConditionStmt visitCondtion_stmt(SqlParser.Condtion_stmtContext ctx) {
        System.out.println("visit condition stmt");
        Scope parentScope = Main.parentScope;

        ConditionStmt conditionStmt = new ConditionStmt();
        conditionStmt.setOk(true);

        boolean check = false;
        Scope currentScope = Main.symbolTable.getScopes().get(Main.symbolTable.getScopes().size() - 1);

        if (ctx.expr() != null) {
            Type conditionExpType = new Type();
            String typeName = "";
            check = false;
            if (ctx.expr().literal_value() != null) {
                if (ctx.expr().literal_value().NUMERIC_LITERAL() != null) {
                    System.out.print("ERROR IN CONDITION ");
                    System.out.println("In Line " + ctx.getStart().getLine() + " And " + ctx.getStart().getCharPositionInLine());
                } else if (ctx.expr().literal_value().STRING_LITERAL() != null) {
                    System.out.print("ERROR IN CONDITION ");
                    System.out.println("In Line " + ctx.getStart().getLine() + " And " + ctx.getStart().getCharPositionInLine());
                } else if (ctx.expr().literal_value().BLOB_LITERAL() != null) {
                    System.out.print("ERROR IN CONDITION ");
                    System.out.println("In Line " + ctx.getStart().getLine() + " And " + ctx.getStart().getCharPositionInLine());
                } else if (ctx.expr().literal_value().K_CURRENT_TIME() != null) {
                    System.out.print("ERROR IN CONDITION ");
                    System.out.println("In Line " + ctx.getStart().getLine() + " And " + ctx.getStart().getCharPositionInLine());
                } else if (ctx.expr().literal_value().K_CURRENT_DATE() != null) {
                    System.out.print("ERROR IN CONDITION ");
                    System.out.println("In Line " + ctx.getStart().getLine() + " And " + ctx.getStart().getCharPositionInLine());
                } else if (ctx.expr().literal_value().K_CURRENT_TIMESTAMP() != null) {
                    System.out.print("ERROR IN CONDITION ");
                    System.out.println("In Line " + ctx.getStart().getLine() + " And " + ctx.getStart().getCharPositionInLine());
                }
            } else if (ctx.expr().database_exp() != null) {
                if (ctx.expr().database_exp().column_name().any_name() != null) {
                    if (!ctx.expr().database_exp().column_name().any_name().getText().startsWith("\"")) {
                        typeName = Type.BOOLEAN_CONST;
                        conditionExpType.setValue(ctx.expr().database_exp().column_name().any_name().getText());
                        check = true;
                    }
                    System.out.print("ERROR IN CONDITION ");
                    System.out.println("In Line " + ctx.getStart().getLine() + " And " + ctx.getStart().getCharPositionInLine());
                }
            } else if (ctx.expr().K_TRUE() != null) {
                typeName = Type.BOOLEAN_CONST;
                conditionExpType.setValue(ctx.expr().K_TRUE().getText());
                check = true;
            } else if (ctx.expr().K_FALSE() != null) {
                conditionExpType.setValue(ctx.expr().K_FALSE().getText());
                typeName = Type.BOOLEAN_CONST;
                check = true;
            }

            if (check) {
                conditionExpType.setName(typeName);
                Symbol conditionExpSymbol = new Symbol();
                conditionExpSymbol.setName("condition exp value " + conditionExpType.getValue());
                conditionExpSymbol.setScope(currentScope);
                conditionExpSymbol.setType(conditionExpType);
                currentScope.addSymbol("condition exp value " + conditionExpType.getValue(), conditionExpSymbol);
                conditionStmt.setSymbol(conditionExpSymbol);
            }
        }


        if (ctx.expr() != null)
            conditionStmt.setExp(visitExpr(ctx.expr()));
        Symbol symbol = lastSymbol(currentScope);
        if (symbol.getName().equals("operator between two exp")) {
//            System.out.println("jjjj "+symbol.getName());
            conditionStmt.setSymbol(symbol);
        }
        conditionStmt.setLine(ctx.getStart().getLine()); //get line number
        conditionStmt.setCol(ctx.getStart().getCharPositionInLine());

        return conditionStmt;
    }

    @Override
    public BodyAllStmt visitBody_all_stmt(SqlParser.Body_all_stmtContext ctx) {
        System.out.println("visit body all stmt");
        BodyAllStmt bodyAllStmt = new BodyAllStmt();
        bodyAllStmt.setOk(true);

        Scope bodyScope = Main.parentScope;

        for (int i = 0; i < ctx.java_anystmt().size(); i++) {
            Main.parentScope = bodyScope;
            bodyAllStmt.setJavaAnyStmt(visitJava_anystmt(ctx.java_anystmt(i)));
            // here you should add the type of expr in java any stmt ..
        }
        return bodyAllStmt;
    }

    @Override
    public ConditionStmt visitIf_condition(SqlParser.If_conditionContext ctx) {
        System.out.println("visit if condition");
        ConditionStmt conditionStmt = visitCondtion_stmt(ctx.condtion_stmt());
        conditionStmt.setOk(true);
        return conditionStmt;
    }

    @Override
    public IfNormal visitJavaIF_stmt(SqlParser.JavaIF_stmtContext ctx) {
        System.out.println("visit if stmt ");
        IfNormal ifNormal = new IfNormal();
        ifNormal.setOk(true);

        Scope ifScope = new Scope();
        ifScope.setId("if" + ctx.if_condition().condtion_stmt().expr().getText());
        ifScope.setParent(Main.parentScope);
        Main.symbolTable.addScope(ifScope);
        Main.parentScope = ifScope;

        ifNormal.setTypeIf("IF NORMAL");
        Main.parentScope = ifScope;
        ifNormal.setConditionStmt(visitIf_condition(ctx.if_condition()));
        Main.parentScope = ifScope;
        ifNormal.setBodyAllStmt(visitBody_all_stmt(ctx.body_all_stmt()));
        for (int i = 0; i < ctx.java_else().size(); i++) {
            Main.parentScope = ifScope;
            ifNormal.setAnElse(visitJava_else(ctx.java_else(i)));
        }
        return ifNormal;
    }

    @Override
    public Else visitJava_else(SqlParser.Java_elseContext ctx) {
        System.out.println("visit else ");
        Else anElse = new Else();
        anElse.setOk(true);


        Scope elseScope = new Scope();
        elseScope.setId("else ");
        elseScope.setParent(Main.parentScope.getParent());
        System.out.println("else scope" + elseScope.getId());
        System.out.println("else parent scope" + elseScope.getParent().getId());
        Main.symbolTable.addScope(elseScope);

        if (ctx.javaIF_stmt() != null) {
            Main.parentScope = elseScope;
            anElse.setIfNormal(visitJavaIF_stmt(ctx.javaIF_stmt()));
        }
        if (ctx.body_all_stmt() != null) {
            Main.parentScope = elseScope;
            anElse.setBodyAllStmt(visitBody_all_stmt(ctx.body_all_stmt()));
        }
        return anElse;
    }

    @Override
    public IfLine visitJavaIFline_stmt(SqlParser.JavaIFline_stmtContext ctx) {
        System.out.println("visit if line ");
        IfLine ifLine = new IfLine();
        ifLine.setOk(true);

        Scope iflineScope = new Scope();

        iflineScope.setId("IfLine" + ctx.condtion_stmt().expr().getText());
        iflineScope.setParent(Main.parentScope);
        System.out.println("IfLine scope" + iflineScope.getId());
        System.out.println("IfLine parent scope" + iflineScope.getParent().getId());
        Main.symbolTable.addScope(iflineScope);
        iflineScope = Main.parentScope;

        ifLine.setTypeIf("IF LINE");
        if (ctx.condtion_stmt() != null) {
            Main.parentScope = iflineScope;
            ifLine.setConditionStmt(visitCondtion_stmt(ctx.condtion_stmt()));
        }
        Main.parentScope = iflineScope;
        ifLine.setBodyIfLine(visitIfline_body(ctx.ifline_body()));

        return ifLine;
    }

    @Override
    public BodyIfLine visitIfline_body(SqlParser.Ifline_bodyContext ctx) {
        System.out.println("visit If line body");
        BodyIfLine bodyIfLine = new BodyIfLine();
        bodyIfLine.setOk(true);

        Scope Body = Main.parentScope;
        System.out.println("IfLineBody scopeParent" + Body.getId());
        if (ctx.realized_condition() != null) {
            Main.parentScope = Body;
            bodyIfLine.setRealizedCondition(visitRealized_condition(ctx.realized_condition()));
        }
        if (ctx.unrealized_condition() != null) {
            Main.parentScope = Body;
            bodyIfLine.setUnRealizedConditon(visitUnrealized_condition(ctx.unrealized_condition()));
        }
        return bodyIfLine;
    }

    @Override
    public UnRealizedConditon visitUnrealized_condition(SqlParser.Unrealized_conditionContext ctx) {
        System.out.println("visit If realized condition");
        UnRealizedConditon unRealizedConditon = new UnRealizedConditon();
        unRealizedConditon.setOk(true);


        Scope unRealizedScope = new Scope();

        if (ctx.expr() != null)
            unRealizedScope.setId("unRealized " + ctx.expr().getText());
        else if (ctx.differnt_kind_of_for_increament() != null)
            unRealizedScope.setId("unRealized" + ctx.differnt_kind_of_for_increament().getText());
        else
            unRealizedScope.setId("unRealized " + ctx.complete_ifLine().javaIFline_stmt().condtion_stmt().getText());
        unRealizedScope.setParent(Main.parentScope);
        System.out.println("unRealized scope" + unRealizedScope.getId());
        System.out.println("unRealized parent scope" + unRealizedScope.getParent().getId());
        Main.symbolTable.addScope(unRealizedScope);

        if (ctx.expr() != null) {
            Main.parentScope = unRealizedScope;
            unRealizedConditon.setExps(visitExpr(ctx.expr()));
            Symbol symbol = editSymbol(unRealizedScope, "operator between two exp");
            if (symbol.getExpression() != null)
                unRealizedConditon.setSymbol(symbol);
        }

        if (ctx.differnt_kind_of_for_increament() != null) {
            Main.parentScope = unRealizedScope;
            unRealizedConditon.setDifferentKindOfForIncreament(visitDiffernt_kind_of_for_increament(ctx.differnt_kind_of_for_increament()));
            if (ctx.differnt_kind_of_for_increament().discreament_var() != null || ctx.differnt_kind_of_for_increament().increament_var() != null) {
                Symbol difSymbol = editSymbol(unRealizedScope, unRealizedConditon.getDifferentKindOfForIncreament().getIncrDecr().getAnyName().getAnyName());
                System.out.println("the name of symbol" + difSymbol.getName());
                unRealizedConditon.setSymbol(difSymbol);
            } else if (ctx.differnt_kind_of_for_increament().any_name() != null) {
                Symbol difSymbol = editSymbol(unRealizedScope, unRealizedConditon.getDifferentKindOfForIncreament().getAnyName().getAnyName());
                System.out.println("the name of dif " + difSymbol.getName());
                unRealizedConditon.setSymbol(difSymbol);
            }
        } else
            System.out.println(" else ..");
        if (ctx.complete_ifLine() != null) {
            Main.parentScope = unRealizedScope;
            unRealizedConditon.setCompleteIfLine(visitComplete_ifLine(ctx.complete_ifLine()));
        }

        unRealizedConditon.setLine(ctx.getStart().getLine()); //get line number
        unRealizedConditon.setCol(ctx.getStart().getCharPositionInLine());

        return unRealizedConditon;
    }

    @Override
    public RealizedCondition visitRealized_condition(SqlParser.Realized_conditionContext ctx) {
        System.out.println("visit If realized condition");
        RealizedCondition realizedCondition = new RealizedCondition();
        realizedCondition.setOk(true);

        Scope realizedScope = new Scope();
        if (ctx.expr() != null)
            realizedScope.setId("Realized " + ctx.expr().getText());
        else if (ctx.differnt_kind_of_for_increament() != null)
            realizedScope.setId("Realized " + ctx.differnt_kind_of_for_increament().getText());
        else
            realizedScope.setId("Realized " + ctx.complete_ifLine().javaIFline_stmt().condtion_stmt().getText());
        realizedScope.setParent(Main.parentScope);
        System.out.println("Realized scope " + realizedScope.getId());
        System.out.println("Realized parent scope " + realizedScope.getParent().getId());
        Main.symbolTable.addScope(realizedScope);

        if (ctx.expr() != null) {
            Main.parentScope = realizedScope;
            realizedCondition.setExps(visitExpr(ctx.expr()));
            Symbol symbol = editSymbol(realizedScope, "operator between two exp");
            if (symbol.getExpression() != null)
                realizedCondition.setSymbol(symbol);

        }

        if (ctx.differnt_kind_of_for_increament() != null) {
            Main.parentScope = realizedScope;
            realizedCondition.setDifferentKindOfForIncreament(visitDiffernt_kind_of_for_increament(ctx.differnt_kind_of_for_increament()));
            if (ctx.differnt_kind_of_for_increament().increament_var() != null || ctx.differnt_kind_of_for_increament().discreament_var() != null) {
                Symbol difSymbol = editSymbol(realizedScope, realizedCondition.getDifferentKindOfForIncreament().getIncrDecr().getAnyName().getAnyName());
                System.out.println("the name of dif " + difSymbol.getName());
                realizedCondition.setSymbol(difSymbol);
            } else if (ctx.differnt_kind_of_for_increament().any_name() != null) {
                Symbol difSymbol = editSymbol(realizedScope, realizedCondition.getDifferentKindOfForIncreament().getAnyName().getAnyName());
                System.out.println("the name of dif " + difSymbol.getName());
                realizedCondition.setSymbol(difSymbol);
            }
        } else System.out.println(" else ..");
        if (ctx.complete_ifLine() != null) {
            Main.parentScope = realizedScope;
            realizedCondition.setCompleteIfLine(visitComplete_ifLine(ctx.complete_ifLine()));
        }

        realizedCondition.setLine(ctx.getStart().getLine()); //get line number
        realizedCondition.setCol(ctx.getStart().getCharPositionInLine());
        return realizedCondition;
    }


    @Override
    public CompleteIfLine visitComplete_ifLine(SqlParser.Complete_ifLineContext ctx) {
        System.out.println("visit complete if line");
        CompleteIfLine completeIfLine = new CompleteIfLine();
        completeIfLine.setOk(true);

        if (ctx.javaIFline_stmt() != null)
            completeIfLine.setIfLine(visitJavaIFline_stmt(ctx.javaIFline_stmt()));
        return completeIfLine;
    }

    @Override
    public DifferentKindOfForIncreament visitDiffernt_kind_of_for_increament(SqlParser.Differnt_kind_of_for_increamentContext ctx) {
        System.out.println("visit different kind of for increment");
        Scope currentScope = Main.parentScope;

        DifferentKindOfForIncreament differentKindOfForIncreament = new DifferentKindOfForIncreament();
        differentKindOfForIncreament.setOk(true);

        Type varType = new Type();
        varType.setName(Type.NUMBER_CONST);

        String nameVariable;


        // for any name in this rule
        if (ctx.any_name() != null) {
            differentKindOfForIncreament.setAnyName(visitAny_name(ctx.any_name()));
            System.out.println("ff " + ctx.any_name().getText());
            // the type f variable
            Symbol varSymbol = new Symbol();
            varSymbol.setType(varType);
            varSymbol.setScope(currentScope);
            varSymbol.setName(ctx.any_name().getText());
            System.out.println("nameSymbol" + varSymbol.getName());
            System.out.println("type Symbol" + varSymbol.getType().getName());
            System.out.println("scope Symbol" + varSymbol.getScope().getId());
            currentScope.addSymbol(ctx.any_name().getText(), varSymbol);
        }
        if (ctx.signal() != null) {
            differentKindOfForIncreament.setFirstSignal(ctx.signal().getText());
            System.out.println("sizeee" + ctx.children.size());
        }
        if (ctx.children.size() >= 3)
            if (ctx.children.get(3).getText().equals("+"))
                differentKindOfForIncreament.setSecondSignal("+");
            else if (ctx.children.get(3).getText().equals("-"))
                differentKindOfForIncreament.setSecondSignal("-");

        /// for increment and decrement
        IncrDecr incrDecr;
        if (ctx.increament_var() != null) {
            incrDecr = new IncrDecr();
            incrDecr.setOk(true);
            incrDecr.setAnyName(visitAny_name(ctx.increament_var().any_name()));
            incrDecr.setSignedNumber("+");

            // the type of var in increment
            Symbol varIncSymbol = new Symbol();
            varIncSymbol.setType(varType);
            varIncSymbol.setScope(currentScope);
            varIncSymbol.setName(ctx.increament_var().any_name().getText());
            currentScope.addSymbol(ctx.increament_var().any_name().getText(), varIncSymbol);


            if (ctx.increament_var().children.get(0).equals("+"))
                incrDecr.setState("prev");
            else incrDecr.setState("next");
            System.out.println(incrDecr.getState() + "       ppppppp");
            differentKindOfForIncreament.setIncrDecr(incrDecr);
        } else if (ctx.discreament_var() != null) {
            incrDecr = new IncrDecr();
            incrDecr.setOk(true);
            incrDecr.setAnyName(visitAny_name(ctx.discreament_var().any_name()));
            incrDecr.setSignedNumber("-");

            // the type of the var in discrement
            Symbol varDiscSymbol = new Symbol();
            varDiscSymbol.setType(varType);
            varDiscSymbol.setScope(currentScope);
            varDiscSymbol.setName(ctx.discreament_var().any_name().getText());
            currentScope.addSymbol(ctx.discreament_var().any_name().getText(), varDiscSymbol);

            if (ctx.increament_var() != null)
                if (ctx.increament_var().children.get(0).equals("-"))
                    incrDecr.setState("prev");
                else incrDecr.setState("next");
            differentKindOfForIncreament.setIncrDecr(incrDecr);
        } else if (ctx.literal_value() != null) { /// for litereal value
            differentKindOfForIncreament.setLiteralValue(visitLiteral_value(ctx.literal_value()));

            Symbol literalSymbol = new Symbol();
            Type type = new Type();
            if (ctx.literal_value().STRING_LITERAL() != null)
                type.setName(Type.CHAR_CONST);
            else if (ctx.literal_value().NUMERIC_LITERAL() != null)
                type.setName(Type.NUMBER_CONST);
            literalSymbol.setType(type);
            literalSymbol.setScope(currentScope);
            literalSymbol.setName(ctx.literal_value().getText());
            currentScope.addSymbol(ctx.literal_value().getText(), literalSymbol);

        } else if (ctx.call_function() != null)  /// i must add the type of the return value of function
        {
            Main.parentScope = currentScope;
            differentKindOfForIncreament.setCallFunction(visitCall_function(ctx.call_function()));
        }

        return differentKindOfForIncreament;
    }


    @Override
    public ForNoraml visitJavaFor_stmt(SqlParser.JavaFor_stmtContext ctx) {
        System.out.println("visit for normal");
        ForNoraml forNoraml = new ForNoraml();
        forNoraml.setOk(true);

        Scope forScope = new Scope();
        forScope.setId("for");
        forScope.setParent(Main.parentScope);
        System.out.println("for scope" + forScope.getId());
        System.out.println("for parent scope" + forScope.getParent().getId());
        Main.symbolTable.addScope(forScope);
        Main.parentScope = forScope;

        forNoraml.setDecVar(visitVar_stmt(ctx.var_stmt()));
        Symbol symbol = editSymbol(forScope, ctx.var_stmt().any_name().getText());
        Type type = new Type();
        type.setName(Type.NUMBER_CONST);
        symbol.setType(type);
        System.out.println("the type of symbol " + symbol.getType().getName());

        Main.parentScope = forScope;
        forNoraml.setExp(visitExpr(ctx.expr()));
        Symbol expSymbol = editSymbol(forScope, "operator between two exp");
        if (expSymbol.getExpression() != null)
            forNoraml.setSymbol(expSymbol);
        Main.parentScope = forScope;
        forNoraml.setDifferentKindOfForIncreament(visitDiffernt_kind_of_for_increament(ctx.differnt_kind_of_for_increament()));
        Main.parentScope = forScope;
        forNoraml.setBodyAllStmt(visitBody_all_stmt(ctx.body_all_stmt()));

        forNoraml.setLine(ctx.getStart().getLine()); //get line number
        forNoraml.setCol(ctx.getStart().getCharPositionInLine());
        return forNoraml;
    }

    @Override
    public ForEach visitJavaForeach_stmt(SqlParser.JavaForeach_stmtContext ctx) {
        System.out.println("visit for each");
        ForEach forEach = new ForEach();
        forEach.setOk(true);

        Scope foreachScope = new Scope();
        foreachScope.setId("for");
        foreachScope.setParent(Main.parentScope);
        System.out.println("for scope" + foreachScope.getId());
        System.out.println("for parent scope" + foreachScope.getParent().getId());
        Main.symbolTable.addScope(foreachScope);

        Main.parentScope = foreachScope;
        forEach.setDecVar(visitVar_stmt(ctx.var_stmt())); // the type of this determent in the bodyOfAllStmt ..in expr

        forEach.setAnyName(visitAny_name(ctx.any_name()));
        Main.parentScope = foreachScope;
        forEach.setBodyAllStmt(visitBody_all_stmt(ctx.body_all_stmt()));

        return forEach;
    }


    @Override
    public SelectStmt visitFactored_select_stmt(SqlParser.Factored_select_stmtContext ctx) {
        System.out.println("visit Factored Select stmt");
        SelectStmt selectStmt = new SelectStmt();
        selectStmt.setOk(true);
        Scope currentScope = Main.parentScope;

        Main.parentScope = currentScope;
        selectStmt.setSelectOrValue(visitSelect_core(ctx.select_core()));


        if (ctx.ordering_term() != null)
            for (int i = 0; i < ctx.ordering_term().size(); i++) {
                Main.parentScope = currentScope;
                selectStmt.setOrderingTerms(visitOrdering_term(ctx.ordering_term(i)));
            }
        if (ctx.expr() != null)
            for (int i = 0; i < ctx.expr().size(); i++) {
                Main.parentScope = currentScope;
                selectStmt.setExps(visitExpr(ctx.expr(i)));
            }
        return selectStmt;
    }

    @Override
    public SelectStmt visitSelect_stmt(SqlParser.Select_stmtContext ctx) {
        System.out.println("visit Select stmt");
        SelectStmt selectStmt = new SelectStmt();
        selectStmt.setOk(true);
        selectStmt.setSelectOrValue(visitSelect_or_values(ctx.select_or_values()));
        if (ctx.ordering_term() != null)
            for (int i = 0; i < ctx.ordering_term().size(); i++)
                selectStmt.setOrderingTerms(visitOrdering_term(ctx.ordering_term(i)));
        if (ctx.expr() != null)
            for (int i = 0; i < ctx.expr().size(); i++)
                selectStmt.setExps(visitExpr(ctx.expr(i)));
        return selectStmt;
    }

    @Override
    public SelectOrValue visitSelect_or_values(SqlParser.Select_or_valuesContext ctx) {
        System.out.println("visit select or value");
        SelectOrValue selectOrValue = new SelectOrValue();
        selectOrValue.setOK(true);
        Scope parentScope = Main.parentScope;
        Scope selectOrValueScope = new Scope();
        selectOrValueScope.setId("select_In");
        selectOrValueScope.setParent(parentScope);
        Main.symbolTable.addScope(selectOrValueScope);
        Main.parentScope = selectOrValueScope;

        SelectAll selectAll = new SelectAll();


        if (ctx.result_column() != null)
            for (int i = 0; i < ctx.result_column().size(); i++) {
                Main.parentScope = selectOrValueScope;
                selectOrValue.setResultColumns(visitResult_column(ctx.result_column(i)));
                ArrayList<String> columnsName = new ArrayList<>();
                ArrayList<AggFun> aggFuns = new ArrayList<>();
                for (int j = 0; j < selectOrValue.getResultColumns().size(); j++) {
                    if (selectOrValue.getResultColumns().get(j).getColumnName() != null) {
                        columnsName.add(selectOrValue.getResultColumns().get(j).getColumnName());
                    }
                    if (selectOrValue.getResultColumns().get(j).getStar() != null) {
                        columnsName.add("*");
                    }
                    if (selectOrValue.getResultColumns().get(j).getAggFun() != null) {
                        aggFuns.add(selectOrValue.getResultColumns().get(j).getAggFun());
                    }
                }

                selectAll.setAggFuns(aggFuns);
                selectAll.setColumnNames(columnsName);
            }
        if (ctx.sel_table_AM() != null) {
            Main.parentScope = selectOrValueScope;
            selectOrValue.setSelTableAm(visitSel_table_AM(ctx.sel_table_AM()));

            ArrayList<String> tableNames = new ArrayList<>();
            for (int i = 0; i < selectOrValue.getSelTableAm().getTableOrSubOnes().size(); i++) {
                tableNames.add(selectOrValue.getSelTableAm().getTableOrSubOnes().get(i).getTableName());
            }
            selectAll.setTableNames(tableNames);


        }
        if (ctx.sel_exp_where_AM() != null) {
            Main.parentScope = selectOrValueScope;
            selectOrValue.setExpSqlAMWhere(visitSel_exp_where_AM(ctx.sel_exp_where_AM()));

            if (ctx.sel_exp_where_AM().expr().K_AND() != null | ctx.sel_exp_where_AM().expr().K_OR() != null) {
                WhereCondition whereCondition = new WhereCondition();

                if (ctx.sel_exp_where_AM().expr().K_AND() != null)
                    whereCondition.setKey("AND");
                else if (ctx.sel_exp_where_AM().expr().K_OR() != null)
                    whereCondition.setKey("OR");


                TableAndColumnName tableAndColumnNameAll = new TableAndColumnName();
                if (ctx.sel_exp_where_AM().expr().expr(0).expr(0).database_exp().database_name() != null)
                    tableAndColumnNameAll.setDataBaseName(ctx.sel_exp_where_AM().expr().expr(0).expr(0).database_exp().database_name().any_name().getText());
                if (ctx.sel_exp_where_AM().expr().expr(0).expr(0).database_exp().table_name() != null)
                    tableAndColumnNameAll.setTableName(ctx.sel_exp_where_AM().expr().expr(0).expr(0).database_exp().table_name().any_name().getText());
                tableAndColumnNameAll.setColName(ctx.sel_exp_where_AM().expr().expr(0).expr(0).database_exp().column_name().any_name().getText());
                whereCondition.setWhereNameCondition1(tableAndColumnNameAll);

                System.out.println("nanan is " + whereCondition.getWhereNameCondition1());
                ArrayList<TableAndColumnName> nameWhereCondiotion2 = new ArrayList<>();
                if (ctx.sel_exp_where_AM().expr().expr(0).second_sel_all() != null) {
                    for (int i = 0; i < ctx.sel_exp_where_AM().expr().expr(0).second_sel_all().expr().size(); i++) {
                        TableAndColumnName tableAndColumnName = new TableAndColumnName();
                        if (ctx.sel_exp_where_AM().expr().expr(0).second_sel_all().expr(i).database_exp() != null) {
                            if (ctx.sel_exp_where_AM().expr().expr(0).second_sel_all().expr(i).database_exp().database_name() != null) {
                                tableAndColumnName.setDataBaseName(ctx.sel_exp_where_AM().expr().expr(0).second_sel_all().expr(i).database_exp().database_name().any_name().getText());
                            }
                            tableAndColumnName.setTableName(ctx.sel_exp_where_AM().expr().expr(0).second_sel_all().expr(i).database_exp().table_name().any_name().getText());
                            tableAndColumnName.setColName(ctx.sel_exp_where_AM().expr().expr(0).second_sel_all().expr(i).database_exp().column_name().any_name().getText());
                        } else
                            tableAndColumnName.setColName(ctx.sel_exp_where_AM().expr().expr(0).second_sel_all().expr(i).literal_value().getText());

                        nameWhereCondiotion2.add(tableAndColumnName);
                        System.out.println("sss " + nameWhereCondiotion2.get(i).getColName());
                    }

                } else if (ctx.sel_exp_where_AM().expr().expr(0).expr(1) != null) {
                    TableAndColumnName tableAndColumnName = new TableAndColumnName();
                    if (ctx.sel_exp_where_AM().expr().expr(0).expr(1).database_exp() != null) {
                        if (ctx.sel_exp_where_AM().expr().expr(0).expr(1).database_exp().database_name() != null) {
                            tableAndColumnName.setDataBaseName(ctx.sel_exp_where_AM().expr().expr(0).expr(1).database_exp().database_name().any_name().getText());
                        }
                        tableAndColumnName.setTableName(ctx.sel_exp_where_AM().expr().expr(0).expr(1).database_exp().table_name().any_name().getText());
                        tableAndColumnName.setColName(ctx.sel_exp_where_AM().expr().expr(0).expr(1).database_exp().column_name().any_name().getText());
                    } else
                        tableAndColumnName.setColName(ctx.sel_exp_where_AM().expr().expr(0).expr(1).literal_value().getText());
                    nameWhereCondiotion2.add(tableAndColumnName);
                    System.out.println("kk " + nameWhereCondiotion2.get(0));
                }
                whereCondition.setOperation(ctx.sel_exp_where_AM().expr().expr(0).children.get(1).getText());
                whereCondition.setWhereNameCondition2(nameWhereCondiotion2);
                System.out.println("ddd " + whereCondition.getOperation());


                WhereCondition whereCondition2 = new WhereCondition();
                whereCondition2.setOperation(ctx.sel_exp_where_AM().expr().expr(1).children.get(1).getText());
                System.out.println("ddd " + whereCondition2.getOperation());

                TableAndColumnName tableAndColumnNameSecond = new TableAndColumnName();
                if (ctx.sel_exp_where_AM().expr().expr(1).expr(0).database_exp().database_name() != null) {
                    tableAndColumnNameSecond.setDataBaseName(ctx.sel_exp_where_AM().expr().expr(1).expr(0).database_exp().database_name().any_name().getText());
                }
                if (ctx.sel_exp_where_AM().expr().expr(1).expr(0).database_exp().table_name() != null)
                    tableAndColumnNameSecond.setTableName(ctx.sel_exp_where_AM().expr().expr(1).expr(0).database_exp().table_name().any_name().getText());
                tableAndColumnNameSecond.setColName(ctx.sel_exp_where_AM().expr().expr(1).expr(0).database_exp().column_name().any_name().getText());
                whereCondition2.setWhereNameCondition1(tableAndColumnNameSecond);

                ArrayList<TableAndColumnName> nameSecondWhereCondiotion2 = new ArrayList<>();
                if (ctx.sel_exp_where_AM().expr().expr(1).second_sel_all() != null) {
                    TableAndColumnName tableAndColumnName = new TableAndColumnName();
                    for (int i = 0; i < ctx.sel_exp_where_AM().expr().expr(1).second_sel_all().expr().size(); i++) {
                        if (ctx.sel_exp_where_AM().expr().expr(1).second_sel_all().expr(i).database_exp().database_name() != null) {
                            tableAndColumnName.setDataBaseName(ctx.sel_exp_where_AM().expr().expr(1).second_sel_all().expr(i).database_exp().database_name().any_name().getText());
                        }
                        if (ctx.sel_exp_where_AM().expr().expr(1).second_sel_all().expr(i).database_exp() != null) {
                            tableAndColumnName.setTableName(ctx.sel_exp_where_AM().expr().expr(1).second_sel_all().expr(i).database_exp().table_name().any_name().getText());
                            tableAndColumnName.setColName(ctx.sel_exp_where_AM().expr().expr(1).second_sel_all().expr(i).database_exp().column_name().any_name().getText());
                        } else
                            tableAndColumnName.setColName(ctx.sel_exp_where_AM().expr().expr(1).second_sel_all().expr(i).literal_value().getText());
                        nameSecondWhereCondiotion2.add(tableAndColumnName);
                        System.out.println("mmm " + nameSecondWhereCondiotion2.get(i));
                    }

                } else if (ctx.sel_exp_where_AM().expr().expr(1).expr(1) != null) {
                    TableAndColumnName tableAndColumnName = new TableAndColumnName();
                    if (ctx.sel_exp_where_AM().expr().expr(1).expr(1).database_exp() != null) {
                        if (ctx.sel_exp_where_AM().expr().expr(1).expr(1).database_exp().database_name() != null) {
                            tableAndColumnName.setDataBaseName(ctx.sel_exp_where_AM().expr().expr(1).expr(1).database_exp().database_name().any_name().getText());
                        }
                        tableAndColumnName.setTableName(ctx.sel_exp_where_AM().expr().expr(1).expr(1).database_exp().table_name().any_name().getText());
                        tableAndColumnName.setColName(ctx.sel_exp_where_AM().expr().expr(1).expr(1).database_exp().column_name().any_name().getText());
                    } else
                        tableAndColumnName.setColName(ctx.sel_exp_where_AM().expr().expr(1).expr(1).literal_value().getText());
                    nameSecondWhereCondiotion2.add(tableAndColumnName);
                    System.out.println("pp " + nameSecondWhereCondiotion2.get(0));
                }
                whereCondition2.setWhereNameCondition2(nameSecondWhereCondiotion2);


                whereCondition.setWhereCondition(whereCondition2);

                selectAll.setWhereCondition(whereCondition);
            } else {
                WhereCondition whereCondition = new WhereCondition();

                if (ctx.sel_exp_where_AM().expr() != null) {
                    System.out.println("kfsdkskdlfklsd");
                    whereCondition.setOperation(ctx.sel_exp_where_AM().expr().children.get(1).getText());
                    TableAndColumnName tableAndColumnNameAllSecond = new TableAndColumnName();
                    if (ctx.sel_exp_where_AM().expr().expr(0).database_exp().database_name() != null) {
                        tableAndColumnNameAllSecond.setDataBaseName(ctx.sel_exp_where_AM().expr().expr(0).database_exp().database_name().any_name().getText());
                    }
                    if (ctx.sel_exp_where_AM().expr().expr(0).database_exp().table_name() != null)
                        tableAndColumnNameAllSecond.setTableName(ctx.sel_exp_where_AM().expr().expr(0).database_exp().table_name().any_name().getText());
                    tableAndColumnNameAllSecond.setColName(ctx.sel_exp_where_AM().expr().expr(0).database_exp().column_name().any_name().getText());
                    whereCondition.setWhereNameCondition1(tableAndColumnNameAllSecond);

                    ArrayList<TableAndColumnName> nameWhereCondition2 = new ArrayList<>();
                    if (ctx.sel_exp_where_AM().expr().second_sel_all() != null) {
                        for (int i = 0; i < ctx.sel_exp_where_AM().expr().second_sel_all().expr().size(); i++) {
                            TableAndColumnName tableAndColumnName = new TableAndColumnName();
                            if (ctx.sel_exp_where_AM().expr().second_sel_all().expr(i).database_exp() != null) {
                                if (ctx.sel_exp_where_AM().expr().second_sel_all().expr(i).database_exp().database_name() != null) {
                                    tableAndColumnName.setDataBaseName(ctx.sel_exp_where_AM().expr().second_sel_all().expr(i).database_exp().database_name().any_name().getText());
                                }
                                if (ctx.sel_exp_where_AM().expr().second_sel_all().expr(i).database_exp().table_name() != null)
                                    tableAndColumnName.setTableName(ctx.sel_exp_where_AM().expr().second_sel_all().expr(i).database_exp().table_name().any_name().getText());
                                tableAndColumnName.setColName(ctx.sel_exp_where_AM().expr().second_sel_all().expr(i).database_exp().column_name().any_name().getText());
                            } else
                                tableAndColumnName.setColName(ctx.sel_exp_where_AM().expr().second_sel_all().expr(i).literal_value().getText());
                            nameWhereCondition2.add(tableAndColumnName);
                        }

                    } else if (ctx.sel_exp_where_AM().expr().expr(1) != null) {
                        TableAndColumnName tableAndColumnName = new TableAndColumnName();
                        if (ctx.sel_exp_where_AM().expr().expr(1).database_exp() != null) {
                            if (ctx.sel_exp_where_AM().expr().expr(1).database_exp().database_name() != null) {
                                tableAndColumnName.setDataBaseName(ctx.sel_exp_where_AM().expr().expr(1).database_exp().database_name().any_name().getText());
                            }
                            tableAndColumnName.setTableName(ctx.sel_exp_where_AM().expr().expr(1).database_exp().table_name().any_name().getText());
                            tableAndColumnName.setColName(ctx.sel_exp_where_AM().expr().expr(1).database_exp().column_name().any_name().getText());
                        } else
                            tableAndColumnName.setColName(ctx.sel_exp_where_AM().expr().expr(1).literal_value().getText());
                        nameWhereCondition2.add(tableAndColumnName);
                    }
                    whereCondition.setWhereNameCondition2(nameWhereCondition2);
                }
                selectAll.setWhereCondition(whereCondition);
                System.out.println("wewwe " + selectAll.getWhereCondition().getOperation());
            }

            if (selectAll.getWhereCondition().getWhereNameCondition1().getTableName() != null) {
                System.out.println("Table name is " + selectAll.getWhereCondition().getWhereNameCondition1().getTableName());
            }
            System.out.println("column name is  " + selectAll.getWhereCondition().getWhereNameCondition1().getColName());
            for (int i = 0; i < selectAll.getWhereCondition().getWhereNameCondition2().size(); i++) {
                if (selectAll.getWhereCondition().getWhereNameCondition2().get(i).getTableName() != null) {
                    System.out.println("Table name is second " + selectAll.getWhereCondition().getWhereNameCondition2().get(i).getTableName());
                }
                System.out.println("column name is  second " + selectAll.getWhereCondition().getWhereNameCondition2().get(i).getColName());
            }
            if (selectAll.getWhereCondition().getWhereCondition() != null) {
                System.out.println("key is  " + selectAll.getWhereCondition().getKey());
                if (selectAll.getWhereCondition().getWhereCondition().getWhereNameCondition1().getTableName() != null)
                    System.out.println("Table name in  where is " + selectAll.getWhereCondition().getWhereCondition().getWhereNameCondition1().getTableName());
                System.out.println("columnn name in  where is " + selectAll.getWhereCondition().getWhereCondition().getWhereNameCondition1().getColName());

                for (int i = 0; i < selectAll.getWhereCondition().getWhereCondition().getWhereNameCondition2().size(); i++) {
                    if (selectAll.getWhereCondition().getWhereCondition().getWhereNameCondition2().get(i).getTableName() != null)
                        System.out.println("Table name in  where second is " + selectAll.getWhereCondition().getWhereCondition().getWhereNameCondition2().get(i).getTableName());
                    System.out.println("column name in  where second is " + selectAll.getWhereCondition().getWhereCondition().getWhereNameCondition2().get(i).getColName());
                }
            }

        }
        if (ctx.sel_exp_groupBy_AM() != null) {
            Main.parentScope = selectOrValueScope;
            selectOrValue.setExpSqlAMGroupBy(visitSel_exp_groupBy_AM(ctx.sel_exp_groupBy_AM()));
        }
        if (ctx.sel_exp_having_AM() != null) {
            Main.parentScope = selectOrValueScope;
            selectOrValue.setExpSqlAMHaving(visitSel_exp_having_AM(ctx.sel_exp_having_AM()));
        }
        if (ctx.expr() != null) {
            for (int i = 0; i < ctx.expr().size(); i++) {
                Main.parentScope = selectOrValueScope;
                selectOrValue.setExps(visitExpr(ctx.expr(i)));
            }
        }
        for (int i = 0; i < selectAll.getColumnNames().size(); i++) {
            System.out.println("column is " + selectAll.getColumnNames().get(i));
        }
        selectOrValue.setScope(selectOrValueScope);
        selectOrValue.setParent(false);

        selectOrValue.setSelectAll(selectAll);

        selectOrValue.setLine(ctx.getStart().getLine()); //get line number
        selectOrValue.setCol(ctx.getStart().getCharPositionInLine());
        return selectOrValue;
    }

    private void selectCheck() {

        SelectOrValue selectOrValue = selectOrValueAll;
        if (selectOrValue == null)
            System.out.println("Rarararaar");
        ArrayList resultColumn = new ArrayList();
        for (int i = 0; i < selectOrValue.getResultColumns().size(); i++) {
            System.out.println("wqqwqw");
            if (selectOrValue.getResultColumns().get(i).getColumnName() != null) {
                System.out.println("tatattata " + selectOrValue.getResultColumns().get(i).getColumnName());
                resultColumn.add(selectOrValue.getResultColumns().get(i).getColumnName());
            } else if (selectOrValue.getResultColumns().get(i).getStar() != null)
                resultColumn.add(selectOrValue.getResultColumns().get(i).getStar());
        }
        ArrayList<String> nameTable = new ArrayList<>();
        if (selectOrValue.getSelTableAm() != null)
            for (int i = 0; i < selectOrValue.getSelTableAm().getTableSubFathers().size(); i++)
                if (selectOrValue.getSelTableAm().getTableOrSubOnes().get(i).getTableName() != null)
                    nameTable.add(selectOrValue.getSelTableAm().getTableOrSubOnes().get(i).getTableName());
        String whereCondition;
        if (selectOrValue.getScope() == null)
            System.out.println("momoooooooooooooooooo");
        Symbol symbolWhere = editSymbol(selectOrValue.getScope(), "where");
        if (symbolWhere != null)
            System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyy");


    }

    @Override
    public SelTableAm visitSel_table_AM(SqlParser.Sel_table_AMContext ctx) {
        System.out.println("visit Sel table AM");
        SelTableAm selTableAm = new SelTableAm();
        selTableAm.setOK(true);
        if (ctx.table_or_subquery() != null)
            for (int i = 0; i < ctx.table_or_subquery().size(); i++) {
                if (ctx.table_or_subquery(i).table_or_sub_one() != null)
                    selTableAm.setTableOrSubOnes(visitTable_or_sub_one(ctx.table_or_subquery(i).table_or_sub_one()));

                if (ctx.table_or_subquery(i).table_or_sub_two() != null)
                    selTableAm.setTableOrSubTwos(visitTable_or_sub_two(ctx.table_or_subquery(i).table_or_sub_two()));

                if (ctx.table_or_subquery(i).table_or_sub_three() != null)
                    selTableAm.setTableOrSubThrees(visitTable_or_sub_three(ctx.table_or_subquery(i).table_or_sub_three()));

//                selTableAm.setTableSubFathers(visitTable_or_subquery(ctx.table_or_subquery(i)));
            }
        if (ctx.join_clause() != null)
            selTableAm.setJoinCaluse(visitJoin_clause(ctx.join_clause()));

        return selTableAm;
    }

    @Override
    public TableOrSub visitTable_or_subquery(SqlParser.Table_or_subqueryContext ctx) {
        System.out.println("visit table or sub ");
        TableOrSub tableOrSub = new TableOrSub();
        tableOrSub.setOK(true);
        if (ctx.table_or_sub_one() != null)
            tableOrSub.setTableOrSubOne(visitTable_or_sub_one(ctx.table_or_sub_one()));
        if (ctx.table_or_sub_two() != null)
            tableOrSub.setTableOrSubTwo(visitTable_or_sub_two(ctx.table_or_sub_two()));
        if (ctx.table_or_sub_three() != null)
            tableOrSub.setTableOrSubThree(visitTable_or_sub_three(ctx.table_or_sub_three()));
        return tableOrSub;
    }

    @Override
    public TableOrSubOne visitTable_or_sub_one(SqlParser.Table_or_sub_oneContext ctx) {
        System.out.println("visit table or sub one");
        TableOrSubOne tableOrSubOne = new TableOrSubOne();
        Scope currentScope = Main.parentScope;

        Symbol symbolFrom = new Symbol();

        String result = "";
        if (ctx.database_name() != null && ctx.table_name() != null)
            result = ctx.database_name().any_name().getText() + "." + ctx.table_name().any_name().getText();
        else if (ctx.table_name() != null)
            result = ctx.table_name().any_name().getText();

        if (ctx.K_AS() != null)
            result += " as";
        if (ctx.table_alias() != null)
            result += ctx.table_alias().any_name().getText();

        tableOrSubOne.setOK(true);
        if (ctx.database_name() != null)
            tableOrSubOne.setDatabaseName(ctx.database_name().any_name().getText());
        if (ctx.table_name() != null)
            tableOrSubOne.setTableName(ctx.table_name().any_name().getText());
        if (ctx.table_alias() != null)
            tableOrSubOne.setAliasName(visitAny_name(ctx.table_alias().any_name()));
        if (ctx.index_name() != null)
            tableOrSubOne.setIndexName(visitAny_name(ctx.index_name().any_name()));

        symbolFrom.setName("table result " + result);
        symbolFrom.setScope(currentScope);
        currentScope.addSymbol("table result " + result, symbolFrom);

        return tableOrSubOne;
    }

    @Override
    public TableOrSubTwo visitTable_or_sub_two(SqlParser.Table_or_sub_twoContext ctx) {
        System.out.println("visit table or sub two");
        TableOrSubTwo tableOrSubTwo = new TableOrSubTwo();
        tableOrSubTwo.setOK(true);
        if (ctx.table_or_subquery() != null)
            for (int i = 0; i < ctx.table_or_subquery().size(); i++)
                tableOrSubTwo.setTableSubFathers(visitTable_or_subquery(ctx.table_or_subquery(i)));

        if (ctx.join_clause() != null)
            tableOrSubTwo.setJoinCaluse(visitJoin_clause(ctx.join_clause()));
        if (ctx.table_alias() != null)
            tableOrSubTwo.setTablaalias(visitAny_name(ctx.table_alias().any_name()));

        return tableOrSubTwo;
    }

    @Override
    public TableOrSubThree visitTable_or_sub_three(SqlParser.Table_or_sub_threeContext ctx) {
        System.out.println("table or sub three");
        TableOrSubThree tableOrSubThree = new TableOrSubThree();
        tableOrSubThree.setOK(true);
        tableOrSubThree.setSelectStmt(visitSelect_stmt(ctx.select_stmt()));
        if (ctx.table_alias() != null)
            tableOrSubThree.setTableAlias(visitAny_name(ctx.table_alias().any_name()));

        return tableOrSubThree;
    }

    @Override
    public JoinCaluse visitJoin_clause(SqlParser.Join_clauseContext ctx) {
        System.out.println("visit join clause");
        JoinCaluse joinCaluse = new JoinCaluse();
        joinCaluse.setOK(true);
        ArrayList<String> tableFromNames = new ArrayList<>();
        for (int i = 0; i < ctx.table_or_subquery().size(); i++) {
            tableFromNames.add(ctx.table_or_subquery(i).table_or_sub_one().table_name().any_name().getText());
        }

        joinCaluse.setTableFromNames(tableFromNames);

        /*
        join_clause
 : ( table_or_subquery ( ',' table_or_subquery )*) (join)*
 ;

join : join_operator table_or_subquery join_constraint ;

         */


        for (int i = 0; i < ctx.join().size(); i++) {

            JoinClauseSub joinClauseSub = new JoinClauseSub();
            joinClauseSub.setOperater(ctx.join(i).join_operator().getText());
            if (ctx.join(i).table_or_subquery().table_or_sub_one() != null)
                joinClauseSub.setTableInJoinName(ctx.join(i).table_or_subquery().table_or_sub_one().table_name().any_name().getText());
            else {
                joinClauseSub.setTableInJoinName(ctx.join(i).table_or_subquery().table_or_sub_three().table_alias().any_name().getText());

            }
            if (ctx.join(i).table_or_subquery().table_or_sub_one() != null)
                if (ctx.join(i).table_or_subquery().table_or_sub_one().table_alias() != null)
                    joinClauseSub.setTableAliasName(ctx.join(i).table_or_subquery().table_or_sub_one().table_alias().any_name().getText());
            joinClauseSub.setTableSubFather(visitTable_or_subquery(ctx.join(i).table_or_subquery()));
            if (ctx.join(i).join_constraint() != null) {
                if (ctx.join(i).join_constraint().expr() != null) {
                    if (ctx.join(i).join_constraint().expr().expr(0).database_exp().table_name() != null) {
                        joinClauseSub.setConditionTableName1(ctx.join(i).join_constraint().expr().expr(0).database_exp().table_name().any_name().getText());
                    }
                    if (ctx.join(i).join_constraint().expr().expr(0).database_exp().column_name() != null) {
                        joinClauseSub.setConditionColumnName1(ctx.join(i).join_constraint().expr().expr(0).database_exp().column_name().any_name().getText());
                    }
                    if (ctx.join(i).join_constraint().expr().expr(1).database_exp().table_name() != null) {
                        joinClauseSub.setConditionTableName2(ctx.join(i).join_constraint().expr().expr(1).database_exp().table_name().any_name().getText());
                    }
                    if (ctx.join(i).join_constraint().expr().expr(1).database_exp().column_name() != null) {
                        joinClauseSub.setConditionColumnName2(ctx.join(i).join_constraint().expr().expr(1).database_exp().column_name().any_name().getText());
                    }
                }
            }
            joinClauseSub.setJoinConstraintsExp(visitExpr(ctx.join(i).join_constraint().expr()));
            joinCaluse.setJoinClauseSubs(joinClauseSub);
        }
        return joinCaluse;
    }

    @Override
    public Object visitJoin(SqlParser.JoinContext ctx) {
        return super.visitJoin(ctx);
    }

    @Override
    public ExpSqlAM visitSel_exp_where_AM(SqlParser.Sel_exp_where_AMContext ctx) {
        System.out.println("visit sel exp where");
        ExpSqlAM expSqlAM = new ExpSqlAM();
        expSqlAM.setOK(true);
        expSqlAM.setType("WHERE");
        expSqlAM.setExps(visitExpr(ctx.expr()));
        return expSqlAM;
    }

    @Override
    public ExpSqlAM visitSel_exp_groupBy_AM(SqlParser.Sel_exp_groupBy_AMContext ctx) {
        System.out.println("visit sel exp Group By");
        ExpSqlAM expSqlAM = new ExpSqlAM();
        expSqlAM.setOK(true);
        expSqlAM.setType("GROUPBY");
        for (int i = 0; i < ctx.expr().size(); i++)
            expSqlAM.setExps(visitExpr(ctx.expr(i)));
        return expSqlAM;
    }

    @Override
    public ExpSqlAM visitSel_exp_having_AM(SqlParser.Sel_exp_having_AMContext ctx) {
        System.out.println("visit sel exp HAVING");
        ExpSqlAM expSqlAM = new ExpSqlAM();
        expSqlAM.setOK(true);
        expSqlAM.setType("HAVING");
        expSqlAM.setExps(visitExpr(ctx.expr()));
        return expSqlAM;
    }

    @Override
    public Exp visitExpr(SqlParser.ExprContext ctx) {

        boolean ok = false;
        boolean checkIn = false;
        Scope currentScope = null;
        if (Main.symbolTable.getScopes() != null) {
            if (Main.symbolTable.getScopes().size() > 0) {
                ok = true;
                currentScope = Main.symbolTable.getScopes().get(Main.symbolTable.getScopes().size() - 1);
            }
        }

        if (ctx.literal_value() != null) {
            LiteralValue exp = new LiteralValue();
            exp.setLiteralValue(ctx.literal_value().getText());

        } else if (ctx.database_exp() != null) {
            DatabaseExp databaseExp = new DatabaseExp();
            databaseExp.setAnyNames(visitAny_name(ctx.database_exp().column_name().any_name()));
        }
        if (ctx.function_exp() != null) {

            return visitFunction_exp(ctx.function_exp());
        } else if (ctx.operator_exp() != null) {
            return visitOperator_exp(ctx.operator_exp());
        } else if (ctx.expr().size() == 2) {

            System.out.println("operator is  " + ctx.children.get(1).getText());
            TwoExp exp = new TwoExp();
            exp.setOk(true);
            exp.setOperator(ctx.children.get(1).getText());
            if (ctx.K_IN() != null) {
                checkIn = true;

                String whereColumn = "";
                whereColumn = ctx.expr(0).database_exp().column_name().any_name().getText();
                Scope scope = Main.parentScope;
                Symbol symbolWhere = new Symbol();
                symbolWhere.setName(whereColumn);
                symbolWhere.setScope(scope);
                symbolWhere.setLine(ctx.getStart().getLine());
                symbolWhere.setCol(ctx.getStart().getCharPositionInLine());
                scope.addSymbol("where ", symbolWhere);
                exp.setExpLeft(visitExpr(ctx.expr(0)));
                exp.setExpRight(visitExpr(ctx.expr(1)));
                return exp;
            } else { ////////////////// symbol operator
                Symbol operatorSymbol = new Symbol();
                operatorSymbol.setName("operator between two exp");
                Expression expression = new Expression();
                if (ctx.expr(0).literal_value() != null) {
                    Type type = new Type();
                    if (ctx.expr(0).literal_value().NUMERIC_LITERAL() != null) {
                        type.setName(Type.NUMBER_CONST);
                    } else if (ctx.expr(0).literal_value().STRING_LITERAL() != null) {
                        type.setName(Type.CHAR_CONST);
                    } else if (ctx.expr(0).literal_value().BLOB_LITERAL() != null) {
                        type.setName(Type.BLOB_LITERAL);
                    } else if (ctx.expr(0).literal_value().K_CURRENT_TIME() != null) {
                        type.setName(Type.TIME);
                    } else if (ctx.expr(0).literal_value().K_CURRENT_DATE() != null) {
                        type.setName(Type.DATE);
                    } else if (ctx.expr(0).literal_value().K_CURRENT_TIMESTAMP() != null) {
                        type.setName(Type.TIMESTAMP);
                    }
                    expression.setType(type);
                    expression.setValue1(ctx.expr(0).literal_value().getText());
                } else if (ctx.expr(0).database_exp() != null) {
                    Type type = new Type();
                    if (ctx.expr(0).database_exp().column_name() != null)
                        type.setName(Type.STRING_CONST);
                    if (!ctx.expr(0).database_exp().column_name().any_name().getText().startsWith("\""))
                        expression.setIfVariable1(true);
                    expression.setType(type);
                    expression.setValue1(ctx.expr(0).database_exp().column_name().any_name().IDENTIFIER().getText());
                }
                if (ctx.expr(1).literal_value() != null) {
                    Type type = new Type();
                    if (ctx.expr(1).literal_value().NUMERIC_LITERAL() != null) {
                        type.setName(Type.NUMBER_CONST);
                    } else if (ctx.expr(1).literal_value().STRING_LITERAL() != null) {
                        type.setName(Type.CHAR_CONST);
                    } else if (ctx.expr(1).literal_value().BLOB_LITERAL() != null) {
                        type.setName(Type.BLOB_LITERAL);
                    } else if (ctx.expr(1).literal_value().K_CURRENT_TIME() != null) {
                        type.setName(Type.TIME);
                    } else if (ctx.expr(1).literal_value().K_CURRENT_DATE() != null) {
                        type.setName(Type.DATE);
                    } else if (ctx.expr(1).literal_value().K_CURRENT_TIMESTAMP() != null) {
                        type.setName(Type.TIMESTAMP);
                    }
                    expression.setType(type);
                    expression.setValue2(ctx.expr(1).literal_value().getText());
                } else if (ctx.expr(1).database_exp() != null) {
                    Type type = new Type();
                    if (ctx.expr(1).database_exp().column_name() != null)
                        type.setName(Type.STRING_CONST);
                    if (!ctx.expr(1).database_exp().column_name().any_name().getText().startsWith("\""))
                        expression.setIfVariable2(true);
                    expression.setType(type);
                    expression.setValue2(ctx.expr(1).database_exp().column_name().any_name().IDENTIFIER().getText());
                }
                if (ok && !checkIn) {
                    expression.setOperator(ctx.children.get(1).getText());
                    operatorSymbol.setExpression(expression);
                    operatorSymbol.setType(expression.getType());
                    operatorSymbol.setScope(currentScope);
                    currentScope.addSymbol(operatorSymbol.getName(), operatorSymbol);
                }

                exp.setExpLeft(visitExpr(ctx.expr(0)));
                exp.setExpRight(visitExpr(ctx.expr(1)));
            }
            return exp;
        } else if (ctx.expr().size() == 1) {
            boolean check = false;
            Type expType = new Type();
            String typeName = "";
            if (ctx.expr(0).literal_value() != null) {
                if (ctx.expr(0).literal_value().NUMERIC_LITERAL() != null) {
                    check = true;
                    typeName = Type.NUMBER_CONST;
                } else if (ctx.expr(0).literal_value().STRING_LITERAL() != null) {
                    typeName = Type.CHAR_CONST;
                    check = true;
                } else if (ctx.expr(0).literal_value().BLOB_LITERAL() != null) {
                    check = true;
                    typeName = Type.BLOB_LITERAL;
                } else if (ctx.expr(0).literal_value().K_CURRENT_TIME() != null) {
                    check = true;
                    typeName = Type.TIME;
                } else if (ctx.expr(0).literal_value().K_CURRENT_DATE() != null) {
                    check = true;
                    typeName = Type.DATE;
                } else if (ctx.expr(0).literal_value().K_CURRENT_TIMESTAMP() != null) {
                    check = true;
                    typeName = Type.TIMESTAMP;
                }
                expType.setValue(ctx.expr(0).literal_value().getText());
            } else if (ctx.expr(0).database_exp() != null) {
                if (ctx.expr(0).database_exp().column_name().any_name() != null) {
                    check = true;
                    typeName = Type.STRING_CONST;
                    expType.setValue(ctx.expr(0).database_exp().column_name().any_name().IDENTIFIER().getText());
                }
            } else if (ctx.expr(0).K_TRUE() != null) {
                check = true;
                typeName = Type.BOOLEAN_CONST;
                expType.setValue(ctx.expr(0).K_TRUE().getText());
            } else if (ctx.expr(0).K_FALSE() != null) {
                expType.setValue(ctx.expr(0).K_FALSE().getText());
                check = true;
                typeName = Type.BOOLEAN_CONST;
            }
            if (check) {
                expType.setName(typeName);
                Symbol returnSymbol = new Symbol();
                returnSymbol.setName("exp " + expType.getValue());
                returnSymbol.setScope(currentScope);
                returnSymbol.setIsReturn(true);
                returnSymbol.setType(expType);
                currentScope.addSymbol("exp " + expType.getValue(), returnSymbol);
            }
            visitExpr(ctx.expr(0));
        } else if (ctx.second_sel_all() != null) {
            SecondSellAll secondSellAll = visitSecond_sel_all(ctx.second_sel_all());
            secondSellAll.setOk(true);
            if (ctx.K_NOT() != null)
                secondSellAll.setkNot("NOT");
            secondSellAll.setExpSelInExp(visitExpr(ctx.expr(0)));

        } else if (ctx.sel_expr() != null) {
            return visitSelect_stmt(ctx.sel_expr().select_stmt());
        }
        return null;
    }


    @Override
    public DatabaseExp visitDatabase_exp(SqlParser.Database_expContext ctx) {
        System.out.println("visit database exp");
        DatabaseExp databaseExp = new DatabaseExp();
        databaseExp.setOk(true);
        AnyName anyName = new AnyName();
        if (ctx.database_name() != null) {
            anyName = visitAny_name(ctx.database_name().any_name());
            databaseExp.setAnyNames(anyName);
        }
        if (ctx.column_name() != null) {
            anyName = visitAny_name(ctx.column_name().any_name());
            databaseExp.setAnyNames(anyName);
        }
        if (ctx.table_name() != null) {
            anyName = visitAny_name(ctx.table_name().any_name());
            databaseExp.setAnyNames(anyName);
        }
        return databaseExp;
    }

    @Override
    public OperaterExp visitOperator_exp(SqlParser.Operator_expContext ctx) {
        System.out.println("visit operator exp");
        OperaterExp operaterExp = new OperaterExp();
        operaterExp.setOk(true);

        Scope currentScope = Main.symbolTable.getScopes().get(Main.symbolTable.getScopes().size() - 1);

        operaterExp.setOperator(ctx.unary_operator().getText());

        ////////////////// symbol operator
        Symbol operatorSymbol = new Symbol();

        operatorSymbol.setName(operaterExp.getOperator());
        operatorSymbol.setIsOperator(true);
        Type type = new Type();
        type.setName("Operator " + operaterExp.getOperator());
        operatorSymbol.setType(type);
        operatorSymbol.setScope(currentScope);
        currentScope.addSymbol(operaterExp.getOperator(), operatorSymbol);

        operaterExp.setExpLeft(visitExpr(ctx.expr()));
        return operaterExp;
    }

    @Override
    public FunctionCallExp visitFunction_exp(SqlParser.Function_expContext ctx) {
        System.out.println("visit function expr");
        FunctionCallExp functionCallExp = new FunctionCallExp();
        functionCallExp.setOk(true);

        boolean ok = false;
        Scope currentScope = null;
        if (Main.symbolTable.getScopes() != null)
            if (Main.symbolTable.getScopes().size() > 0) {
                ok = true;
                currentScope = Main.symbolTable.getScopes().get(Main.symbolTable.getScopes().size() - 1);
            }

        if (ctx.K_DISTINCT() != null) {
            functionCallExp.setK_DISTINCT(ctx.K_DISTINCT().getText());
            ////////////////// symbol Keyword
            Symbol keyowrdSymbol = new Symbol();

            keyowrdSymbol.setName(functionCallExp.getK_DISTINCT());
            keyowrdSymbol.setIsKeyword(true);
            Type type = new Type();
            type.setName("KeyWord " + functionCallExp.getK_DISTINCT());
            keyowrdSymbol.setType(type);
            keyowrdSymbol.setScope(currentScope);
            currentScope.addSymbol(functionCallExp.getK_DISTINCT(), keyowrdSymbol);

        }

        functionCallExp.setFunctionName(visitAny_name(ctx.function_name().any_name()));

        if (ok) {
            Symbol functionExpNameSymbol = new Symbol();
            String nameValue = ctx.function_name().any_name().IDENTIFIER().getText();
            functionExpNameSymbol.setName("Call Function" + nameValue);
            Type type = new Type();
            type.setName("Call Function Exp");
            functionExpNameSymbol.setType(type);
            functionExpNameSymbol.setScope(currentScope);
            currentScope.addSymbol(nameValue, functionExpNameSymbol);
        }

        for (int i = 0; i < ctx.expr().size(); i++)
            functionCallExp.setParameter(visitExpr(ctx.expr(i)));
        return functionCallExp;
    }

    @Override
    public SecondSellAll visitSecond_sel_all(SqlParser.Second_sel_allContext ctx) {
        System.out.println("visit second sel all");
        Scope currentScope = Main.parentScope;

        SecondSellAll secondSellAll = new SecondSellAll();
        secondSellAll.setOk(true);
        if (ctx.select_stmt() != null) {
            Main.parentScope = currentScope;
            secondSellAll.setSelectStmt(visitSelect_stmt(ctx.select_stmt()));
        }
        if (ctx.expr() != null)
            for (int i = 0; i < ctx.expr().size(); i++)
                secondSellAll.setExps(visitExpr(ctx.expr(i)));
        if (ctx.database_name() != null)
            secondSellAll.setDatabaseName(visitAny_name(ctx.database_name().any_name()));
        if (ctx.table_name() != null)
            secondSellAll.setTableName(visitAny_name(ctx.table_name().any_name()));
        return secondSellAll;
    }

    @Override
    public OrderingTerm visitOrdering_term(SqlParser.Ordering_termContext ctx) {
        System.out.println("visit Ordering term");
        OrderingTerm orderingTerm = new OrderingTerm();
        orderingTerm.setOK(true);
        orderingTerm.setExp(visitExpr(ctx.expr()));
        return orderingTerm;
    }

    /*
    result_column
     : '*'
     | table_name '.' '*'
     | expr ( K_AS? column_alias )?
     ;
     */
    @Override
    public ResultColumn visitResult_column(SqlParser.Result_columnContext ctx) {
        Scope scopeParent = Main.parentScope;
        ResultColumn resultColumn = new ResultColumn();
        resultColumn.setOK(true);
        Symbol symbol = new Symbol();
        String result = "";
        if (ctx.star() != null) {
            resultColumn.setStar("*");
            result = "*";
        }
        if (ctx.column_alias() != null)
            resultColumn.setColumnAlias(ctx.column_alias().getText());
        if (ctx.expr() != null) {
            resultColumn.setExp(visitExpr(ctx.expr()));
            if (ctx.expr() != null) {
                if (ctx.expr().database_exp() != null) {
                    if (ctx.expr().database_exp().table_name() != null && ctx.expr().database_exp().column_name() != null) {
                        resultColumn.setTableName(ctx.expr().database_exp().table_name().any_name().getText());
                        resultColumn.setColumnName(ctx.expr().database_exp().column_name().any_name().getText());
                        result = resultColumn.getTableName() + "." + resultColumn.getColumnName();
                    } else if (ctx.expr().database_exp().column_name() != null) {
                        resultColumn.setColumnName(ctx.expr().database_exp().column_name().any_name().getText());
                        result = resultColumn.getColumnName();
                    }
                }
                if (ctx.expr().function_exp() != null) {
                    AggFun aggFun = new AggFun();
                    aggFun.setAggFunTableName(ctx.expr().function_exp().function_name().any_name().getText());
                    if (ctx.expr().function_exp().star() != null)
                        aggFun.setAggFunParam("*");
                    else
                        aggFun.setAggFunParam(ctx.expr().function_exp().expr(0).database_exp().column_name().any_name().getText());
                    if (ctx.column_alias() != null)
                        aggFun.setTableAliasName(ctx.column_alias().getText());

                    resultColumn.setAggFun(aggFun);

                }
            }

        } else if (ctx.table_name() != null) {
            resultColumn.setTableNameOnly(visitAny_name(ctx.table_name().any_name()));
            result = resultColumn.getTableNameOnly();
        }
        symbol.setName("result column " + result);
        System.out.println("res" + symbol.getName() + scopeParent.getId());
        symbol.setScope(scopeParent);
        scopeParent.addSymbol("result column " + result, symbol);


        resultColumn.setLine(ctx.getStart().getLine()); //get line number
        resultColumn.setCol(ctx.getStart().getCharPositionInLine());

        return resultColumn;
    }

    @Override
    public SelectOrValue visitSelect_core(SqlParser.Select_coreContext ctx) {
        SelectAll selectAll = new SelectAll();

        System.out.println("visit select or value");
        SelectOrValue selectOrValue = new SelectOrValue();
        selectOrValue.setOK(true);
        Scope scopeParent = Main.parentScope;

        Scope scope = new Scope();
        scope.setId("select_");
        scope.setParent(scopeParent);
        Main.symbolTable.addScope(scope);

        if (ctx.result_column() != null)
            for (int i = 0; i < ctx.result_column().size(); i++) {
                Main.parentScope = scope;
                selectOrValue.setResultColumns(visitResult_column(ctx.result_column(i)));
                ArrayList<String> columnsName = new ArrayList<>();
                ArrayList<AggFun> aggFuns = new ArrayList<>();
                for (int j = 0; j < selectOrValue.getResultColumns().size(); j++) {
                    if (selectOrValue.getResultColumns().get(j).getColumnName() != null) {
                        columnsName.add(selectOrValue.getResultColumns().get(j).getColumnName());
                    }
                    if (selectOrValue.getResultColumns().get(j).getStar() != null) {
                        columnsName.add("*");
                    }
                    if (selectOrValue.getResultColumns().get(j).getAggFun() != null) {
                        aggFuns.add(selectOrValue.getResultColumns().get(j).getAggFun());
                    }
                }

                selectAll.setAggFuns(aggFuns);
                selectAll.setColumnNames(columnsName);
            }
        if (ctx.sel_table_AM() != null) {
            Main.parentScope = scope;
            selectOrValue.setSelTableAm(visitSel_table_AM(ctx.sel_table_AM()));

            if (ctx.sel_table_AM().join_clause() == null) {
                ArrayList<String> tableNames = new ArrayList<>();
                for (int i = 0; i < selectOrValue.getSelTableAm().getTableOrSubOnes().size(); i++) {
                    tableNames.add(selectOrValue.getSelTableAm().getTableOrSubOnes().get(i).getTableName());
                }
                selectAll.setTableNames(tableNames);
                for (int i = 0; i < tableNames.size(); i++) {
                    System.out.println("table name is is  " + tableNames.get(i));
                }
            } else if (ctx.sel_table_AM().join_clause() != null) {

                if (ctx.sel_table_AM().join_clause().join(0) != null) {
                    if (ctx.sel_table_AM().join_clause().join(0).table_or_subquery().table_or_sub_three() != null)
                        if (ctx.sel_table_AM().join_clause().join(0).table_or_subquery().table_or_sub_three().select_stmt().select_or_values() != null) {
                            SelectOrValue selectOrValue1 = visitSelect_or_values(ctx.sel_table_AM().join_clause().join(0).table_or_subquery().table_or_sub_three().select_stmt().select_or_values());
                            selectAll.setSelectAll(selectOrValue1.getSelectAll());
                            System.out.println("sert " + selectOrValue1.getSelectAll().getTableNames().get(0));
                        }
                }
                JoinCaluse joinCaluse = selectOrValue.getSelTableAm().getJoinCaluse();
                selectAll.setTableNames(joinCaluse.getTableFromNames());


                ArrayList<InnerJoin> innerJoinArrayList = new ArrayList<>();
                for (int i = 0; i < joinCaluse.getJoinClauseSubs().size(); i++) {
                    InnerJoin innerJoin = new InnerJoin();
                    innerJoin.setTableName(joinCaluse.getJoinClauseSubs().get(i).getTableInJoinName());
                    innerJoin.setOperation(joinCaluse.getJoinClauseSubs().get(i).getOperater());
                    if (joinCaluse.getJoinClauseSubs().get(i).getTableAliasName() != null)
                        innerJoin.setTableNameCall(joinCaluse.getJoinClauseSubs().get(i).getTableAliasName());
                    innerJoin.setConditionTableName1(joinCaluse.getJoinClauseSubs().get(i).getConditionTableName1());
                    innerJoin.setConditionTableName2(joinCaluse.getJoinClauseSubs().get(i).getConditionTableName2());
                    innerJoin.setConditionColumnName1(joinCaluse.getJoinClauseSubs().get(i).getConditionColumnName1());
                    innerJoin.setConditionColumnName2(joinCaluse.getJoinClauseSubs().get(i).getConditionColumnName2());
                    innerJoinArrayList.add(innerJoin);
                }
                selectAll.setInnerJoins(innerJoinArrayList);

            }


        }

        if (ctx.sel_exp_where_AM() != null) {
            Main.parentScope = scope;
            selectOrValue.setExpSqlAMWhere(visitSel_exp_where_AM(ctx.sel_exp_where_AM()));

            if (ctx.sel_exp_where_AM().expr().K_AND() != null | ctx.sel_exp_where_AM().expr().K_OR() != null) {
                WhereCondition whereCondition = new WhereCondition();

                if (ctx.sel_exp_where_AM().expr().K_AND() != null)
                    whereCondition.setKey("AND");
                else if (ctx.sel_exp_where_AM().expr().K_OR() != null)
                    whereCondition.setKey("OR");


                TableAndColumnName tableAndColumnNameAll = new TableAndColumnName();
                if (ctx.sel_exp_where_AM().expr().expr(0).expr(0).database_exp().database_name() != null)
                    tableAndColumnNameAll.setDataBaseName(ctx.sel_exp_where_AM().expr().expr(0).expr(0).database_exp().database_name().any_name().getText());
                if (ctx.sel_exp_where_AM().expr().expr(0).expr(0).database_exp().table_name() != null)
                    tableAndColumnNameAll.setTableName(ctx.sel_exp_where_AM().expr().expr(0).expr(0).database_exp().table_name().any_name().getText());
                tableAndColumnNameAll.setColName(ctx.sel_exp_where_AM().expr().expr(0).expr(0).database_exp().column_name().any_name().getText());
                whereCondition.setWhereNameCondition1(tableAndColumnNameAll);

                System.out.println("nanan is " + whereCondition.getWhereNameCondition1());
                ArrayList<TableAndColumnName> nameWhereCondiotion2 = new ArrayList<>();
                if (ctx.sel_exp_where_AM().expr().expr(0).second_sel_all() != null) {
                    for (int i = 0; i < ctx.sel_exp_where_AM().expr().expr(0).second_sel_all().expr().size(); i++) {
                        TableAndColumnName tableAndColumnName = new TableAndColumnName();
                        if (ctx.sel_exp_where_AM().expr().expr(0).second_sel_all().expr(i).database_exp() != null) {
                            if (ctx.sel_exp_where_AM().expr().expr(0).second_sel_all().expr(i).database_exp().database_name() != null) {
                                tableAndColumnName.setDataBaseName(ctx.sel_exp_where_AM().expr().expr(0).second_sel_all().expr(i).database_exp().database_name().any_name().getText());
                            }
                            tableAndColumnName.setTableName(ctx.sel_exp_where_AM().expr().expr(0).second_sel_all().expr(i).database_exp().table_name().any_name().getText());
                            tableAndColumnName.setColName(ctx.sel_exp_where_AM().expr().expr(0).second_sel_all().expr(i).database_exp().column_name().any_name().getText());
                        } else
                            tableAndColumnName.setColName(ctx.sel_exp_where_AM().expr().expr(0).second_sel_all().expr(i).literal_value().getText());

                        nameWhereCondiotion2.add(tableAndColumnName);
                        System.out.println("sss " + nameWhereCondiotion2.get(i).getColName());
                    }

                } else if (ctx.sel_exp_where_AM().expr().expr(0).expr(1) != null) {
                    TableAndColumnName tableAndColumnName = new TableAndColumnName();
                    if (ctx.sel_exp_where_AM().expr().expr(0).expr(1).database_exp() != null) {
                        if (ctx.sel_exp_where_AM().expr().expr(0).expr(1).database_exp().database_name() != null) {
                            tableAndColumnName.setDataBaseName(ctx.sel_exp_where_AM().expr().expr(0).expr(1).database_exp().database_name().any_name().getText());
                        }
                        tableAndColumnName.setTableName(ctx.sel_exp_where_AM().expr().expr(0).expr(1).database_exp().table_name().any_name().getText());
                        tableAndColumnName.setColName(ctx.sel_exp_where_AM().expr().expr(0).expr(1).database_exp().column_name().any_name().getText());
                    } else
                        tableAndColumnName.setColName(ctx.sel_exp_where_AM().expr().expr(0).expr(1).literal_value().getText());
                    nameWhereCondiotion2.add(tableAndColumnName);
                    System.out.println("kk " + nameWhereCondiotion2.get(0));
                }
                whereCondition.setOperation(ctx.sel_exp_where_AM().expr().expr(0).children.get(1).getText());
                whereCondition.setWhereNameCondition2(nameWhereCondiotion2);
                System.out.println("ddd " + whereCondition.getOperation());


                WhereCondition whereCondition2 = new WhereCondition();
                whereCondition2.setOperation(ctx.sel_exp_where_AM().expr().expr(1).children.get(1).getText());
                System.out.println("ddd " + whereCondition2.getOperation());

                TableAndColumnName tableAndColumnNameSecond = new TableAndColumnName();
                if (ctx.sel_exp_where_AM().expr().expr(1).expr(0).database_exp().database_name() != null) {
                    tableAndColumnNameSecond.setDataBaseName(ctx.sel_exp_where_AM().expr().expr(1).expr(0).database_exp().database_name().any_name().getText());
                }
                if (ctx.sel_exp_where_AM().expr().expr(1).expr(0).database_exp().table_name() != null)
                    tableAndColumnNameSecond.setTableName(ctx.sel_exp_where_AM().expr().expr(1).expr(0).database_exp().table_name().any_name().getText());
                tableAndColumnNameSecond.setColName(ctx.sel_exp_where_AM().expr().expr(1).expr(0).database_exp().column_name().any_name().getText());
                whereCondition2.setWhereNameCondition1(tableAndColumnNameSecond);

                ArrayList<TableAndColumnName> nameSecondWhereCondiotion2 = new ArrayList<>();
                if (ctx.sel_exp_where_AM().expr().expr(1).second_sel_all() != null) {
                    TableAndColumnName tableAndColumnName = new TableAndColumnName();
                    for (int i = 0; i < ctx.sel_exp_where_AM().expr().expr(1).second_sel_all().expr().size(); i++) {
                        if (ctx.sel_exp_where_AM().expr().expr(1).second_sel_all().expr(i).database_exp().database_name() != null) {
                            tableAndColumnName.setDataBaseName(ctx.sel_exp_where_AM().expr().expr(1).second_sel_all().expr(i).database_exp().database_name().any_name().getText());
                        }
                        if (ctx.sel_exp_where_AM().expr().expr(1).second_sel_all().expr(i).database_exp() != null) {
                            tableAndColumnName.setTableName(ctx.sel_exp_where_AM().expr().expr(1).second_sel_all().expr(i).database_exp().table_name().any_name().getText());
                            tableAndColumnName.setColName(ctx.sel_exp_where_AM().expr().expr(1).second_sel_all().expr(i).database_exp().column_name().any_name().getText());
                        } else
                            tableAndColumnName.setColName(ctx.sel_exp_where_AM().expr().expr(1).second_sel_all().expr(i).literal_value().getText());
                        nameSecondWhereCondiotion2.add(tableAndColumnName);
                        System.out.println("mmm " + nameSecondWhereCondiotion2.get(i));
                    }

                } else if (ctx.sel_exp_where_AM().expr().expr(1).expr(1) != null) {
                    TableAndColumnName tableAndColumnName = new TableAndColumnName();
                    if (ctx.sel_exp_where_AM().expr().expr(1).expr(1).database_exp() != null) {
                        if (ctx.sel_exp_where_AM().expr().expr(1).expr(1).database_exp().database_name() != null) {
                            tableAndColumnName.setDataBaseName(ctx.sel_exp_where_AM().expr().expr(1).expr(1).database_exp().database_name().any_name().getText());
                        }
                        tableAndColumnName.setTableName(ctx.sel_exp_where_AM().expr().expr(1).expr(1).database_exp().table_name().any_name().getText());
                        tableAndColumnName.setColName(ctx.sel_exp_where_AM().expr().expr(1).expr(1).database_exp().column_name().any_name().getText());
                    } else
                        tableAndColumnName.setColName(ctx.sel_exp_where_AM().expr().expr(1).expr(1).literal_value().getText());
                    nameSecondWhereCondiotion2.add(tableAndColumnName);
                    System.out.println("pp " + nameSecondWhereCondiotion2.get(0));
                }
                whereCondition2.setWhereNameCondition2(nameSecondWhereCondiotion2);


                whereCondition.setWhereCondition(whereCondition2);

                selectAll.setWhereCondition(whereCondition);
            } else {
                WhereCondition whereCondition = new WhereCondition();

                if (ctx.sel_exp_where_AM().expr() != null) {
                    System.out.println("kfsdkskdlfklsd");
                    whereCondition.setOperation(ctx.sel_exp_where_AM().expr().children.get(1).getText());
                    TableAndColumnName tableAndColumnNameAllSecond = new TableAndColumnName();
                    if (ctx.sel_exp_where_AM().expr().expr(0).database_exp() != null)
                        if (ctx.sel_exp_where_AM().expr().expr(0).database_exp().database_name() != null) {
                            tableAndColumnNameAllSecond.setDataBaseName(ctx.sel_exp_where_AM().expr().expr(0).database_exp().database_name().any_name().getText());
                        }
                    if (ctx.sel_exp_where_AM().expr().expr(0).database_exp() != null)
                        if (ctx.sel_exp_where_AM().expr().expr(0).database_exp().table_name() != null)
                            tableAndColumnNameAllSecond.setTableName(ctx.sel_exp_where_AM().expr().expr(0).database_exp().table_name().any_name().getText());
                    if (ctx.sel_exp_where_AM().expr().expr(0).database_exp() != null)
                        tableAndColumnNameAllSecond.setColName(ctx.sel_exp_where_AM().expr().expr(0).database_exp().column_name().any_name().getText());
                    whereCondition.setWhereNameCondition1(tableAndColumnNameAllSecond);

                    ArrayList<TableAndColumnName> nameWhereCondition2 = new ArrayList<>();
                    if (ctx.sel_exp_where_AM().expr().second_sel_all() != null) {
                        for (int i = 0; i < ctx.sel_exp_where_AM().expr().second_sel_all().expr().size(); i++) {
                            TableAndColumnName tableAndColumnName = new TableAndColumnName();
                            if (ctx.sel_exp_where_AM().expr().second_sel_all().expr(i).database_exp() != null) {
                                if (ctx.sel_exp_where_AM().expr().second_sel_all().expr(i).database_exp().database_name() != null) {
                                    tableAndColumnName.setDataBaseName(ctx.sel_exp_where_AM().expr().second_sel_all().expr(i).database_exp().database_name().any_name().getText());
                                }
                                if (ctx.sel_exp_where_AM().expr().second_sel_all().expr(i).database_exp().table_name() != null)
                                    tableAndColumnName.setTableName(ctx.sel_exp_where_AM().expr().second_sel_all().expr(i).database_exp().table_name().any_name().getText());
                                tableAndColumnName.setColName(ctx.sel_exp_where_AM().expr().second_sel_all().expr(i).database_exp().column_name().any_name().getText());
                            } else
                                tableAndColumnName.setColName(ctx.sel_exp_where_AM().expr().second_sel_all().expr(i).literal_value().getText());
                            nameWhereCondition2.add(tableAndColumnName);
                        }

                    } else if (ctx.sel_exp_where_AM().expr().expr(1) != null) {
                        TableAndColumnName tableAndColumnName = new TableAndColumnName();
                        if (ctx.sel_exp_where_AM().expr().expr(1).database_exp() != null) {
                            if (ctx.sel_exp_where_AM().expr().expr(1).database_exp().database_name() != null) {
                                tableAndColumnName.setDataBaseName(ctx.sel_exp_where_AM().expr().expr(1).database_exp().database_name().any_name().getText());
                            }
                            tableAndColumnName.setTableName(ctx.sel_exp_where_AM().expr().expr(1).database_exp().table_name().any_name().getText());
                            tableAndColumnName.setColName(ctx.sel_exp_where_AM().expr().expr(1).database_exp().column_name().any_name().getText());
                        } else if (ctx.sel_exp_where_AM().expr().expr(1).literal_value() != null)
                            tableAndColumnName.setColName(ctx.sel_exp_where_AM().expr().expr(1).literal_value().getText());
                        nameWhereCondition2.add(tableAndColumnName);
                    }
                    whereCondition.setWhereNameCondition2(nameWhereCondition2);
                }
                selectAll.setWhereCondition(whereCondition);
                System.out.println("wewwe " + selectAll.getWhereCondition().getOperation());
            }

            if (selectAll.getWhereCondition().getWhereNameCondition1().getTableName() != null) {
                System.out.println("Table name is " + selectAll.getWhereCondition().getWhereNameCondition1().getTableName());
            }
            System.out.println("column name is  " + selectAll.getWhereCondition().getWhereNameCondition1().getColName());
            for (int i = 0; i < selectAll.getWhereCondition().getWhereNameCondition2().size(); i++) {
                if (selectAll.getWhereCondition().getWhereNameCondition2().get(i).getTableName() != null) {
                    System.out.println("Table name is second " + selectAll.getWhereCondition().getWhereNameCondition2().get(i).getTableName());
                }
                System.out.println("column name is  second " + selectAll.getWhereCondition().getWhereNameCondition2().get(i).getColName());
            }
            if (selectAll.getWhereCondition().getWhereCondition() != null) {
                System.out.println("key is  " + selectAll.getWhereCondition().getKey());
                if (selectAll.getWhereCondition().getWhereCondition().getWhereNameCondition1().getTableName() != null)
                    System.out.println("Table name in  where is " + selectAll.getWhereCondition().getWhereCondition().getWhereNameCondition1().getTableName());
                System.out.println("columnn name in  where is " + selectAll.getWhereCondition().getWhereCondition().getWhereNameCondition1().getColName());

                for (int i = 0; i < selectAll.getWhereCondition().getWhereCondition().getWhereNameCondition2().size(); i++) {
                    if (selectAll.getWhereCondition().getWhereCondition().getWhereNameCondition2().get(i).getTableName() != null)
                        System.out.println("Table name in  where second is " + selectAll.getWhereCondition().getWhereCondition().getWhereNameCondition2().get(i).getTableName());
                    System.out.println("column name in  where second is " + selectAll.getWhereCondition().getWhereCondition().getWhereNameCondition2().get(i).getColName());
                }
            }

        }
        if (ctx.sel_exp_groupBy_AM() != null) {
            Main.parentScope = scope;
            selectOrValue.setExpSqlAMGroupBy(visitSel_exp_groupBy_AM(ctx.sel_exp_groupBy_AM()));
            if (ctx.sel_exp_groupBy_AM().expr(0) != null) {
                GroupBy groupBy = new GroupBy();
                groupBy.setGroupByColumnName(ctx.sel_exp_groupBy_AM().expr(0).database_exp().column_name().any_name().getText());
                selectAll.setGroupBy(groupBy);
            }
        }
        if (ctx.sel_exp_having_AM() != null) {
            Main.parentScope = scope;
            selectOrValue.setExpSqlAMHaving(visitSel_exp_having_AM(ctx.sel_exp_having_AM()));
            if (ctx.sel_exp_having_AM().expr() != null) {
                if (ctx.sel_exp_having_AM().expr().expr(0) != null) {
                    HavingCOndition havingCOndition = new HavingCOndition();
                    AggFun aggFun = new AggFun();
                    aggFun.setAggFunTableName(ctx.sel_exp_having_AM().expr().expr(0).function_exp().function_name().any_name().getText());
                    aggFun.setAggFunParam(ctx.sel_exp_having_AM().expr().expr(0).function_exp().expr(0).database_exp().column_name().any_name().getText());
                    havingCOndition.setAggFun(aggFun);
                    havingCOndition.setCondition1(ctx.sel_exp_having_AM().expr().expr(1).literal_value().getText());
                    selectAll.setHavingCondition(havingCOndition);
                }
            }
        }
        if (ctx.expr() != null) {
            for (int i = 0; i < ctx.expr().size(); i++) {
                Main.parentScope = scope;
                selectOrValue.setExps(visitExpr(ctx.expr(i)));
            }
        }
        selectAll.setVariableName(variableName);

//        System.out.println("jajahahanana "+selectAll.getVariableName());
        for (int i = 0; i < selectAll.getColumnNames().size(); i++) {
            System.out.println("column is " + selectAll.getColumnNames().get(i));
        }
        selectOrValue.setSelectAll(selectAll);

        selectOrValue.setScope(scope);
        selectOrValue.setParent(true);

        for (int i = 0; i < selectAll.getAggFuns().size(); i++) {
            System.out.println("agg " + selectAll.getAggFuns().get(i).getTableAliasName());
        }
        System.out.println("kaka " + selectAll.getTableNames().get(0));
//        System.out.println("kakdaandnd "+selectAll.getWhereCondition().getWhereCondition().getOperation());
//        for (int i = 0; i < selectAll.getWhereCondition().getWhereNameCondition2().size(); i++) {
//            System.out.println("nababsn " + selectAll.getWhereCondition().getWhereNameCondition2().get(i).getColName());
//        }

//        System.out.println("where 2 "+selectAll.getWhereCondition().getWhereCondition().getWhereNameCondition1().getDataBaseName());

        Main.symbolTable.getDeclaredTypes().add(selectAll);

        return selectOrValue;
    }


    @Override
    public LiteralValue visitLiteral_value(SqlParser.Literal_valueContext ctx) {
        System.out.println("visit Literal value");
        LiteralValue literalValue = new LiteralValue();
        literalValue.setOk(true);
        if (ctx.NUMERIC_LITERAL() != null) {
            System.out.println("  1  " + ctx.NUMERIC_LITERAL().getSymbol().getText());
            literalValue.setLiteralValue(ctx.NUMERIC_LITERAL().getSymbol().getText());
        }
        if (ctx.STRING_LITERAL() != null) {
            System.out.println("  2  " + ctx.STRING_LITERAL().getSymbol().getText());
            literalValue.setLiteralValue(ctx.STRING_LITERAL().getSymbol().getText());
        }
        if (ctx.BLOB_LITERAL() != null) {
            System.out.println("  3  " + ctx.BLOB_LITERAL().getSymbol().getText());
            literalValue.setLiteralValue(ctx.BLOB_LITERAL().getSymbol().getText());
        }
        if (ctx.K_NULL() != null) {
            System.out.println("  4  " + ctx.K_NULL().getSymbol().getText());
            literalValue.setLiteralValue(ctx.K_NULL().getSymbol().getText());
        }
        if (ctx.K_CURRENT_DATE() != null) {
            System.out.println("  5  " + ctx.K_CURRENT_DATE().getSymbol().getText());
            literalValue.setLiteralValue(ctx.K_CURRENT_DATE().getSymbol().getText());
        }
        if (ctx.K_CURRENT_TIME() != null) {
            System.out.println("  6  " + ctx.K_CURRENT_TIME().getSymbol().getText());
            literalValue.setLiteralValue(ctx.K_CURRENT_TIME().getSymbol().getText());
        }
        if (ctx.K_CURRENT_TIMESTAMP() != null) {
            System.out.println("  7  " + ctx.K_CURRENT_TIMESTAMP().getSymbol().getText());
            literalValue.setLiteralValue(ctx.K_CURRENT_TIMESTAMP().getSymbol().getText());
        }
        return literalValue;
    }

    @Override
    public AnyName visitAny_name(SqlParser.Any_nameContext ctx) {

        System.out.println("visit any name");
        String name = "";

        SqlParser.Any_nameContext ctx2 = ctx;
        if (ctx != null)
            name = ctx.getText();
        System.out.println("test name  : " + name);
        AnyName anyName = new AnyName();
        anyName.setAnyName(name);
        anyName.setOk(true);

//        anyName.setLine(ctx.getStart().getLine()); //get line number
//        anyName.setCol(ctx.getStart().getCharPositionInLine());
        return anyName;
    }


    @Override
    public FunctionDefinition visitFunction(SqlParser.FunctionContext ctx) {
        System.out.println(" visit function definition  ");

        FunctionDefinition functionDefinition = new FunctionDefinition();
        functionDefinition.setOk(true);

        Scope funScope = new Scope();

        String k_funname = "";
        if (ctx.function_name() != null) {
            k_funname = ctx.function_name().any_name().IDENTIFIER().getText();
            System.out.println(" the key is :" + k_funname);
            functionDefinition.setNamefun(k_funname);
            funScope.setId("fun_" + k_funname);
            System.out.println("in function");
            System.out.println(funScope.getId());
        }
        funScope.setParent(null);
        System.out.println("fun scope " + funScope.getId());
        Main.symbolTable.addScope(funScope);

        // Any name | signed numer | expr
        Exp exp = new Exp();
        boolean check = false;
        // this is return value
        if (ctx.expr() != null) {
            Type returnType = new Type();
            String typeName = "";
            if (ctx.expr().literal_value() != null) {
                if (ctx.expr().literal_value().NUMERIC_LITERAL() != null) {
                    typeName = Type.NUMBER_CONST;
                } else if (ctx.expr().literal_value().STRING_LITERAL() != null) {
                    typeName = Type.CHAR_CONST;
                } else if (ctx.expr().literal_value().BLOB_LITERAL() != null) {
                    typeName = Type.BLOB_LITERAL;
                } else if (ctx.expr().literal_value().K_CURRENT_TIME() != null) {
                    typeName = Type.TIME;
                } else if (ctx.expr().literal_value().K_CURRENT_DATE() != null) {
                    typeName = Type.DATE;
                } else if (ctx.expr().literal_value().K_CURRENT_TIMESTAMP() != null) {
                    typeName = Type.TIMESTAMP;
                }
                check = true;
                returnType.setValue(ctx.expr().literal_value().getText());
            } else if (ctx.expr().database_exp() != null) {
                if (ctx.expr().database_exp().column_name().any_name() != null) {
                    typeName = Type.STRING_CONST;
                    check = true;
                    returnType.setValue(ctx.expr().database_exp().column_name().any_name().IDENTIFIER().getText());
                }
            } else if (ctx.expr().K_TRUE() != null) {
                typeName = Type.BOOLEAN_CONST;
                returnType.setValue(ctx.expr().K_TRUE().getText());
                check = true;
            } else if (ctx.expr().K_FALSE() != null) {
                returnType.setValue(ctx.expr().K_FALSE().getText());
                typeName = Type.BOOLEAN_CONST;
                check = true;
            }

            if (check) {
                returnType.setName(typeName);
                Scope currentScope = Main.symbolTable.getScopes().get(Main.symbolTable.getScopes().size() - 1);
                Symbol returnSymbol = new Symbol();
                returnSymbol.setName("return " + returnType.getValue());
                returnSymbol.setScope(currentScope);
                returnSymbol.setIsReturn(true);
                returnSymbol.setType(returnType);
//                System.out.println("return type " + returnSymbol.getType().getName());
                currentScope.addSymbol("return " + returnType.getValue(), returnSymbol);
//                System.out.println("sizeeeee is "+Main.symbolTable.getScopes().size());
            }

            exp = visitExpr(ctx.expr());
            Symbol symbol = lastSymbol(funScope);
            symbol.setIsReturn(true);
//            System.out.println("TOPO" + symbol.getExpression().getValue1());
//            System.out.println("Symbol is  " + symbol.getName());
            functionDefinition.setSymbolReturn(symbol);
            functionDefinition.setExp(exp);
        }

        // ParameterList
        if (ctx.parameter_list() != null)
            for (int i = 0; i < ctx.parameter_list().size(); i++) {
                Main.parentScope = funScope;
                functionDefinition.setParmeterLists(visitParameter_list(ctx.parameter_list(i)));
            }

        // java stmt
        ArrayList<JavaStmt> javaStmt = new ArrayList<>();

        for (int i = 0; i < ctx.java_stmt().size(); i++) {
            Main.parentScope = funScope;
            javaStmt.add(visitJava_stmt(ctx.java_stmt(i)));
        }
        functionDefinition.setJavaStmt(javaStmt);

        functionDefinition.setLine(ctx.getStart().getLine()); //get line number
        functionDefinition.setCol(ctx.getStart().getCharPositionInLine());
        return functionDefinition;
    }

    public Symbol editSymbol(Scope scope, String key) {
        Symbol symbol = new Symbol();
        for (Map.Entry<String, Symbol> entry : scope.symbolMap.entrySet()) {
            if (entry.getKey().equals(key)) {
                System.out.println("the key: " + entry.getKey());
//                System.out.println(  "the value: "+entry.getValue().getType().getName());
                symbol = entry.getValue();
            }
        }
        return symbol;
    }

    public Symbol lastSymbol(Scope scope) {
        Symbol symbol = new Symbol();
        for (Map.Entry<String, Symbol> entry : scope.symbolMap.entrySet()) {
            symbol = entry.getValue();
        }
        return symbol;
    }


    @Override
    public CallFunction visitCall_function(SqlParser.Call_functionContext ctx) {
        System.out.println(" visit call function  ");
        CallFunction callFunction = new CallFunction();
        callFunction.setOk(true);

        Scope callFunScope = new Scope();
        // fun name
        String k_funname = " ";
        SqlParser.Call_functionContext ctx2 = ctx;
        if (ctx.function_name() != null) {
            k_funname = ctx.function_name().getText();
            System.out.println(" the key is :" + k_funname);
        }
        callFunction.setNamefun(k_funname);
        callFunScope.setParent(Main.parentScope);
        callFunScope.setId("callFun " + callFunction.getNamefun());
        System.out.println("call fun Scope " + callFunScope.getId());
        System.out.println("parent Scope " + callFunScope.getParent().getId());
        Main.symbolTable.addScope(callFunScope);

        // argument list ..

        if (ctx.argument_list() != null) {
            Main.parentScope = callFunScope.getParent();
            Scope argumentListScope = new Scope();
            argumentListScope.setId("argumentList  " + k_funname);
            argumentListScope.setParent(Main.parentScope);
            System.out.println(" argument scope " + argumentListScope.getId());
            System.out.println(" perant argument scope  " + argumentListScope.getParent().getId());
            Main.symbolTable.addScope(argumentListScope);
            callFunction.setArgumentList(visitArgument_list(ctx.argument_list()));

        }

        callFunction.setScope(callFunScope);

        // here i must add the type of the call fun
        //  Symbol symbolReturn = editSymbol(Main.parentScope,)

        callFunction.setLine(ctx.getStart().getLine()); //get line number
        callFunction.setCol(ctx.getStart().getCharPositionInLine());
        return callFunction;
    }

    @Override
    public Switch visitJava_switch_stmt(SqlParser.Java_switch_stmtContext ctx) {
        System.out.println("visit switch ");
        Scope parentScope = Main.parentScope;
        Scope switchScope = new Scope();
        Switch anSwitch = new Switch();

        anSwitch.setOk(true);
        if (ctx.IDENTIFIER() != null)
            anSwitch.setArgumentName(ctx.IDENTIFIER().getText());
        else if (ctx.NUMERIC_LITERAL() != null)
            anSwitch.setArgumentName(ctx.NUMERIC_LITERAL().getText());

        switchScope.setId("switch(" + anSwitch.getArgumentName() + ")");
        switchScope.setParent(parentScope);
        Main.symbolTable.addScope(switchScope);

        if (ctx.call_function() != null) {
            Main.parentScope = switchScope;
            anSwitch.setCallFunction(visitCall_function(ctx.call_function()));
        }

        if (ctx.case_switch() != null)
            for (int i = 0; i < ctx.case_switch().size(); i++) {
                Main.parentScope = switchScope;
                anSwitch.setCaseSwitches(visitCase_switch(ctx.case_switch(i)));
                System.out.println("rararar " + anSwitch.getCaseSwitches().size());
            }

        if (ctx.default_switch() != null) {
            Main.parentScope = switchScope;
            anSwitch.setDefaultSwitch(visitDefault_switch(ctx.default_switch()));
        }

        anSwitch.setScope(switchScope);

        anSwitch.setLine(ctx.getStart().getLine()); //get line number
        anSwitch.setCol(ctx.getStart().getCharPositionInLine());
        return anSwitch;
    }

    @Override
    public CaseSwitch visitCase_switch(SqlParser.Case_switchContext ctx) {
        System.out.println("visit Case switch");

        Scope parentScope = Main.parentScope;

        CaseSwitch caseSwitch = new CaseSwitch();
        caseSwitch.setOk(true);

        if (ctx.any_name() != null)
            caseSwitch.setValueCase(ctx.any_name().getText());
        else if (ctx.signed_number() != null)
            caseSwitch.setValueCase(ctx.signed_number().getText());


        Scope caseScope = new Scope();
        caseScope.setParent(parentScope);
        caseScope.setId("case : " + caseSwitch.getValueCase());
        Main.symbolTable.addScope(caseScope);

        if (ctx.java_anystmt() != null)
            for (int i = 0; i < ctx.java_anystmt().size(); i++) {
                Main.parentScope = caseScope;
                caseSwitch.setJavaAnyStmts(visitJava_anystmt(ctx.java_anystmt(i)));
            }

        caseSwitch.setScope(caseScope);

        caseSwitch.setLine(ctx.getStart().getLine()); //get line number
        caseSwitch.setCol(ctx.getStart().getCharPositionInLine());
        return caseSwitch;
    }

    @Override
    public DefaultSwitch visitDefault_switch(SqlParser.Default_switchContext ctx) {
        System.out.println("visit default switch");

        Scope parentScope = Main.parentScope;

        DefaultSwitch defaultSwitch = new DefaultSwitch();
        defaultSwitch.setOk(true);
        Scope defaultScope = new Scope();
        defaultScope.setId("default : ");
        defaultScope.setParent(parentScope);
        Main.symbolTable.addScope(defaultScope);

        if (ctx.java_anystmt() != null)
            for (int i = 0; i < ctx.java_anystmt().size(); i++) {
                Main.parentScope = defaultScope;
                defaultSwitch.setJavaAnyStmts(visitJava_anystmt(ctx.java_anystmt(i)));
            }

        return defaultSwitch;
    }

    @Override
    public While visitJavaWhile_stmt(SqlParser.JavaWhile_stmtContext ctx) {
        System.out.println(" visit while stmt  ");

        Scope parentScope = Main.parentScope;

        While whileNormal = new While();
        whileNormal.setOk(true);

        Scope whileScope = new Scope();
        whileScope.setId("while : ");
        whileScope.setParent(parentScope);
        Main.symbolTable.addScope(whileScope);


        // key while
        String K_while = " ";
        SqlParser.JavaWhile_stmtContext ctx1 = ctx;
        if (ctx.K_WHILE() != null)
            K_while = ctx.K_WHILE().getText();
        System.out.println(" the key is :" + K_while);
        whileNormal.setK_while(K_while);


        // conndition while
        Main.parentScope = whileScope;
        ConditionStmt conditionStmt = visitCondtion_stmt(ctx.condtion_stmt());
        if (conditionStmt != null) {
            System.out.println("momo");
        }
        whileNormal.setConditionStmt(conditionStmt);

        // body of while
        Main.parentScope = whileScope;
        BodyAllStmt bodyAllStmt = visitBody_all_stmt(ctx.body_all_stmt());
        whileNormal.setBodyAllStmt(bodyAllStmt);

        return whileNormal;
    }

    @Override
    public DoWhile visitJava_do_while(SqlParser.Java_do_whileContext ctx) {
        System.out.println(" visit do_while  ");

        Scope parentScope = Main.parentScope;

        Scope doWhileScope = new Scope();
        doWhileScope.setId("do while ");
        doWhileScope.setParent(parentScope);
        Main.symbolTable.addScope(doWhileScope);


        DoWhile doWhile = new DoWhile();
        doWhile.setOk(true);
        // key do
        String K_do = " ";
        SqlParser.Java_do_whileContext ctx1 = ctx;
        if (ctx.K_DO() != null)
            K_do = ctx.K_DO().getText();
        System.out.println(" the key is :" + K_do);
        doWhile.setK_do(K_do);


        // key while
        String K_while = " ";
        SqlParser.Java_do_whileContext ctx2 = ctx;
        if (ctx.K_WHILE() != null)
            K_while = ctx.K_WHILE().getText();
        System.out.println(" the key is :" + K_while);
        doWhile.setK_while(K_while);


        // condtiion stmt
        Main.parentScope = doWhileScope;
        System.out.println("this con " + Main.parentScope.getId());
        ConditionStmt conditionStmt = visitCondtion_stmt(ctx.condtion_stmt());
        doWhile.setConditionStmt(conditionStmt);


        // body of do while
        Main.parentScope = doWhileScope;
        System.out.println("this body  " + Main.parentScope.getId());
        BodyAllStmt bodyAllStmt = visitBody_all_stmt(ctx.body_all_stmt());
        doWhile.setBodyAllStmt(bodyAllStmt);


        return doWhile;
    }

    @Override
    public PrintStmt visitPrint_stm(SqlParser.Print_stmContext ctx) {
        System.out.println("visit print stmt");
        Scope parentScope = Main.parentScope;

        PrintStmt printStmt = new PrintStmt();
        Scope printScope = new Scope();
        printScope.setParent(parentScope);
        printScope.setParent(Main.parentScope);
        printScope.setId("print ");
        Main.symbolTable.addScope(printScope);

        boolean check = false;
        Scope currentScope = Main.symbolTable.getScopes().get(Main.symbolTable.getScopes().size() - 1);
        for (int i = 0; i < ctx.expr().size(); i++) {
            Type printExpType = new Type();
            String typeName = "";
            check = false;
            if (ctx.expr(i).literal_value() != null) {
                if (ctx.expr(i).literal_value().NUMERIC_LITERAL() != null) {
                    typeName = Type.NUMBER_CONST;
                } else if (ctx.expr(i).literal_value().STRING_LITERAL() != null) {
                    typeName = Type.CHAR_CONST;
                } else if (ctx.expr(i).literal_value().BLOB_LITERAL() != null) {
                    typeName = Type.BLOB_LITERAL;
                } else if (ctx.expr(i).literal_value().K_CURRENT_TIME() != null) {
                    typeName = Type.TIME;
                } else if (ctx.expr(i).literal_value().K_CURRENT_DATE() != null) {
                    typeName = Type.DATE;
                } else if (ctx.expr(i).literal_value().K_CURRENT_TIMESTAMP() != null) {
                    typeName = Type.TIMESTAMP;
                }
                check = true;
                printExpType.setValue(ctx.expr(i).literal_value().getText());
            } else if (ctx.expr(i).database_exp() != null) {
                if (ctx.expr(i).database_exp().column_name().any_name() != null) {
                    typeName = Type.STRING_CONST;
                    check = true;
                    if (!ctx.expr(i).database_exp().column_name().any_name().getText().startsWith("\"")) {
                        System.out.println("jj");
                        printStmt.addNamesVariable(ctx.expr(i).database_exp().column_name().any_name().getText());
                    }
                    printExpType.setValue(ctx.expr(i).database_exp().column_name().any_name().IDENTIFIER().getText());
                }
            } else if (ctx.expr(i).K_TRUE() != null) {
                typeName = Type.BOOLEAN_CONST;
                printExpType.setValue(ctx.expr(i).K_TRUE().getText());
                check = true;
            } else if (ctx.expr(i).K_FALSE() != null) {
                printExpType.setValue(ctx.expr(i).K_FALSE().getText());
                typeName = Type.BOOLEAN_CONST;
                check = true;
            }

            if (check) {
                printExpType.setName(typeName);
                Symbol printExpSymbol = new Symbol();
                printExpSymbol.setName("print value " + printExpType.getValue());
                printExpSymbol.setScope(currentScope);
                printExpSymbol.setType(printExpType);
                currentScope.addSymbol("print value " + printExpType.getValue(), printExpSymbol);
            }
        }


        for (int i = 0; i < ctx.expr().size(); i++) {
            printStmt.setExps(visitExpr(ctx.expr().get(i)));
        }
        System.out.println(false);

        printStmt.setLine(ctx.getStart().getLine()); //get line number
        printStmt.setCol(ctx.getStart().getCharPositionInLine());
        printStmt.setScope(printScope);
        return printStmt;
    }

    @Override
    public ParmeterList visitParameter_list(SqlParser.Parameter_listContext ctx) {
        System.out.println(" visit parameter list  ");
        ParmeterList parmeterList = new ParmeterList();
        parmeterList.setOk(true);
        if (ctx.item() != null) {
            for (int i = 0; i < ctx.item().size(); i++)
                parmeterList.setItems(visitItem(ctx.item(i)));
        }
        return parmeterList;
    }

    @Override
    public Item visitItem(SqlParser.ItemContext ctx) {
        System.out.println("visit Item ");
        Item item = new Item();
        item.setOk(true);

        Scope parentScope = Main.parentScope;
        boolean check = false;

        Symbol varSymbol = new Symbol();
        String nameValue = ctx.var_stmt().any_name().IDENTIFIER().getText();
        varSymbol.setIsParam(true);
        varSymbol.setName(nameValue);
        varSymbol.setScope(parentScope);
        Scope currentScope = Main.symbolTable.getScopes().get(Main.symbolTable.getScopes().size() - 1);

        if (ctx.expr() != null) {
            Type expType = new Type();
            String typeName = "";
            check = false;
            if (ctx.expr().literal_value() != null) {
                if (ctx.expr().literal_value().NUMERIC_LITERAL() != null) {
                    typeName = Type.NUMBER_CONST;
                } else if (ctx.expr().literal_value().STRING_LITERAL() != null) {
                    typeName = Type.CHAR_CONST;
                } else if (ctx.expr().literal_value().BLOB_LITERAL() != null) {
                    typeName = Type.BLOB_LITERAL;
                } else if (ctx.expr().literal_value().K_CURRENT_TIME() != null) {
                    typeName = Type.TIME;
                } else if (ctx.expr().literal_value().K_CURRENT_DATE() != null) {
                    typeName = Type.DATE;
                } else if (ctx.expr().literal_value().K_CURRENT_TIMESTAMP() != null) {
                    typeName = Type.TIMESTAMP;
                }
                check = true;
                expType.setValue(ctx.expr().literal_value().getText());
            } else if (ctx.expr().database_exp() != null) {
                if (ctx.expr().database_exp().column_name().any_name() != null) {
                    typeName = Type.STRING_CONST;
                    check = true;
                    expType.setValue(ctx.expr().database_exp().column_name().any_name().IDENTIFIER().getText());
                }
            } else if (ctx.expr().K_TRUE() != null) {
                typeName = Type.BOOLEAN_CONST;
                expType.setValue(ctx.expr().K_TRUE().getText());
                check = true;
            } else if (ctx.expr().K_FALSE() != null) {
                expType.setValue(ctx.expr().K_FALSE().getText());
                typeName = Type.BOOLEAN_CONST;
                check = true;
            }

            if (check) {
                expType.setName(typeName);
                System.out.println("type is rr " + expType.getName());
                varSymbol.setType(expType);
                varSymbol.setIsParam(true);
                currentScope.addSymbol("var " + nameValue, varSymbol);
            }
        }

        if (ctx.expr() != null) {
            check = true;
            item.setExp(visitExpr(ctx.expr()));
            Symbol symbol = lastSymbol(currentScope);
            symbol.setName(nameValue);
            symbol.setIsParam(true);
            symbol.setScope(parentScope);
        }

        Main.parentScope = parentScope;
        item.setDecVar(visitVar_stmt(ctx.var_stmt()));
        Symbol symbol = lastSymbol(parentScope);
        symbol.setIsParam(true);

        return item;
    }

    @Override
    public ArgumentList visitArgument_list(SqlParser.Argument_listContext ctx) {
        System.out.println(" visit argument list ");
        ArgumentList argumentList = new ArgumentList();
        argumentList.setOk(true);

        ArrayList<DifferentKindOfForIncreament> differentKindOfForIncreaments = new ArrayList<>();
        if (ctx.differnt_kind_of_for_increament() != null)
            for (int i = 0; i < ctx.differnt_kind_of_for_increament().size(); i++)
                differentKindOfForIncreaments.add(visitDiffernt_kind_of_for_increament(ctx.differnt_kind_of_for_increament(i)));
        argumentList.setDifferentKindOfForIncreament(differentKindOfForIncreaments);


        // expr
        ArrayList<Exp> exps = new ArrayList<>();
        if (ctx.expr() != null) {
            for (int i = 0; i < ctx.expr().size(); i++)
                exps.add(visitExpr(ctx.expr(i)));
            argumentList.setExps(exps);
        }

        return argumentList;
    }

    @Override
    public SignedNumber visitSigned_number(SqlParser.Signed_numberContext ctx) {

        System.out.println("visit signed number ");
        String number = "";

        SqlParser.Signed_numberContext ctx2 = ctx;
        if (ctx != null)
            number = ctx.getText();
        System.out.println(" test number  : " + number);
        SignedNumber signedNumber = new SignedNumber();
        signedNumber.setSiNumber(number);
        signedNumber.setOk(true);
        return signedNumber;

    }

    @Override
    public CreateTableStatmt visitCreate_table_stmt(SqlParser.Create_table_stmtContext ctx) {
        System.out.println("visit create table stmt");
        CreateTableStatmt createTableStatmt = new CreateTableStatmt();

        Scope createScope = new Scope();
        createScope.setId(ctx.table_name().any_name().getText());
        createScope.setParent(Main.parentScope);
        Main.symbolTable.addScope(createScope);
        System.out.println("createTable scope " + createScope.getId());
        System.out.println("createTable parent scope " + createScope.getParent().getId());

        if (ctx.table_name() != null)
            createTableStatmt.setTableName(ctx.table_name().any_name().getText());
        // Symbol tableSymbol = new Symbol();
        // tableSymbol.setScope(createScope);
        //  tableSymbol.setName(ctx.table_name().any_name().getText());

        Type tableType = new Type();

        // tableType.setName(ctx.type().getText()); // temp
        tableType.setName(ctx.table_name().any_name().getText());
        tableType.setValue("table");
        //  tableSymbol.setType(tableType);
        //  createScope.addSymbol(ctx.table_name().any_name().getText(), tableSymbol);
        Main.symbolTable.addType(tableType);
        ArrayList<ColumnDef> columnDefs = new ArrayList<>();
        for (int i = 0; i < ctx.column_def().size(); i++)
            if (ctx.column_def(i) != null) {
                columnDefs.add(visitColumn_def(ctx.column_def(i)));
            }
        createTableStatmt.setColumnDefs(columnDefs);
        if (ctx.type() != null) {
            createTableStatmt.setType(ctx.type().getText());
            tableType.setTypeTable(createTableStatmt.getType());
            System.out.println(" type :" + createTableStatmt.getType());
        }
        if (ctx.path() != null) {
            createTableStatmt.setPath(ctx.path().getText());
            tableType.setPath(createTableStatmt.getPath());
            System.out.println(" path :" + createTableStatmt.getPath());
        }


        createTableStatmt.setTypetable(tableType);// add to class ..

        createTableStatmt.setLine(ctx.getStart().getLine()); //get line number
        createTableStatmt.setCol(ctx.getStart().getCharPositionInLine());
        return createTableStatmt;
    }

    @Override
    public ColumnDef visitColumn_def(SqlParser.Column_defContext ctx) {
        System.out.println("visit column def  ");
        ColumnDef columnDef = new ColumnDef();
        columnDef.setOK(true);


        Scope currentScope = Main.symbolTable.getScopes().get(Main.symbolTable.getScopes().size() - 1);
        Type currentType = Main.symbolTable.getDeclaredTypes().get(Main.symbolTable.getDeclaredTypes().size() - 1);

        Symbol ColDefSymbol = editSymbol(currentScope, ctx.column_name().any_name().getText());
        if (ColDefSymbol.getName() != null) {

            columnDef.setColDefTablSymbol(null);
        } else {
            ColDefSymbol.setName(ctx.column_name().any_name().getText());

            ColDefSymbol.setScope(currentScope);

            Type type = new Type();
            AnyName columnName = visitAny_name(ctx.column_name().any_name());
            columnDef.setColumnName(columnName);
            type.setName(ctx.column_name().any_name().getText());
            type.setFrom(currentScope.getId()); // the name of type ..
            System.out.println(" from :  " + type.getFrom());


            if (ctx.type_name() != null)
                for (int j = 0; j < ctx.type_name().size(); j++) {
                    columnDef.setTypeNames(visitType_name(ctx.type_name(j)));


                    String typeName = ctx.type_name().get(j).name().getText();
//                if (typeName.equals(Type.STRING_CONST) || typeName.equals(Type.NUMBER_CONST) || typeName.equals(Type.CHAR_CONST)
//                        || typeName.equals(Type.BOOLEAN_CONST) || typeName.equals(Type.TIME) || typeName.equals(Type.DATE)) {
                    type.setValue(typeName);
                    System.out.println("the type (value )of col " + type.getName());
                    ColDefSymbol.setType(type);
                    currentType.addType(type.getName(), type);
                    Main.symbolTable.addTypeToArrayTypes(type);

                }
            else System.out.println("this type not found .. ");// temp
            //  }
            currentScope.addSymbol(ctx.column_name().any_name().getText(), ColDefSymbol);
            columnDef.setColDefTablSymbol(ColDefSymbol);
        }

        columnDef.setLine(ctx.getStart().getLine()); //get line number
        columnDef.setCol(ctx.getStart().getCharPositionInLine());

        return columnDef;

    }


    @Override
    public TypeName visitType_name(SqlParser.Type_nameContext ctx) {
        System.out.println(" visit Type Name  ");
        TypeName typeName = new TypeName();
        typeName.setOK(true);


        AnyName name = visitAny_name(ctx.name().any_name());
        typeName.setName(name);

        if (ctx.signed_number() != null)
            for (int i = 0; i < ctx.signed_number().size(); i++)
                typeName.setSignedNumbers(visitSigned_number(ctx.signed_number(i)));

        if (ctx.any_name() != null)
            typeName.setAnyName(visitAny_name(ctx.any_name(0)));

        return typeName;
    }

    public static void convertToClass() throws IOException {

        Type type = new Type();
        String keyClass = "public class ";
        String className = "";
        Map<String, String> columns = new HashMap<>();
        for (int i = 0; i < Main.symbolTable.getDeclaredTypes().size(); i++) {
            columns = new HashMap<>();
            type = Main.symbolTable.getDeclaredTypes().get(i);
            className = type.getName();


            if (type.getValue() != null) {
                try {

                    File file = new File("C:\\Users\\Amina Alkurde\\IdeaProjects\\created classes\\src\\" + className + ".java");
                    FileWriter fileWriter = new FileWriter(file, false);
                    BufferedWriter buffer = new BufferedWriter(fileWriter);
                    PrintWriter printWriter = new PrintWriter(buffer);

                    if (!file.exists()) {
                        file.createNewFile();
                    }
                    printWriter.println("import java.util.ArrayList;" +
                            "\n" +
                            //    "import com.google.gson.Gson;\n" +
                            "import org.json.simple.JSONArray;\n" +
                            "import org.json.simple.JSONObject;\n" +
                            "import org.json.simple.parser.JSONParser;\n" +
                            "\n" +
                            "import java.io.*;\n" +
                            "import java.lang.reflect.*;\n" +
                            "import java.text.ParseException;\n" +
                            "import java.util.*;\n");

                    printWriter.println(keyClass + className + "{");

                    String d = "";
                    String b = className + "(){}";
                    String atrr[] = new String[Main.symbolTable.getDeclaredTypes().get(i).getColumns().size()];
                    int ii = 0;

                    for (Map.Entry<String, Type> map : Main.symbolTable.getDeclaredTypes().get(i).getColumns().entrySet()) {
                        if (map.getValue().getName() != null) {
                            String Type = map.getValue().getValue();
                            if (map.getValue().getValue().equalsIgnoreCase("string")) {
                                printWriter.println("public String " + map.getValue().getName() + ";");
                                //  d += "                 obj." + map.getValue().getName() + "=(String ) values[" + ii + "];\n";
                            } else if (map.getValue().getValue().equalsIgnoreCase("number")) {
                                printWriter.println("public double " + map.getValue().getName() + ";");
                                //   d += "                 obj." + map.getValue().getName() + "=Double.parseDouble(values[" + ii + "] );\n";
                            } else if (map.getValue().getValue().equalsIgnoreCase("boolean")) {
                                printWriter.println("public boolean " + map.getValue().getName() + ";");
                                //  d += "                 obj." + map.getValue().getName() + "=Boolean.parseBoolean(values[" + ii + "] );\n";
                            } else if (!Type.equalsIgnoreCase("string") && !Type.equalsIgnoreCase("number")
                                    && !Type.equalsIgnoreCase("boolean") && !Type.equalsIgnoreCase("char")
                                    && !Type.equalsIgnoreCase("blobLiteral") && !Type.equalsIgnoreCase("time")
                                    && !Type.equalsIgnoreCase("date") && !Type.equalsIgnoreCase("timeStamp")) {

                                printWriter.println("public " + Type + " " + map.getValue().getName() + "= new " + Type + "();");
                            } else {
                                printWriter.println("public " + map.getValue().getValue() + " " + map.getValue().getName() + ";");
                            }

                        }

                    }


//                b += "}";


                    if (type.getClass() != null && type.getValue() != "type") {
                        for (int k = 0; k < Main.symbolTable.getColumnTypes().size(); k++) {

                            String namefrom = Main.symbolTable.getColumnTypes().get(k).getFrom();
                            String NameType = Main.symbolTable.getColumnTypes().get(k).getName();
                            String ValueType = Main.symbolTable.getColumnTypes().get(k).getValue();
                            if (namefrom.equalsIgnoreCase(className)) {
                                if (ValueType.equalsIgnoreCase("string"))
                                    d += "                 obj." + NameType + "=(String ) values[" + ii + "];\n";
                                else if (ValueType.equalsIgnoreCase("number"))
                                    d += "                 obj." + NameType + "=Double.parseDouble(values[" + ii + "] );\n";
                                else if (ValueType.equalsIgnoreCase("boolean"))
                                    d += "                 obj." + NameType + "=Boolean.parseBoolean(values[" + ii + "] );\n";

                            }
                            ii++;
                        }
                        printWriter.println("public static String path =   " + (type).getPath() + "  ; ");
                        printWriter.println("public static String type = " + (type).getTypeTable() + "  ; ");
                        printWriter.println(b);

                        printWriter.println("\npublic static ArrayList<" + className + "> data = new ArrayList<>();");
                        printWriter.println(" \n \npublic static void Load() throws IOException, ParseException, IllegalAccessException, InstantiationException, ClassNotFoundException , org.json.simple.parser.ParseException{\n" +
                                        "        if(type.equals(\"csv\")){\n" +
                                        "            try (BufferedReader br = new BufferedReader(new FileReader(path))) {\n" +
                                        "                String line;\n" +
                                        "                while ((line = br.readLine()) != null) {\n" +
                                        "                    String [] values = line.split(\",\");\n" +
                                        "                    " + className + " obj = new " + className + "();\n" + d +
                                        "\n" +
                                        "                    data.add(obj);\n" +
                                        "                }\n" +
                                        "            }\n" +
                                        "        }\n" +
                                        "        else if(type.equals(\"json\")){\n" +
                                        "            JSONParser parser = new JSONParser();\n" +
                                        "            Object obj = parser.parse(new FileReader(" + (type).getPath() + "));\n" +
                                        "            JSONObject jsonObject =(JSONObject) obj;\n " +
                                        "            JSONArray jsonArray =(JSONArray) jsonObject.get(\"" + className + "\");\n" +
                                        "\n" +
                                        "            Iterator iterator = jsonArray.iterator();\n" +
                                        "             while(iterator.hasNext()) {\n" +
                                        "             JSONObject jsonObject1=(JSONObject )  iterator.next();\n" +
                                        className + "  " + className + "1 = new " + className + "();\n"
///
                        );
                        for (String key : Main.symbolTable.getDeclaredTypes().get(i).getColumns().keySet()) {
                            String Type = Main.symbolTable.getDeclaredTypes().get(i).getColumns().get(key).getValue();//?
                            if (Type.equalsIgnoreCase("number"))
                                Type = "Long";
                            else if (Type.equalsIgnoreCase("string"))
                                Type = "String";

                            if (!Type.equalsIgnoreCase("String") && !Type.equalsIgnoreCase("Long")
                                    && !Type.equalsIgnoreCase("boolean") && !Type.equalsIgnoreCase("char")
                                    && !Type.equalsIgnoreCase("blobLiteral") && !Type.equalsIgnoreCase("time")
                                    && !Type.equalsIgnoreCase("date") && !Type.equalsIgnoreCase("timeStamp"))
//
                                printWriter.println("" + className + "1.set" + key + "(( JSONObject ) jsonObject1.get(\"" + key + "\"));\n");
                            else
                                printWriter.println("" + className + "1.set" + key + "((" + Type + ") jsonObject1.get(\"" + key + "\"));\n");
                        }
                        printWriter.println(className + ".data.add(" + className + "1);\n" +
                                "}\n" +
                                "}\n" +
                                "}\n"
                        );
                    }
                    for (String key : Main.symbolTable.getDeclaredTypes().get(i).getColumns().keySet()) {
                        String Type = Main.symbolTable.getDeclaredTypes().get(i).getColumns().get(key).getValue();//?
                        if (Type.equalsIgnoreCase("number"))
                            Type = "double";
                        else if (Type.equalsIgnoreCase("string"))
                            Type = "String";

                        // Type :address
                        // key :Address

                        if (!Type.equalsIgnoreCase("String") && !Type.equalsIgnoreCase("double")
                                && !Type.equalsIgnoreCase("boolean") && !Type.equalsIgnoreCase("char")
                                && !Type.equalsIgnoreCase("blobLiteral") && !Type.equalsIgnoreCase("time")
                                && !Type.equalsIgnoreCase("date") && !Type.equalsIgnoreCase("timeStamp")) {
//
                            for (int j = 0; j < Main.symbolTable.getDeclaredTypes().size(); j++) {

                                if (Main.symbolTable.getDeclaredTypes().get(j).getName() != null) {
                                    System.out.println("the name of type :" + Main.symbolTable.getDeclaredTypes().get(j).getName());
                                    System.out.println("the value of type :" + Main.symbolTable.getDeclaredTypes().get(j).getValue());
                                    if (Main.symbolTable.getDeclaredTypes().get(j).getName().equalsIgnoreCase(Type)) // address
                                    {

                                        printWriter.println("public " + Type + "  get" + key + "() {\n" +
                                                "                            return " + key + ";\n" +
                                                "                        }\n" +
                                                "\n" +
                                                "            public void set" + key + "( JSONObject " + key + ") {\n");


                                        for (String testKey : Main.symbolTable.getDeclaredTypes().get(j).getColumns().keySet()) {

                                            String testType = Main.symbolTable.getDeclaredTypes().get(j).getColumns().get(testKey).getValue();
                                            if (testType.equalsIgnoreCase("number"))
                                                testType = "double";
                                            else if (testType.equalsIgnoreCase("string"))
                                                testType = "String";

                                            if (!testType.equalsIgnoreCase("String") && !testType.equalsIgnoreCase("double")
                                                    && !testType.equalsIgnoreCase("boolean") && !testType.equalsIgnoreCase("char")
                                                    && !testType.equalsIgnoreCase("blobLiteral") && !testType.equalsIgnoreCase("time")
                                                    && !testType.equalsIgnoreCase("date") && !testType.equalsIgnoreCase("timeStamp"))

                                                printWriter.println("this.get" + key + "().set" + testKey + "(( JSONObject )" + key + ".get(\"" + testKey + "\"));\n  ");
                                            else
                                                printWriter.println("this.get" + key + "().set" + testKey + "((" + testType + ")" + key + ".get(\"" + testKey + "\"));\n");

                                        }
                                        printWriter.println("}");
                                    }

                                }
                            }
                        } else
                            printWriter.println("public " + Type + "  get" + key + "() {\n" +
                                    "                            return " + key + ";\n" +
                                    "                        }\n" +
                                    "\n" +
                                    "                        public void set" + key + "(" + Type + " " + key + ") {\n" +
                                    "                            this." + key + " =" + key + ";\n" +
                                    "                        }\n"

                            );
                    }
                    if (type.getValue() != "type")
                        printWriter.println(" \n \npublic static ArrayList< " + className + "> getData () throws IOException, ParseException, IllegalAccessException, InstantiationException, ClassNotFoundException , org.json.simple.parser.ParseException{\n" +
                                "       if(data.isEmpty())\n" +
                                "        {\n" +
                                "           Load(); " +
                                "        }\n" +
                                "        return data;\n" +
                                "    }\n");


                    printWriter.println("}");
                    printWriter.close();
                } catch (IOException e) {

                }
            }


            if (type instanceof SelectAll) {

                if (((SelectAll) type).getSelectAll() == null) {

                    className = ((SelectAll) type).getVariableName();
                    File file = new File("C:\\Users\\Amina Alkurde\\IdeaProjects\\created classes\\src\\" + className + ".java");
                    FileWriter fileWriter = new FileWriter(file, false);
                    BufferedWriter buffer = new BufferedWriter(fileWriter);
                    PrintWriter printWriter = new PrintWriter(buffer);

                    if (!file.exists()) {
                        file.createNewFile();
                    }

                    printWriter.println("import java.util.ArrayList;" +
                            "\n" +
                            //    "import com.google.gson.Gson;\n" +
                            "import org.json.simple.JSONArray;\n" +
                            "import org.json.simple.JSONObject;\n" +
                            "import org.json.simple.parser.JSONParser;\n" +
                            "\n" +
                            "import java.io.*;\n" +
                            "import java.lang.reflect.*;\n" +
                            "import java.text.ParseException;\n" +
                            "import java.util.*;\n");

                    printWriter.println(keyClass + className + "{");

                    String d = "";
                    String b = className + "(){}";
                    ArrayList<String> attrColumnNameResult = new ArrayList<>();

                    ArrayList<String> columnNamesAll = new ArrayList<>();


                    String atrrTableName[] = new String[((SelectAll) type).getTableNames().size()];


                    Map<String, Type> columnsInMap = new HashMap<String, Type>();

                    for (int k = 0; k < ((SelectAll) type).getTableNames().size(); k++) {
                        // String TableName=((SelectAll) type).getTableNames().get(k);
                        for (int p = 0; p < Main.symbolTable.getDeclaredTypes().size(); p++) {
                            if (((SelectAll) type).getTableNames().get(k).equals(Main.symbolTable.getDeclaredTypes().get(p).getName())) {
                                for (int j = 0; j < ((SelectAll) type).getColumnNames().size(); j++) {
                                    for (Map.Entry<String, Type> entry : Main.symbolTable.getDeclaredTypes().get(p).getColumns().entrySet()) {

                                        if (((SelectAll) type).getColumnNames().get(k).equals("*")) {
                                            String key = "null";
                                            boolean ok = false;
                                            for (int g = 0; g < attrColumnNameResult.size(); g++) {
                                                if (attrColumnNameResult.get(g).equals(entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k)))
                                                    ok = true;
                                            }
                                            if (!ok) {
                                                attrColumnNameResult.add(entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k));
                                                if (entry.getValue().getValue().equalsIgnoreCase("string")) {
                                                    printWriter.println("private String " + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + ";");
                                                    key = "String";
                                                } else if (entry.getValue().getValue().equalsIgnoreCase("number")) {
                                                    printWriter.println("private Double " + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + ";");
                                                    key = "Double";
                                                } else {
                                                    printWriter.println("private " + entry.getValue().getValue() + " " + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + ";");
                                                    key = entry.getValue().getValue();
                                                }
                                                printWriter.println("public " + key + "  get" + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + "() {\n" +
                                                        "                            return " + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + ";\n" +
                                                        "                        }\n" +
                                                        "\n" +
                                                        "                        public void set" + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + "(" + key + " " + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + ") {\n" +
                                                        "                            this." + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + " =" + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + ";\n" +
                                                        "                        }\n"

                                                );
                                            }

                                        }
                                        if (((SelectAll) type).getColumnNames().get(j).equals(entry.getKey())) {
                                            String key = "";
                                            boolean ok = false;
                                            for (int g = 0; g < attrColumnNameResult.size(); g++) {
                                                if (attrColumnNameResult.get(g).equals(entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k)))
                                                    ok = true;
                                            }
                                            if (!ok) {
                                                attrColumnNameResult.add(entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k));
                                                if (entry.getValue().getValue().equalsIgnoreCase("string")) {
                                                    printWriter.println("private String " + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + ";");
                                                    key = "String";
                                                } else if (entry.getValue().getValue().equalsIgnoreCase("number")) {
                                                    printWriter.println("private Double " + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + ";");
                                                    key = "Double";
                                                } else {
                                                    printWriter.println("private " + entry.getValue().getValue() + " " + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + ";");
                                                    key = entry.getValue().getValue();
                                                }
                                                printWriter.println("public " + key + "  get" + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + "() {\n" +
                                                        "                            return " + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + ";\n" +
                                                        "                        }\n" +
                                                        "\n" +
                                                        "                        public void set" + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + "(" + key + " " + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + ") {\n" +
                                                        "                            this." + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + " =" + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + ";\n" +
                                                        "                        }\n"

                                                );
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    System.out.println("jajananan " + attrColumnNameResult.size());


                    printWriter.println("\npublic static ArrayList<" + className + "> data = new ArrayList<>();");
                    if (((SelectAll) type).getInnerJoins() != null) {
                        printWriter.println("\npublic static ArrayList<" + ((SelectAll) type).getInnerJoins().get(0).getTableName() + "> " + ((SelectAll) type).getInnerJoins().get(0).getTableName() + "S" + " = new ArrayList<>();");
                    }
                    for (int j = 0; j < ((SelectAll) type).getTableNames().size(); j++) {
                        printWriter.println("\npublic static ArrayList<" + ((SelectAll) type).getTableNames().get(j) + "> " + ((SelectAll) type).getTableNames().get(j) + "S" + " = new ArrayList<>();");
                    }

                    ////this is from amina
                    if (((SelectAll) type).getAggFuns() != null) {
                        for (int k = 0; k < ((SelectAll) type).getTableNames().size(); k++) {
                            for (int p = 0; p < Main.symbolTable.getDeclaredTypes().size(); p++) {
                                if (((SelectAll) type).getTableNames().get(k).equals(Main.symbolTable.getDeclaredTypes().get(p).getName())) {
                                    for (int j = 0; j < ((SelectAll) type).getAggFuns().size(); j++) {
                                        for (Map.Entry<String, Type> entry : Main.symbolTable.getDeclaredTypes().get(p).getColumns().entrySet()) {
                                            if (((SelectAll) type).getAggFuns().get(j).getAggFunParam().equalsIgnoreCase("*")) {
                                                String key = "";
                                                boolean ok = false;
                                                for (int g = 0; g < attrColumnNameResult.size(); g++) {
                                                    if (attrColumnNameResult.get(g).equals(entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k))) {
                                                        ok = true;
                                                    }
                                                }
                                                if (!ok) {
                                                    System.out.println("tat " + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k));
                                                    attrColumnNameResult.add(entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k));
                                                    if (entry.getValue().getValue().equalsIgnoreCase("string")) {
                                                        printWriter.println("private String " + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + ";");
                                                        key = "String";
                                                    } else if (entry.getValue().getValue().equalsIgnoreCase("number")) {
                                                        printWriter.println("private Double " + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + ";");
                                                        key = "Double";
                                                    } else {
                                                        printWriter.println("private " + entry.getValue().getValue() + " " + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + ";");
                                                        key = entry.getValue().getValue();
                                                    }

                                                    printWriter.println("public " + key + "  get" + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + "() {\n" +
                                                            "                            return " + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + ";\n" +
                                                            "                        }\n" +
                                                            "\n" +
                                                            "                        public void set" + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + "(" + key + " " + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + ") {\n" +
                                                            "                            this." + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + " =" + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + ";\n" +
                                                            "                        }\n"

                                                    );
                                                }

                                            }
                                            if (((SelectAll) type).getAggFuns().get(j).getAggFunParam().equals(entry.getKey())) {
                                                System.out.println("ddkkss " + ((SelectAll) type).getAggFuns().get(j).getAggFunParam());
                                                boolean ok = false;
                                                for (int g = 0; g < attrColumnNameResult.size(); g++) {
                                                    if (attrColumnNameResult.get(g).equals(entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k)))
                                                        ok = true;
                                                }

                                                if (!ok) {
                                                    attrColumnNameResult.add(entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k));
                                                    System.out.println("bab " + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k));

                                                    String key = "";
                                                    if (entry.getValue().getValue().equalsIgnoreCase("string")) {
                                                        printWriter.println("private String " + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + ";");
                                                        key = "String";
                                                    } else if (entry.getValue().getValue().equalsIgnoreCase("number")) {
                                                        printWriter.println("private Double " + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + ";");
                                                        key = "Double";
                                                    } else {
                                                        printWriter.println("private " + entry.getValue().getValue() + " " + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + ";");
                                                        key = entry.getValue().getValue();
                                                    }
                                                    printWriter.println("public " + key + "  get" + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + "() {\n" +
                                                            "                            return " + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + ";\n" +
                                                            "                        }\n" +
                                                            "\n" +
                                                            "                        public void set" + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + "(" + key + " " + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + ") {\n" +
                                                            "                            this." + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + " =" + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k) + ";\n" +
                                                            "                        }\n"

                                                    );
                                                }
                                                System.out.println("hahah " + entry.getValue().getName() + "_" + ((SelectAll) type).getTableNames().get(k));
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        System.out.println("size is rr " + attrColumnNameResult.size());
                    }
                    ///this from amina

                    if (((SelectAll) type).getAggFuns() != null) {
                        for (int g = 0; g < ((SelectAll) type).getAggFuns().size(); g++) {

                            String AggName = ((SelectAll) type).getAggFuns().get(g).getAggFunTableName();
                            String AggParameter = ((SelectAll) type).getAggFuns().get(g).getAggFunParam();
                            String VarName = ((SelectAll) type).getAggFuns().get(g).getTableAliasName();


                            for (int t = 0; t < Main.symbolTable.getDeclaredAggregationFunction().size(); t++) {

                                if (Main.symbolTable.getDeclaredAggregationFunction().get(t).getAggregationFunctionName().equalsIgnoreCase(AggName)) {

                                    String AggPath = Main.symbolTable.getDeclaredAggregationFunction().get(t).getJarPath();
                                    String AggClassName = Main.symbolTable.getDeclaredAggregationFunction().get(t).getClassName();
                                    String AggMethodName = Main.symbolTable.getDeclaredAggregationFunction().get(t).getMethodName();

                                    // add the var to class
                                    printWriter.println(" public Double " + AggName + " ;\n" +

                                            " public Double " + VarName + " ;\n ");
                                    String fromTable = ((SelectAll) type).getTableNames().get(0);
//


                                    //    String fromTable = "";
//                                for (int f = 0; f < Main.symbolTable.getColumnTypes().size(); f++) {
//                                    if (Main.symbolTable.getColumnTypes().get(f).getName().equalsIgnoreCase(AggParameter)) {
//                                        fromTable = Main.symbolTable.getColumnTypes().get(f).getFrom();
//                                    }
//                                }

                                    // add the method to class cl
                                    if (VarName != null) {
                                        if (AggParameter.equalsIgnoreCase("*")) {
                                            printWriter.println("private static void " + VarName + "_Agg (ArrayList<" + className + "> list ){ \n" +
                                                    "ArrayList<Double> my_list = new ArrayList<>();\n" +
                                                    "for(" + className + " _" + className + " : " + "list)\n" +
                                                    " my_list.add(0.0);\n" +
                                                    "Double d =(Double) Main.callAgg(my_list," + AggPath + "," + "\"" + AggName + ".jar\"," + AggClassName + "," + AggMethodName + ");\n" +
                                                    "for(" + className + " _" + className + " : " + "list)\n" +
                                                    "_" + className + "." + VarName + "=d;\n" +
                                                    "}");
                                        } else {
                                            printWriter.println("private static void " + VarName + "_Agg (ArrayList<" + className + "> list ){ \n" +
                                                    "ArrayList<Double> my_list = new ArrayList<>();\n" +
                                                    "for(" + className + " _" + className + " : " + "list)\n" +
                                                    " my_list.add(_" + className + "." + AggParameter + "_" + fromTable + ");\n" +
                                                    "Double d =(Double) Main.callAgg(my_list," + AggPath + "," + "\"" + AggName + ".jar\"," + AggClassName + "," + AggMethodName + ");\n" +
                                                    "for(" + className + " _" + className + " : " + "list)\n" +
                                                    "_" + className + "." + VarName + "=d;\n" +
                                                    "}");
                                        }
                                    }

                                }

                            }
                        }
                    }

                    printWriter.println("\n   public static " + "ArrayList<" + ((SelectAll) type).getVariableName() + ">" + " getData() throws IllegalAccessException, ParseException, IOException, InstantiationException, org.json.simple.parser.ParseException, ClassNotFoundException {\n" +
                            "     \n" +
                            "\n");

                    for (int k = 0; k < ((SelectAll) type).getTableNames().size(); k++) {
                        printWriter.println(((SelectAll) type).getTableNames().get(k) + "S" + " = " + ((SelectAll) type).getTableNames().get(k) + "." + "getData();");
                    }

                    if (((SelectAll) type).getInnerJoins() != null) {
                        printWriter.println("\n" + ((SelectAll) type).getInnerJoins().get(0).getTableName() + "S" + "= " + ((SelectAll) type).getInnerJoins().get(0).getTableName() + "." + "getData();");
                    }
                    printWriter.println("initData();");
//                printWriter.println("print();");

                    if (((SelectAll) type).getInnerJoins() != null) {
                        printWriter.println("innerJoin();");
                    }

                    if (((SelectAll) type).getWhereCondition() != null) {
                        printWriter.println("whereCondition();");
                    }
                    if (((SelectAll) type).getGroupBy() != null) {
                        printWriter.print("groupBySort();");
                    }
                    printWriter.println("                System.out.println(\"\\n\\n\\n\\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\\n\\n\\n\");\n");
                    if (((SelectAll) type).getAggFuns() != null) {
                        for (int s = 0; s < ((SelectAll) type).getAggFuns().size(); s++) {
                            String AggName = ((SelectAll) type).getAggFuns().get(s).getAggFunTableName();
                            String VarName = ((SelectAll) type).getAggFuns().get(s).getTableAliasName();
                            printWriter.println(VarName + "_Agg(data);");
                        }
                    }

                    printWriter.println("print();");


                    printWriter.println("        return data;\n }");

                    printWriter.println(" public static void initData() {");
                    for (int k = 0; k < ((SelectAll) type).getTableNames().size(); k++) {

                        printWriter.println("for(int i = 0 ;i < " + ((SelectAll) type).getTableNames().get(k) + "S" + ".size() ; i++){"
                                + "\n" +
                                "            " + ((SelectAll) type).getVariableName() + "  " + ((SelectAll) type).getVariableName() + "= new " + ((SelectAll) type).getVariableName() + "();\n");
                        for (int j = 0; j < attrColumnNameResult.size(); j++) {
                            String test[] = attrColumnNameResult.get(j).split("_");
                            if (test.length > 1) {
                                if (test[1].equals(((SelectAll) type).getTableNames().get(k)))
                                    printWriter.println("            " + ((SelectAll) type).getVariableName() + ".set" + attrColumnNameResult.get(j) + "(" + ((SelectAll) type).getTableNames().get(k) + "S" + ".get(i).get" + test[0] + "());\n");
                                else
                                    printWriter.println("            " + ((SelectAll) type).getVariableName() + ".set" + attrColumnNameResult.get(j) + "(" + ((SelectAll) type).getTableNames().get(k) + "S" + ".get(i).get" + test[0] + "_" + test[1] + "());\n");
                            }
                        }

                        printWriter.println("            data.add(" + ((SelectAll) type).getVariableName() + ");\n" +
                                "        }");
                    }
                    printWriter.println("}");


                    printWriter.println("public static void print() {\n" +
                            "for (int i = 0; i < data.size(); i++) {\n");
                    for (int j = 0; j < attrColumnNameResult.size(); j++) {
                        String test[] = attrColumnNameResult.get(j).split("_");
                        if (test.length > 1)
                            printWriter.println("System.out.println(\"" + attrColumnNameResult.get(j) + " is \"" + "+ data.get(i).get" + attrColumnNameResult.get(j) + "());");
                    }

                    if (((SelectAll) type).getAggFuns() != null) {
                        for (int s = 0; s < ((SelectAll) type).getAggFuns().size(); s++) {
                            String VarName = ((SelectAll) type).getAggFuns().get(s).getTableAliasName();
                            printWriter.println("System.out.println(\"" + VarName + " is \"" + "+ data.get(i)." + VarName + ");");
                        }
                    }


                    printWriter.println("            System.out.println(\"\\n________________________________________________________\\n\");\n");
                    printWriter.println("}}");


                    if (((SelectAll) type).getInnerJoins() != null) {
                        printWriter.println(" public static void innerJoin(){\n        data = new ArrayList<>();\n");
                        if (((SelectAll) type).getInnerJoins() != null) {
                            if (((SelectAll) type).getInnerJoins().get(0) != null) {
                                String table1 = ((SelectAll) type).getInnerJoins().get(0).getConditionTableName1() + "S";
                                String table2 = ((SelectAll) type).getInnerJoins().get(0).getConditionTableName2() + "S";
                                String column1 = ((SelectAll) type).getInnerJoins().get(0).getConditionColumnName1();
                                String column2 = ((SelectAll) type).getInnerJoins().get(0).getConditionColumnName2();
                                printWriter.println("for (int i = 0; i < " + table1 + ".size(); i++) {\n" +
                                        "            for (int j = 0; j < " + table2 + ".size(); j++) {\n");

                                if (tablesNamePrev.size() > 0)
                                    printWriter.println(
                                            "                if(" + table1 + ".get(i).get" + column1 + "_" + tablesNamePrev.get(0) + "() ==" + table2 + ".get(j).get" + column2 + "()){\n" +
                                                    "        " + ((SelectAll) type).getVariableName() + " " + ((SelectAll) type).getVariableName() + " = new " + ((SelectAll) type).getVariableName() + "();\n" + "");

                                else {
                                    printWriter.println(
                                            "                if(" + table1 + ".get(i).get" + column1 + "() ==" + table2 + ".get(j).get" + column2 + "()){\n" +
                                                    "        " + ((SelectAll) type).getVariableName() + " " + ((SelectAll) type).getVariableName() + " = new " + ((SelectAll) type).getVariableName() + "();\n" + "");

                                }

                                for (int j = 0; j < attrColumnNameResult.size(); j++) {
                                    String test[] = attrColumnNameResult.get(j).split("_");
                                    if (test[1] != null) {
                                        if ((test[1] + "S").equals(table1)) {
                                            printWriter.println(
                                                    "                    " + ((SelectAll) type).getVariableName() + ".set" + attrColumnNameResult.get(j) + "(" + table1 + ".get(i).get" + test[0] + "());\n"
                                            );
                                        } else if (!(test[1] + "S").equals(table1) && !(test[1] + "S").equals(table2)) {
                                            printWriter.println(
                                                    "                    " + ((SelectAll) type).getVariableName() + ".set" + attrColumnNameResult.get(j) + "(" + table1 + ".get(j).get" + test[0] + "_" + test[1] + "());\n"
                                            );
                                        } else if ((test[1] + "S").equals(table2)) {
                                            printWriter.println(
                                                    "                    " + ((SelectAll) type).getVariableName() + ".set" + attrColumnNameResult.get(j) + "(" + table2 + ".get(j).get" + test[0] + "());\n"
                                            );
                                        }
                                    }
                                }
                                printWriter.println(
                                        "                    data.add(" + ((SelectAll) type).getVariableName() + ");        " +
                                                "}\n" +
                                                "            }\n" +
                                                "        }");
                            }
                        }

                        printWriter.println("}");
                    }

                    if (((SelectAll) type).getWhereCondition() != null) {

                        printWriter.println("public static void whereCondition(){\n" +
                                "    ArrayList<" + ((SelectAll) type).getVariableName() + "> dataTest = new ArrayList<>();\n" +
                                "        ArrayList<" + ((SelectAll) type).getVariableName() + "> whereArray = new ArrayList<>();\n   ");

                        WhereCondition whereCondition = ((SelectAll) type).getWhereCondition();

                        if (whereCondition.getOperation().equals(">") || whereCondition.getOperation().equals("<") || whereCondition.getOperation().equals("=") || whereCondition.getOperation().equals(">=") || whereCondition.getOperation().equals("<=")) {
//                        if (whereCondition.getKey() == null) {
                            for (int j = 0; j < ((SelectAll) type).getTableNames().size(); j++) {
                                printWriter.println(" for (int i = 0; i < " + ((SelectAll) type).getTableNames().get(j) + "S.size(); i++) {");
                                if (whereCondition.getWhereNameCondition1().getTableName() != null && !whereCondition.getWhereNameCondition1().getTableName().equals(((SelectAll) type).getTableNames().get(j))) {
                                    if (whereCondition.getOperation().equalsIgnoreCase("=")) {
                                        printWriter.println("if(");
                                        printWriter.print(((SelectAll) type).getTableNames().get(j)
                                                + "S.get(i).get" + whereCondition.getWhereNameCondition1().getTableName()
                                                + "().get" + whereCondition.getWhereNameCondition1().getColName() + "() "
                                                + "==" + " " + whereCondition.getWhereNameCondition2().get(0).getColName() + "");
                                        if (whereCondition.getKey() != null) {
                                            String key = "";
                                            if (whereCondition.getKey().equalsIgnoreCase("and")) key = " && ";
                                            else if (whereCondition.getKey().equalsIgnoreCase("or")) key = " || ";
                                            printWriter.print(" " + key + " ");
                                            if (whereCondition.getWhereCondition().getOperation().equalsIgnoreCase("=")) {
                                                printWriter.print(((SelectAll) type).getTableNames().get(j)
                                                        + "S.get(i).get" + whereCondition.getWhereCondition().getWhereNameCondition1().getColName() + "() "
                                                        + "==" + " " + whereCondition.getWhereCondition().getWhereNameCondition2().get(0).getColName() + "");
                                            } else {
                                                printWriter.print(((SelectAll) type).getTableNames().get(j)
                                                        + "S.get(i).get" + whereCondition.getWhereCondition().getWhereNameCondition1().getColName() + "() "
                                                        + whereCondition.getWhereCondition().getOperation() + " " + whereCondition.getWhereCondition().getWhereNameCondition2().get(0).getColName() + "");
                                            }

                                        }

                                    } else {
                                        printWriter.println("if(");
                                        printWriter.print(((SelectAll) type).getTableNames().get(j) + "S.get(i).get" + whereCondition.getWhereNameCondition1().getTableName() + "().get" + whereCondition.getWhereNameCondition1().getColName() + "() " + whereCondition.getOperation() + " " + whereCondition.getWhereNameCondition2().get(0).getColName());


//                                        printWriter.print("){");
//                                        printWriter.print("                      " + ((SelectAll) type).getVariableName() + " " + ((SelectAll) type).getVariableName() + "= new " + ((SelectAll) type).getVariableName() + "();" +
//                                                "");
                                    }
                                    printWriter.print("){");
                                    printWriter.print("                      " + ((SelectAll) type).getVariableName() + " " + ((SelectAll) type).getVariableName() + "= new " + ((SelectAll) type).getVariableName() + "();" +
                                            "");

                                } else {
                                    if (whereCondition.getOperation().equalsIgnoreCase("=")) {
                                        printWriter.println("if(");
                                        printWriter.print(((SelectAll) type).getTableNames().get(j) + "S.get(i).get" + whereCondition.getWhereNameCondition1().getColName() + "() " + "==" + " " + whereCondition.getWhereNameCondition2().get(0).getColName());

                                        if (whereCondition.getKey() != null) {
                                            String key = "";
                                            if (whereCondition.getKey().equalsIgnoreCase("and")) key = " && ";
                                            else if (whereCondition.getKey().equalsIgnoreCase("or")) key = " || ";
                                            printWriter.print(" " + key + " ");
                                            if (whereCondition.getWhereCondition().getOperation().equalsIgnoreCase("=")) {
                                                printWriter.print(((SelectAll) type).getTableNames().get(j)
                                                        + "S.get(i).get" + whereCondition.getWhereCondition().getWhereNameCondition1().getColName() + "() "
                                                        + "==" + " " + whereCondition.getWhereCondition().getWhereNameCondition2().get(0).getColName() + "");
                                            } else {
                                                printWriter.print(((SelectAll) type).getTableNames().get(j)
                                                        + "S.get(i).get" + whereCondition.getWhereCondition().getWhereNameCondition1().getColName() + "() "
                                                        + whereCondition.getWhereCondition().getOperation() + " " + whereCondition.getWhereCondition().getWhereNameCondition2().get(0).getColName() + "");
                                            }

                                        }

                                        printWriter.print("){");
                                        printWriter.print("                      " + ((SelectAll) type).getVariableName() + " " + ((SelectAll) type).getVariableName() + "= new " + ((SelectAll) type).getVariableName() + "();" +
                                                "");
                                    } else {
                                        printWriter.println("if(");
                                        printWriter.print(((SelectAll) type).getTableNames().get(j) + "S.get(i).get" + whereCondition.getWhereNameCondition1().getColName() + "() " + whereCondition.getOperation() + " " + whereCondition.getWhereNameCondition2().get(0).getColName());

                                        if (whereCondition.getKey() != null) {
                                            String key = "";
                                            if (whereCondition.getKey().equalsIgnoreCase("and")) key = " && ";
                                            else if (whereCondition.getKey().equalsIgnoreCase("or")) key = " || ";
                                            printWriter.print(" " + key + " ");
                                            if (whereCondition.getWhereCondition().getOperation().equalsIgnoreCase("=")) {
                                                printWriter.print(((SelectAll) type).getTableNames().get(j)
                                                        + "S.get(i).get" + whereCondition.getWhereCondition().getWhereNameCondition1().getColName() + "() "
                                                        + "==" + " " + whereCondition.getWhereCondition().getWhereNameCondition2().get(0).getColName() + "");
                                            } else {
                                                printWriter.print(((SelectAll) type).getTableNames().get(j)
                                                        + "S.get(i).get" + whereCondition.getWhereCondition().getWhereNameCondition1().getColName() + "() "
                                                        + whereCondition.getWhereCondition().getOperation() + " " + whereCondition.getWhereCondition().getWhereNameCondition2().get(0).getColName() + "");
                                            }

                                        }

                                        printWriter.print("){");
                                        printWriter.print(
                                                "                      " + ((SelectAll) type).getVariableName() + " " + ((SelectAll) type).getVariableName() + "= new " + ((SelectAll) type).getVariableName() + "();" +
                                                        "");
                                    }
                                }

                                for (int h = 0; h < attrColumnNameResult.size(); h++) {
                                    String test[] = attrColumnNameResult.get(h).split("_");
                                    if (test.length > 1) {
                                        if (test[1].equals(((SelectAll) type).getTableNames().get(j))) {
                                            printWriter.println("            " + ((SelectAll) type).getVariableName() + ".set" + attrColumnNameResult.get(h) + "(" + ((SelectAll) type).getTableNames().get(j) + "S" + ".get(i).get" + test[0] + "());\n");
                                        } else if (test[2].equals(((SelectAll) type).getTableNames().get(j)))
                                            printWriter.println("            " + ((SelectAll) type).getVariableName() + ".set" + attrColumnNameResult.get(h) + "(" + ((SelectAll) type).getTableNames().get(j) + "S" + ".get(i).get" + test[0] + "_" + test[1] + "());\n");
                                    }
                                }


                                printWriter.println("                whereArray.add(" + ((SelectAll) type).getVariableName() + ");\n");

                                printWriter.println("}}");

                                printWriter.println(" for (int i = 0; i < whereArray.size(); i++) {\n" +
                                        "            for (int j = 0; j < data.size(); j++) {");

                                if (attrColumnNameResult.size() > 0) {
                                    printWriter.print("if(");

                                    String test1[] = attrColumnNameResult.get(0).split("_");

                                    if (test1.length > 1) {
                                        if (test1[1].equals(((SelectAll) type).getTableNames().get(j))) {
                                            printWriter.print("             whereArray.get(i).get" + attrColumnNameResult.get(0) + "()" + ".equals( " + "data.get(j).get" + attrColumnNameResult.get(0) + "())  ");
                                        } else if (test1[2].equals(((SelectAll) type).getTableNames().get(j))) {
                                            printWriter.print("             whereArray.get(i).get" + attrColumnNameResult.get(0) + "()" + ".equals( " + "data.get(j).get" + attrColumnNameResult.get(0) + "())  ");

                                        }
                                    }
                                    for (int h = 1; h < attrColumnNameResult.size(); h++) {
                                        String test[] = attrColumnNameResult.get(h).split("_");

                                        if (test.length > 1) {
                                            if (test[1].equals(((SelectAll) type).getTableNames().get(j))) {
                                                printWriter.print("            && whereArray.get(i).get" + attrColumnNameResult.get(h) + "()" + ".equals( " + "data.get(j).get" + attrColumnNameResult.get(h) + "()) ");
                                            } else if (test[2].equals(((SelectAll) type).getTableNames().get(j))) {
                                                printWriter.print("             &&whereArray.get(i).get" + attrColumnNameResult.get(h) + "()" + ".equals( " + "data.get(j).get" + attrColumnNameResult.get(h) + "())  ");

                                            }
                                        }
                                    }


                                    printWriter.print("){\n");
                                }
                                printWriter.println("dataTest.add(whereArray.get(i));\n" +
                                        "                    data.remove(j);\n" +
                                        "                }}\n" +
                                        "            }\n" +
                                        "        data = dataTest;" +
                                        "\n }");


                            }
//                        }
//                        else if (whereCondition.getKey() != null) {
//                            String key = "";
//                            if (whereCondition.getKey().equalsIgnoreCase("and")) key = " && ";
//                            else if (whereCondition.getKey().equalsIgnoreCase("or")) key = " || ";
//
////                            if ()
//
//                        }


                        }

                        if (whereCondition.getOperation().equalsIgnoreCase("in")) {
                            printWriter.println(" ArrayList<String> number = new ArrayList<>();\n");

                            for (int j = 0; j < whereCondition.getWhereNameCondition2().size(); j++) {
                                String numberD = whereCondition.getWhereNameCondition2().get(j).getColName();
                                printWriter.println("        number.add(" + numberD + ");\n");
                            }

                            for (int j = 0; j < ((SelectAll) type).getTableNames().size(); j++) {
                                printWriter.println("for(int i = 0 ;i < " + ((SelectAll) type).getTableNames().get(j) + "S.size() ;i++){\n");

                                if (whereCondition.getWhereNameCondition1().getDataBaseName() != null) {
                                    printWriter.println("            if(number.contains(" + ((SelectAll) type).getTableNames().get(j) + "S.get(i).get" + whereCondition.getWhereNameCondition1().getDataBaseName() + "().get" + whereCondition.getWhereNameCondition1().getTableName() + "().get" + whereCondition.getWhereNameCondition1().getColName() + "())){\n");

                                } else if (whereCondition.getWhereNameCondition1().getTableName() != null && !whereCondition.getWhereNameCondition1().getTableName().equals(((SelectAll) type).getTableNames().get(j)))
                                    printWriter.println("            if(number.contains(" + ((SelectAll) type).getTableNames().get(j) + "S.get(i).get" + whereCondition.getWhereNameCondition1().getTableName() + "().get" + whereCondition.getWhereNameCondition1().getColName() + "())){\n");
                                else
                                    printWriter.println("            if(number.contains(" + ((SelectAll) type).getTableNames().get(j) + "S.get(i).get" + whereCondition.getWhereNameCondition1().getColName() + "())){\n");

                                printWriter.println("                      " + ((SelectAll) type).getVariableName() + " " + ((SelectAll) type).getVariableName() + "= new " + ((SelectAll) type).getVariableName() + "();");

                                for (int h = 0; h < attrColumnNameResult.size(); h++) {
                                    String test[] = attrColumnNameResult.get(h).split("_");
                                    if (test.length > 1) {
                                        if (test[1].equals(((SelectAll) type).getTableNames().get(j))) {
                                            printWriter.println("            " + ((SelectAll) type).getVariableName() + ".set" + attrColumnNameResult.get(h) + "(" + ((SelectAll) type).getTableNames().get(j) + "S" + ".get(i).get" + test[0] + "());\n");
                                        } else
                                            printWriter.println("            " + ((SelectAll) type).getVariableName() + ".set" + attrColumnNameResult.get(h) + "(" + ((SelectAll) type).getTableNames().get(j) + "S" + ".get(i).get" + test[0] + "_" + test[1] + "());\n");
                                    }
                                }


                                printWriter.println("                whereArray.add(" + ((SelectAll) type).getVariableName() + ");\n");

                                printWriter.println("}}");

                                printWriter.println(" for (int i = 0; i < whereArray.size(); i++) {\n" +
                                        "            for (int j = 0; j < data.size(); j++) {");


                                if (attrColumnNameResult.size() > 0) {
                                    printWriter.print("if(");
                                    String test1[] = attrColumnNameResult.get(0).split("_");

                                    if (test1[1].equals(((SelectAll) type).getTableNames().get(j))) {
                                        printWriter.print("             whereArray.get(i).get" + attrColumnNameResult.get(0) + "()" + ".equals( " + "data.get(j).get" + attrColumnNameResult.get(0) + "())  ");
                                    } else if (test1[2].equals(((SelectAll) type).getTableNames().get(j))) {
                                        printWriter.print("             whereArray.get(i).get" + attrColumnNameResult.get(0) + "()" + ".equals( " + "data.get(j).get" + attrColumnNameResult.get(0) + "())  ");

                                    }
                                    for (int h = 1; h < attrColumnNameResult.size(); h++) {
                                        String test[] = attrColumnNameResult.get(h).split("_");

                                        if (test.length > 1) {
                                            if (test[1].equals(((SelectAll) type).getTableNames().get(j))) {
                                                printWriter.print("            && whereArray.get(i).get" + attrColumnNameResult.get(h) + "()" + ".equals( " + "data.get(j).get" + attrColumnNameResult.get(h) + "()) ");
                                            } else if (test[2].equals(((SelectAll) type).getTableNames().get(j))) {
                                                printWriter.print("             &&whereArray.get(i).get" + attrColumnNameResult.get(h) + "()" + ".equals( " + "data.get(j).get" + attrColumnNameResult.get(h) + "())  ");

                                            }
                                        }
                                    }

                                    printWriter.print("){\n");
                                }
                                printWriter.println("dataTest.add(whereArray.get(i));\n" +
                                        "                    data.remove(j);\n" +
                                        "                }}\n" +
                                        "            }\n" +
                                        "        data = dataTest;" +
                                        "\n }");


//
//                                   "                System.out.println(\"yeeeeeeeeeeeees\"" +
//                                    "" +
//                                    ");\n" +
//
//
//                                    "            }\n");


                            }
                        }

                        if (whereCondition.getOperation().equalsIgnoreCase("like")) {


                            for (int j = 0; j < ((SelectAll) type).getTableNames().size(); j++) {
                                printWriter.println("for(int i = 0 ;i < " + ((SelectAll) type).getTableNames().get(j) + "S.size() ;i++){\n");

                                String temp = whereCondition.getWhereNameCondition2().get(0).getColName().substring(1, whereCondition.getWhereNameCondition2().get(0).getColName().length() - 1);
                                temp = "\"" + temp + "\"";


                                if (whereCondition.getKey() != null) {

                                    String key = "";
                                    if (whereCondition.getKey().equalsIgnoreCase("or")) key = " || ";
                                    else if (whereCondition.getKey().equalsIgnoreCase("and")) key = " && ";
                                    if (whereCondition.getWhereNameCondition1().getDataBaseName() != null) {
                                        printWriter.print("            if(" +
                                                ((SelectAll) type).getTableNames().get(j) +
                                                "S.get(i).get" + whereCondition.getWhereNameCondition1().getDataBaseName()
                                                + "().get" + whereCondition.getWhereNameCondition1().getTableName()
                                                + "().get" + whereCondition.getWhereNameCondition1().getColName()
                                                + "().equals(" + temp + ")");

                                        printWriter.print(key);
                                        String temp2 = whereCondition.getWhereCondition().getWhereNameCondition2().get(0).getColName().substring(1, whereCondition.getWhereCondition().getWhereNameCondition2().get(0).getColName().length() - 1);
                                        temp2 = "\"" + temp2 + "\"";

                                        if (whereCondition.getWhereCondition().getWhereNameCondition1().getDataBaseName() != null) {
                                            printWriter.print(((SelectAll) type).getTableNames().get(j) +
                                                    "S.get(i).get" + whereCondition.getWhereCondition().getWhereNameCondition1().getDataBaseName()
                                                    + "().get" + whereCondition.getWhereCondition().getWhereNameCondition1().getTableName()
                                                    + "().get" + whereCondition.getWhereCondition().getWhereNameCondition1().getColName()
                                                    + "().equals(" + temp2 + ")");
                                        } else if (whereCondition.getWhereCondition().getWhereNameCondition1().getTableName() != null) {
                                            printWriter.print(((SelectAll) type).getTableNames().get(j) +
                                                    "S.get(i).get" + whereCondition.getWhereCondition().getWhereNameCondition1().getTableName()
                                                    + "().get" + whereCondition.getWhereCondition().getWhereNameCondition1().getColName()
                                                    + "().equals(" + temp2 + ")");
                                        } else {
                                            printWriter.print(((SelectAll) type).getTableNames().get(j) +
                                                    "S.get(i).get" + whereCondition.getWhereCondition().getWhereNameCondition1().getColName()
                                                    + "().equals(" + temp2 + ")");
                                        }
                                        printWriter.println("){\n");

                                    } else if (whereCondition.getWhereNameCondition1().getTableName() != null && !whereCondition.getWhereNameCondition1().getTableName().equals(((SelectAll) type).getTableNames().get(j))) {
                                        {
                                            printWriter.println("            if(" + ((SelectAll) type).getTableNames().get(j)
                                                    + "S.get(i).get" + whereCondition.getWhereNameCondition1().getTableName()
                                                    + "().get" + whereCondition.getWhereNameCondition1().getColName()
                                                    + "().startsWith(" + temp + ")");
                                            printWriter.print(key);

                                            String temp2 = whereCondition.getWhereCondition().getWhereNameCondition2().get(0).getColName().substring(1, whereCondition.getWhereCondition().getWhereNameCondition2().get(0).getColName().length() - 1);
                                            temp2 = "\"" + temp2 + "\"";

                                            if (whereCondition.getWhereCondition().getWhereNameCondition1().getDataBaseName() != null) {
                                                printWriter.print(((SelectAll) type).getTableNames().get(j) +
                                                        "S.get(i).get" + whereCondition.getWhereCondition().getWhereNameCondition1().getDataBaseName()
                                                        + "().get" + whereCondition.getWhereCondition().getWhereNameCondition1().getTableName()
                                                        + "().get" + whereCondition.getWhereCondition().getWhereNameCondition1().getColName()
                                                        + "().equals(" + temp2 + ")");
                                            } else if (whereCondition.getWhereCondition().getWhereNameCondition1().getTableName() != null) {
                                                printWriter.print(((SelectAll) type).getTableNames().get(j) +
                                                        "S.get(i).get" + whereCondition.getWhereCondition().getWhereNameCondition1().getTableName()
                                                        + "().get" + whereCondition.getWhereCondition().getWhereNameCondition1().getColName()
                                                        + "().equals(" + temp2 + ")");
                                            } else {
                                                System.out.println("kajsjhdhd ");

                                                printWriter.print(((SelectAll) type).getTableNames().get(j) +
                                                        "S.get(i).get" + whereCondition.getWhereCondition().getWhereNameCondition1().getColName()
                                                        + "().equals(" + temp2 + ")");
                                            }
                                            printWriter.print("){\n");


                                        }
                                    } else {
                                        printWriter.println("            if(" + ((SelectAll) type).getTableNames().get(j)
                                                + "S.get(i).get" + whereCondition.getWhereNameCondition1().getColName()
                                                + "().startsWith(" + temp + ")");
                                        printWriter.print(key);
                                        String temp2 = whereCondition.getWhereCondition().getWhereNameCondition2().get(0).getColName().substring(1, whereCondition.getWhereCondition().getWhereNameCondition2().get(0).getColName().length() - 1);
                                        temp2 = "\"" + temp2 + "\"";

                                        if (whereCondition.getWhereCondition().getWhereNameCondition1().getDataBaseName() != null) {
                                            printWriter.print(((SelectAll) type).getTableNames().get(j) +
                                                    "S.get(i).get" + whereCondition.getWhereCondition().getWhereNameCondition1().getDataBaseName()
                                                    + "().get" + whereCondition.getWhereCondition().getWhereNameCondition1().getTableName()
                                                    + "().get" + whereCondition.getWhereCondition().getWhereNameCondition1().getColName()
                                                    + "().equals(" + temp2 + ")");
                                        } else if (whereCondition.getWhereCondition().getWhereNameCondition1().getTableName() != null) {
                                            printWriter.print(((SelectAll) type).getTableNames().get(j) +
                                                    "S.get(i).get" + whereCondition.getWhereCondition().getWhereNameCondition1().getTableName()
                                                    + "().get" + whereCondition.getWhereCondition().getWhereNameCondition1().getColName()
                                                    + "().equals(" + temp2 + ")");
                                        } else {
                                            printWriter.print(((SelectAll) type).getTableNames().get(j) +
                                                    "S.get(i).get" + whereCondition.getWhereCondition().getWhereNameCondition1().getColName()
                                                    + "().equals(" + temp2 + ")");
                                        }
                                        printWriter.print("){\n");
                                    }
                                }

                                if (whereCondition.getKey() == null) {
                                    if (whereCondition.getWhereNameCondition1().getDataBaseName() != null) {
                                        printWriter.println("            if(" + ((SelectAll) type).getTableNames().get(j) + "S.get(i).get" + whereCondition.getWhereNameCondition1().getDataBaseName() + "().get" + whereCondition.getWhereNameCondition1().getTableName() + "().get" + whereCondition.getWhereNameCondition1().getColName() + "().startsWith(" + temp + ")){\n");

                                    } else if (whereCondition.getWhereNameCondition1().getTableName() != null && !whereCondition.getWhereNameCondition1().getTableName().equals(((SelectAll) type).getTableNames().get(j)))
                                        printWriter.println("            if(" + ((SelectAll) type).getTableNames().get(j) + "S.get(i).get" + whereCondition.getWhereNameCondition1().getTableName() + "().get" + whereCondition.getWhereNameCondition1().getColName() + "().startsWith(" + temp + ")){\n");
                                    else
                                        printWriter.println("            if(" + ((SelectAll) type).getTableNames().get(j) + "S.get(i).get" + whereCondition.getWhereNameCondition1().getColName() + "().startsWith(" + temp + ")){\n");
                                }
                                printWriter.println("                      " + ((SelectAll) type).getVariableName() + " " + ((SelectAll) type).getVariableName() + "= new " + ((SelectAll) type).getVariableName() + "();");

                                for (int h = 0; h < attrColumnNameResult.size(); h++) {
                                    String test[] = attrColumnNameResult.get(h).split("_");

                                    if (test.length > 1) {
                                        if (test[1].equals(((SelectAll) type).getTableNames().get(j))) {
                                            printWriter.println("            " + ((SelectAll) type).getVariableName() + ".set" + attrColumnNameResult.get(h) + "(" + ((SelectAll) type).getTableNames().get(j) + "S" + ".get(i).get" + test[0] + "());\n");
                                        } else if (test[2].equals(((SelectAll) type).getTableNames().get(j)))
                                            printWriter.println("            " + ((SelectAll) type).getVariableName() + ".set" + attrColumnNameResult.get(h) + "(" + ((SelectAll) type).getTableNames().get(j) + "S" + ".get(i).get" + test[0] + "_" + test[1] + "());\n");
                                    }
                                }


                                printWriter.println("                whereArray.add(" + ((SelectAll) type).getVariableName() + ");\n");

                                printWriter.println("}}");

                                printWriter.println(" for (int i = 0; i < whereArray.size(); i++) {\n" +
                                        "            for (int j = 0; j < data.size(); j++) {");

                                if (attrColumnNameResult.size() > 0) {
                                    printWriter.print("if(");
                                    String test1[] = attrColumnNameResult.get(0).split("_");

                                    if (test1.length > 1) {
                                        if (test1[1].equals(((SelectAll) type).getTableNames().get(j))) {
                                            printWriter.print("             whereArray.get(i).get" + attrColumnNameResult.get(0) + "()" + ".equals( " + "data.get(j).get" + attrColumnNameResult.get(0) + "())  ");
                                        } else if (test1[2].equals(((SelectAll) type).getTableNames().get(j))) {
                                            printWriter.print("             whereArray.get(i).get" + attrColumnNameResult.get(0) + "()" + ".equals( " + "data.get(j).get" + attrColumnNameResult.get(0) + "())  ");
                                        }
                                    }
                                    for (int h = 1; h < attrColumnNameResult.size(); h++) {
                                        String test[] = attrColumnNameResult.get(h).split("_");
                                        if (test.length > 1) {
                                            if (test[1].equals(((SelectAll) type).getTableNames().get(j))) {
                                                printWriter.print("            && whereArray.get(i).get" + attrColumnNameResult.get(h) + "()" + ".equals( " + "data.get(j).get" + attrColumnNameResult.get(h) + "()) ");
                                            } else if (test[2].equals(((SelectAll) type).getTableNames().get(j))) {
                                                printWriter.print("             &&whereArray.get(i).get" + attrColumnNameResult.get(h) + "()" + ".equals( " + "data.get(j).get" + attrColumnNameResult.get(h) + "())  ");

                                            }
                                        }
                                    }
                                    printWriter.print("){\n");
                                }
                                printWriter.println("dataTest.add(whereArray.get(i));\n" +
                                        "                    data.remove(j);\n" +
                                        "                }}\n" +
                                        "            }\n" +
                                        "        data = dataTest;" +
                                        "\n }");


                            }


                        }

                    }


                    if (((SelectAll) type).getGroupBy() != null) {

                        for (int j = 0; j < ((SelectAll) type).getTableNames().size(); j++) {
                            GroupBy groupBy = ((SelectAll) type).getGroupBy();
                            String sortKey = groupBy.getGroupByColumnName();
                            printWriter.println(" public static void groupBySort(){");
                            for (int k = 0; k < attrColumnNameResult.size(); k++) {
                                String test[] = attrColumnNameResult.get(k).split("_");
                                System.out.println("test is " + test[0] + "   key is " + sortKey);
                                if (test[0].equalsIgnoreCase(sortKey)) {
                                    printWriter.println("Collections.sort(data, Comparator.comparing(" + ((SelectAll) type).getVariableName() + "::get" + attrColumnNameResult.get(k) + "));");
                                }
                            }

                            printWriter.println("}");

                        }
                    }


                    printWriter.println("}");
                    printWriter.close();
                    tablesNamePrev = ((SelectAll) type).getTableNames();
                }

            }


        }

    }

    public static void FunctionMainClass(Parse parse) {
        try {
            File file = new File("C:\\Users\\Amina Alkurde\\IdeaProjects\\created classes\\src\\Main" + ".java");
            FileWriter fileWriter = new FileWriter(file, false);
            BufferedWriter buffer = new BufferedWriter(fileWriter);
            PrintWriter printWriter = new PrintWriter(buffer);

            if (!file.exists()) {
                file.createNewFile();
            }

            printWriter.println("import java.io.*;\n" +
                    "import java.lang.reflect.InvocationTargetException;\n" +
                    "import java.lang.reflect.Method;" +
                    "import java.lang.reflect.Field;\n" +
                    "import java.lang.reflect.Modifier;\n" +
                    "import java.util.ArrayList;\n" +
                    "import java.util.HashMap;\n" +
                    "import java.util.List;\n" +
                    "import java.text.ParseException;\n" +
                    "import java.util.Map;\n" +
                    "import java.util.*;\n" +
                    "import java.net.MalformedURLException;\n" +
                    "import java.net.URL;\n" +
                    "import java.net.URLClassLoader;\n" +
                    "import java.util.stream.Collectors;\n\n");

            printWriter.println("public class Main { \n  " +
                    "         public  static ArrayList list = new ArrayList();\n" +
                    "   public static void main(String[] args)  throws ClassNotFoundException, MalformedURLException, NoSuchMethodException, InstantiationException, IllegalAccessException, ParseException, IOException, org.json.simple.parser.ParseException \n" +
                    "{\n");

            printWriter.println(variableName + ".getData();");
            printWriter.print("\n}");

            //   String load = tablesToLoad(p);
            //   printWriter.println(load);
            printWriter.println("\n\n\n public static Object callAgg(ArrayList<Double> list, String JarPath, String JarName, String ClassName, String MethodName) {\n" +
                    "\t\ttry {\n" +
                    "\n" +
                    "\t\t\tURLClassLoader myClassLoader = new URLClassLoader(\n" +
                    "\t\t\t\t\tnew URL[]{new File(JarPath + JarName).toURI().toURL()},\n" +
                    "\t\t\t\t\tMain.class.getClassLoader()\n" +
                    "\t\t\t);\n" +
                    "\n" +
                    "\t\t\tClass<?> myClass = Class.forName(ClassName, true, myClassLoader);\n" +
                    "\t\t\tMethod mySingeltonGetterMethod = myClass.getMethod(\"get\" + ClassName,\n" +
                    "\t\t\t\t\tnull);\n" +
                    "\t\t\tObject myObject = mySingeltonGetterMethod.invoke(null);\n" +
                    "\t\t\treturn myObject.getClass().getMethod(MethodName, List.class)\n" +
                    "\t\t\t\t\t.invoke(myObject, list);\n" +
                    "\t\t} catch (ClassNotFoundException | NoSuchMethodException | InvocationTargetException | IllegalAccessException | MalformedURLException e) {\n" +
                    "\t\t\te.printStackTrace();\n" +
                    "\t\t}\n" +
                    "\t\treturn null;\n" +
                    "\t}");
            printWriter.println("\n}");
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

/*
        for(int i = 0; i <data.size() ;i++){
            System.out.println("color id "+data.get(i).getid_colors());
            System.out.println("color color "+data.get(i).getcolor_colors());
            System.out.println("Unis id "+data.get(i).getid_Unis());
            System.out.println("Unis name "+data.get(i).getname_Unis());

            System.out.println("\n\n_______________________________________");
        }
    var cl = select id,color,name from colors,Unis inner join cl on colors.id = Unis.id where id > 3;


//        cl.getData();

        SortTest sort = new SortTest();
        sort.setName("amina");
        sort.setId(1);
        names.add(sort);
        sort.setName("ali");
        sort.setId(2);
        names.add(sort);

        sort.setName("emad");
        sort.setId(3);
        names.add(sort);


        Collections.sort(names, Comparator.comparing(SortTest::getName));

        names.forEach(System.out::println);



 */
