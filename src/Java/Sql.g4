grammar Sql;

parse
 : (  error | sql_stmt_list| defVariable |function )* EOF;

error: UNEXPECTED_CHAR
   {
     throw new RuntimeException("UNEXPECTED_CHAR=" + $UNEXPECTED_CHAR.text);
   }
 ;

defVariable:var_equal_stmt ;

sql_stmt_list
 : ';'* sql_stmt ( ';'+ sql_stmt )* ';'*
 ;

sql_stmt :(
                 create_table_stmt
                | factored_select_stmt
                | create_type_stmt
                |aggregationFunction
                              )
 ;

java_anystmt:
  sql_stmt_list
 |sql_stmt
 |java_stmt
 |K_CONTINUE ';'
 ;


 java_stmt:
 (call_function (';')? ) // for call func in func
 |javaIF_stmt
 |javaIFline_stmt ';'
 |javaFor_stmt
 |javaForeach_stmt
 |javaWhile_stmt
 |java_do_while
 |var_equal_stmt
 |java_switch_stmt
 |differnt_kind_of_for_increament ';' //new    done
 |print_stm  // new

 ;




var_stmt: (K_VAR)? any_name ;

//var_equal_stmt : var_stmt | (var_stmt '=' ( (javaIFline_stmt ) |sql_stmt  | expr ) ) ';' ; // new edit amina   just keep ifline
var_equal_stmt : (var_stmt ('='  (call_function  |sql_stmt  | javaIFline_stmt|expr ))? ) ';' ; // new edit amina   just keep ifline


condtion_stmt:
('(')? ('!')?  (  ( ('(')? expr (')')? ) ) (')')?  // new   done

;

body_all_stmt : ( ( '{'( java_anystmt (';') ? )* '}' ) | ( ( java_anystmt  (';') ?)  )  ) ;


if_condition:K_IF condtion_stmt;

javaIF_stmt : if_condition (body_all_stmt )  (java_else)*;
java_else :  K_ELSE  ((javaIF_stmt) | (body_all_stmt))  ;

javaIFline_stmt :condtion_stmt '?' ifline_body ;   ////   new   /// why find question mark in condition stmt????
ifline_body: ('(')? realized_condition  (')')? ':'  ('(')?  unrealized_condition (')')?;

unrealized_condition :( expr | differnt_kind_of_for_increament | complete_ifLine);
realized_condition :(expr | differnt_kind_of_for_increament | complete_ifLine);
complete_ifLine:javaIFline_stmt;



//  edit under
//
//increament_and_decrement_operator:  ((any_name | signed_number ) ('++' |'-'  ) )
//| (  ('++' |'-' ) (any_name | signed_number )) ;   ////true

///  the new  oprations are content more stateso


differnt_kind_of_for_increament :
 (any_name signal ASSIGN ('+'|'-')?)?(increament_var | discreament_var | literal_value|call_function) ;

  /* Example i++ ++i*/
 increament_var
  :(any_name PLUS PLUS)
   |(PLUS PLUS any_name)
  ;
  /* Example i-- --i*/
 discreament_var
  :(any_name MINUS )
   |(MINUS  any_name)
  ;

 signal
 :  STAR
   |DIV
   |MOD
   |PLUS
   |MINUS
 ;

javaFor_stmt :K_FOR '(' var_stmt '=' signed_number ';' (expr)';' differnt_kind_of_for_increament ')'
body_all_stmt;


javaForeach_stmt:K_FOR '(' var_stmt ':' any_name')'
                  body_all_stmt;


parameter_list :  item (',' item )*  ;
item :var_stmt ('=' expr)? ;

argument_list : ( (expr|differnt_kind_of_for_increament) (',' (expr|differnt_kind_of_for_increament) ) *  ) ;

call_function :  function_name '('(argument_list)? ')'  ;

function : function_name '(' (parameter_list)* ')'  '{' (java_stmt (';')? )* (K_RETURN  ( expr ) ';' )? '}' ;


javaWhile_stmt: K_WHILE condtion_stmt  body_all_stmt ;

java_do_while : K_DO  body_all_stmt K_WHILE condtion_stmt ';';


java_switch_stmt:K_SWITCH '(' (IDENTIFIER | NUMERIC_LITERAL|call_function) ')' '{' (case_switch)* (default_switch)? '}';
case_switch:K_CASE (any_name|signed_number) ':' (( (java_anystmt)* K_BREAK ';' ) | ( '{'(java_anystmt)* K_BREAK ';' '}') ) ;
default_switch:K_DEFAULT ':' ((java_anystmt)* |('{' (java_anystmt)*'}'));


print_stm
 :K_PRINT '('  ( expr   )*  ')' ';'+;

//create_table_stmt
//  : K_CREATE  K_TABLE ( K_IF K_NOT K_EXISTS )?
//   ( database_name '.' )? table_name
//   ( '(' column_def ( ',' table_constraint | ',' column_def )* ')'
//)
// ;

factored_select_stmt               ///// this is same selectStmt same same
 :  select_core
   ( K_ORDER K_BY ordering_term ( ',' ordering_term )* )?
   ( K_LIMIT expr ( ( K_OFFSET | ',' ) expr )? )?
 ;

select_core
 : K_SELECT ( K_DISTINCT | K_ALL )? result_column ( ',' result_column )*
    ( sel_table_AM )?
      (sel_exp_where_AM)?
      ( sel_exp_groupBy_AM ( sel_exp_having_AM)? )?
 | K_VALUES '(' expr ( ',' expr )* ')' ( ',' '(' expr ( ',' expr )* ')' )*
 ;

 ordering_term
  : expr  ( K_ASC | K_DESC )?
  ;

result_column
 : star
 | table_name '.' '*'
 | expr ( K_AS? column_alias )?
 ;
 star : '*';

sel_table_AM:K_FROM ( table_or_subquery ( ',' table_or_subquery )* | join_clause ) ;

sel_exp_where_AM: K_WHERE expr ;
sel_exp_groupBy_AM:K_GROUP K_BY expr ( ',' expr )*;
sel_exp_having_AM: K_HAVING expr;



table_or_subquery  //?
 : table_or_sub_one
 | table_or_sub_two
 | table_or_sub_three
 ;
table_or_sub_one:( database_name '.' )? table_name ( K_AS? table_alias )?
                    ( K_INDEXED K_BY index_name
                    | K_NOT K_INDEXED )?;


table_or_sub_two:'(' ( table_or_subquery ( ',' table_or_subquery )*
                        | join_clause )
                    ')' ( K_AS? table_alias )?;

table_or_sub_three:'(' select_stmt ')' ( K_AS? table_alias )?;

 ///tableAlias anyName

join_clause
 : ( table_or_subquery ( ',' table_or_subquery )*) (join)*
 ;

join : join_operator table_or_subquery join_constraint ;


select_stmt
  : select_or_values
  ( K_ORDER K_BY ordering_term ( ',' ordering_term )* )?
   ( K_LIMIT expr ( ( K_OFFSET | ',' ) expr )? )?
 ;

select_or_values
 : K_SELECT ( K_DISTINCT | K_ALL )? result_column ( ',' result_column )*
   ( sel_table_AM )?
   (sel_exp_where_AM)?
   ( sel_exp_groupBy_AM ( sel_exp_having_AM)? )?
 | K_VALUES '(' expr ( ',' expr )* ')' ( ',' '(' expr ( ',' expr )* ')' )*
 ;



column_def
 : column_name ( column_constraint | type_name )*
 ;

type_name
 : name ( '(' signed_number (any_name)? ')'
         | '(' signed_number (any_name)? ',' signed_number (any_name)? ')' )?
 ;

column_constraint
 : ( K_CONSTRAINT name )?
   ( column_constraint_primary_key
   | column_constraint_foreign_key
   | column_constraint_not_null
   | column_constraint_null
   | K_UNIQUE
   | K_CHECK '(' expr ')'
   | column_default
   | K_COLLATE collation_name
   )
 ;


column_constraint_primary_key
 : K_PRIMARY K_KEY ( K_ASC | K_DESC )?  K_AUTOINCREMENT?
 ;
 /*example column_constraint_primary_key :
  primary key
  primary key  asc
  primary key desc
  primary key  asc autoincrement
  primary key desc autoincrement
  primary autoincrement
  */

column_constraint_foreign_key
 : foreign_key_clause
 ;


column_constraint_not_null
 : K_NOT K_NULL
 ;
 /* not null */
column_constraint_null
 : K_NULL
 ;
/* null */

column_default
 : K_DEFAULT (column_default_value | '(' expr ')' | K_NEXTVAL '(' expr ')' | any_name )  ( '::' any_name+ )?
 ;
/* default 22 anyname
   default name anyname

  default (n>9) anyname
   default nextval (n>9) anyname

    default anyname anyname

     default    :: anyname
*/
column_default_value
 : ( signed_number | literal_value )
 ;


expr
   : literal_value //
   | database_exp //
   | operator_exp
   | expr '||' expr
   | expr ( '*' | '/' | '%' ) expr
   | expr (  '+' | '-'  ) expr //
   | expr ( '<<' | '>>' | '&' | '|' ) expr
   | expr ( '<' | '<=' | '>' | '>=' ) expr
   | expr ( '=' | '==' | '!=' | '<>' | K_IS | K_IS K_NOT | K_IN | K_LIKE | K_GLOB | K_MATCH | K_REGEXP ) expr
   | expr K_AND expr
   | expr K_OR expr
   | function_exp
   | '(' expr ')'
   | expr K_NOT? K_IN second_sel_all
   | sel_expr
   | K_TRUE //
   | K_FALSE //

   ;



database_exp:( ( database_name '.' )? table_name '.' )? column_name;
operator_exp:unary_operator expr;
function_exp:function_name '(' ( K_DISTINCT? expr ( ',' expr )* | star )? ')';
second_sel_all:( '(' ( select_stmt
                                 | expr ( ',' expr )*
                                 )?
                             ')'
                           | ( database_name '.' )? table_name );
sel_expr:( ( K_NOT )? K_EXISTS )? '(' select_stmt ')';


foreign_key_clause
 : K_REFERENCES ( database_name '.' )? foreign_table ( '(' fk_target_column_name ( ',' fk_target_column_name )* ')' )?
   ( ( K_ON ( K_DELETE | K_UPDATE ) ( K_SET K_NULL
                                    | K_SET K_DEFAULT
                                    | K_CASCADE
                                    | K_RESTRICT
                                    | K_NO K_ACTION )
     | K_MATCH name
     )
   )*
   ( K_NOT? K_DEFERRABLE ( K_INITIALLY K_DEFERRED | K_INITIALLY K_IMMEDIATE )? K_ENABLE? )?
 ;


fk_target_column_name
 : name
 ;

indexed_column
 : column_name ( K_COLLATE collation_name )? ( K_ASC | K_DESC )?
 ;

table_constraint
 : ( K_CONSTRAINT name )?
   ( table_constraint_primary_key
   | table_constraint_key
   | table_constraint_unique
   | K_CHECK '(' expr ')'
   | table_constraint_foreign_key
   )
 ;



table_constraint_primary_key
 : K_PRIMARY K_KEY '(' indexed_column ( ',' indexed_column )* ')'
 ;
/* Exmple table_constraint_primary_key :

constraint toto primary key (col_name asc)
 constraint toto primary key  (col_name desc )
 constraint toto primary key  (col_name collate coll_name)
 */
table_constraint_foreign_key
 : K_FOREIGN K_KEY '(' fk_origin_column_name ( ',' fk_origin_column_name )* ')' foreign_key_clause
 ;
/*example table_constraint_foreign_key :
foreign key (fk_col_name) references data_name . forg_table ( fr_col_name )
foreign key (fk_col_name1 ,fk_col_name2) references data_name . forg_table ( fr_col_name1 ,fr_col_name2 )
*/
table_constraint_unique
 : K_UNIQUE K_KEY? name? '(' indexed_column ( ',' indexed_column )* ')'
 ;
 /*example table_constraint_unique :

  unique key name (col_name asc)
   unique key name (col_name desc)
    unique key name (col_name collate coll_name)

  */

table_constraint_key
 : K_KEY name? '(' indexed_column ( ',' indexed_column )* ')'
 ;
 /* example table_constraint_key :

  key name (col_name asc)
   key name (col_name desc)
    key name (col_name collate coll_name))
 */

fk_origin_column_name
 : column_name
 ;



join_operator
 : ','
 |  ( K_LEFT K_OUTER? | K_INNER  )? K_JOIN
 ;

join_constraint
    : ( K_ON expr)?
 ;



signed_number
 : ( ( '+' | '-' )? NUMERIC_LITERAL | '*' )
 ;
///////////////////////////////////////////
create_table_stmt
 : K_CREATE  K_TABLE table_name
   '(' column_def (  ',' column_def )*
    ')'
   ( K_TYPE '=' type
      K_PATH '=' path)?
   ';'
;
type : IDENTIFIER;
path : IDENTIFIER  ('/' IDENTIFIER )* ;
///////////////////////////////////////////////////

 ///////////////////////////////////

 create_type_stmt :K_CREATE K_TYPE name_type '(' (( column_def_type (',' column_def_type )*)?) ')' ';';
 column_def_type : column_name any_name;
 name_type :any_name;
 /////////////////////////////////////////////////

 /////////////////////////////////////////////////
 jarPath:  any_name;
 className : any_name ;
 methodName : any_name ;
 returnType : any_name | NUMERIC_LITERAL | K_TRUE | K_FALSE  ;
 parameter_AggFunction : paramete_array ;


//arrya_def: var_stmt '[' ']'  '=' paramete_array;

paramete_array:'['(  any_name_item | number_item )? ']'   (';')?;
any_name_item :(any_name ) (',' (any_name ))* ;
number_item: ( signed_number) (',' ( signed_number))* ;

 aggregationFunction
 :
 K_CREATE K_AGGREGATION_FUNCTION function_name '(' jarPath ',' className ',' methodName ',' returnType ','  parameter_AggFunction ')'
 ;

//////////////////////////////////////////////////////

literal_value //?
 : NUMERIC_LITERAL
 | STRING_LITERAL
 | BLOB_LITERAL
 | K_NULL
 | K_CURRENT_TIME
 | K_CURRENT_DATE
 | K_CURRENT_TIMESTAMP
 ;

unary_operator
 : '-'
 | '+'
 | '~'
 | K_NOT
 ;
  opration :
  '>'
  |'<'
  |'='
  ;


module_argument // TODO check what exactly is permitted here
 : expr
 | column_def
 ;

column_alias
 : IDENTIFIER
 | STRING_LITERAL
 ;



keyword
 : K_ABORT
 | K_ACTION
 | K_ADD
 | K_AFTER
 | K_ALL
 | K_ALTER
 | K_ANALYZE
 | K_AND
 | K_AS
 | K_ASC
 | K_ATTACH
 | K_AUTOINCREMENT
 | K_BEFORE
 | K_BEGIN
 | K_BETWEEN
 | K_BY
 | K_BREAK
 | K_CASCADE
 | K_CASE
 | K_CAST
 | K_CHECK
 | K_COLLATE
 | K_COLUMN
 | K_COMMIT
 | K_CONFLICT
 | K_CONSTRAINT
 | K_CONTINUE
 | K_CREATE
 | K_CROSS
 | K_CURRENT_DATE
 | K_CURRENT_TIME
 | K_CURRENT_TIMESTAMP
 | K_DATABASE
 | K_DEFAULT
 | K_DEFERRABLE
 | K_DEFERRED
 | K_DELETE
 | K_DESC
 | K_DETACH
 | K_DISTINCT
 | K_DO
 | K_DROP
 | K_EACH
 | K_ELSE
 | K_END
 | K_ENABLE
 | K_ESCAPE
 | K_EXCEPT
 | K_EXCLUSIVE
 | K_EXISTS
 | K_EXPLAIN
 | K_FAIL
 | K_FOR
 | K_FOREIGN
 | K_FROM
 | K_FULL
 | K_GLOB
 | K_GROUP
 | K_HAVING
 | K_IF
 | K_IGNORE
 | K_IMMEDIATE
 | K_IN
 | K_INDEX
 | K_INDEXED
 | K_INITIALLY
 | K_INNER
 | K_INSERT
 | K_INSTEAD
 | K_INTERSECT
 | K_INTO
 | K_IS
 | K_ISNULL
 | K_JOIN
 | K_KEY
 | K_LEFT
 | K_LIKE
 | K_LIMIT
 | K_MATCH
 | K_NATURAL
 | K_NO
 | K_NOT
 | K_NOTNULL
 | K_NULL
 | K_OF
 | K_OFFSET
 | K_ON
 | K_OR
 | K_ORDER
 | K_OUTER
 | K_PLAN
 | K_PRAGMA
 | K_PRIMARY
 | K_PRINT
 | K_QUERY
 | K_RAISE
 | K_RECURSIVE
 | K_REFERENCES
 | K_REGEXP
 | K_REINDEX
 | K_RELEASE
 | K_RENAME
 | K_REPLACE
 | K_RESTRICT
 | K_RIGHT
 | K_RETURN
 | K_ROLLBACK
 | K_ROW
 | K_SAVEPOINT
 | K_SELECT
 | K_SET
 | K_SWITCH
 | K_TABLE
 | K_TRUE
 | K_TEMP
 | K_TEMPORARY
 | K_THEN
 | K_TO
 | K_TRANSACTION
 | K_TRIGGER
 | K_UNION
 | K_UNIQUE
 | K_UPDATE
 | K_USING
 | K_VACUUM
 | K_VALUES
 | K_VIEW
 | K_VIRTUAL
 | K_WHEN
 | K_WHERE
 | K_WITH
 | K_WITHOUT
 | K_NEXTVAL

 ;

// TODO check all names below

//[a-zA-Z_0-9\t \-\[\]\=]+
//unknown
// : .+
// ;

table_alias
 : any_name
 ;

name
 : any_name
 ;

function_name
 : any_name
 ;

database_name
 : any_name
 ;

table_name
 : any_name
 ;

column_name
 : any_name
 ;

collation_name
 : any_name
 ;

foreign_table
 : any_name
 ;

index_name
 : any_name
 ;

savepoint_name
 : any_name
 ;




any_name
 : IDENTIFIER
// | keyword
 | STRING_LITERAL
 | '(' any_name ')'

 ;


SCOL : ';';
DOT : '.';
OPEN_PAR : '(';
CLOSE_PAR : ')';
COMMA : ',';
ASSIGN : '=';
STAR : '*';
PLUS : '+';
MINUS : '-';
TILDE : '~';
PIPE2 : '||';
DIV : '/';
MOD : '%';
LT2 : '<<';
GT2 : '>>';
AMP : '&';
PIPE : '|';
LT : '<';
LT_EQ : '<=';
GT : '>';
GT_EQ : '>=';
EQ : '==';
NOT_EQ1 : '!=';
NOT_EQ2 : '<>';

// http://www.sqlite.org/lang_keywords.html
K_ABORT : A B O R T;
K_ACTION : A C T I O N;
K_ADD : A D D;
K_AFTER : A F T E R;
K_ALL : A L L;
K_ALTER : A L T E R;
K_ANALYZE : A N A L Y Z E;
K_AND : A N D;
K_AS : A S;
K_ASC : A S C;
K_ATTACH : A T T A C H;
K_AUTOINCREMENT : A U T O I N C R E M E N T;
K_BEFORE : B E F O R E;
K_BEGIN : B E G I N;
K_BETWEEN : B E T W E E N;
K_BY : B Y;
K_BREAK : B R E A K ;
K_CASCADE : C A S C A D E;
K_CASE : C A S E;
K_CAST : C A S T;
K_CHECK : C H E C K;
K_COLLATE : C O L L A T E;
K_COLUMN : C O L U M N;
K_COMMIT : C O M M I T;
K_CONFLICT : C O N F L I C T;
K_CONSTRAINT : C O N S T R A I N T;
K_CONTINUE : C O N T I N U E ;
K_CREATE : C R E A T E;
K_CROSS : C R O S S;
K_CURRENT_DATE : C U R R E N T '_' D A T E;
K_CURRENT_TIME : C U R R E N T '_' T I M E;
K_CURRENT_TIMESTAMP : C U R R E N T '_' T I M E S T A M P;
K_DATABASE : D A T A B A S E;
K_DEFAULT : D E F A U L T;
K_DEFERRABLE : D E F E R R A B L E;
K_DEFERRED : D E F E R R E D;
K_DELETE : D E L E T E;
K_DESC : D E S C;
K_DETACH : D E T A C H;
K_DISTINCT : D I S T I N C T;
//k_DOUBLE : D O U B L E;
K_DO : D O ;
K_DROP : D R O P;
K_EACH : E A C H;
K_ELSE : E L S E;
K_END : E N D;
K_ENABLE : E N A B L E;
K_ESCAPE : E S C A P E;
K_EXCEPT : E X C E P T;
K_EXCLUSIVE : E X C L U S I V E;
K_EXISTS : E X I S T S;
K_EXPLAIN : E X P L A I N;
K_FAIL : F A I L;
K_FALSE : F A L S E;
K_FOR : F O R;
K_FOREIGN : F O R E I G N;
K_FROM : F R O M;
K_FULL : F U L L;
//k_FLOAT : F L O A T ;
K_GLOB : G L O B;
K_GROUP : G R O U P;
K_HAVING : H A V I N G;
K_IF : I F;
K_IGNORE : I G N O R E;
K_IMMEDIATE : I M M E D I A T E;
K_IN : I N;
//K_INT: I N T;
K_INDEX : I N D E X;
K_INDEXED : I N D E X E D;
K_INITIALLY : I N I T I A L L Y;
K_INNER : I N N E R;
K_INSERT : I N S E R T;
K_INSTEAD : I N S T E A D;
K_INTERSECT : I N T E R S E C T;
K_INTO : I N T O;
K_IS : I S;
K_ISNULL : I S N U L L;
K_JOIN : J O I N;
K_JSON : J S O N;
K_KEY : K E Y;
K_LEFT : L E F T;
K_LIKE : L I K E;
K_LIMIT : L I M I T;
K_MATCH : M A T C H;
K_NATURAL : N A T U R A L;
K_NEXTVAL : N E X T V A L;
K_NO : N O;
K_NOT : N O T;
K_NOTNULL : N O T N U L L;
K_NULL : N U L L;
K_OF : O F;
K_OFFSET : O F F S E T;
K_ON : O N;
K_ONLY : O N L Y;
K_OR : O R;
K_ORDER : O R D E R;
K_OUTER : O U T E R;
K_PLAN : P L A N;
K_PRAGMA : P R A G M A;
K_PRIMARY : P R I M A R Y;
K_PRINT : P R I N T ;
K_QUERY : Q U E R Y;
K_RAISE : R A I S E;
K_RECURSIVE : R E C U R S I V E;
K_REFERENCES : R E F E R E N C E S;
K_REGEXP : R E G E X P;
K_REINDEX : R E I N D E X;
K_RELEASE : R E L E A S E;
K_RENAME : R E N A M E;
K_REPLACE : R E P L A C E;
K_RESTRICT : R E S T R I C T;
K_RETURN   : R E T U R N;
K_RIGHT : R I G H T;
K_ROLLBACK : R O L L B A C K;
K_ROW : R O W;
K_SAVEPOINT : S A V E P O I N T;
K_SELECT : S E L E C T;
K_SET : S E T;
K_SWITCH : S W I T C H ;
K_TABLE : T A B L E;
K_TEMP : T E M P;
K_TEMPORARY : T E M P O R A R Y;
K_THEN : T H E N;
K_TO : T O;
K_TRUE : T R U E;
K_TRANSACTION : T R A N S A C T I O N;
K_TRIGGER : T R I G G E R;
K_UNION : U N I O N;
K_UNIQUE : U N I Q U E;
K_UPDATE : U P D A T E;
K_USING : U S I N G;
K_VACUUM : V A C U U M;
K_VALUES : V A L U E S;
K_VIEW : V I E W;
K_VIRTUAL : V I R T U A L;
K_VAR : V A R;
K_WHEN : W H E N;
K_WHERE : W H E R E;
K_WITH : W I T H;
K_WITHOUT : W I T H O U T;
K_WHILE : W H I L E;
K_TYPE: T Y P E;
K_PATH: P A T H;
K_AGGREGATION_FUNCTION: A G G R E G A T I O N '_' F U N C T I O N ;


IDENTIFIER //?
 : '"' (~'"' | '""')* '"'
 | '`' (~'`' | '``')* '`'
 //| '[' ~']'* ']'
 | [a-zA-Z_] [a-zA-Z_0-9]* // TODO check: needs more chars in set
 ;

NUMERIC_LITERAL
 : DIGIT+ ( '.' DIGIT* )? ( E [-+]? DIGIT+ )?
 | '.' DIGIT+ ( E [-+]? DIGIT+ )?
 ;

BIND_PARAMETER
 : '?' DIGIT*
 | [:@$] IDENTIFIER
 ;

STRING_LITERAL //?
 : '\'' ( ~'\'' | '\'\'' )* '\''
 ;

BLOB_LITERAL //?
 : X STRING_LITERAL
 ;

SINGLE_LINE_COMMENT
 : '--'~[\r\n]* -> channel(HIDDEN)
 ;

MULTILINE_COMMENT
 : '/*' .*? ( '*/' | EOF ) -> channel(HIDDEN)
 ;

SPACES
 : [ \u000B\t\r\n] -> channel(HIDDEN)
 ;

UNEXPECTED_CHAR
 : .
 ;

fragment DIGIT : [0-9];

fragment A : [aA];
fragment B : [bB];
fragment C : [cC];
fragment D : [dD];
fragment E : [eE];
fragment F : [fF];
fragment G : [gG];
fragment H : [hH];
fragment I : [iI];
fragment J : [jJ];
fragment K : [kK];
fragment L : [lL];
fragment M : [mM];
fragment N : [nN];
fragment O : [oO];
fragment P : [pP];
fragment Q : [qQ];
fragment R : [rR];
fragment S : [sS];
fragment T : [tT];
fragment U : [uU];
fragment V : [vV];
fragment W : [wW];
fragment X : [xX];
fragment Y : [yY];
fragment Z : [zZ];
