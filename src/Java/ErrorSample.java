package Java;

import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class ErrorSample {


    public void writeError(String msg) throws IOException {

        try (
                Writer writer = Files.newBufferedWriter(
                        Paths.get("samples//Error.txt"), StandardCharsets.UTF_8,
                        StandardOpenOption.WRITE,
                        StandardOpenOption.APPEND)) {
            writer.write(msg + "\n");

        }
    }

}

