package Java.AST.temp.function;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.decleration.DeclerationVariable;
import Java.AST.temp.exp.Exp;

public class Item extends Node {

    private boolean ok ;

    private Exp exp;
    private DeclerationVariable decVar;

    public Item() {
        ok = false;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public Exp getExp() {
        return exp;
    }

    public void setExp(Exp exp) {
        this.exp = exp;
    }

    public DeclerationVariable getDecVar() {
        return decVar;
    }

    public void setDecVar(DeclerationVariable decVar) {
        this.decVar = decVar;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
    }
}
