package Java.AST.temp.function;

import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.other.AnyName;
import Java.SymbolTable.Scope;

public class CallFunction extends Function {


    private ArgumentList argumentList = new ArgumentList();
    private AnyName anyName;

    private Scope scope;


    public AnyName getAnyName() {
        return anyName;
    }

    public void setAnyName(AnyName anyName) {
        this.anyName = anyName;
    }

    public ArgumentList getArgumentList() {
        return argumentList;
    }

    public void setArgumentList(ArgumentList argumentList) {
        this.argumentList = argumentList;
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public int getCol() {
        return super.getCol();
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if (this.argumentList != null)
            this.argumentList.accept(astVisitor);
        if (this.anyName != null)
            this.anyName.accept(astVisitor);
    }
}
