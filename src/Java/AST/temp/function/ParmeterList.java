package Java.AST.temp.function;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;

import java.util.ArrayList;

public class ParmeterList extends Node {

    private boolean ok;
    private ArrayList<Item> items;

    public ParmeterList() {
        this.ok = false;
        items = new ArrayList<>();
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(Item items) {
        this.items.add(items);
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }


    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        for (int i = 0; i < this.items.size(); i++)
            if (this.items.get(i) != null)
                this.items.get(i).accept(astVisitor);
    }
}
