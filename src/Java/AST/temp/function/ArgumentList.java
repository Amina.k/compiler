package Java.AST.temp.function;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.decleration.DifferentKindOfForIncreament;
import Java.AST.temp.exp.Exp;

import java.util.ArrayList;

public class ArgumentList extends Node {

    private boolean ok;

    ArrayList<Exp> exps = new ArrayList<>();
    ArrayList<CallFunction> callFunctions = new ArrayList<>();
    private ArrayList<DifferentKindOfForIncreament> differentKindOfForIncreament = new ArrayList<>();

    public ArrayList<DifferentKindOfForIncreament> getDifferentKindOfForIncreament() {
        return differentKindOfForIncreament;
    }

    public void setDifferentKindOfForIncreament(ArrayList<DifferentKindOfForIncreament> differentKindOfForIncreament) {
        this.differentKindOfForIncreament = differentKindOfForIncreament;
    }

    public ArgumentList() {
        ok = false;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public ArrayList<Exp> getExps() {
        return exps;
    }

    public void setExps(ArrayList<Exp> exps) {
        this.exps = exps;
    }

    public ArrayList<CallFunction> getCallFunctions() {
        return callFunctions;
    }

    public void setCallFunctions(ArrayList<CallFunction> callFunctions) {
        this.callFunctions = callFunctions;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

        for (int i = 0; i < this.exps.size(); i++)
            if (this.exps.get(i) != null)
                this.exps.get(i).accept(astVisitor);
        for (int i = 0; i < this.callFunctions.size(); i++)
            if (this.callFunctions.get(i) != null)
                this.callFunctions.get(i).accept(astVisitor);
        for (int i = 0; i < this.differentKindOfForIncreament.size(); i++)
            if (this.differentKindOfForIncreament.get(i) != null)
                this.differentKindOfForIncreament.get(i).accept(astVisitor);


    }
}
