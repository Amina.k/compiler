package Java.AST.temp.function;

import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.exp.Exp;
import Java.AST.temp.other.JavaAnyStmt;
import Java.AST.temp.other.JavaStmt;
import Java.SymbolTable.Symbol;

import java.util.ArrayList;

public class FunctionDefinition extends Function {

    private ArrayList<ParmeterList> parmeterLists = new ArrayList<>();
    private ArrayList<JavaStmt> javaStmt = new ArrayList<>();

    private Exp exp = new Exp();

    private Symbol symbolReturn ;


    public Exp getExp() {
        return exp;
    }

    public void setExp(Exp exp) {
        this.exp = exp;
    }

    public ArrayList<ParmeterList> getParmeterLists() {
        return parmeterLists;
    }

    public void setParmeterLists(ParmeterList parmeterLists) {
        this.parmeterLists.add(parmeterLists);
    }

    public ArrayList<JavaStmt> getJavaStmt() {
        return javaStmt;
    }

    public void setJavaStmt(ArrayList<JavaStmt> javaStmt) {
        this.javaStmt = javaStmt;
    }


    public Symbol getSymbolReturn() {
        return symbolReturn;
    }

    public void setSymbolReturn(Symbol symbolReturn) {
        this.symbolReturn = symbolReturn;
    }

    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public int getCol() {
        return super.getCol();
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        for (int i = 0; i < parmeterLists.size(); i++)
            if (this.parmeterLists.get(i) != null)
                this.parmeterLists.get(i).accept(astVisitor);
        if (this.exp != null)
            this.exp.accept(astVisitor);
        for (int i = 0; i < this.javaStmt.size(); i++)
            if (this.javaStmt.get(i) != null)
                this.javaStmt.get(i).accept(astVisitor);

    }
}
