package Java.AST.temp.function;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.Symbol;

public class Function extends Node {

    private boolean ok;

    private String Namefun;


    public Function() {
        ok = false;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public String getNamefun() {
        return Namefun;
    }

    public void setNamefun(String namefun) {
        Namefun = namefun;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
    }
}
