package Java.AST.temp.condition;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.other.BodyAllStmt;

public class Else extends Node {

    private boolean ok;

    private IF ifNormal;
    private BodyAllStmt bodyAllStmt;

    public Else() {
        ok = false;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public IF getIfNormal() {
        return ifNormal;
    }

    public void setIfNormal(IF ifNormal) {
        this.ifNormal = ifNormal;
    }

    public BodyAllStmt getBodyAllStmt() {
        return bodyAllStmt;
    }

    public void setBodyAllStmt(BodyAllStmt bodyAllStmt) {
        this.bodyAllStmt = bodyAllStmt;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if (this.ifNormal != null)
            this.ifNormal.accept(astVisitor);
        if (this.bodyAllStmt != null)
            this.bodyAllStmt.accept(astVisitor);
    }
}
