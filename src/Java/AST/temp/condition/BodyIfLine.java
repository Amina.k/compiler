package Java.AST.temp.condition;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;

public class BodyIfLine extends Node {
private RealizedCondition realizedCondition;
private UnRealizedConditon unRealizedConditon;


    private boolean ok;

    public BodyIfLine() {
     ok= false;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public UnRealizedConditon getUnRealizedConditon() {
        return unRealizedConditon;
    }

    public void setUnRealizedConditon(UnRealizedConditon unRealizedConditon) {
        this.unRealizedConditon = unRealizedConditon;
    }

    public RealizedCondition getRealizedCondition() {
        return realizedCondition;
    }

    public void setRealizedCondition(RealizedCondition realizedCondition) {
        this.realizedCondition = realizedCondition;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

        if(this.realizedCondition !=null)
            this.realizedCondition.accept(astVisitor);

        if(this.unRealizedConditon!=null)
            this.unRealizedConditon.accept(astVisitor);


    }
}
