package Java.AST.temp.condition;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.decleration.DifferentKindOfForIncreament;
import Java.AST.temp.exp.Exp;
import Java.SymbolTable.Symbol;

import java.util.ArrayList;

public class UnRealizedConditon extends Node {


    private Exp exps;
    private CompleteIfLine completeIfLine;

    private boolean ok;

    Symbol symbol;

    private DifferentKindOfForIncreament differentKindOfForIncreament;

    public DifferentKindOfForIncreament getDifferentKindOfForIncreament() {
        return differentKindOfForIncreament;
    }

    public void setDifferentKindOfForIncreament(DifferentKindOfForIncreament differentKindOfForIncreament) {
        this.differentKindOfForIncreament = differentKindOfForIncreament;
    }

    public Symbol getSymbol() {
        return symbol;
    }

    public void setSymbol(Symbol symbol) {
        this.symbol = symbol;
    }


    public UnRealizedConditon() {
        ok = false;
//        exps = new ArrayList<>();
//        completeIfLine = new ArrayList<>();
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }


    public Exp getExps() {
        return exps;
    }

    public void setExps(Exp exps) {
        this.exps = exps;
    }

    public CompleteIfLine getCompleteIfLine() {
        return completeIfLine;
    }

    public void setCompleteIfLine(CompleteIfLine completeIfLine) {
        this.completeIfLine = completeIfLine;
    }

    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public int getCol() {
        return super.getCol();
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

        if (this.exps != null)
            this.exps.accept(astVisitor);


        if (this.completeIfLine != null)
            this.completeIfLine.accept(astVisitor);

        if (this.differentKindOfForIncreament != null)
            this.differentKindOfForIncreament.accept(astVisitor);

    }
}
