package Java.AST.temp.condition;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.exp.Exp;
import Java.SymbolTable.Symbol;

public class ConditionStmt extends Node {


    private boolean ok;

    private Exp exp;

    private Symbol symbol;

    public ConditionStmt() {
        ok = false;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public Exp getExp() {
        return exp;
    }

    public void setExp(Exp exp) {
        this.exp = exp;
    }

    public Symbol getSymbol() {
        return symbol;
    }

    public void setSymbol(Symbol symbol) {
        this.symbol = symbol;
    }

    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public int getCol() {
        return super.getCol();
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if (this.exp != null)
            this.exp.accept(astVisitor);
    }
}
