package Java.AST.temp.condition;

import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.other.BodyAllStmt;

public class IfNormal extends IF {

    private BodyAllStmt bodyAllStmt;

    private Else anElse;

    public IfNormal() {
        super();
    }


    public Else getAnElse() {
        return anElse;
    }

    public void setAnElse(Else anElse) {
        this.anElse = anElse;
    }

    public BodyAllStmt getBodyAllStmt() {
        return bodyAllStmt;
    }

    public void setBodyAllStmt(BodyAllStmt bodyAllStmt) {
        this.bodyAllStmt = bodyAllStmt;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        super.accept(astVisitor);
        if (this.bodyAllStmt != null)
            this.bodyAllStmt.accept(astVisitor);
        if (this.anElse != null)
            this.anElse.accept(astVisitor);
    }
}
