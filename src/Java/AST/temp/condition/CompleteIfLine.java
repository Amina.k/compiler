package Java.AST.temp.condition;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;

public class CompleteIfLine extends Node {

    private boolean ok;
    private IfLine ifLine;

    public CompleteIfLine() {
        ok = false;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public IfLine getIfLine() {
        return ifLine;
    }

    public void setIfLine(IfLine ifLine) {
        this.ifLine = ifLine;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if (this.ifLine != null)
            this.ifLine.accept(astVisitor);
    }
}
