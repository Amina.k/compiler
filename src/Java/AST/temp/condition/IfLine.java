package Java.AST.temp.condition;

import Java.AST.Visitor.ASTVisitor;
/*

javaIFline_stmt :condtion_stmt '?' ifline_body ;
ifline_body: ('(')? realized_condition  (')')? ':'  ('(')?  unrealized_condition (')')?;
unrealized_condition :( expr | complete_ifLine);
realized_condition :(expr | complete_ifLine);
complete_ifLine:javaIFline_stmt;
 */

public class IfLine extends IF {

    private BodyIfLine bodyIfLine;

    public IfLine() {
        super();
    }


    public BodyIfLine getBodyIfLine() {
        return bodyIfLine;
    }

    public void setBodyIfLine(BodyIfLine bodyIfLine) {
        this.bodyIfLine = bodyIfLine;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if (this.bodyIfLine != null)
            this.bodyIfLine.accept(astVisitor);
//        for(int i =0 ;i < exps.size() ;i++)
//            this.exps.get(i).accept(astVisitor);
    }
}
