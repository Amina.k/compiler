package Java.AST.temp.condition;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;

public class IF extends Node {

    private boolean ok;

    private ConditionStmt conditionStmt;

    private String typeIf;


    public IF() {
        ok = false;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public String getTypeIf() {
        return typeIf;
    }

    public void setTypeIf(String typeIf) {
        this.typeIf = typeIf;
    }

    public ConditionStmt getConditionStmt() {
        return conditionStmt;
    }

    public void setConditionStmt(ConditionStmt conditionStmt) {
        this.conditionStmt = conditionStmt;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if (this.conditionStmt != null)
            this.conditionStmt.accept(astVisitor);
    }
}
