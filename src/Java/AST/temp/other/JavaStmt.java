package Java.AST.temp.other;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.condition.IF;
import Java.AST.temp.decleration.DifferentKindOfForIncreament;
import Java.AST.temp.decleration.VarEqualStmt;
import Java.AST.temp.exp.Exp;
import Java.AST.temp.function.CallFunction;
import Java.AST.temp.function.Function;
import Java.AST.temp.loop.For;
import Java.AST.temp.loop.perSwitch;
import Java.AST.temp.loop.perWhile;
import Java.AST.temp.print.PrintStmt;

public class JavaStmt extends Node {

    private boolean ok;

    private For forEach;
    private For forNoraml;
    private IF ifNOrmal;
    private IF ifLine;
    private CallFunction callFunction;
    private VarEqualStmt varEqualStmt;
    private PrintStmt PrintStmt;
    private perWhile aWhile, doWhile;
    private perSwitch aSwitch;
    private Function functionDef, functioncall;
    private Exp exp;

    private DifferentKindOfForIncreament differentKindOfForIncreament;

    public JavaStmt() {
        ok = false;
    }


    public Exp getExp() {
        return exp;
    }

    public void setExp(Exp exp) {
        this.exp = exp;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }


    public CallFunction getCallFunction() {
        return callFunction;
    }

    public void setCallFunction(CallFunction callFunction) {
        this.callFunction = callFunction;
    }

    public VarEqualStmt getVarEqualStmt() {
        return varEqualStmt;
    }

    public void setVarEqualStmt(VarEqualStmt varEqualStmt) {
        this.varEqualStmt = varEqualStmt;
    }

    public Function getFunctioncall() {
        return functioncall;
    }

    public void setFunctioncall(Function functioncall) {
        this.functioncall = functioncall;
    }

    public Function getFunctionDef() {
        return functionDef;
    }

    public void setFunctionDef(Function functionDef) {
        this.functionDef = functionDef;
    }

    public perWhile getaWhile() {
        return aWhile;
    }

    public void setaWhile(perWhile aWhile) {
        this.aWhile = aWhile;
    }

    public perWhile getDoWhile() {
        return doWhile;
    }

    public void setDoWhile(perWhile doWhile) {
        this.doWhile = doWhile;
    }

    public perSwitch getaSwitch() {
        return aSwitch;
    }

    public void setaSwitch(perSwitch aSwitch) {
        this.aSwitch = aSwitch;
    }

    public Java.AST.temp.print.PrintStmt getPrintStmt() {
        return PrintStmt;
    }

    public void setPrintStmt(Java.AST.temp.print.PrintStmt printStmt) {
        PrintStmt = printStmt;
    }

    public IF getIfNOrmal() {
        return ifNOrmal;
    }

    public void setIfNOrmal(IF ifNOrmal) {
        this.ifNOrmal = ifNOrmal;
    }

    public IF getIfLine() {
        return ifLine;
    }

    public void setIfLine(IF ifLine) {
        this.ifLine = ifLine;
    }

    public For getForEach() {
        return forEach;
    }

    public void setForEach(For forEach) {
        this.forEach = forEach;
    }

    public For getForNoraml() {
        return forNoraml;
    }

    public void setForNoraml(For forNoraml) {
        this.forNoraml = forNoraml;
    }

    public DifferentKindOfForIncreament getDifferentKindOfForIncreament() {
        return differentKindOfForIncreament;
    }

    public void setDifferentKindOfForIncreament(DifferentKindOfForIncreament differentKindOfForIncreament) {
        this.differentKindOfForIncreament = differentKindOfForIncreament;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if (this.exp != null)
            this.exp.accept(astVisitor);
        if (this.callFunction != null)
            this.callFunction.accept(astVisitor);
        if (this.forEach != null)
            this.forEach.accept(astVisitor);
        if (this.forNoraml != null)
            this.forNoraml.accept(astVisitor);
        if (this.ifNOrmal != null)
            this.ifNOrmal.accept(astVisitor);
        if (this.ifLine != null)
            this.ifLine.accept(astVisitor);
        if (this.varEqualStmt != null)
            this.varEqualStmt.accept(astVisitor);
        if (this.PrintStmt != null)
            this.PrintStmt.accept(astVisitor);
        if (this.aWhile != null)
            this.aWhile.accept(astVisitor);
        if (this.doWhile != null)
            this.doWhile.accept(astVisitor);
        if (this.aSwitch != null)
            this.aSwitch.accept(astVisitor);
        if (this.functioncall != null)
            this.functioncall.accept(astVisitor);
        if (this.functionDef != null)
            this.functionDef.accept(astVisitor);

    }
}
