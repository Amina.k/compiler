package Java.AST.temp.other;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;

import java.util.ArrayList;

public class BodyAllStmt extends Node {

    private boolean ok;

    private ArrayList<JavaAnyStmt> javaAnyStmt;

    public BodyAllStmt() {
        ok = false;
        javaAnyStmt = new ArrayList<>();
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public ArrayList<JavaAnyStmt> getJavaAnyStmt() {
        return javaAnyStmt;
    }

    public void setJavaAnyStmt(JavaAnyStmt javaAnyStmt) {
        this.javaAnyStmt.add(javaAnyStmt);
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        for (int i = 0; i < javaAnyStmt.size(); i++)
            if (this.javaAnyStmt.get(i)!= null)
                this.javaAnyStmt.get(i).accept(astVisitor);
    }
}
