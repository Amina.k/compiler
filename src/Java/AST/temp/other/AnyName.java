package Java.AST.temp.other;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;

public class AnyName extends Node {

    private boolean ok ;

    private String AnyName;

    public AnyName() {
        ok = false;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public String getAnyName() {
        return AnyName;
    }

    public void setAnyName(String anyName) {
        AnyName = anyName;
    }

    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public int getCol() {
        return super.getCol();
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
    }
}
