package Java.AST.temp.other;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;

public class SignedNumber extends Node {

    private boolean ok;

    private String SiNumber;

    public SignedNumber() {
        ok = false;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public String getSiNumber() {
        return SiNumber;
    }

    public void setSiNumber(String siNumber) {
        SiNumber = siNumber;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
    }
}
