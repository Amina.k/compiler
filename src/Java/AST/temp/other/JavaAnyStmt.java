package Java.AST.temp.other;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.SQL.sqlStmt.SqlStmt;
import Java.AST.temp.SQL.sqlStmt.SqlStmtList;
import Java.AST.temp.decleration.DifferentKindOfForIncreament;
import Java.AST.temp.exp.Exp;
import Java.AST.temp.function.CallFunction;

/*

java_anystmt:
  sql_stmt_list
 |sql_stmt
 |java_stmt
 |(call_function (';')? )
 | ( expr ';' )
 |K_CONTINUE ';'
|differnt_kind_of_for_increament  //new
 ;

 */
public class JavaAnyStmt extends Node {

    private boolean ok;

    ////كلهن او بيناتهن

    private JavaStmt javaStmt;
    private SqlStmt sqlStmt;
    private SqlStmtList sqlStmtList;

    private DifferentKindOfForIncreament differentKindOfForIncreament;
    private Exp exp;

    public JavaAnyStmt() {
        ok = false;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }


    public SqlStmtList getSqlStmtList() {
        return sqlStmtList;
    }

    public void setSqlStmtList(SqlStmtList sqlStmtList) {
        this.sqlStmtList = sqlStmtList;
    }

    public SqlStmt getSqlStmt() {
        return sqlStmt;
    }

    public void setSqlStmt(SqlStmt sqlStmt) {
        this.sqlStmt = sqlStmt;
    }

    public DifferentKindOfForIncreament getDifferentKindOfForIncreament() {
        return differentKindOfForIncreament;
    }

    public void setDifferentKindOfForIncreament(DifferentKindOfForIncreament differentKindOfForIncreament) {
        this.differentKindOfForIncreament = differentKindOfForIncreament;
    }

    public JavaStmt getJavaStmt() {
        return javaStmt;
    }

    public void setJavaStmt(JavaStmt javaStmt) {
        this.javaStmt = javaStmt;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

        if (this.exp != null)
            this.exp.accept(astVisitor);
        if (this.sqlStmtList != null)
            this.sqlStmtList.accept(astVisitor);
        if (this.sqlStmt != null)
            this.sqlStmt.accept(astVisitor);
        if (this.javaStmt != null)
            this.javaStmt.accept(astVisitor);
    }
}
