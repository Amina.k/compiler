package Java.AST.temp.other;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;

public class KeyWord extends Node {

    private boolean ok ;

    private String keyword;

    public KeyWord() {
        ok  = false;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
    }
}
