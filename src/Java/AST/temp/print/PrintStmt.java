package Java.AST.temp.print;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.exp.Exp;
import Java.SymbolTable.Scope;

import java.util.ArrayList;

public class PrintStmt extends Node {

    private boolean ok;

    private ArrayList<Exp> exps = new ArrayList<>();

    private ArrayList<String> namesVariable= new ArrayList<>();

    private Scope scope;


    public PrintStmt() {
        ok = false;
        exps = new ArrayList<>();
    }


    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public ArrayList<Exp> getExps() {
        return exps;
    }

    public void setExps(Exp exps) {
        this.exps.add(exps);
    }

    public ArrayList<String> getNamesVariable() {
        return namesVariable;
    }

    public void addNamesVariable(String namesVariable) {
        this.namesVariable.add(namesVariable);
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        for (int i = 0; i < this.exps.size(); i++)
            if (this.exps.get(i) != null)
                this.exps.get(i).accept(astVisitor);
    }
}
