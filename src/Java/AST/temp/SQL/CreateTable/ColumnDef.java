package Java.AST.temp.SQL.CreateTable;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.other.AnyName;
import Java.SymbolTable.Symbol;

import java.util.ArrayList;

public class ColumnDef extends Node {

    AnyName columnName;

    ArrayList<TypeName> typeNames = new ArrayList<>();
    boolean OK;
    Symbol ColDefTablSymbol = new Symbol();

    public Symbol getColDefTablSymbol() {
        return ColDefTablSymbol;
    }

    public void setColDefTablSymbol(Symbol colDefTablSymbol) {
        ColDefTablSymbol = colDefTablSymbol;
    }

    public ColumnDef() {
        this.OK = false;
    }

    public boolean isOK() {
        return OK;
    }

    public void setOK(boolean OK) {
        this.OK = OK;
    }


    public void setTypeNames(TypeName typeNames) {
        this.typeNames.add(typeNames);
    }

    public ArrayList<TypeName> getTypeNames() {
        return typeNames;
    }


    public AnyName getColumnName() {
        return columnName;
    }

    public void setColumnName(AnyName columnName) {
        this.columnName = columnName;
    }


    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public int getCol() {
        return super.getCol();
    }

    @Override
    public void accept(ASTVisitor astVisitor) {

        astVisitor.visit(this);

        if (this.columnName != null)
            this.columnName.accept(astVisitor);


        for (int j = 0; j < typeNames.size(); j++)
            if (this.typeNames.get(j) != null)
                this.typeNames.get(j).accept(astVisitor);

    }
}
