package Java.AST.temp.SQL.CreateTable;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.SQL.CreateTable.ColumnDef;
import Java.SymbolTable.Symbol;
import Java.SymbolTable.Type;

import java.util.ArrayList;

public class CreateTableStatmt extends Node {
    /*
    create_table_stmt
     : K_CREATE  K_TABLE table_name
       '(' column_def (  ',' column_def )*
       ( K_TYPE '=' type
          K_PATH '=' path)?
       ')'  ';'
    ;
    type : IDENTIFIER;
    path : IDENTIFIER  ('/' IDENTIFIER )* ;
     */
    private String tableName;
    private ArrayList<ColumnDef> columnDefs = new ArrayList<>();
    private String type;
    private String path;
    private Type typetable;

    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public int getCol() {
        return super.getCol();
    }

    public Type getTypetable() {
        return typetable;
    }

    public void setTypetable(Type typetable) {
        this.typetable = typetable;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public ArrayList<ColumnDef> getColumnDefs() {
        return columnDefs;
    }

    public void setColumnDefs(ArrayList<ColumnDef> columnDefs) {
        this.columnDefs = columnDefs;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);


        for (int i = 0; i < columnDefs.size(); i++)
            if (this.columnDefs.get(i) != null)
                this.columnDefs.get(i).accept(astVisitor);
    }
}
