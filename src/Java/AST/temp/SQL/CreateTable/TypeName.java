package Java.AST.temp.SQL.CreateTable;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.other.AnyName;
import Java.AST.temp.other.SignedNumber;

import java.util.ArrayList;

public class TypeName extends Node {

    private String COMA = ", ";
    private boolean OK;

    private AnyName name;
    private AnyName anyName;
    private ArrayList<SignedNumber> signedNumbers = new ArrayList<>();


    public ArrayList<SignedNumber> getSignedNumbers() {
        return signedNumbers;
    }

    public void setSignedNumbers(SignedNumber signedNumbers) {
        this.signedNumbers.add(signedNumbers);
    }

    public TypeName() {
        this.OK = false;
    }

    public boolean isOK() {
        return OK;
    }

    public void setOK(boolean OK) {
        this.OK = OK;
    }

    public AnyName getName() {
        return name;
    }

    public void setName(AnyName name) {
        this.name = name;
    }

    public AnyName getAnyName() {
        return anyName;
    }

    public void setAnyName(AnyName anyName) {
        this.anyName = anyName;
    }

    public String getCOMA() {
        return COMA;
    }

    public void setCOMA(String COMA) {
        this.COMA = COMA;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

        if (this.name != null)
            this.name.accept(astVisitor);

        for (int j = 0; j < this.signedNumbers.size(); j++)
            if (this.signedNumbers.get(j) != null)
                this.signedNumbers.get(j).accept(astVisitor);

        if (this.anyName != null)
            this.anyName.accept(astVisitor);
    }
}
