package Java.AST.temp.SQL.selectStmt;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.exp.Exp;

public class OrderingTerm extends Node {

    private Exp exp;

    private boolean OK;

    public OrderingTerm() {
        this.OK = false;
    }

    public boolean isOK() {
        return OK;
    }

    public void setOK(boolean OK) {
        this.OK = OK;
    }

    public Exp getExp() {
        return exp;
    }

    public void setExp(Exp exp) {
        this.exp = exp;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

        if (this.exp != null)
            this.exp.accept(astVisitor);
    }
}
