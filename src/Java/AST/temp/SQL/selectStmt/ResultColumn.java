package Java.AST.temp.SQL.selectStmt;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.SQL.otherSql.AggFun;
import Java.AST.temp.exp.Exp;
import Java.AST.temp.other.AnyName;

public class ResultColumn extends Node {

    private boolean OK;
/*
result_column
 : '*'
 | table_name '.' '*'
 | expr ( K_AS? column_alias )?
 ;
 */
    private String star ;
    private String tableNameOnly;
    private Exp exp;
    private String columnAlias;
    private String tableName;
    private String columnName;

    private AggFun aggFun;

    public AggFun getAggFun() {
        return aggFun;
    }

    public void setAggFun(AggFun aggFun) {
        this.aggFun = aggFun;
    }

    public ResultColumn() {
        this.OK = false;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public boolean isOK() {
        return OK;
    }

    public void setOK(boolean OK) {
        this.OK = OK;
    }

    public String getTableNameOnly() {
        return tableNameOnly;
    }

    public void setTableNameOnly(AnyName tableNameOnly) {
        this.tableNameOnly = tableNameOnly.getAnyName()+"."+"*";
    }

    public Exp getExp() {
        return exp;
    }

    public void setExp(Exp exp) {
        this.exp = exp;
    }

    public String getColumnAlias() {
        return columnAlias;
    }

    public void setColumnAlias(String columnAlias) {
        this.columnAlias = columnAlias;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public int getCol() {
        return super.getCol();
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
//
//        if (this.tableName != null)
//            this.tableName.accept(astVisitor);
        if (this.exp != null)
            this.exp.accept(astVisitor);

    }
}
