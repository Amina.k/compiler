package Java.AST.temp.SQL.sqlStmt;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;

import java.util.ArrayList;

public class SqlStmtList extends Node {

    private boolean ok;

    private ArrayList<SqlStmt> sqlStmts;

    public SqlStmtList() {
        ok = false;
        sqlStmts = new ArrayList<>();
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public ArrayList<SqlStmt> getSqlStmts() {
        return sqlStmts;
    }

    public void setSqlStmts(SqlStmt sqlStmts) {
        this.sqlStmts.add(sqlStmts);
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        for (int i = 0; i < this.sqlStmts.size(); i++)
            if (this.sqlStmts.get(i) != null)
                this.sqlStmts.get(i).accept(astVisitor);
    }
}
