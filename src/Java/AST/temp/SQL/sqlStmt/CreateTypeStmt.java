package Java.AST.temp.SQL.sqlStmt;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.other.AnyName;
import Java.SymbolTable.Symbol;
import Java.SymbolTable.Type;

import java.util.ArrayList;

public class CreateTypeStmt extends Node {
//    create_type_stmt :K_CREATE K_TYPE name_type '(' (( column_def_type (',' column_def_type )*)?) ')' ';';

    private String nameType;
    private ArrayList<ColDefType> colDefTypes = new ArrayList<>();
    private boolean ok;

    private Type type;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public int getCol() {
        return super.getCol();
    }

    public String getNameType() {
        return nameType;
    }

    public void setNameType(String nameType) {
        this.nameType = nameType;
    }

    public ArrayList<ColDefType> getColDefTypes() {
        return colDefTypes;
    }

    public void setColDefTypes(ArrayList<ColDefType> colDefTypes) {
        this.colDefTypes = colDefTypes;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        for (int i = 0; i < this.colDefTypes.size(); i++)
            if (this.colDefTypes.get(i) != null)
                this.colDefTypes.get(i).accept(astVisitor);

    }
}
