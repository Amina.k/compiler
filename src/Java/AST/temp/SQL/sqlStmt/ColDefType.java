package Java.AST.temp.SQL.sqlStmt;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.other.AnyName;
import Java.SymbolTable.Symbol;

public class ColDefType extends Node {
    //    column_def_type : column_name any_name;
    private AnyName columnName;
    private AnyName TypeName;
    private boolean ok;
    private Symbol symbolColumn;

    public Symbol getSymbolColumn() {
        return symbolColumn;
    }

    public void setSymbolColumn(Symbol symbolColumn) {
        this.symbolColumn = symbolColumn;
    }

    public AnyName getTypeName() {
        return TypeName;
    }

    public void setTypeName(AnyName typeName) {
        TypeName = typeName;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public AnyName getColumnName() {
        return columnName;
    }

    public void setColumnName(AnyName columnName) {
        this.columnName = columnName;
    }


    @Override
    public int getCol() {
        return super.getCol();
    }

    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if (this.columnName != null)
            this.columnName.accept(astVisitor);

        if (this.TypeName != null)
            this.TypeName.accept(astVisitor);
    }
}
