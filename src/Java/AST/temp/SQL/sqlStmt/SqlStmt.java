package Java.AST.temp.SQL.sqlStmt;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.SQL.CreateTable.CreateTableStatmt;
import Java.AST.temp.exp.SelectStmt;
import Java.SymbolTable.Symbol;

public class SqlStmt extends Node {


    private boolean ok;
    //// OR All  ممكن يجي وحدة بس من كل هدول

    private SelectStmt factoredSelectStmt;   ////this is factored select stmt
    private CreateTableStatmt createTableStatmt;
    private CreateTypeStmt createTypeStmt;
    private AggregationFunction aggregationFunction;

    private Symbol symbol;

    private String nameTable ;

    /// the all
    public AggregationFunction getAggregationFunction() {
        return aggregationFunction;
    }

    public void setAggregationFunction(AggregationFunction aggregationFunction) {
        this.aggregationFunction = aggregationFunction;
    }


    public SqlStmt() {
        ok = false;
    }


    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public CreateTableStatmt getCreateTableStatmt() {
        return createTableStatmt;
    }

    public void setCreateTableStatmt(CreateTableStatmt createTableStatmt) {
        this.createTableStatmt = createTableStatmt;
    }

    public SelectStmt getFactoredSelectStmt() {
        return factoredSelectStmt;
    }

    public void setFactoredSelectStmt(SelectStmt factoredSelectStmt) {
        this.factoredSelectStmt = factoredSelectStmt;
    }

    public CreateTypeStmt getCreateTypeStmt() {
        return createTypeStmt;
    }

    public void setCreateTypeStmt(CreateTypeStmt createTypeStmt) {
        this.createTypeStmt = createTypeStmt;
    }

    public String getNameTable() {
        return nameTable;
    }

    public void setNameTable(String nameTable) {
        this.nameTable = nameTable;
    }

    public Symbol getSymbol() {
        return symbol;
    }

    public void setSymbol(Symbol symbol) {
        this.symbol = symbol;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

        if (this.createTableStatmt != null)
            this.createTableStatmt.accept(astVisitor);

        if (this.factoredSelectStmt != null)
            this.factoredSelectStmt.accept(astVisitor);

        if (this.createTypeStmt != null)
            this.createTypeStmt.accept(astVisitor);

    }
}
