package Java.AST.temp.SQL.otherSql;

import java.util.ArrayList;

public class WhereCondition {

    private TableAndColumnName whereNameCondition1; /// where year
    private ArrayList<TableAndColumnName> whereNameCondition2 = new ArrayList<>();  ///where year in (2008,2020)
    private String operation; /// > = in like
    private WhereCondition whereCondition; /// where year = 2009 and year = 2052
    private String key; /// and or

    public TableAndColumnName getWhereNameCondition1() {
        return whereNameCondition1;
    }

    public void setWhereNameCondition1(TableAndColumnName whereNameCondition1) {
        this.whereNameCondition1 = whereNameCondition1;
    }

    public ArrayList<TableAndColumnName> getWhereNameCondition2() {
        return whereNameCondition2;
    }

    public void setWhereNameCondition2(ArrayList<TableAndColumnName> whereNameCondition2) {
        this.whereNameCondition2 = whereNameCondition2;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public WhereCondition getWhereCondition() {
        return whereCondition;
    }

    public void setWhereCondition(WhereCondition whereCondition) {
        this.whereCondition = whereCondition;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
