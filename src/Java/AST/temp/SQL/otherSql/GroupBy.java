package Java.AST.temp.SQL.otherSql;

public class GroupBy {

    private String groupByTableName;
    private String groupByColumnName;

    public String getGroupByTableName() {
        return groupByTableName;
    }

    public void setGroupByTableName(String groupByTableName) {
        this.groupByTableName = groupByTableName;
    }

    public String getGroupByColumnName() {
        return groupByColumnName;
    }

    public void setGroupByColumnName(String groupByColumnName) {
        this.groupByColumnName = groupByColumnName;
    }
}
