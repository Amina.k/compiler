package Java.AST.temp.SQL.otherSql;

public class HavingCOndition {

    private AggFun aggFun;
    private String condition1;
    private String operation;

    public AggFun getAggFun() {
        return aggFun;
    }

    public void setAggFun(AggFun aggFun) {
        this.aggFun = aggFun;
    }

    public String getCondition1() {
        return condition1;
    }

    public void setCondition1(String condition1) {
        this.condition1 = condition1;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }
}
