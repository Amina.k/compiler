package Java.AST.temp.SQL.otherSql;

//INNER JOIN MARKS M ON C.ID = M.COURSEID

public class InnerJoin {

    private String tableName; //Mark
    private String tableNameCall ; //m


    private String conditionTableName1;
    private String conditionColumnName1;

    private String conditionTableName2;
    private String conditionColumnName2;

    private String operation;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableNameCall() {
        return tableNameCall;
    }

    public void setTableNameCall(String tableNameCall) {
        this.tableNameCall = tableNameCall;
    }

    public String getConditionTableName1() {
        return conditionTableName1;
    }

    public void setConditionTableName1(String conditionTableName1) {
        this.conditionTableName1 = conditionTableName1;
    }

    public String getConditionColumnName1() {
        return conditionColumnName1;
    }

    public void setConditionColumnName1(String conditionColumnName1) {
        this.conditionColumnName1 = conditionColumnName1;
    }

    public String getConditionTableName2() {
        return conditionTableName2;
    }

    public void setConditionTableName2(String conditionTableName2) {
        this.conditionTableName2 = conditionTableName2;
    }

    public String getConditionColumnName2() {
        return conditionColumnName2;
    }

    public void setConditionColumnName2(String conditionColumnName2) {
        this.conditionColumnName2 = conditionColumnName2;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }
}
