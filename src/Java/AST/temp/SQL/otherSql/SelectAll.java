package Java.AST.temp.SQL.otherSql;

import Java.SymbolTable.Type;

import java.util.ArrayList;

public class SelectAll extends Type {

    private String variableName;

    private ArrayList<String> columnNames;
    private ArrayList<String> tableNames;

    private SelectAll selectAll;

    private ArrayList<AggFun> aggFuns;

    private ArrayList<InnerJoin> innerJoins;
    private WhereCondition whereCondition;
    private HavingCOndition havingCondition;
    private GroupBy groupBy;

    public SelectAll getSelectAll() {
        return selectAll;
    }

    public void setSelectAll(SelectAll selectAll) {
        this.selectAll = selectAll;
    }

    public String getVariableName() {
        return variableName;
    }

    public void setVariableName(String variableName) {
        this.variableName = variableName;
    }

    public ArrayList<String> getColumnNames() {
        return columnNames;
    }

    public void setColumnNames(ArrayList<String> columnNames) {
        this.columnNames = columnNames;
    }

    public WhereCondition getWhereCondition() {
        return whereCondition;
    }

    public void setWhereCondition(WhereCondition whereCondition) {
        this.whereCondition = whereCondition;
    }

    public ArrayList<String> getTableNames() {
        return tableNames;
    }

    public void setTableNames(ArrayList<String> tableNames) {
        this.tableNames = tableNames;
    }

    public ArrayList<AggFun> getAggFuns() {
        return aggFuns;
    }

    public void setAggFuns(ArrayList<AggFun> aggFuns) {
        this.aggFuns = aggFuns;
    }

    public ArrayList<InnerJoin> getInnerJoins() {
        return innerJoins;
    }

    public void setInnerJoins(ArrayList<InnerJoin> innerJoins) {
        this.innerJoins = innerJoins;
    }

    public HavingCOndition getHavingCondition() {
        return havingCondition;
    }

    public void setHavingCondition(HavingCOndition havingCondition) {
        this.havingCondition = havingCondition;
    }

    public GroupBy getGroupBy() {
        return groupBy;
    }

    public void setGroupBy(GroupBy groupBy) {
        this.groupBy = groupBy;
    }
}
