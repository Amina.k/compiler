package Java.AST.temp.SQL.otherSql;

public class AggFun {

    private String aggFunTableName;
    private String aggFunParam;
    private  String tableAliasName;


    public String getTableAliasName() {
        return tableAliasName;
    }

    public void setTableAliasName(String tableAliasName) {
        this.tableAliasName = tableAliasName;
    }

    public String getAggFunTableName() {
        return aggFunTableName;
    }

    public void setAggFunTableName(String aggFunTableName) {
        this.aggFunTableName = aggFunTableName;
    }

    public String getAggFunParam() {
        return aggFunParam;
    }

    public void setAggFunParam(String aggFunParam) {
        this.aggFunParam = aggFunParam;
    }
}
