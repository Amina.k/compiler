package Java.AST.temp.SQL.selectOrValue;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.SQL.selectOrValue.tableOrSub.*;

import java.util.ArrayList;

public class SelTableAm extends Node {

    private boolean OK;

    private ArrayList<TableSubFather> tableSubFathers;
    private JoinCaluse joinCaluse;
    private ArrayList<TableOrSubOne> tableOrSubOnes = new ArrayList<>();
    private ArrayList<TableOrSubTwo> tableOrSubTwos = new ArrayList<>();
    private ArrayList<TableOrSubThree> tableOrSubThrees = new ArrayList<>();


    public SelTableAm() {
        tableSubFathers = new ArrayList<>();
        this.OK = false;
    }


    public boolean isOK() {
        return OK;
    }

    public void setOK(boolean OK) {
        this.OK = OK;
    }

    public JoinCaluse getJoinCaluse() {
        return joinCaluse;
    }

    public void setJoinCaluse(JoinCaluse joinCaluse) {
        this.joinCaluse = joinCaluse;
    }

    public ArrayList<TableSubFather> getTableSubFathers() {
        return tableSubFathers;
    }

    public void setTableSubFathers(TableSubFather tableSubFathers) {
        this.tableSubFathers.add(tableSubFathers);
    }

    public ArrayList<TableOrSubOne> getTableOrSubOnes() {
        return tableOrSubOnes;
    }

    public void setTableOrSubOnes(TableOrSubOne tableOrSubOnes) {
        this.tableOrSubOnes.add(tableOrSubOnes);
    }

    public ArrayList<TableOrSubTwo> getTableOrSubTwos() {
        return tableOrSubTwos;
    }

    public void setTableOrSubTwos(TableOrSubTwo tableOrSubTwos) {
        this.tableOrSubTwos.add(tableOrSubTwos);
    }

    public ArrayList<TableOrSubThree> getTableOrSubThrees() {
        return tableOrSubThrees;
    }

    public void setTableOrSubThrees(TableOrSubThree tableOrSubThrees) {
        this.tableOrSubThrees.add(tableOrSubThrees);
    }

    @Override
    public void accept(ASTVisitor astVisitor) {


        astVisitor.visit(this);

        if (this.joinCaluse != null)
            this.joinCaluse.accept(astVisitor);

        for (int i = 0; i < this.tableSubFathers.size(); i++)
            if (this.tableSubFathers.get(i) != null)
                this.tableSubFathers.get(i).accept(astVisitor);
    }
}
