package Java.AST.temp.SQL.selectOrValue.tableOrSub;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.other.AnyName;

import java.util.ArrayList;

public class TableOrSubTwo extends Node {

    private boolean OK;

    private ArrayList<TableSubFather> tableSubFathers;
    private AnyName tablaalias;
    private JoinCaluse joinCaluse;


    public TableOrSubTwo() {
        tableSubFathers = new ArrayList<>();
        this.OK = false;
    }


    public boolean isOK() {
        return OK;
    }

    public void setOK(boolean OK) {
        this.OK = OK;
    }

    public JoinCaluse getJoinCaluse() {
        return joinCaluse;
    }

    public void setJoinCaluse(JoinCaluse joinCaluse) {
        this.joinCaluse = joinCaluse;
    }

    public ArrayList<TableSubFather> getTableSubFathers() {
        return tableSubFathers;
    }

    public void setTableSubFathers(TableSubFather tableSubFathers) {
        this.tableSubFathers.add(tableSubFathers);
    }

    public AnyName getTablaalias() {
        return tablaalias;
    }

    public void setTablaalias(AnyName tablaalias) {
        this.tablaalias = tablaalias;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

        if (this.tablaalias != null)
            this.tablaalias.accept(astVisitor);
        for (int i = 0; i < this.tableSubFathers.size(); i++)
            if (this.tableSubFathers.get(i) != null)
                this.tableSubFathers.get(i).accept(astVisitor);
        if (this.joinCaluse != null)
            this.joinCaluse.accept(astVisitor);
    }
}
