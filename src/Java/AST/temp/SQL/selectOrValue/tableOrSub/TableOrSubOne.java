package Java.AST.temp.SQL.selectOrValue.tableOrSub;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.other.AnyName;

public class TableOrSubOne extends Node {

    private boolean OK;

    private String databaseName;
    private String tableName;
    private AnyName aliasName;
    private AnyName indexName;


    public TableOrSubOne() {
        this.OK = false;
    }


    public boolean isOK() {
        return OK;
    }

    public void setOK(boolean OK) {
        this.OK = OK;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public AnyName getAliasName() {
        return aliasName;
    }

    public void setAliasName(AnyName aliasName) {
        this.aliasName = aliasName;
    }

    public AnyName getIndexName() {
        return indexName;
    }

    public void setIndexName(AnyName indexName) {
        this.indexName = indexName;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {

        astVisitor.visit(this);

//        if (this.databaseName != null)
//            this.databaseName.accept(astVisitor);
//        if (this.tableName != null)
//            this.tableName.accept(astVisitor);
        if (this.aliasName != null)
            this.aliasName.accept(astVisitor);
        if (this.indexName != null)
            this.indexName.accept(astVisitor);
    }
}
