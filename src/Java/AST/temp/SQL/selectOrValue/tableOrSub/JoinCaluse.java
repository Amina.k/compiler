package Java.AST.temp.SQL.selectOrValue.tableOrSub;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;

import java.util.ArrayList;

public class JoinCaluse extends Node {

    private boolean OK;

    private ArrayList<TableSubFather> tableSubFather;
    private ArrayList<String> tableFromNames;
    private ArrayList<JoinClauseSub> joinClauseSubs;

    public JoinCaluse() {
        joinClauseSubs = new ArrayList<>();
        tableSubFather = new ArrayList<>();
        this.OK = false;
    }

    public ArrayList<String> getTableFromNames() {
        return tableFromNames;
    }

    public void setTableFromNames(ArrayList<String> tableFromNames) {
        this.tableFromNames = tableFromNames;
    }

    public boolean isOK() {
        return OK;
    }

    public void setOK(boolean OK) {
        this.OK = OK;
    }


    public ArrayList<TableSubFather> getTableSubFather() {
        return tableSubFather;
    }

    public void setTableSubFather(ArrayList<TableSubFather> tableSubFather) {
        this.tableSubFather = tableSubFather;
    }

    public ArrayList<JoinClauseSub> getJoinClauseSubs() {
        return joinClauseSubs;
    }

    public void setJoinClauseSubs(JoinClauseSub joinClauseSubs) {
        this.joinClauseSubs.add(joinClauseSubs);
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);


        for (int i = 0; i < this.tableSubFather.size(); i++)
            if (this.tableSubFather.get(i) != null)
                this.tableSubFather.get(i).accept(astVisitor);

        for (int i = 0; i < this.joinClauseSubs.size(); i++)
            if (this.joinClauseSubs.get(i) != null)
                this.joinClauseSubs.get(i).accept(astVisitor);
    }
}
