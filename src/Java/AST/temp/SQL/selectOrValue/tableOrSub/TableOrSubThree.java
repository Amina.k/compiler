package Java.AST.temp.SQL.selectOrValue.tableOrSub;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.exp.SelectStmt;
import Java.AST.temp.other.AnyName;

public class TableOrSubThree extends Node {

    private boolean OK;

    private SelectStmt selectStmt;
    private AnyName tableAlias;


    public TableOrSubThree() {
        this.OK = false;
    }


    public boolean isOK() {
        return OK;
    }

    public void setOK(boolean OK) {
        this.OK = OK;
    }

    public SelectStmt getSelectStmt() {
        return selectStmt;
    }

    public void setSelectStmt(SelectStmt selectStmt) {
        this.selectStmt = selectStmt;
    }

    public AnyName getTableAlias() {
        return tableAlias;
    }

    public void setTableAlias(AnyName tableAlias) {
        this.tableAlias = tableAlias;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

        if (this.selectStmt != null)
            this.selectStmt.accept(astVisitor);

        if (this.tableAlias != null)
            this.tableAlias.accept(astVisitor);
    }
}
