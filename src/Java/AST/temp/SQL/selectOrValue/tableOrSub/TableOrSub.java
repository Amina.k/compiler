package Java.AST.temp.SQL.selectOrValue.tableOrSub;

import Java.AST.Visitor.ASTVisitor;

public class TableOrSub extends TableSubFather {

    private boolean OK;

    private TableOrSubOne tableOrSubOne;
    private TableOrSubTwo tableOrSubTwo;
    private TableOrSubThree tableOrSubThree;

    public TableOrSub() {
        this.OK = false;
    }


    public boolean isOK() {
        return OK;
    }

    public void setOK(boolean OK) {
        this.OK = OK;
    }

    public TableOrSubOne getTableOrSubOne() {
        return tableOrSubOne;
    }

    public void setTableOrSubOne(TableOrSubOne tableOrSubOne) {
        this.tableOrSubOne = tableOrSubOne;
    }

    public TableOrSubTwo getTableOrSubTwo() {
        return tableOrSubTwo;
    }

    public void setTableOrSubTwo(TableOrSubTwo tableOrSubTwo) {
        this.tableOrSubTwo = tableOrSubTwo;
    }

    public TableOrSubThree getTableOrSubThree() {
        return tableOrSubThree;
    }

    public void setTableOrSubThree(TableOrSubThree tableOrSubThree) {
        this.tableOrSubThree = tableOrSubThree;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {


        astVisitor.visit(this);

        if (this.tableOrSubOne != null)
            this.tableOrSubOne.accept(astVisitor);

        if (this.tableOrSubTwo != null)
            this.tableOrSubTwo.accept(astVisitor);

        if (this.tableOrSubThree != null)
            this.tableOrSubThree.accept(astVisitor);
    }
}
