package Java.AST.temp.SQL.selectOrValue.tableOrSub;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.exp.Exp;

public class JoinClauseSub extends Node {

    private boolean OK;

    private String Operater;
    private TableSubFather tableSubFather;
    private String tableInJoinName;
    private String tableAliasName;
    private Exp joinConstraintsExp;

    private String conditionTableName1;
    private String conditionColumnName1;

    private String conditionTableName2;
    private String conditionColumnName2;



    public JoinClauseSub() {
        this.OK = false;
    }


    public boolean isOK() {
        return OK;
    }

    public void setOK(boolean OK) {
        this.OK = OK;
    }

    public String getOperater() {
        return Operater;
    }

    public void setOperater(String operater) {
        Operater = operater;
    }

    public TableSubFather getTableSubFather() {
        return tableSubFather;
    }

    public void setTableSubFather(TableSubFather tableSubFather) {
        this.tableSubFather = tableSubFather;
    }

    public Exp getJoinConstraintsExp() {
        return joinConstraintsExp;
    }

    public void setJoinConstraintsExp(Exp joinConstraintsExp) {
        this.joinConstraintsExp = joinConstraintsExp;
    }

    public String getTableInJoinName() {
        return tableInJoinName;
    }

    public void setTableInJoinName(String tableInJoinName) {
        this.tableInJoinName = tableInJoinName;
    }

    public String getTableAliasName() {
        return tableAliasName;
    }

    public void setTableAliasName(String tableAliasName) {
        this.tableAliasName = tableAliasName;
    }

    public String getConditionTableName1() {
        return conditionTableName1;
    }

    public void setConditionTableName1(String conditionTableName1) {
        this.conditionTableName1 = conditionTableName1;
    }

    public String getConditionColumnName1() {
        return conditionColumnName1;
    }

    public void setConditionColumnName1(String conditionColumnName1) {
        this.conditionColumnName1 = conditionColumnName1;
    }

    public String getConditionTableName2() {
        return conditionTableName2;
    }

    public void setConditionTableName2(String conditionTableName2) {
        this.conditionTableName2 = conditionTableName2;
    }

    public String getConditionColumnName2() {
        return conditionColumnName2;
    }

    public void setConditionColumnName2(String conditionColumnName2) {
        this.conditionColumnName2 = conditionColumnName2;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {

        astVisitor.visit(this);

        if (this.tableSubFather != null)
            this.tableSubFather.accept(astVisitor);

        if (this.joinConstraintsExp != null)
            this.joinConstraintsExp.accept(astVisitor);
    }
}
