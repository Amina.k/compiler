package Java.AST.temp.SQL.selectOrValue.tableOrSub;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;

public class TableSubFather extends Node {

    private boolean OK;

    public TableSubFather() {
        this.OK = false;
    }

    public boolean isOK() {
        return OK;
    }

    public void setOK(boolean OK) {
        this.OK = OK;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
    }
}
