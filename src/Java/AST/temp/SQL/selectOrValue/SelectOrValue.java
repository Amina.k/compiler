package Java.AST.temp.SQL.selectOrValue;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.SQL.otherSql.SelectAll;
import Java.AST.temp.SQL.selectStmt.ResultColumn;
import Java.AST.temp.exp.Exp;
import Java.SymbolTable.Scope;

import java.util.ArrayList;

public class SelectOrValue extends Node {

    private SelectAll selectAll;

    private boolean OK;

    private ArrayList<ResultColumn> resultColumns;
    private ArrayList<Exp> exps;
    private SelTableAm selTableAm;
    private ExpSqlAM expSqlAMWhere;
    private ExpSqlAM expSqlAMGroupBy;
    private ExpSqlAM expSqlAMHaving;

    private Scope scope;
    private boolean isParent = false;
//    private SelectOrValue selectOrValue;

    public SelectOrValue() {
        resultColumns = new ArrayList<>();
        exps = new ArrayList<>();
        this.OK = false;
    }


    public SelectAll getSelectAll() {
        return selectAll;
    }

    public void setSelectAll(SelectAll selectAll) {
        this.selectAll = selectAll;
    }

    public boolean isOK() {
        return OK;
    }

    public void setOK(boolean OK) {
        this.OK = OK;
    }

    public ArrayList<ResultColumn> getResultColumns() {
        return resultColumns;
    }

    public void setResultColumns(ResultColumn resultColumns) {
        this.resultColumns.add(resultColumns);
    }

    public ArrayList<Exp> getExps() {
        return exps;
    }

    public void setExps(Exp exps) {
        this.exps.add(exps);
    }

    public SelTableAm getSelTableAm() {
        return selTableAm;
    }

    public void setSelTableAm(SelTableAm selTableAm) {
        this.selTableAm = selTableAm;
    }

    public ExpSqlAM getExpSqlAMWhere() {
        return expSqlAMWhere;
    }

    public void setExpSqlAMWhere(ExpSqlAM expSqlAMWhere) {
        this.expSqlAMWhere = expSqlAMWhere;
    }

    public ExpSqlAM getExpSqlAMGroupBy() {
        return expSqlAMGroupBy;
    }

    public void setExpSqlAMGroupBy(ExpSqlAM expSqlAMGroupBy) {
        this.expSqlAMGroupBy = expSqlAMGroupBy;
    }

    public ExpSqlAM getExpSqlAMHaving() {
        return expSqlAMHaving;
    }

    public void setExpSqlAMHaving(ExpSqlAM expSqlAMHaving) {
        this.expSqlAMHaving = expSqlAMHaving;
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public boolean isParent() {
        return isParent;
    }

    public void setParent(boolean parent) {
        isParent = parent;
    }

    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public int getCol() {
        return super.getCol();
    }

    //    public SelectOrValue getSelectOrValue() {
//        return selectOrValue;
//    }
//
//    public void setSelectOrValue(SelectOrValue selectOrValue) {
//        this.selectOrValue = selectOrValue;
//    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        super.accept(astVisitor);
        astVisitor.visit(this);

        for (int i = 0; i < this.resultColumns.size(); i++)
            if (this.resultColumns.get(i) != null)
                this.resultColumns.get(i).accept(astVisitor);

        for (int j = 0; j < resultColumns.size(); j++)
            if (this.resultColumns.get(j) != null)
                this.resultColumns.get(j).accept(astVisitor);

        if (this.selTableAm != null)
            this.selTableAm.accept(astVisitor);

        if (this.expSqlAMWhere != null)
            this.expSqlAMWhere.accept(astVisitor);

        if (this.expSqlAMGroupBy != null)
            this.expSqlAMGroupBy.accept(astVisitor);

        if (this.expSqlAMHaving != null)
            this.expSqlAMHaving.accept(astVisitor);
    }
}
