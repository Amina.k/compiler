package Java.AST.temp.SQL.selectOrValue;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.exp.Exp;

import java.util.ArrayList;

public class ExpSqlAM extends Node {

    private boolean OK;

    private String Type;
    private ArrayList<Exp> exps;


    public ExpSqlAM() {
        this.OK = false;
        exps = new ArrayList<>();
    }

    public boolean isOK() {
        return OK;
    }

    public void setOK(boolean OK) {
        this.OK = OK;
    }


    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public ArrayList<Exp> getExps() {
        return exps;
    }

    public void setExps(Exp exps) {
        this.exps.add(exps);
    }

    @Override
    public void accept(ASTVisitor astVisitor) {

        astVisitor.visit(this);
        for (int j = 0; j < this.exps.size(); j++)
            if (this.exps.get(j) != null)
                this.exps.get(j).accept(astVisitor);

    }
}
