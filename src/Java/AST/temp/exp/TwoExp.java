package Java.AST.temp.exp;

import Java.AST.Visitor.ASTVisitor;

public class TwoExp extends OperaterExp {

    private Exp expRight;

    public TwoExp() {

    }

    public Exp getExpRight() {
        return expRight;
    }

    public void setExpRight(Exp expRight) {
        this.expRight = expRight;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if (this.expRight!= null)
            this.expRight.accept(astVisitor);
    }
}
