package Java.AST.temp.exp;

import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.other.AnyName;

import java.util.ArrayList;

public class SecondSellAll extends Exp {
    /// هدول يلي موجودين قبل القاعدرة تبع ال secondSellAll بال expr
    private Exp expSelInExp;
    private String kNot;
///////////

    private SelectStmt selectStmt;  /// ممكن يجي ممكن لا
    private ArrayList<Exp> exps; //// ممكن يجي ممكن لا
    ////------- هدول بيجوو   اوو |||   يلي تحت
    private AnyName databaseName;  //// ممكن يجي ممكنن لا
    private AnyName tableName;//// اكيد بيجي


    public SecondSellAll() {
        exps = new ArrayList<>();
    }

    public String getkNot() {
        return kNot;
    }

    public void setkNot(String kNot) {
        this.kNot = kNot;
    }

    public Exp getExpSelInExp() {
        return expSelInExp;
    }

    public void setExpSelInExp(Exp expSelInExp) {
        this.expSelInExp = expSelInExp;
    }

    public SelectStmt getSelectStmt() {
        return selectStmt;
    }

    public void setSelectStmt(SelectStmt selectStmt) {
        this.selectStmt = selectStmt;
    }

    public ArrayList<Exp> getExps() {
        return exps;
    }

    public void setExps(Exp exps) {
        this.exps.add(exps);
    }

    public AnyName getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(AnyName databaseName) {
        this.databaseName = databaseName;
    }

    public AnyName getTableName() {
        return tableName;
    }

    public void setTableName(AnyName tableName) {
        this.tableName = tableName;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if (this.selectStmt != null)
            this.selectStmt.accept(astVisitor);
        for (int i = 0; i < this.exps.size(); i++)
            if (this.exps.get(i) != null)
                this.exps.get(i).accept(astVisitor);

        if (this.databaseName != null)
            this.databaseName.accept(astVisitor);
        if (this.tableName != null)
            this.tableName.accept(astVisitor);
    }

}
