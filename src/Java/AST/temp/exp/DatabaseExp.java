package Java.AST.temp.exp;

import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.other.AnyName;

import java.util.ArrayList;

public class DatabaseExp extends Exp {

    ArrayList<AnyName> anyNames;


    public DatabaseExp() {
        anyNames = new ArrayList<>();
    }

    public ArrayList<AnyName> getAnyNames() {
        return anyNames;
    }

    public void setAnyNames(AnyName anyName) {
        this.anyNames.add(anyName);
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        for (int i = 0; i < anyNames.size(); i++)
            if (this.anyNames.get(i) != null)
                this.anyNames.get(i).accept(astVisitor);
    }
}
