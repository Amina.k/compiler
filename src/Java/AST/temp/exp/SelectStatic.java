package Java.AST.temp.exp;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.SQL.selectOrValue.SelectOrValue;

public class SelectStatic {

    public static SelectOrValue selectOrValue;

    public SelectOrValue getSelectOrValue() {
        return selectOrValue;
    }

    public void setSelectOrValue(SelectOrValue selectOrValue) {
        this.selectOrValue = selectOrValue;
    }
}
