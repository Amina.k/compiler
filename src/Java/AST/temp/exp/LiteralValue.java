package Java.AST.temp.exp;

import Java.AST.Visitor.ASTVisitor;

public class LiteralValue extends Exp {

    private String literalValue;

    public LiteralValue() {
    }

    public String getLiteralValue() {
        return literalValue;
    }

    public void setLiteralValue(String literalValue) {
        this.literalValue = literalValue;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
    }
}
