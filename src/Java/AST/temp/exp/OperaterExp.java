package Java.AST.temp.exp;

import Java.AST.Visitor.ASTVisitor;

public class OperaterExp extends Exp {

    private String operator;
    private Exp expLeft;

    public OperaterExp(){
    }

    public Exp getExpLeft() {
        return expLeft;
    }

    public void setExpLeft(Exp expLeft) {
        this.expLeft = expLeft;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if (this.expLeft!= null)
            this.expLeft.accept(astVisitor);
    }
}
