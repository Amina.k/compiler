package Java.AST.temp.exp;

import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.SQL.selectOrValue.SelectOrValue;
import Java.AST.temp.SQL.selectStmt.OrderingTerm;

import java.util.ArrayList;

public class SelectStmt extends Exp {

    private SelectOrValue selectOrValue;
    private ArrayList<Exp> exps;
    private ArrayList<OrderingTerm> orderingTerms;

    public SelectStmt() {
        exps = new ArrayList<>();
        orderingTerms = new ArrayList<>();
    }

    public SelectOrValue getSelectOrValue() {
        return selectOrValue;
    }

    public void setSelectOrValue(SelectOrValue selectOrValue) {
        this.selectOrValue = selectOrValue;
    }

    public ArrayList<Exp> getExps() {
        return exps;
    }

    public void setExps(Exp exps) {
        this.exps.add(exps);
    }

    public ArrayList<OrderingTerm> getOrderingTerms() {
        return orderingTerms;
    }

    public void setOrderingTerms(OrderingTerm orderingTerms) {
        this.orderingTerms.add(orderingTerms);
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if (this.selectOrValue != null)
            this.selectOrValue.accept(astVisitor);

        for (int i = 0; i < this.orderingTerms.size(); i++)
            if (this.orderingTerms.get(i) != null)
                this.orderingTerms.get(i).accept(astVisitor);


        for (int i = 0; i < this.exps.size(); i++)
            if (this.exps.get(i) != null)
                this.exps.get(i).accept(astVisitor);

    }
}
