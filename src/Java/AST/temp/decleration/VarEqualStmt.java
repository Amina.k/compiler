package Java.AST.temp.decleration;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.SQL.sqlStmt.SqlStmt;
import Java.AST.temp.condition.IfLine;
import Java.AST.temp.exp.Exp;
import Java.AST.temp.function.CallFunction;
import Java.SymbolTable.Symbol;

/*
var_equal_stmt : var_stmt
|(var_stmt '=' ( (javaIFline_stmt) |sql_stmt  | expr |call_function | read_json) ) ';' ; // new edit amina   just keep ifline

 */


public class VarEqualStmt extends Node {

    private boolean ok;

    private DeclerationVariable declerationVariable;    ///var stmt
    /// add condition stmt and of line
    private IfLine ifLine;
    private SqlStmt sqlStmt;
    private Exp exp;
    private CallFunction callFunction;

    private Symbol symbol;

    public VarEqualStmt() {
        ok = false;
    }

    public IfLine getIfLine() {
        return ifLine;
    }

    public void setIfLine(IfLine ifLine) {
        this.ifLine = ifLine;
    }

    public Exp getExp() {
        return exp;
    }

    public void setExp(Exp exp) {
        this.exp = exp;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public SqlStmt getSqlStmt() {
        return sqlStmt;
    }

    public void setSqlStmt(SqlStmt sqlStmt) {
        this.sqlStmt = sqlStmt;
    }

    public DeclerationVariable getDeclerationVariable() {
        return declerationVariable;
    }

    public void setDeclerationVariable(DeclerationVariable declerationVariable) {
        this.declerationVariable = declerationVariable;
    }

    public CallFunction getCallFunction() {
        return callFunction;
    }

    public void setCallFunction(CallFunction callFunction) {
        this.callFunction = callFunction;
    }

    public Symbol getSymbol() {
        return symbol;
    }

    public void setSymbol(Symbol symbol) {
        this.symbol = symbol;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

        if (this.declerationVariable != null)
            this.declerationVariable.accept(astVisitor);
        if (this.ifLine != null)
            this.ifLine.accept(astVisitor);
        if (this.sqlStmt != null)
            this.sqlStmt.accept(astVisitor);
        if (this.exp != null)
            this.exp.accept(astVisitor);
        if (this.callFunction != null)
            this.callFunction.accept(astVisitor);
    }
}
