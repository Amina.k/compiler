package Java.AST.temp.decleration;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.other.AnyName;
import Java.SymbolTable.Symbol;

public class DeclerationVariable extends Node {

    private boolean ok;

    private String nameVar;
    private String Var;

    private Symbol symbolVariable;

    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public int getCol() {
        return super.getCol();
    }

    public DeclerationVariable() {
        ok = false;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public String getVar() {
        return Var;
    }

    public void setVar(String var) {
        Var = var;
    }

    public String getNameVar() {
        return nameVar;
    }

    public void setNameVar(String nameValue) {
        this.nameVar = nameValue;
    }

    public Symbol getSymbolVariable() {
        return symbolVariable;
    }

    public void setSymbolVariable(Symbol symbolVariable) {
        this.symbolVariable = symbolVariable;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

    }
}
