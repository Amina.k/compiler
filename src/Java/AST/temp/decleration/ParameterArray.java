package Java.AST.temp.decleration;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.other.AnyName;

import java.util.ArrayList;

public class ParameterArray extends Node {


//    paramete_array:'['(  any_name_item | number_item )? ']'   (';')?;
//    any_name_item :(any_name ) (',' (any_name ))* ;
//    number_item: ( signed_number) (',' ( signed_number))* ;
//

    private AnyNameItem anyNameItem;
    private NumberItem numberItem;
    private boolean ok;

    public AnyNameItem getAnyNameItem() {
        return anyNameItem;
    }

    public void setAnyNameItem(AnyNameItem anyNameItem) {
        this.anyNameItem = anyNameItem;
    }

    public NumberItem getNumberItem() {
        return numberItem;
    }

    public void setNumberItem(NumberItem numberItem) {
        this.numberItem = numberItem;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if (this.numberItem != null)
            this.numberItem.accept(astVisitor);

        if (this.anyNameItem != null)
            this.anyNameItem.accept(astVisitor);
    }
}
