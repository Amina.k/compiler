package Java.AST.temp.decleration;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.exp.LiteralValue;
import Java.AST.temp.function.CallFunction;
import Java.AST.temp.other.AnyName;

/*

 (any_name signal ASSIGN ('+'|'-')?)?(increament_var | discreament_var | literal_value|call_function|read_json) ;

 */

public class DifferentKindOfForIncreament extends Node {

    private boolean ok;

    private AnyName anyName;
    private String firstSignal;
    private String secondSignal;

    private IncrDecr incrDecr;

    private LiteralValue literalValue;
    private CallFunction callFunction;


    public DifferentKindOfForIncreament() {
        ok = false;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public AnyName getAnyName() {
        return anyName;
    }

    public void setAnyName(AnyName anyName) {
        this.anyName = anyName;
    }

    public String getFirstSignal() {
        return firstSignal;
    }

    public void setFirstSignal(String firstSignal) {
        this.firstSignal = firstSignal;
    }

    public String getSecondSignal() {
        return secondSignal;
    }

    public void setSecondSignal(String secondSignal) {
        this.secondSignal = secondSignal;
    }

    public IncrDecr getIncrDecr() {
        return incrDecr;
    }

    public void setIncrDecr(IncrDecr incrDecr) {
        this.incrDecr = incrDecr;
    }


    public LiteralValue getLiteralValue() {
        return literalValue;
    }

    public void setLiteralValue(LiteralValue literalValue) {
        this.literalValue = literalValue;
    }

    public CallFunction getCallFunction() {
        return callFunction;
    }

    public void setCallFunction(CallFunction callFunction) {
        this.callFunction = callFunction;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if (this.anyName != null)
            this.anyName.accept(astVisitor);
        if (this.incrDecr != null)
            this.incrDecr.accept(astVisitor);
        if (this.literalValue != null)
            this.literalValue.accept(astVisitor);
        if (this.callFunction != null)
            this.callFunction.accept(astVisitor);

    }
}
