package Java.AST.temp.decleration;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.other.SignedNumber;

import java.util.ArrayList;

public class NumberItem extends Node {

    private ArrayList<Integer> signedNumber = new ArrayList<>();
    private SignedNumber signedNumber_;

    public SignedNumber getSignedNumber_() {
        return signedNumber_;
    }

    public void setSignedNumber_(SignedNumber signedNumber_) {
        this.signedNumber_ = signedNumber_;
    }

    private boolean ok;

    public ArrayList<Integer> getSignedNumber() {
        return signedNumber;
    }

    public void setSignedNumber(ArrayList<Integer> signedNumber) {
        this.signedNumber = signedNumber;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
    }
}
