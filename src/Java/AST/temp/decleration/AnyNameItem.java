package Java.AST.temp.decleration;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.other.AnyName;

import java.util.ArrayList;

public class AnyNameItem extends Node {

    private ArrayList<AnyName> anyNames = new ArrayList<>();
    private boolean ok;

    public ArrayList<AnyName> getAnyNames() {
        return anyNames;
    }

    public void setAnyNames(ArrayList<AnyName> anyNames) {
        this.anyNames = anyNames;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        super.accept(astVisitor);
        for (int i = 0; i < anyNames.size(); i++)
            if (this.anyNames.get(i) != null)
                this.anyNames.get(i).accept(astVisitor);
    }
}
