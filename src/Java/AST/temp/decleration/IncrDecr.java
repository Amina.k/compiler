package Java.AST.temp.decleration;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.other.AnyName;

public class IncrDecr extends Node {

    private boolean ok;

    private AnyName anyName;
    private String signedNumber;
    private String state;

    public IncrDecr() {
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public AnyName getAnyName() {
        return anyName;
    }

    public void setAnyName(AnyName anyName) {
        this.anyName = anyName;
    }

    public String getSignedNumber() {
        return signedNumber;
    }

    public void setSignedNumber(String signedNumber) {
        this.signedNumber = signedNumber;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if (this.anyName != null)
            this.anyName.accept(astVisitor);
    }
}
