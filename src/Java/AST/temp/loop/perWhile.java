package Java.AST.temp.loop;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;

public class perWhile extends Node {

    private boolean ok ;

    public perWhile() {
        ok = false;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
    }
}
