package Java.AST.temp.loop;

import Java.AST.Visitor.ASTVisitor;

public class DoWhile extends  While{

    String k_do ;

    public String getK_do() {
        return k_do;
    }

    public void setK_do(String k_do) {
        this.k_do = k_do;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        super.accept(astVisitor);
    }
}
