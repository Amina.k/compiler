package Java.AST.temp.loop;

import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.exp.Exp;
import Java.AST.temp.function.CallFunction;
import Java.SymbolTable.Scope;

import java.util.ArrayList;

public class Switch extends perSwitch {


    private String argumentName;
    private CallFunction callFunction;
    private ArrayList<CaseSwitch> caseSwitches;
    private DefaultSwitch defaultSwitch;

    private Scope scope;

    public Switch() {
        caseSwitches = new ArrayList<>();
    }

    public String getArgumentName() {
        return argumentName;
    }

    public void setArgumentName(String argumentName) {
        this.argumentName = argumentName;
    }

    public CallFunction getCallFunction() {
        return callFunction;
    }

    public void setCallFunction(CallFunction callFunction) {
        this.callFunction = callFunction;
    }

    public ArrayList<CaseSwitch> getCaseSwitches() {
        return caseSwitches;
    }

    public void setCaseSwitches(CaseSwitch caseSwitches) {
        this.caseSwitches.add(caseSwitches);
    }

    public DefaultSwitch getDefaultSwitch() {
        return defaultSwitch;
    }

    public void setDefaultSwitch(DefaultSwitch defaultSwitch) {
        this.defaultSwitch = defaultSwitch;
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public int getCol() {
        return super.getCol();
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
//        if (this.expArg != null)
//            this.expArg.accept(astVisitor);
        super.accept(astVisitor);

        if (this.callFunction != null)
            this.callFunction.accept(astVisitor);
        for (int i = 0; i < caseSwitches.size(); i++) {
            System.out.println("mamammamamam");
            if (this.caseSwitches.get(i) != null)
                this.caseSwitches.get(i).accept(astVisitor);
        }
        if (this.defaultSwitch != null)
            this.defaultSwitch.accept(astVisitor);


    }
}
