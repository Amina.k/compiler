package Java.AST.temp.loop;

import Java.AST.Visitor.ASTVisitor;

public class WhileNormal extends While {

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
    }
}
