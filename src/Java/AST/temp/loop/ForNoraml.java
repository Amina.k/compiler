package Java.AST.temp.loop;

import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.decleration.DifferentKindOfForIncreament;
import Java.AST.temp.exp.Exp;
import Java.AST.temp.other.BodyAllStmt;
import Java.SymbolTable.Symbol;

public class ForNoraml extends For {

    private Exp exp;
    private DifferentKindOfForIncreament differentKindOfForIncreament;
    private BodyAllStmt bodyAllStmt;

    private Symbol symbol;

    public ForNoraml() {
    }

    public Exp getExp() {
        return exp;
    }

    public void setExp(Exp exp) {
        this.exp = exp;
    }

    public BodyAllStmt getBodyAllStmt() {
        return bodyAllStmt;
    }

    public void setBodyAllStmt(BodyAllStmt bodyAllStmt) {
        this.bodyAllStmt = bodyAllStmt;
    }

    public DifferentKindOfForIncreament getDifferentKindOfForIncreament() {
        return differentKindOfForIncreament;
    }

    public void setDifferentKindOfForIncreament(DifferentKindOfForIncreament differentKindOfForIncreament) {
        this.differentKindOfForIncreament = differentKindOfForIncreament;
    }

    public Symbol getSymbol() {
        return symbol;
    }

    public void setSymbol(Symbol symbol) {
        this.symbol = symbol;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if (this.exp != null)
            this.exp.accept(astVisitor);
        if (this.differentKindOfForIncreament != null)
            this.differentKindOfForIncreament.accept(astVisitor);
        if (this.bodyAllStmt != null)
            this.bodyAllStmt.accept(astVisitor);


    }
}
