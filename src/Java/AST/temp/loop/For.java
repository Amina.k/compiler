package Java.AST.temp.loop;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.decleration.DeclerationVariable;

public class For extends Node {

    private boolean ok;

    private DeclerationVariable decVar;

    public For() {
        ok = false;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public DeclerationVariable getDecVar() {
        return decVar;
    }

    public void setDecVar(DeclerationVariable decVar) {
        this.decVar = decVar;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        this.decVar.accept(astVisitor);
        if (this.decVar!= null)
            this.decVar.accept(astVisitor);
    }
}
