package Java.AST.temp.loop;

import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.condition.ConditionStmt;
import Java.AST.temp.other.BodyAllStmt;

public class While extends perWhile {


    private String k_while;
    private BodyAllStmt bodyAllStmt;
    private ConditionStmt conditionStmt;

    public String getK_while() {
        return k_while;
    }

    public void setK_while(String k_while) {
        this.k_while = k_while;
    }

    public BodyAllStmt getBodyAllStmt() {
        return bodyAllStmt;
    }

    public void setBodyAllStmt(BodyAllStmt bodyAllStmt) {
        this.bodyAllStmt = bodyAllStmt;
    }

    public ConditionStmt getConditionStmt() {
        return conditionStmt;
    }

    public void setConditionStmt(ConditionStmt conditionStmt) {
        this.conditionStmt = conditionStmt;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        super.accept(astVisitor);
        if (this.bodyAllStmt != null)
            this.bodyAllStmt.accept(astVisitor);
        if (this.conditionStmt != null)
        {
            System.out.println("cocopp");
            this.conditionStmt.accept(astVisitor);
        }
    }
}
