package Java.AST.temp.loop;

import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.other.AnyName;
import Java.AST.temp.other.BodyAllStmt;

// form
    /*
    for(Object t : Arrayname)
    so Arrayname is anyname
    and Object t is decleration variable and this found in class for

     */
public class ForEach extends For {

    private AnyName anyName;
    private BodyAllStmt bodyAllStmt;


    public ForEach() {
    }

    public BodyAllStmt getBodyAllStmt() {
        return bodyAllStmt;
    }

    public void setBodyAllStmt(BodyAllStmt bodyAllStmt) {
        this.bodyAllStmt = bodyAllStmt;
    }

    public AnyName getAnyName() {
        return anyName;
    }

    public void setAnyName(AnyName anyName) {
        this.anyName = anyName;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if (this.anyName!= null)
            this.anyName.accept(astVisitor);
        if (this.bodyAllStmt!= null)
            this.bodyAllStmt.accept(astVisitor);


    }
}
