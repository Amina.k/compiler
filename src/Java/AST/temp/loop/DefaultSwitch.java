package Java.AST.temp.loop;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.other.JavaAnyStmt;

import java.util.ArrayList;


public class DefaultSwitch extends Node {

    private boolean ok;

    private ArrayList<JavaAnyStmt> javaAnyStmts;

    public DefaultSwitch() {
        ok = false;
        javaAnyStmts = new ArrayList<>();
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public ArrayList<JavaAnyStmt> getJavaAnyStmts() {
        return javaAnyStmts;
    }

    public void setJavaAnyStmts(JavaAnyStmt javaAnyStmts) {
        this.javaAnyStmts.add(javaAnyStmts);
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

        for (int i = 0; i < this.javaAnyStmts.size(); i++)
            if (this.javaAnyStmts.get(i)!= null)
                this.javaAnyStmts.get(i).accept(astVisitor);
    }
}
