package Java.AST.temp.loop;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.other.JavaAnyStmt;
import Java.SymbolTable.Scope;

import java.util.ArrayList;


public class CaseSwitch extends Node {

    private boolean ok;

    private String valueCase;
    private ArrayList<JavaAnyStmt> javaAnyStmts;

    private Scope scope;

    public CaseSwitch() {
        ok = false;
        javaAnyStmts = new ArrayList<>();
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public String getValueCase() {
        return valueCase;
    }

    public void setValueCase(String valueCase) {
        this.valueCase = valueCase;
    }

    public ArrayList<JavaAnyStmt> getJavaAnyStmts() {
        return javaAnyStmts;
    }

    public void setJavaAnyStmts(JavaAnyStmt javaAnyStmts) {
        this.javaAnyStmts.add(javaAnyStmts);
    }

    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public int getCol() {
        return super.getCol();
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        for (int i = 0; i < this.javaAnyStmts.size(); i++)
            if (this.javaAnyStmts.get(i)!= null)
                this.javaAnyStmts.get(i).accept(astVisitor);

    }
}
