package Java.AST.Visitor;


import Java.AST.Parse;
import Java.AST.temp.SQL.CreateTable.*;
import Java.AST.temp.SQL.CreateTable.CreateTableStatmt;
import Java.AST.temp.SQL.selectOrValue.ExpSqlAM;
import Java.AST.temp.SQL.selectOrValue.SelTableAm;
import Java.AST.temp.SQL.selectOrValue.SelectOrValue;
import Java.AST.temp.SQL.selectOrValue.tableOrSub.*;
import Java.AST.temp.SQL.selectStmt.OrderingTerm;
import Java.AST.temp.SQL.selectStmt.ResultColumn;
import Java.AST.temp.SQL.sqlStmt.*;
import Java.AST.temp.condition.*;
import Java.AST.temp.decleration.*;
import Java.AST.temp.exp.*;
import Java.AST.temp.function.*;
import Java.AST.temp.loop.*;
import Java.AST.temp.other.*;
import Java.AST.temp.print.PrintStmt;
import Java.ErrorSample;
import Java.Main;
import Java.SymbolTable.Scope;
import Java.SymbolTable.Symbol;
import Java.SymbolTable.Type;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import java.util.Map;

public class BaseASTVisitor implements ASTVisitor {

    ErrorSample errorSample = new ErrorSample();

    SelectOrValue selectOrValueParent;
    String typeWhere = "";
    Symbol symbolWhere;
    Symbol sqlSelectSymbol;

    @Override
    public void visit(Parse p) {
        System.out.println(" ast Parse ");
    }

    @Override
    public void visit(ConditionStmt conditionStmt) {
        System.out.println("ast ConditionStmt");
        Symbol symbolCondition = conditionStmt.getSymbol();
        Scope currentScope = symbolCondition.getScope();
        if (symbolCondition != null) {
            Scope parentScope = symbolCondition.getScope();
            if (!symbolCondition.getName().equals("operator between two exp")) {
                if (symbolCondition.getType() != null)
                    if (!(symbolCondition.getType().getValue().equalsIgnoreCase("true")) && !(symbolCondition.getType().getValue().equalsIgnoreCase("false"))) {
                        if (!checkSymbol(parentScope.getParent(), "var " + symbolCondition.getType().getValue()))
                            writeMsg("This Variable " + symbolCondition.getName() + " is not declared in line " + conditionStmt.getLine() + " and col " + conditionStmt.getCol());
                    }
            } else {
                if (symbolCondition.getExpression() != null) {
                    String value1 = null, value2 = null;
                    if (symbolCondition.getExpression().isIfVariable1()) {
                        value1 = symbolCondition.getExpression().getValue1();
                        if (!checkSymbol(parentScope.getParent(), "var " + value1))
                            writeMsg("This Variable " + value1 + " is not declared in line " + conditionStmt.getLine() + " and col " + conditionStmt.getCol());

                    }
                    if (symbolCondition.getExpression().isIfVariable2()) {
                        value2 = symbolCondition.getExpression().getValue2();
                        if (!checkSymbol(parentScope.getParent(), "var " + value2))
                            writeMsg("This Variable " + value2 + " is not declared in line " + conditionStmt.getLine() + " and col " + conditionStmt.getCol());

                    }
                }

            }
        }
    }

    @Override
    public void visit(IF anIf) {
        System.out.println("ast IF");
    }

    @Override
    public void visit(Else anElse) {

    }

    @Override
    public void visit(IfLine ifLine) {

        System.out.println("ast IfLine");


    }

    @Override
    public void visit(BodyIfLine bodyIfLine) {

    }

    @Override
    public void visit(CompleteIfLine completeIfLine) {

    }

    @Override
    public void visit(RealizedCondition realizedCondition) {
        System.out.println("ast Realzed Condtion");


        if (realizedCondition.getSymbol() != null) {
            Symbol symbol = realizedCondition.getSymbol();
            if (symbol.getExpression() != null) {
                String value1 = null;
                if (symbol.getExpression().isIfVariable1()) {
                    value1 = symbol.getExpression().getValue1();
                    System.out.println("the value1 : " + value1);


                    Scope varScope = symbol.getScope();
                    if (symbol.getType() != null) {
                        System.out.println("1");
                        if (!(value1.startsWith("\"")) && !(value1.equals("string"))) {

                            System.out.println("2");
                            if (!checkSymbol(varScope.getParent(), "var " + value1)) {
                                System.out.println("3");
                                writeMsg("This Variable " + value1 + " is not declared in line " + realizedCondition.getLine() + " and col " + realizedCondition.getCol());
                            }
                        }
                    }
                }
            }
            System.out.println("the symbol name : " + symbol.getName());
            System.out.println("the symbol id : " + symbol.getScope().getId());
            if (realizedCondition.getDifferentKindOfForIncreament().getIncrDecr() != null) {
                String nameVar = " ";
                nameVar = realizedCondition.getDifferentKindOfForIncreament().getIncrDecr().getAnyName().getAnyName();

                if (!checkSymbol(symbol.getScope().getParent(), "var " + nameVar)) {

                    writeMsg("This Variable " + nameVar + " is not declared in line " + realizedCondition.getLine() + " and col " + realizedCondition.getCol());
                }

            } else if (realizedCondition.getDifferentKindOfForIncreament().getAnyName().getAnyName() != null) {
                String nameVar = " ";
                nameVar = realizedCondition.getDifferentKindOfForIncreament().getAnyName().getAnyName();

                if (!checkSymbol(symbol.getScope().getParent(), "var " + nameVar)) {

                    writeMsg("This Variable " + nameVar + " is not declared in line " + realizedCondition.getLine() + " and col " + realizedCondition.getCol());
                }

            }

        }
    }

    @Override
    public void visit(UnRealizedConditon unRealizedConditon) {
        System.out.println("ast UnRealzed Condtion");


        if (unRealizedConditon.getSymbol() != null) {
            Symbol symbol = unRealizedConditon.getSymbol();
            if (symbol.getExpression() != null) {
                String value1 = null;
                if (symbol.getExpression().isIfVariable1()) {
                    value1 = symbol.getExpression().getValue1();
                    System.out.println("the value1 : " + value1);


                    Scope varScope = symbol.getScope();
                    if (symbol.getType() != null) {
                        System.out.println("1");
                        if (!(value1.startsWith("\"")) && !(value1.equals("string"))) {

                            System.out.println("2");
                            if (!checkSymbol(varScope.getParent(), "var " + value1)) {
                                System.out.println("3");
                                writeMsg("This Variable " + value1 + " is not declared in line " + unRealizedConditon.getLine() + " and col " + unRealizedConditon.getCol());
                            }
                        }
                    }
                }
            }
            System.out.println("the symbol name : " + symbol.getName());
            System.out.println("the symbol id : " + symbol.getScope().getId());
            if (unRealizedConditon.getDifferentKindOfForIncreament().getIncrDecr() != null) {
                String nameVar = " ";
                nameVar = unRealizedConditon.getDifferentKindOfForIncreament().getIncrDecr().getAnyName().getAnyName();

                if (!checkSymbol(symbol.getScope().getParent(), "var " + nameVar)) {

                    writeMsg("This Variable " + nameVar + " is not declared in line " + unRealizedConditon.getLine() + " and col " + unRealizedConditon.getCol());
                }
            } else if (unRealizedConditon.getDifferentKindOfForIncreament().getAnyName().getAnyName() != null) {
                String nameVar = " ";
                nameVar = unRealizedConditon.getDifferentKindOfForIncreament().getAnyName().getAnyName();

                if (!checkSymbol(symbol.getScope().getParent(), "var " + nameVar)) {

                    writeMsg("This Variable " + nameVar + " is not declared in line " + unRealizedConditon.getLine() + " and col " + unRealizedConditon.getCol());
                }

            }
        }
    }

    @Override
    public void visit(IfNormal ifNormal) {
        System.out.println("ast IfNormal");
    }

    @Override
    public void visit(DeclerationVariable declerationVariable) {
        System.out.println("ast DeclerationVariable");
        System.out.println("name var is " + declerationVariable.getNameVar());
        System.out.println("Type is " + declerationVariable.getVar());
    }

    @Override
    public void visit(DifferentKindOfForIncreament differentKindOfForIncreament) {
        System.out.println("ast Incremnt");
        System.out.println("State IncDec is " + differentKindOfForIncreament.getFirstSignal());

        //     String nameVar = differentKindOfForIncreament.getIncrDecr().getAnyName().getAnyName();

    }

    @Override
    public void visit(IncrDecr incrDecr) {

    }

    @Override
    public void visit(VarEqualStmt varEqualStmt) {

        if (varEqualStmt.getSymbol() != null) {
            Symbol symbol = varEqualStmt.getSymbol();
            if (sqlSelectSymbol == null)
                sqlSelectSymbol = symbol;
            else {
                if ((!symbol.getType().getName().equals(sqlSelectSymbol.getType().getName())) && (symbol.getType().getValue().equals(sqlSelectSymbol.getType().getValue()))) {
                    writeMsg("you are using another type in variable in line " + symbol.getLine() + " and col " + symbol.getCol());
                }
            }

        }


        Symbol varSymbol = varEqualStmt.getDeclerationVariable().getSymbolVariable();
        if (varSymbol == null)
            writeMsg("Error the variable is founded in line " + varEqualStmt.getDeclerationVariable().getLine() + " and col " + varEqualStmt.getDeclerationVariable().getCol());
        else {
            Scope varScope = varSymbol.getScope();
            String nameSymbol = varSymbol.getName();
            if (varSymbol.getType() != null) {
                if (!(varSymbol.getType().getValue().startsWith("\"")) && varSymbol.getType().getName().equals("string")) {
                    if (!checkSymbol(varScope, "var " + varSymbol.getType().getValue())) {
                        writeMsg("This Variable " + varSymbol.getType().getValue() + " is not declared in line " + varEqualStmt.getDeclerationVariable().getLine() + " and col " + varEqualStmt.getDeclerationVariable().getCol());
                    }
                }
            }
            if (nameSymbol.contains("var ")) {
                if (checkSymbolInScopeParent(varScope, nameSymbol)) {
                    System.out.println(varSymbol.getName());
                    if (checkSymbolInScopeParent(varScope, varSymbol.getName())) {
                        writeMsg("Error the variable is founded in line " + varEqualStmt.getDeclerationVariable().getLine() + " and col " + varEqualStmt.getDeclerationVariable().getCol());
                    }
                }
            } else if (!nameSymbol.contains("var ")) {
                Symbol symbolType = editSymbol(varScope, "var " + varSymbol.getName());
//                writeMsg(symbolType.getName()+symbolType.getType().getName());
                if (!checkSymbol(varScope, "var " + varSymbol.getName())) {
                    writeMsg("This Variable " + varSymbol.getName() + " is not declared in line " + varEqualStmt.getDeclerationVariable().getLine() + " and col " + varEqualStmt.getDeclerationVariable().getCol());
                } else if (varSymbol.getType() != null && symbolType.getType() != null) {
                    if (!(varSymbol.getType().getName().equals(symbolType.getType().getName()))) {
                        writeMsg("you are using another type in variable in line " + varEqualStmt.getDeclerationVariable().getLine() + " and col " + varEqualStmt.getDeclerationVariable().getCol());
                        writeMsg("type first " + varSymbol.getType().getName() + " type second " + symbolType.getType().getName());
                    }
                }
            }
        }
    }

    @Override
    public void visit(Exp exp) {
        System.out.println("ast Exp  ");
    }

    @Override
    public void visit(DatabaseExp databaseExp) {
        System.out.println(" ast DatabaseExp ");
        /*    ArrayList<AnyName> anyNames;
         */
        for (int i = 0; i < databaseExp.getAnyNames().size(); i++)
            System.out.println("dataBase name is : " + databaseExp.getAnyNames().get(i).getAnyName());
    }

    @Override
    public void visit(FunctionCallExp functionCallExp) {
        System.out.println(" ast FunctionCallExp ");
        /*  private AnyName functionName;
    private ArrayList<Exp> parameter;
*/

        System.out.println("Call function Name is : " + functionCallExp.getFunctionName().getAnyName());

    }

    @Override
    public void visit(LiteralValue literalValue) {

        System.out.println("ast LiteralValue : " + literalValue.getLiteralValue());
    }

    @Override
    public void visit(OperaterExp operaterExp) {
        System.out.println("ast OperaterExp");
        /*
    private String operator;
    private Exp expLeft;*/
        System.out.println("Operator is : " + operaterExp.getOperator().toString());
    }

    @Override
    public void visit(TwoExp twoExp) {
        System.out.println("ast TwoExp");
    }

    @Override
    public void visit(Switch anSwitch) {
        System.out.println("ast switch");
        Scope scopeSwitch = anSwitch.getScope();
        String nameCondition = anSwitch.getArgumentName();
        if (nameCondition != null && !(nameCondition.startsWith("\""))) {
//            writeMsg(scopeSwitch.getParent().getId());
            if (!checkSymbol(scopeSwitch.getParent(), "var " + nameCondition))
                writeMsg("This Variable " + nameCondition + " is not declared in line " + anSwitch.getLine() + " and col " + anSwitch.getCol());
        }

    }

    @Override
    public void visit(CaseSwitch caseSwitch) {

        System.out.println("ast Case switch");
        System.out.println("Value case is  " + caseSwitch.getValueCase());
        Scope scope = caseSwitch.getScope();
        String nameCase = caseSwitch.getValueCase();
        if (!checkSymbol(scope.getParent(), "var " + nameCase))
            writeMsg("This Variable " + nameCase + " is not declared in line " + caseSwitch.getLine() + " and col " + caseSwitch.getCol());
    }

    @Override
    public void visit(DefaultSwitch defaultSwitch) {

    }

    @Override
    public void visit(DoWhile doWhile) {

    }

    @Override
    public void visit(perSwitch perSwitch) {

    }

    @Override
    public void visit(perWhile perWhile) {

    }

    @Override
    public void visit(While anWhile) {

    }

    @Override
    public void visit(WhileNormal whileNormal) {

    }

    @Override
    public void visit(For anFor) {
        System.out.println("ast For");
    }

    @Override
    public void visit(ForEach forEach) {
        System.out.println("ast ForEach");
        String varName = forEach.getDecVar().getNameVar();
        String valueName = forEach.getAnyName().getAnyName();
        System.out.println("var name " + varName);
        System.out.println("value name" + valueName);


    }

    @Override
    public void visit(ForNoraml forNoraml) {
        System.out.println("ast ForNoraml");
        String fVarName = null;
        String secName;

        if (forNoraml.getDecVar() != null) {
            fVarName = forNoraml.getDecVar().getNameVar();
        }
        if (forNoraml.getSymbol() != null) {
            Symbol symbol = forNoraml.getSymbol();
            if (symbol.getExpression() != null) {
                String value1 = null, value2 = null;
                if (symbol.getExpression().isIfVariable1()) {
                    value1 = symbol.getExpression().getValue1();
                }
                if (symbol.getExpression().isIfVariable2()) {
                    value2 = symbol.getExpression().getValue2();
                }
                if ((!fVarName.equals(value1)) && (!fVarName.equals(value2)))
                    writeMsg("This Variable is not declared in line " + forNoraml.getLine() + " and col " + forNoraml.getCol());
                else secName = fVarName;
            }

        }
        if (!forNoraml.getDifferentKindOfForIncreament().getIncrDecr().getAnyName().getAnyName().equals(fVarName))
            writeMsg("This Variable is not declared in line " + forNoraml.getLine() + " and col " + forNoraml.getCol());

    }

    @Override
    public void visit(AnyName anyName) {
        System.out.println("ast anyName");
        System.out.println("Name is : " + anyName.getAnyName());
    }

    @Override
    public void visit(BodyAllStmt bodyAllStmt) {
        System.out.println("ast BodyAllStmt");
    }

    @Override
    public void visit(JavaAnyStmt javaAnyStmt) {
        System.out.println("ast JavaAnyStmt");
    }

    @Override
    public void visit(JavaStmt javaStmt) {
        System.out.println("ast JavaStmt");
    }

    @Override
    public void visit(KeyWord keyWord) {
        System.out.println("ast KeyWord");
    }

    @Override
    public void visit(Function function) {
        System.out.println("ast Function");

    }

    @Override
    public void visit(ArgumentList argumentList) {
        System.out.println("ast ArgumentList");
    }

    @Override
    public void visit(CallFunction callFunction) {
        System.out.println("ast CallFunction");
        Scope scopeCallFun = callFunction.getScope();
        String idName = scopeCallFun.getId();
        System.out.println("in call function");
        System.out.println(idName);
//        fun_toto
//        callFun batoul
        String temp = idName.substring(8, idName.length());
        System.out.println("temp is " + temp);

        if (!(checkScope("fun_" + temp))) {
            writeMsg("This function " + idName + " is not declared in line " + callFunction.getLine() + " and col " + callFunction.getCol());
        }
    }

    @Override
    public void visit(FunctionDefinition functionDefinition) {
        System.out.println("ast FunctionDefinition");
        Symbol symbolReturnFunction = functionDefinition.getSymbolReturn();
        if (symbolReturnFunction != null) {
            if (symbolReturnFunction.getExpression() != null) {
                if (symbolReturnFunction.getExpression().isIfVariable1()) {
                    if (!checkSymbol(symbolReturnFunction.getScope(), "var " + symbolReturnFunction.getExpression().getValue1()))
                        writeMsg("This Variable " + symbolReturnFunction.getExpression().getValue1() + " is not declared in line " + functionDefinition.getLine() + " and col " + functionDefinition.getCol());
                }
                if (symbolReturnFunction.getExpression().isIfVariable2()) {
                    if (!checkSymbol(symbolReturnFunction.getScope(), "var " + symbolReturnFunction.getExpression().getValue2()))
                        writeMsg("This Variable " + symbolReturnFunction.getExpression().getValue2() + " is not declared in line " + functionDefinition.getLine() + " and col " + functionDefinition.getCol());
                }
            } else if (symbolReturnFunction.getType().getName().equals(Type.STRING_CONST) && !symbolReturnFunction.getType().getValue().startsWith("\"")) {
                if (!checkSymbol(symbolReturnFunction.getScope(), "var " + symbolReturnFunction.getType().getValue()))
                    writeMsg("This Variable return " + symbolReturnFunction.getType().getValue() + " is not declared in line " + functionDefinition.getLine() + " and col " + functionDefinition.getCol());
            }
        }
    }


    @Override
    public void visit(ParmeterList parmeterList) {
        System.out.println("ast ParameterList");

    }

    @Override
    public void visit(Item item) {
        System.out.println("ast Item");
        System.out.println("item is : " + item.getDecVar().getNameVar());


    }

    @Override
    public void visit(SignedNumber signedNumber) {
        System.out.println("ast SignedNumber : " + signedNumber.getSiNumber());
    }

    @Override
    public void visit(PrintStmt printStmt) {
        System.out.println("ast PrintStmt");
        Scope scope = printStmt.getScope();
        ArrayList<String> namesVariable = printStmt.getNamesVariable();
        for (int i = 0; i < printStmt.getNamesVariable().size(); i++) {
            if (!checkSymbol(scope.getParent(), "var " + namesVariable.get(i)))
                writeMsg("This Variable is not declared in line " + printStmt.getLine() + " and col " + printStmt.getCol());
        }
        namesVariable = checkPrint(scope, "operator between two exp");
        for (int i = 0; i < namesVariable.size(); i++) {
            if (!checkSymbol(scope.getParent(), "var " + namesVariable.get(i)))
                writeMsg("This Variable is not declared in line " + printStmt.getLine() + " and col " + printStmt.getCol());
        }
    }

    private ArrayList<String> checkPrint(Scope scope, String key) {
        Scope parentScope = scope;
        Symbol symbol = new Symbol();
        ArrayList<String> namesVariable = new ArrayList<>();
        for (Map.Entry<String, Symbol> entry : parentScope.symbolMap.entrySet()) {
            if (entry.getKey().equals(key)) {
                symbol = entry.getValue();
                if (symbol.getExpression() != null) {
                    if (symbol.getExpression().isIfVariable1())
                        namesVariable.add(symbol.getExpression().getValue1());
                    if (symbol.getExpression().isIfVariable2())
                        namesVariable.add(symbol.getExpression().getValue2());
                }
            }
        }
        return namesVariable;
    }

    @Override
    public void visit(SqlStmtList sqlStmtList) {
        System.out.println("ast SqlStmtList");
    }

    @Override
    public void visit(SqlStmt sqlStmt) {
        System.out.println("ast sqlstmt");
    }

    @Override
    public void visit(JoinClauseSub joinClauseSub) {
        System.out.println(" ast  joinClauseSub");
        System.out.println(" " + joinClauseSub.getOperater());
    }

    @Override
    public void visit(JoinCaluse joinCaluse) {
        System.out.println(" ast  joinCaluse");
    }

    @Override
    public void visit(TableOrSub tableOrSub) {
        System.out.println(" ast  tableOrSub");
    }


    @Override
    public void visit(CreateTableStatmt createTableStatmt) {

        System.out.println(" ast createTableStmt");

        System.out.println("tablename " + createTableStatmt.getTableName());
        Type type = createTableStatmt.getTypetable();
        if (!checkTheType(type)) {
            writeMsg("This name : " + createTableStatmt.getTypetable().getName() + " in line : " + createTableStatmt.getLine() +
                    " is not allowed  !! " + "There is a type  have a same of this name ..");
        }
    }


    @Override
    public void visit(TypeName typeName) {
        System.out.println(" ast typeName ");
    }

    @Override
    public void visit(SelectStmt selectStmt) {
        System.out.println(" ast selectStmt ");
    }

    @Override
    public void visit(SelectOrValue selectOrValue) {

        System.out.println(" ast selectOrValue ");
        System.out.println("Scope is " + selectOrValue.getScope().getId());
        ArrayList<String> resultColumnParent = new ArrayList();
        ArrayList<String> nameTableParent = new ArrayList<>();

//        if (selectOrValue.isParent()) {
        selectOrValueParent = selectOrValue;
        System.out.println("select scope is  " + selectOrValueParent.getScope().getId());
        if (selectOrValueParent.getSelTableAm() != null) {
            for (int i = 0; i < selectOrValueParent.getSelTableAm().getTableOrSubOnes().size(); i++) {
                if (selectOrValueParent.getSelTableAm().getTableOrSubOnes().get(i).getTableName() != null) {
                    if (!checkIfTableExist(selectOrValueParent.getSelTableAm().getTableOrSubOnes().get(i).getTableName())) {
                        writeMsg("the table " + selectOrValueParent.getSelTableAm().getTableOrSubOnes().get(i).getTableName() + " is not exist in line " + selectOrValueParent.getResultColumns().get(0).getLine() + " and col " + selectOrValueParent.getResultColumns().get(0).getCol());
                    } else
                        nameTableParent.add(selectOrValueParent.getSelTableAm().getTableOrSubOnes().get(i).getTableName());
                }
            }
        }
        for (int i = 0; i < selectOrValueParent.getResultColumns().size(); i++) {
            if (selectOrValueParent.getResultColumns().get(i).getColumnName() != null) {
                if (!checkColumnInTable(nameTableParent, selectOrValueParent.getResultColumns().get(i).getColumnName())) {
                    writeMsg("the column " + selectOrValueParent.getResultColumns().get(i).getColumnName() + " is not exist in line " + selectOrValueParent.getResultColumns().get(i).getLine() + " and col " + selectOrValueParent.getResultColumns().get(i).getCol());
                } else {
                    resultColumnParent.add(selectOrValueParent.getResultColumns().get(i).getColumnName());
//                    String typeCol = getTypeColumn(nameTableParent, selectOrValueParent.getResultColumns().get(i).getColumnName());
//                    writeMsg("typeCol is " + typeCol);
//                    if(!checkIfTypeIsExist(typeCol))
//                        writeMsg("the column Type" + selectOrValueParent.getResultColumns().get(i).getColumnName() + " is not exist in line " + selectOrValueParent.getResultColumns().get(i).getLine() + " and col " + selectOrValueParent.getResultColumns().get(i).getCol());
                }
            } else if (selectOrValueParent.getResultColumns().get(i).getStar() != null) {
                resultColumnParent.add(selectOrValueParent.getResultColumns().get(i).getStar());
            }
        }

        String typeColSelectIn = "";

        Symbol symbol = getSymbol(selectOrValue.getScope(), "where ");
        if (symbol != null) {
//            writeMsg(symbolWhere.getName());
            symbolWhere = symbol;
            if (!checkColumnInTable(nameTableParent, symbol.getName())) {
                writeMsg("the column " + symbol.getName() + " is not exist in line " + symbol.getLine() + " and col " + symbol.getCol());
            } else {
                typeWhere = getTypeColumn(nameTableParent, symbol.getName());
            }
        }
        if (!selectOrValue.isParent()) {
            for (int i = 0; i < resultColumnParent.size(); i++) {
                typeColSelectIn = getTypeColumn(nameTableParent, resultColumnParent.get(i));
            }
            if (!typeWhere.equals(typeColSelectIn)) {
                writeMsg("Type is different in line " + symbolWhere.getLine() + " and col " + symbolWhere.getCol());
            }

        }
    }
//
//    boolean checkIfTypeIsExist(String typeCol) {
//        for (int j = 0; j < Main.symbolTable.getDeclaredTypes().size(); j++) {
//            if (Main.symbolTable.getDeclaredTypes().get(j).equals(typeCol)) {
//                return true;
//            }
//        }
//        return false;
//    }

    String getTypeColumn(ArrayList<String> tableNames, String column) {
        for (int i = 0; i < tableNames.size(); i++) {
            for (int j = 0; j < Main.symbolTable.getDeclaredTypes().size(); j++) {
                if (tableNames.get(i).equals(Main.symbolTable.getDeclaredTypes().get(j).getName())) {
                    String typecol = checkColumn(column, j);
                    if (typecol != null)
                        return typecol;

                }
            }
        }
        return null;
    }

    boolean checkIfTableExist(String tableNames) {
        for (int i = 0; i < Main.symbolTable.getDeclaredTypes().size(); i++) {
            if (tableNames.equals(Main.symbolTable.getDeclaredTypes().get(i).getName())) {
                return true;
            }
        }
        return false;
    }

    boolean checkColumnInTable(ArrayList<String> tableNames, String column) {
        for (int i = 0; i < tableNames.size(); i++) {
            for (int j = 0; j < Main.symbolTable.getDeclaredTypes().size(); j++) {
                if (tableNames.get(i).equals(Main.symbolTable.getDeclaredTypes().get(j).getName())) {
                    if (checkColumn(column, j) != null) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public void visit(TableOrSubOne tableOrSubOne) {

        System.out.println(" ast tableOrSubOne ");

    }

    @Override
    public void visit(TableOrSubThree tableOrSubThree) {
        System.out.println(" ast   tableOrSubThree ");
    }

    @Override
    public void visit(TableOrSubTwo tableOrSubTwo) {
        System.out.println(" ast  tableOrSubTwo ");
    }

    @Override
    public void visit(TableSubFather tableSubFather) {
        System.out.println(" ast  tableSubFather ");
    }

    @Override
    public void visit(ExpSqlAM expSqlAM) {
        System.out.println(" ast  expSqlAM ");
        System.out.println(" " + expSqlAM.getType());
    }

    @Override
    public void visit(SelTableAm selTableAm) {
        System.out.println(" ast  selTableAm");
    }

    @Override
    public void visit(OrderingTerm orderingTerm) {
        System.out.println(" ast  orderingTerm ");
    }

    @Override
    public void visit(ResultColumn resultColumn) {

        System.out.println(" ast  resultColumn ");
        System.out.println(" " + resultColumn.getColumnAlias());
    }


    @Override
    public void visit(AggregationFunction aggregationFunction) {
        System.out.println(" ast  aggregation function ");
    }

    @Override
    public void visit(ParameterArray parameterArray) {
        System.out.println(" ast  parameterArray  ");
    }

    @Override
    public void visit(NumberItem numberItem) {
        System.out.println(" ast  number item in array ");

        System.out.println("the number : " + numberItem.getSignedNumber());
    }

    @Override
    public void visit(AnyNameItem anyNameItem) {
        System.out.println(" ast  any name item in array ");

        System.out.println(" the any name :" + anyNameItem.getAnyNames());
    }


    @Override
    public void visit(CreateTypeStmt createTypeStmt) {
        System.out.println(" ast  create type ");

        Type type = createTypeStmt.getType();
        if (!checkTheType(type)) {
            writeMsg("This name : " + createTypeStmt.getType().getName() + " in line : " + createTypeStmt.getLine() +
                    " is not allowed  !! " + "There is a type  have a same of this name ..");

        }
    }

    @Override
    public void visit(ColDefType colDefType) {
        System.out.println(" ast  column defenition ");

        //duplicate column name
        Symbol symbolColumn = colDefType.getSymbolColumn();
        if (symbolColumn == null)
            writeMsg(" in this line : " + colDefType.getLine() + " a column previously defined ..");

        // check if type declared ..
        if (symbolColumn != null && checkType(symbolColumn))
            writeMsg("this type " + symbolColumn.getType().getValue() + " in line : " + colDefType.getLine() + " is not declared ..");


    }

    @Override
    public void visit(ColumnDef columnDef) {

        System.out.println(" ast columnDef ");
        System.out.println(" the line columnDef :" + columnDef.getLine());
        Symbol columnDefSymbol = columnDef.getColDefTablSymbol();

        // duplicate column name .
        if (columnDefSymbol == null)
            writeMsg(" in this line : " + columnDef.getLine() + " a column previously defined ..");


        // check if type declared ..
        if (columnDefSymbol != null && checkType(columnDefSymbol))
            writeMsg("this type " + columnDefSymbol.getType().getValue() + " in line : " + columnDef.getLine() + " is not declared ..");

    }

    private String checkColumn(String column, int i) {
        for (Map.Entry<String, Type> entry : Main.symbolTable.getDeclaredTypes().get(i).getColumns().entrySet()) {
            if (entry.getKey().equals(column)) {
                return entry.getValue().getValue();
//                return true;
            }
        }
        return null;
    }

    private Boolean checkSymbol(Scope scope, String key) {
        Scope parentScope = scope;
        while (true) {
            for (Map.Entry<String, Symbol> entry : parentScope.symbolMap.entrySet()) {
                if (entry.getKey().equals(key)) {
                    return true;
                }
            }
            if (parentScope.getParent() != null) {
                parentScope = parentScope.getParent();
            } else return false;
        }
    }

    private Boolean checkSymbolInScopeParent(Scope scope, String key) {
        Scope parentScope = scope;
        int k = 0;
        while (true) {
            System.out.println("Parent " + parentScope.getId());
            for (Map.Entry<String, Symbol> entry : parentScope.symbolMap.entrySet()) {
                if (entry.getKey().equals(key)) {
                    k++;
                }
                if (k > 1) {
                    return true;
                }
            }
            if (parentScope.getParent() != null)
                parentScope = parentScope.getParent();
            else return false;
        }
    }

    private Symbol getSymbol(Scope scope, String key) {
        Symbol symbol = new Symbol();
        for (Map.Entry<String, Symbol> entry : scope.symbolMap.entrySet()) {
            if (entry.getKey().equals(key)) {
                return entry.getValue();
            }
        }
        return null;
    }

    private Symbol editSymbol(Scope scope, String key) {
        Scope parentScope = scope;
//        Symbol symbol = new Symbol();
        while (true) {
            for (Map.Entry<String, Symbol> entry : parentScope.symbolMap.entrySet()) {
                if (entry.getKey().equals(key)) {
                    return entry.getValue();
                }
            }
            if (parentScope.getParent() != null)
                parentScope = parentScope.getParent();
            else return null;
        }
    }

    private boolean checkScope(String key) {
        for (int i = 0; i < Main.symbolTable.getScopes().size(); i++) {
            if (Main.symbolTable.getScopes().get(i).getId().equals(key)) {
                return true;
            }
        }
        return false;
    }

    private void writeMsg(String msg) {
        try {
//            printWriter = new PrintWriter("samples//Error.txt");
//            printWriter.println();
//            printWriter.close();
            errorSample.writeError(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean checkTheType(Type type) {
// check the table exist or not ..
        int k = 0;
        for (int i = 0; i < Main.symbolTable.getDeclaredTypes().size(); i++) {
            System.out.println("the value name :" + type.getName());
            System.out.println("the value type saved :" + Main.symbolTable.getDeclaredTypes().get(i).getName());

            if (!type.getName().equals(Main.symbolTable.getDeclaredTypes().get(i).getName()))


                k++;
            //   System.out.println("the k is :" + k);
            //     System.out.println("the full size  is :" + Main.symbolTable.getDeclaredTypes().size());
        }
        if (k == Main.symbolTable.getDeclaredTypes().size() - 1)
            return true;
        else
            return false;


    }

    public boolean checkType(Symbol symbol) {

        int k = 0;
        for (int i = 0; i < Main.symbolTable.getDeclaredTypes().size(); i++) {

            if(symbol != null)
            if (!symbol.getType().getValue().equals(Main.symbolTable.getDeclaredTypes().get(i).getName()))
                k++;

            //   System.out.println("the k is :" + k);
            //     System.out.println("the full size  is :" + Main.symbolTable.getDeclaredTypes().size());
        }
        if (k == Main.symbolTable.getDeclaredTypes().size()) {


            String nameScope = symbol.getScope().getId();

            for (int i = 0; i < Main.symbolTable.getDeclaredTypes().size(); i++) {

                System.out.println("the value symbol :" + symbol.getType().getValue());
                System.out.println("the value type saved :" + Main.symbolTable.getDeclaredTypes().get(i).getName());
                if (Main.symbolTable.getDeclaredTypes().get(i).getName() != null)
                    if (nameScope.contains(Main.symbolTable.getDeclaredTypes().get(i).getName())) {
                        System.out.println("scope contains type :" + Main.symbolTable.getDeclaredTypes().get(i).getName());
                        Type temp = Main.symbolTable.getDeclaredTypes().get(i);


                        for (Map.Entry<String, Type> entry : temp.getColumns().entrySet()) {
                            System.out.println(entry.getValue().getValue() + ":");

                            if (entry.getValue().getValue().equals(symbol.getType().getValue())) {
                                System.out.println(" remove .. ");
//                            temp.getColumns().remove(symbol.getType().getName(), symbol.getType().getValue());
////                            temp.getColumns().remove(symbol.getType());
//                            // add  the stmt is removed
//
//                            System.out.println(" value sym : " + symbol.getType().getValue() + "name sym : " + symbol.getType().getName());

                            } else System.out.println(" not remove .. ");
                        }
                    }
            }

            return true;
        } else
            return false;


    }
}