package Java.AST.Visitor;

import Java.AST.Parse;

import Java.AST.temp.SQL.CreateTable.*;
import Java.AST.temp.SQL.CreateTable.CreateTableStatmt;
import Java.AST.temp.SQL.selectOrValue.ExpSqlAM;
import Java.AST.temp.SQL.selectOrValue.SelTableAm;
import Java.AST.temp.SQL.selectOrValue.SelectOrValue;
import Java.AST.temp.SQL.selectOrValue.tableOrSub.*;
import Java.AST.temp.SQL.selectStmt.OrderingTerm;
import Java.AST.temp.SQL.selectStmt.ResultColumn;
import Java.AST.temp.SQL.sqlStmt.*;
import Java.AST.temp.condition.*;
import Java.AST.temp.decleration.*;
import Java.AST.temp.exp.*;
import Java.AST.temp.function.*;
import Java.AST.temp.loop.*;
import Java.AST.temp.other.*;
import Java.AST.temp.print.PrintStmt;
import Java.SymbolTable.Scope;
import Java.SymbolTable.Symbol;

public interface ASTVisitor {
    void visit(Parse p);

    void visit(ConditionStmt conditionStmt);

    void visit(IF anIf);

    void visit(Else anElse);

    void visit(IfLine ifLine);

    void visit(BodyIfLine bodyIfLine);

    void visit(CompleteIfLine completeIfLine);

    void visit(RealizedCondition realizedCondition);

    void visit(UnRealizedConditon unRealizedConditon);

    void visit(IfNormal ifNormal);

    void visit(DeclerationVariable declerationVariable);

    void visit(DifferentKindOfForIncreament differentKindOfForIncreament);

    void visit(IncrDecr incrDecr);

    void visit(VarEqualStmt varEqualStmt);

    void visit(Exp exp);

    void visit(DatabaseExp databaseExp);

    void visit(FunctionCallExp functionCallExp);

    void visit(LiteralValue literalValue);

    void visit(OperaterExp operaterExp);

    void visit(TwoExp twoExp);

    void visit(Switch anSwitch);

    void visit(CaseSwitch caseSwitch);

    void visit(DefaultSwitch defaultSwitch);

    void visit(DoWhile doWhile);

    void visit(perSwitch perSwitch);

    void visit(perWhile perWhile);

    void visit(While anWhile);

    void visit(WhileNormal whileNormal);

    void visit(For anFor);

    void visit(ForEach forEach);

    void visit(ForNoraml forNoraml);

    void visit(AnyName anyName);

    void visit(BodyAllStmt bodyAllStmt);

    void visit(JavaAnyStmt javaAnyStmt);

    void visit(JavaStmt javaStmt);

    void visit(KeyWord keyWord);

    void visit(Function function);

    void visit(ArgumentList argumentList);

    void visit(CallFunction callFunction);

    void visit(FunctionDefinition functionDefinition);

    void visit(ParmeterList parmeterList);

    void visit(Item item);

    void visit(SignedNumber signedNumber);

    void visit(PrintStmt printStmt);

    void visit(SqlStmtList sqlStmtList);

    void visit(SqlStmt sqlStmt);

    void visit(JoinClauseSub joinClauseSub);

    void visit(JoinCaluse joinCaluse);

    void visit(TableOrSub tableOrSub);

    void visit(TableOrSubOne tableOrSubOne);

    void visit(TableOrSubThree tableOrSubThree);

    void visit(TableOrSubTwo tableOrSubTwo);

    void visit(TableSubFather tableSubFather);

    void visit(ExpSqlAM expSqlAM);

    void visit(SelTableAm selTableAm);

    void visit(OrderingTerm orderingTerm);

    void visit(ResultColumn resultColumn);

    void visit(CreateTableStatmt createTableStatmt);

    void visit(ColumnDef columnDef);

    void visit(TypeName typeName);

    //----------------

    void visit(SelectStmt selectStmt);

    void visit(SelectOrValue selectOrValue);

    //********************************

    void visit(AggregationFunction aggregationFunction);

    void visit(ParameterArray parameterArray);

    void visit(NumberItem numberItem);

    void visit(AnyNameItem anyNameItem);

    void visit(CreateTypeStmt createTypeStmt);

    void visit(ColDefType colDefType);



}
