package Java.AST;

import Java.AST.Visitor.ASTVisitor;
import Java.AST.temp.SQL.sqlStmt.SqlStmtList;
import Java.AST.temp.decleration.VarEqualStmt;
import Java.AST.temp.function.Function;

import java.util.ArrayList;

public class Parse extends Node {

    private ArrayList<VarEqualStmt> varEqualStmts;
    private ArrayList<Function> functions;
    private ArrayList<SqlStmtList> sqlStmtLists;


    public Parse() {
        varEqualStmts = new ArrayList<>();
        functions = new ArrayList<>();
        sqlStmtLists = new ArrayList<>();
    }

    public ArrayList<SqlStmtList> getSqlStmtLists() {
        return sqlStmtLists;
    }

    public void setSqlStmtLists(SqlStmtList sqlStmtLists) {
        this.sqlStmtLists.add(sqlStmtLists);
    }

    public ArrayList<VarEqualStmt> getVarEqualStmts() {
        return varEqualStmts;
    }

    public void setVarEqualStmts(VarEqualStmt varEqualStmts) {
        this.varEqualStmts.add(varEqualStmts);
    }

    public ArrayList<Function> getFunctions() {
        return functions;
    }

    public void setFunctions(Function functions) {
        this.functions.add(functions);
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

        for (int i = 0; i < sqlStmtLists.size(); i++) {
            if (sqlStmtLists.get(i).isOk())
                this.sqlStmtLists.get(i).accept(astVisitor);
        }

        for (int i = 0; i < this.functions.size(); i++)
            if (this.functions.get(i).isOk())
                this.functions.get(i).accept(astVisitor);

        for (int i = 0; i < this.varEqualStmts.size(); i++)
            if (this.varEqualStmts.get(i).isOk())
                this.varEqualStmts.get(i).accept(astVisitor);
    }
}