package Java.SymbolTable;

import Java.Main;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Type {

    private String Value;
    private String name;
    private String from;
    private String TypeTable;
    private String Path;
    private Map<String, Type> columns = new HashMap<String, Type>();


    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTypeTable() {
        return TypeTable;
    }

    public void setTypeTable(String typeTable) {
        TypeTable = typeTable;
    }

    public String getPath() {
        return Path;
    }

    public void setPath(String path) {
        Path = path;
    }

    public final static String NUMBER_CONST = "number";
    public final static String STRING_CONST = "string";
    public final static String BOOLEAN_CONST = "boolean";
    public final static String CHAR_CONST = "char";
    public final static String BLOB_LITERAL = "blobLiteral";
    public final static String TIME = "time";
    public final static String DATE = "date";
    public final static String TIMESTAMP = "timeStamp";

    public void addType(String name, Type type) {
        this.columns.put(name, type);
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Type> getColumns() {


        return columns;
    }

    public void setColumns(Map<String, Type> columns) {
        this.columns = columns;
    }

    public void printType() {
        System.out.println("from : " + from);
        System.out.println("name type  : " + name);
        System.out.println("value type : " + Value + "\n");

        for (Map.Entry<String, Type> entry : columns.entrySet()) {

            System.out.println(entry.getKey() + " : " + entry.getValue().getValue());


        }
    }
}
