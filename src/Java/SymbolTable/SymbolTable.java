package Java.SymbolTable;

import Java.AST.temp.SQL.sqlStmt.AggregationFunction;

import java.util.ArrayList;


public class SymbolTable {

    private ArrayList<Scope> scopes = new ArrayList<Scope>();

    private ArrayList<Type> declaredTypes = new ArrayList<Type>();

    private ArrayList<Type> columnTypes = new ArrayList<>();

    public ArrayList<AggregationFunction> getDeclaredAggregationFunction() {
        return declaredAggregationFunction;
    }

    public void setDeclaredAggregationFunction(ArrayList<AggregationFunction> declaredAggregationFunction) {
        this.declaredAggregationFunction = declaredAggregationFunction;
    }

    private ArrayList<AggregationFunction> declaredAggregationFunction = new ArrayList<AggregationFunction>();

    public SymbolTable() {
        // add the all type ..
        addTypeWithName(Type.NUMBER_CONST);
        addTypeWithName(Type.BOOLEAN_CONST);
        addTypeWithName(Type.BLOB_LITERAL);
        addTypeWithName(Type.CHAR_CONST);
        addTypeWithName(Type.STRING_CONST);
        addTypeWithName(Type.TIME);
        addTypeWithName(Type.TIMESTAMP);
        addTypeWithName(Type.DATE);
    }

    public ArrayList<Scope> getScopes() {
        return scopes;
    }

    public void setScopes(ArrayList<Scope> scopes) {
        this.scopes = scopes;
    }

    public ArrayList<Type> getDeclaredTypes() {
        return declaredTypes;
    }

    public void setDeclaredTypes(ArrayList<Type> declaredTypes) {
        this.declaredTypes = declaredTypes;
    }

    public void addScope(Scope scope) {
        this.scopes.add(scope);
    }

    public void addType(Type type) {
        this.declaredTypes.add(type);
    }

    public void addAgg(AggregationFunction aggregationFunction) {
        this.declaredAggregationFunction.add(aggregationFunction);
    }

    public void addTypeToArrayTypes(Type type) {
        this.columnTypes.add(type);
    }


    public ArrayList<Type> getColumnTypes() {
        return columnTypes;
    }

    public void setColumnTypes(ArrayList<Type> columnTypes) {
        this.columnTypes = columnTypes;
    }

    public void addTypeWithName(String nameType) {
        Type type = new Type();
        type.setName(nameType);

        this.declaredTypes.add(type);
    }

    public void printTable() {
        for (int i = 0; i < scopes.size(); i++) {
            scopes.get(i).PrintScope();
            System.out.println("_____________________________________");

            for (int j = 0; j < declaredTypes.size(); j++)
                declaredTypes.get(j).printType();
            System.out.println("_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _");

            for (int k = 0; k < columnTypes.size(); k++)
                columnTypes.get(k).printType();
            System.out.println(" ****** ********* ********** ***********");


            for (int k = 0; k < declaredAggregationFunction.size(); k++) {
                System.out.println("the path: " + declaredAggregationFunction.get(k).getJarPath());
                System.out.println("the method name: " + declaredAggregationFunction.get(k).getMethodName());
                System.out.println("the class name: " + declaredAggregationFunction.get(k).getClassName());
                System.out.println("the aggfun name: " + declaredAggregationFunction.get(k).getAggregationFunctionName());
            }
        }
    }
}
