package Java.SymbolTable;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;

import java.util.ArrayList;

public class Symbol {

    private String name;
    ArrayList<String> arraySymbolValue = new ArrayList<>();
    private Type type;
    private Scope scope;
    private Expression expression;
    private int line;
    private int col;
    private boolean isParam = false;
    private boolean isReturn = false;
    private boolean isKeyword = false;
    private boolean isOperator = false;

    public ArrayList<String> getArraySymbolValue() {
        return arraySymbolValue;
    }

    public void setArraySymbolValue(ArrayList<String> arraySymbolValue) {
        this.arraySymbolValue = arraySymbolValue;
    }

    public void addSymbolValue(String symbolValue) {
        this.arraySymbolValue.add(symbolValue);
    }

    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    public Scope getScope() {
        return scope;

    }


    public void setName(String name) {
        this.name = name;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    public boolean getIsParam() {
        return isParam;
    }

    public void setIsParam(boolean param) {
        isParam = param;
    }

    public boolean getIsKeyword() {
        return isKeyword;
    }

    public void setIsKeyword(boolean keyword) {
        isKeyword = keyword;
    }

    public boolean getIsReturn() {
        return isReturn;
    }

    public void setIsReturn(boolean aReturn) {
        isReturn = aReturn;
    }

    public boolean getIsOperator() {
        return isOperator;
    }

    public void setIsOperator(boolean assign) {
        isOperator = assign;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public void printSymbol() {
        System.out.println("name symbol " + name);
        System.out.println("scope this symbol is " + scope.getId());
        if (type != null)
            System.out.println("Type is " + type.getName() + " value is " + type.getValue());
        if (isParam)
            System.out.println("is param " + isParam);
        if (isKeyword)
            System.out.println("is Keyword " + isKeyword);
        if (isReturn)
            System.out.println("is Return " + isReturn);
        if (isOperator)
            System.out.println("is Operator " + isOperator);
        if (expression != null)
            System.out.println("value1 is " + expression.getValue1() + " operator is " + expression.getOperator() +
                    "  value2 is " + expression.getValue2() + " Type is " + expression.getType().getName());

    }


}
