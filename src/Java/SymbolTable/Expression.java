package Java.SymbolTable;

public class Expression {

    private String value1;
    private String value2;
    private boolean ifVariable1 = false;
    private boolean ifVariable2 = false;
    private Type type;
    private String operator;

    public String getValue1() {
        return value1;
    }

    public void setValue1(String value1) {
        this.value1 = value1;
    }

    public String getValue2() {
        return value2;
    }

    public void setValue2(String value2) {
        this.value2 = value2;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public boolean isIfVariable1() {
        return ifVariable1;
    }

    public void setIfVariable1(boolean ifVariable1) {
        this.ifVariable1 = ifVariable1;
    }

    public boolean isIfVariable2() {
        return ifVariable2;
    }

    public void setIfVariable2(boolean ifVariable2) {
        this.ifVariable2 = ifVariable2;
    }
}
/*

 */