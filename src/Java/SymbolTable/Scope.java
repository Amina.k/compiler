package Java.SymbolTable;

import Java.AST.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.Main;

import java.util.*;

public class Scope {

    private String id;
    private Scope parent;
    private String returnValue;
    public Map<String, Symbol> symbolMap = new LinkedHashMap<String, Symbol>();


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Scope getParent() {
        return parent;
    }

    public void setParent(Scope parent) {
        this.parent = parent;
    }

    public String getReturnValue() {
        return returnValue;
    }

    public void setReturnValue(String returnValue) {
        this.returnValue = returnValue;
    }

    public Map<String, Symbol> getSymbolMap() {
        return symbolMap;
    }

    public void setSymbolMap(Map<String, Symbol> symbolMap) {
        this.symbolMap = symbolMap;
    }

    public void addSymbol(String name, Symbol symbol) {
        this.symbolMap.put(name, symbol);
    }


    public void PrintScope() {
        System.out.println("id " + id);
        if (parent != null) {
            System.out.println("parent is ");
            parent.PrintScope();
        }
        for (Map.Entry<String, Symbol> entry : symbolMap.entrySet()) {
            System.out.println(entry.getKey() + ":");
            entry.getValue().printSymbol();
        }

    }

}